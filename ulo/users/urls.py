"""
Users URL Configuration
"""

# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# Standard library imports
from __future__ import unicode_literals

# Core django imports
from django.conf.urls import include, url

# Thrid party app imports

# Project imports
from . import views

# ----------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------

urlpatterns = [

	url(r'^available/(?P<username>[\w]+)/$', views.UsernameAvailableView.as_view(), name='available'),

	# User Profile Edit Page
	url(r'^update/$', views.ProfileUpdateView.as_view(), name='profile_update'),
	url(r'^(?P<pk>[\d]+)/image/update/$', views.ProfileUpdateImageView.as_view(), name='profile_update_image'),
	url(r'^(?P<pk>[\d]+)/image/delete/$', views.ProfileDeleteImageView.as_view(), name='profile_delete_image'),


	# Follow User
	url(r'^follow/(?P<pk>[\d]+)/$', views.FollowUserView.as_view(), name='follow'),
	# UnFollow User
	url(r'^unfollow/(?P<pk>[\d]+)/$', views.UnFollowUserView.as_view(), name='unfollow'),


	# User Profile Page: username regex pattern used in users.models.User
	url(r'^(?P<username>[\w]+)/$', views.ProfileView.as_view(), name='profile'),
	# User's followers
	url(r'^(?P<username>[\w]+)/followers/$', views.ProfileFollowersView.as_view(), name='followers'),
	# User's following
	url(r'^(?P<username>[\w]+)/following/$', views.ProfileFollowingView.as_view(), name='following'),
	
]

# ----------------------------------------------------------------------------------------