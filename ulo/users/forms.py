# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# Standard library imports
from __future__ import unicode_literals
import os, re

# Core django imports
from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _

# Thrid party app imports

# Project imports
from .utils import DEFAULT_PROFILE_PICTURE
from .search import user_search
from .widgets import ProfileUpdateDOBWidget, SignUpDOBWidget
from emails.emails import reactivation_email, verify_email
from ulo.fields import PasswordField, UloDOBField, UloPIPField
from ulo.formmixins import CleanUsernameMixin, FileUploadMixin
from ulo.forms import UloForm, UloModelForm
from ulo.utils import reserved_usernames

# ----------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------

USERS_MIN_AGE = 13

# ----------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------

class SignUpForm(CleanUsernameMixin, UloModelForm):

	# Auto generated username.
	generated_username = None

	# Use custom PasswordField for additional validation
	password = PasswordField( label=_('Password'), )
	
	# Use custom Widget to list dates in a drop down select menu
	dob = UloDOBField(

		label=_('Birthday'), 
		localize=True, 
		min_age=USERS_MIN_AGE, 
		widget=SignUpDOBWidget

	)


	class Meta:
		
		model = get_user_model()
		
		fields = ('name', 'email', 'password', 'dob',)
		
		localized_fields = ('dob',)
		
		widgets = {

			'name': forms.TextInput(attrs={'autofocus': 'autofocus'})

		}

		error_messages = {

			'email': {

				'invalid': _('Please enter a valid email address.')

			}

		}


	def __init__(self, *args, **kwargs):
		"""
		
		Set form field attributes.
		
		"""

		kwargs['auto_id'] = 'signup_%s'

		super(SignUpForm, self).__init__(*args, **kwargs)

		for i, field in self.fields.items():
			
			field.widget.attrs.update({

				'placeholder': _(field.label),
				'autocomplete': 'off',
				'class': 'box_border',
				'required': 'required',
			
			})


	def add_account_creation_error(self):

		self.add_error(

			None,
			_('Sorry, it looks like we failed to create your account. Please try again later.')

		)


	def is_valid(self):
		"""

		NOTE Delete this function and add 'username' to Meta.fields to have the
		user create their own username when signing up.

		"""
		valid = super(SignUpForm, self).is_valid()

		if valid == True:

			try:
			
				self.generated_username =  self.auto_username()
			
			except forms.ValidationError as e:

				valid=False


		return valid


	def auto_username(self, max_length=30):
		"""
		
		Return a unique username generate from the user's name or email. Raise a
		ValidationError if no username was generated.

		@param max_length: max length of the username.

		"""

		name = self.cleaned_data.get('name', '')

		username = 	''.join(re.findall(r'[\da-z]+', name, re.I))[:max_length]


		if username == '':

			name = self.cleaned_data.get('email', '').split('@', 1)[0]
		
			username = '_'.join(re.findall(r'[\da-z]+', name, re.I))[:max_length]


		if username == '':

			length = 16

			allowed_chars='abcdefghijklmnopqrstuvwxyz0123456789'
		
			username = get_random_string(length=length, allowed_chars=allowed_chars)

		
		username = username.lower()
		
		username_copy = username
		
		attempt, max_attempt, length = 0, 5, 5


		while attempt < max_attempt:

			if get_user_model().objects.filter(username=username).exists() or \
				username in reserved_usernames:

				attempt = attempt+1

				if attempt >= max_attempt:

					self.add_account_creation_error()

					raise forms.ValidationError(

						'Failed to create username',
						code='auto_username'

					)

				username = (

					username_copy[:max_length-length] + 

					get_random_string(length=length, allowed_chars='0123456789')
				
				)

				length = length+1

			else:

				attempt = max_attempt

		return username.lower()


	def clean_password(self):
		"""
		
		Create a user instance from the USER_ATTRIBUTES fields. This will create a 
		user with only the fields required to run UserAttributeSimilarityValidator which 
		tests the password for similarities with the user's personal information.

		"""
		
		attrs = settings.USER_ATTRIBUTES
		
		user_params = {}
		
		for attr in attrs:

			user_params[attr] = self.cleaned_data.get(attr)
		
		user = get_user_model()(**user_params)
		

		password = self.cleaned_data.get('password')
		
		# Run the validator(s) defined in the settings file (base.py)
		validate_password(password=password, user=user)
		
		user=None

		return password


	def save(self, commit=True):
		"""

		Save the form data to the database, storing a hashed version of the
		user's password and send an email confirmation email to the user.

		"""

		user = super(SignUpForm, self).save(commit=False)
		
		user.set_password(self.cleaned_data['password'])

		# If username is '' set it to None to force a database integrity error.
		# This will happen if you are auto generating the username but did not
		# set self.generated_username to the return value of auto_username().
		if self.generated_username != None or user.username == '':

			user.username = self.generated_username


		if commit:

			try:

				user.save()

				verify_email(user)

			except Exception as e:

				print(e)
				
				self.add_account_creation_error()

				user = None


		return user

# ----------------------------------------------------------------------------------------

class LoginForm(UloForm):

	# Indicates if this form has been loaded within another page.
	pip = UloPIPField(id='login_pip')

	# Email Address
	email = forms.EmailField(
		
		label=_('Email address'),
		
		widget=forms.TextInput(attrs={
			
			'class': 'box_border',
			'autofocus': 'autofocus',
			'placeholder': _('Email address'),
			'required': 'required',
		
		}),
	)

	# Password - PasswordField performs its own validation.
	password = PasswordField(
		
		label=_('Password'),
		
		widget=forms.PasswordInput(attrs={
			
			'class': 'box_border',
			'placeholder': _('Password'),
			'required': 'required',
		
		}),
	
	)

	# Determines when the session should expire
	remember = forms.BooleanField(
	
		label=_('Remember me'),
	
		required=False,
	
	)

	# Redirect path
	redirect_to = forms.CharField(
	
		required = False,
	
		widget=forms.HiddenInput()
	
	)


	def __init__(self, redirect_to=None, *args, **kwargs):
		"""
		Store a reference to the authenticated user and prefix all IDs with the 
		value 'login_'
		"""

		self.user = None

		kwargs['auto_id'] = 'login_%s'

		super(LoginForm, self).__init__(*args, **kwargs)


	def clean(self):
		
		email = self.cleaned_data.get('email')
		password = self.cleaned_data.get('password')

		# LOGIN VALIDATION
		
		try:
			
			# Raise error if the email or password are invalid strings.
			if bool(self.errors):

				raise forms.ValidationError('Field Error')
				
			# Raise error is the information does not match.
			else:

				self.user = authenticate(email=email, password=password)
				
				if self.user == None:
					
					raise forms.ValidationError('Authentication Failure')

			
		except forms.ValidationError as e:

			# Remove all field specific errors
			self.errors.clear()
			
			# Render a generic error messages for all failed login attempts
			raise forms.ValidationError(
			
				_('The email address and password do not match.'), 
				code='invalid_login'
			
			)

		# END LOGIN VALIDATATION


		# ACCOUNT VALIDATATION

		if self.user.is_blocked():
			
			raise forms.ValidationError(self.user.get_block_message(), code='blocked')
		

		if self.user.is_active != True:

			self.user.is_active=True

			self.user.save()
			
			reactivation_email(self.user)

		# END ACCOUNT VALIDATATION

		return self.cleaned_data

# ----------------------------------------------------------------------------------------

class ProfileUpdateForm(UloModelForm):

	# DateFile that checks the user's age is gte min_age
	dob = UloDOBField(label=_('Birthday'), min_age=USERS_MIN_AGE, widget=ProfileUpdateDOBWidget)

	
	class Meta:
		
		model = get_user_model()
		
		fields = ('location', 'dob', 'blurb', 'gender')

		widgets = {

			'blurb': forms.Textarea()

		}


	def __init__(self, *args, **kwargs):

		super(ProfileUpdateForm, self).__init__(*args, **kwargs)

		for i, field in self.fields.items():
			
			field.widget.attrs.update({

				'autocomplete': 'off',
				'class': 'box_border',
				#'required': 'required',
			
			})

# ----------------------------------------------------------------------------------------

class ProfileUpdateImageForm(FileUploadMixin, UloModelForm):
	"""
	Display a form the upload/change the user's profile picture.
	"""

	class Meta:

		model = get_user_model()
		
		fields = ('picture', 'thumbnail')
		
		widgets = {

			'picture': forms.ClearableFileInput(attrs={
			
				'accept': 'image/gif,image/jpeg,image/jpg,image/png',
				'capture': 'capture',
				'class': 'file'
			
			}),
			
		}


	def __init__(self, *args, instance=None, **kwargs):
		"""
		
		Initialise the class and values for all subclasses/mixins.

		"""

		if instance != None and instance.picture != DEFAULT_PROFILE_PICTURE:
			
			self.paths = instance.picture.path, instance.thumbnail.path
		
		else:
		
			self.paths = None


		# FileUploadMixin ensure a new thumbnail is created.
		kwargs['change_image'] = True

		# FileUploadMixin thumbnail dimensions.
		kwargs['thumbnail_size'] = (60, 60)
		
		# FileUploadMixin error message for an empty file dictionary.
		kwargs['upload_error'] = 'Please upload a photo.'

		super(ProfileUpdateImageForm, self).__init__(*args, instance=instance, **kwargs)


	def save(self, commit=True):
		"""
		
		"""

		try:

			with transaction.atomic():

				self.instance.thumbnail = self.cleaned_data['thumbnail']

				user =  super(ProfileUpdateImageForm, self).save(commit=commit)

				if commit:

					user_search.update_instance( user )

		except Exception as e:

			user = None


		if commit and user != None and self.paths != None:

			try:


				print( self.paths[0], self.paths[1] )

				# Remove the old files
			
				os.remove(self.paths[0])
				os.remove(self.paths[1])


				# -------------------- OR --------------------


				# Rename the old files
				
				# rename_pattern = r'\1__%s__deleted__' % self.instance.str_id


				# name = re.sub(

				# 	r'(profile/\d+-\d+/)', 
				# 	rename_pattern,
				# 	self.paths[0]

				# )

				# os.rename(self.paths[0], name)


				# name = re.sub(

				# 	r'(profile/\d+-\d+/)', 
				# 	rename_pattern, 
				# 	self.paths[1]

				# )

				# os.rename(self.paths[1], name)

			
			except FileNotFoundError as e:

				pass

		return user

# ----------------------------------------------------------------------------------------



