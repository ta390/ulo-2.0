# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals
import os, re

# Core django imports
from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.db import connection, IntegrityError, transaction
from django.db.models import F
from django.http import Http404, HttpResponse, HttpResponseForbidden, JsonResponse
from django.middleware.csrf import get_token
from django.shortcuts import redirect
from django.utils.http import urlquote_plus, urlunquote_plus
from django.utils.translation import ugettext_lazy as _

# Thrid party app imports

# Project imports
from .forms import LoginForm, ProfileUpdateForm, ProfileUpdateImageForm, SignUpForm
from .models import Connection
from .search import user_search
from .utils import DEFAULT_PROFILE_PICTURE, DEFAULT_PROFILE_THUMBNAIL
from posts.utils import IMAGE, VIDEO
from ulo.handlers import (
	MediaUploadHandler, MemoryMediaUploadHandler, TemporaryMediaUploadHandler
)
from ulo.utils import (
	dictfetchmany, get_cid, get_messages_json, get_referer_path, reserved_usernames,
	validate_cid
)
from ulo.views import UloView, UloFormView, UloRedirectView, UloUpdateView, UloOwnerUploadView
from ulo.viewmixins import UloLogoutRequiredMixin, UloOwnerRequiredMixin

# ----------------------------------------------------------------------------------------




# SIGNUP, LOGIN, LOGOUT
# ----------------------------------------------------------------------------------------

class SignUpView(UloLogoutRequiredMixin, UloFormView):
	"""
	Display sign up form and create new user account. Log the user in after successfully
	creating the account and send them an email to verify their address.
	"""

	template_name = 'users/signup.html'
	form_class = SignUpForm


	def form_valid(self, form):
		"""
		Save the user to the database and log them into their account.
		"""

		user = form.save()


		if user == None:

			return self.form_invalid(form)

		else:
	
			# Use the non hashed version of the password from the submitted form data.
			user = authenticate(

				email=user.email, 
				password=form.cleaned_data.get('password')

			)

			# Log the new user in.
			if user:
				
				 login(self.request, user)

				 return redirect(reverse('users:profile', args=(user.username,)))
			
			# Redirect to the login page if user authentication failed.
			else:
				
				return redirect(reverse('login'))

# ----------------------------------------------------------------------------------------

class UsernameAvailableView(UloView):

	def get(self, request, *args, **kwargs):

		raise Http404()


	def head(self, request, username, *args, **kwargs):

		if request.is_ajax():
			
			response = HttpResponse('')
			
			response['exists'] = get_user_model().objects.filter(
				username=username).exists() or username.lower() in reserved_usernames;
			
			return response

		raise Http404()

# ----------------------------------------------------------------------------------------

# TODO: Handle cookie error in better way (i.e. if user does not allow cookies)
class LoginView(UloLogoutRequiredMixin, UloFormView):

	template_name = 'users/login.html'
	redirect_name = 'redirect_to'
	form_class = LoginForm


	def get_redirect_url(self):
		"""
		UloLogoutRequiredMixin. Path to redirect a logged in user to.
		"""

		if self.request.method in ('POST', 'PUT'):
			
			return self.get_success_url()

		return reverse('home')


	def get(self, request, redirect_to, *args, **kwargs):
		"""
		Initialise form data with the redirect path if one is given.
		"""

		name = self.redirect_name
		path = self.request.GET.get(name, '')

		# Initialise hidden form field with the redirect path.
		self.initial.update({name: path})
		
		# Add context variable to append the redirect path to the forms url.
		kwargs.update({name: path and '?'+name+'='+urlquote_plus(path)})

		return super(LoginView, self).get(request, *args, **kwargs)


	def get_success_url(self):
		"""
		Return the url to redirect to after a successful post.
		"""

		return self.request.POST.get('redirect_to') or reverse('home')


	def form_valid(self, form):
		"""
		Log the user in. Have the session expire when the browser is closed if the user
		did not select remember me. Return the redirect page as html if the login form
		has been rendered within another page.
		"""

		login(self.request, form.user)

		if form.cleaned_data.get('remember') == None:
			# Expire session when the browser window is closed
			self.request.session.set_expiry(0)
			
		self.force_html = form.cleaned_data.get('pip', False)

		return super(LoginView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class LogoutView(UloRedirectView):
	
	def get_redirect_url(self):
		
		# Destroy all session data for the current request / user
		logout(self.request)
		
		return reverse('home')

# END SIGNUP, LOGIN, LOGOUT
# ----------------------------------------------------------------------------------------




# PROFILE PAGE VIEWS
# ----------------------------------------------------------------------------------------

class BaseProfileView(UloView):
	"""

	Base class for profile views (post, followers, following).

	Subclasses must defined the class variables tab_name and count_name.
	
	"""

	template_name = 'users/profile.html'

	# Database cursor
	cursor = None


	# Context keys used between multiple functions

	# Boolean that indicates if the logged in user is on their own profile.
	is_own_page = 'is_own_page'
	
	# Boolean that indicates if the user is logged in.
	is_auth = 'is_auth'
	
	# ID of the last item displayed to the user.
	max_id = 'max_id'

	# END Context keys used between multiple functions


	def __init__(self, *args, **kwargs):
		"""
		Create a direct connection to the database to perform raw SQL queries.
		
		See subclass implementations of get_tab_content().
		"""

		# Connect to the default database
		self.cursor = connection.cursor()
		
		return super(BaseProfileView, self).__init__(*args, **kwargs)


	def get_user(self, username):
		"""
		Return the user instance for this profile or raise a Http404 exception
		if the user cannot be found or is inactive.
		"""
		try:
			
			return get_user_model().objects.only(

					"name", "username", "blurb", "location", "picture", 
					"posts_count", "followers_count", "following_count"
				
				).get(username=username, is_active=True)

		except get_user_model().DoesNotExist:
			
			raise Http404()

		
	def get_tab_content(self, user, context, per_page):
		"""
		Return an iterable object containing the contents of the selected tab.

		@param user: profile page user instance.
		@param context: template context.
		@param per_page: number of results to display per page for each tab.
		"""
		raise NotImplementedError('Subclasses of BaseProfileView must define get_tab_content()')


	def add_tab_content(self, user, context, per_page=2):
		"""
		Add context information for the selected tab.
		@param user: profile page user instance.
		@param context: template context dictionary.
		@param per_page: number of results to display per page.
		"""

		# Add the next set of results for the selected tab
		context['content'] = self.get_tab_content(user, context, per_page)

		# (True if there are more results, ID of the last result)
		has_next, cid = get_cid(context['content'], self.cursor, per_page)
		
		context['has_next'] = has_next
		
		context[self.max_id] = cid


	def get_context_data(self, **kwargs):
		"""
		Add profile information to the template context.
		"""
		
		# Get the default template context
		context = super(BaseProfileView, self).get_context_data(**kwargs)

		# Is the user logged in
		is_auth = self.request.user.is_authenticated()
		
		# Retrieve the username
		username = self.kwargs.get('username')

		# If the logged in user is visiting their own page...
		if is_auth and self.request.user.username == username:
			
			user = self.request.user
			
			context[self.is_own_page] = True

		# Else find the user...
		else:
			
			user = self.get_user(username)
			
			context[self.is_own_page] = False
			
			# Check if the logged in user if following this user
			context['is_following'] = is_auth and self.request.user.is_following(user)
	

		context['tab'] = self.tab_name

		context['VIDEO'] = VIDEO

		# Add user instance for this profile to the context
		context['profile'] = user
		
		# Add is_auth to the context used by add_tab_content() / get_tab_content()
		context[self.is_auth] = is_auth


		# Add context for the selected tab
		self.add_tab_content(user, context)

		return context


	def get(self, request, username, *args, **kwargs):
		"""
		Return the profile page or tab content.
		"""
			
		# Get the max_id which is the id of the last instance (post, follower or 
		# following) displayed to the user. The value is used as an offset to return 
		# the next set of results.
		max_id = validate_cid(request.GET.get(self.max_id))

		# Return the next set of results for the selected tab. Ajax requests will
		# add the parameter 'tab' to identify this type of request.
		if request.is_ajax() and request.GET.get('tab') != None:

			context = {}

			context[self.is_auth] = request.user.is_authenticated()

			context[self.is_own_page] = request.user.username==username

			# Get the user instance for this profile page
			user = self.get_user(username)

			# If this is the first request for this tab add 'count' to the context
			if max_id == None:

				count = getattr(user, self.counter)
				
				# Short circuit if count is 0
				if count == 0:
				
					return JsonResponse({'count': count}, status=200)
				
				else:
				
					context['count'] = count


			context[self.max_id] = max_id

			# Add context for the selected tab
			self.add_tab_content(user, context)
			
			del context[self.is_auth]

			del context[self.is_own_page]
			
			return JsonResponse(context, status=200)


		# Return the full profile page.
		kwargs[self.max_id] = max_id

		return super(BaseProfileView, self).get(request, username, *args, **kwargs)

# ----------------------------------------------------------------------------------------

class ProfileView(BaseProfileView):
	"""
	Default profile view. Render the profile page with the 'posts' tab selected.
	"""

	tab_name = "posts"

	# Counter field name.
	counter = 'posts_count'
	

	def get_tab_content(self, user, context, per_page):
		"""
		Return the next set of posts.

		The value of context['max_id'] is the id of the last result displayed to the user 
		or None if no results have been displayed. Set LIMIT to per_page plus one but 
		return per_page results. The extra result is used to determine if the rendered 
		page should display a 'load more' button.
		"""

		limit = per_page+1

		# Run the query without an offset if max_id is None
		if context[self.max_id] is None:

			self.cursor.execute(

				 '''SELECT
						
						id AS cid,
						title,
						file0,
						thumbnail,
						published,
						views,
						media_type
					
					FROM posts_post

					WHERE user_id=%s AND is_active=True

					ORDER BY published DESC LIMIT %s

				''', [user.pk, limit]
			)


		# Run the query starting from the offset context['max_id']
		else:

			self.cursor.execute(

				 '''SELECT
						
						id AS cid,
						title,
						file0,
						thumbnail,
						published,
						views,
						media_type
					
					FROM posts_post
					
					WHERE user_id=%s AND is_active=True AND id < %s
					
					ORDER BY published DESC LIMIT %s

				''', [user.pk, context[self.max_id], limit]
			)

		return dictfetchmany(self.cursor, per_page)

# ----------------------------------------------------------------------------------------

class ProfileFollowersView(BaseProfileView):
	"""
	Render the profile page with the 'followers' tab selected.
	"""

	tab_name = "followers"
	
	# Counter field name.
	counter = 'followers_count'


	def get_tab_content(self, user, context, per_page):
		"""
		Return the next set of followers.

		The value of context['max_id'] is the id of the last result displayed to the user 
		or None if no results have been displayed. Set LIMIT to per_page plus one but 
		return per_page results. The extra result is used to determine if the rendered 
		page should display a 'load more' button.
		"""

		limit = per_page+1

		# Add the column is_following to indicate whether the logged in used is following
		# each user returned by the query.
		if context[self.is_auth]:

			# Run the query without an offset if max_id is None
			if context[self.max_id] is None:
				
				self.cursor.execute(
				
					 '''SELECT
							
							followers.id,
							followers.cid,
							followers.name, 
							followers.username, 
							followers.thumbnail,
							following.id AS is_following
						
						FROM (

							SELECT
								
								users_connection.id AS cid,
								users_connection.from_user_id, 
								users_connection.to_user_id,
								users_user.id, 
								users_user.name, 
								users_user.username, 
								users_user.thumbnail

							FROM users_connection
							INNER JOIN users_user 
							ON users_connection.from_user_id=users_user.id
							WHERE users_connection.to_user_id=%s
							ORDER BY users_connection.id DESC LIMIT %s

						) AS followers LEFT JOIN users_connection AS following

							ON followers.from_user_id=following.to_user_id 
							AND following.from_user_id=%s 
							
					''', [user.pk, limit, self.request.user.pk]
				)

			# Run the query starting from the offset context['max_id']
			else:

				self.cursor.execute(
					 '''SELECT
							
							followers.id,
							followers.cid,
							followers.name, 
							followers.username, 
							followers.thumbnail,
							following.id AS is_following
						
						FROM (

							SELECT

								users_connection.id AS cid,
								users_connection.from_user_id, 
								users_connection.to_user_id,
								users_user.id,
								users_user.name, 
								users_user.username, 
								users_user.thumbnail
							
							FROM users_connection
							INNER JOIN users_user 
							ON users_connection.from_user_id=users_user.id
							WHERE users_connection.to_user_id=%s AND users_connection.id < %s
							ORDER BY users_connection.id DESC LIMIT %s

						) AS followers LEFT JOIN users_connection AS following

							ON followers.from_user_id=following.to_user_id 
							AND following.from_user_id=%s 
							
					''', [user.pk, context[self.max_id], limit, self.request.user.pk]
				)


		# Run the query without the extra join when the user is not logged in.
		else:
			
			# Run the query without an offset if max_id is None
			if context[self.max_id] is None:
				
				self.cursor.execute(
					
					'''SELECT 

							users_connection.id AS cid,
							users_connection.from_user_id, 
							users_connection.to_user_id,
							users_user.id,
							users_user.name, 
							users_user.username, 
							users_user.thumbnail
						
						FROM users_connection
						INNER JOIN users_user 
						ON users_connection.from_user_id=users_user.id
						WHERE users_connection.to_user_id=%s
						ORDER BY users_connection.id DESC LIMIT %s

					''', [user.pk, limit]
				)

			# Run the query starting from the offset context['max_id']
			else:
				
				self.cursor.execute(
					
					'''SELECT 

							users_connection.id AS cid,
							users_connection.from_user_id, 
							users_connection.to_user_id,
							users_user.id,
							users_user.name, 
							users_user.username, 
							users_user.thumbnail
						
						FROM users_connection
						INNER JOIN users_user 
						ON users_connection.from_user_id=users_user.id
						WHERE users_connection.to_user_id=%s AND users_connection.id<%s
						ORDER BY users_connection.id DESC LIMIT %s

					''', [user.pk, context[self.max_id], limit]
				)

		return dictfetchmany(self.cursor, per_page)

# ----------------------------------------------------------------------------------------

class ProfileFollowingView(BaseProfileView):
	"""
	Render the profile page with the 'following' tab selected.
	"""
	
	tab_name = "following"

	# Counter field name.
	counter = 'following_count'


	def get_tab_content(self, user, context, per_page):
		"""
		Return the next set of users following this user.

		The value of context['max_id'] is the id of the last result displayed to the user 
		or None if no results have been displayed. Set LIMIT to per_page plus one but 
		return per_page results. The extra result is used to determine if the rendered 
		page should display a 'load more' button.
		"""

		limit = per_page+1

		# Run the query without the extra join when the user is on their own page or not
		# logged in.
		if context[self.is_own_page] or not context[self.is_auth]:

			# Run the query without an offset if max_id is None
			if context[self.max_id] is None:
			
				self.cursor.execute(
			
					'''SELECT 

							users_connection.id AS cid,
							users_connection.from_user_id, 
							users_connection.to_user_id,
							users_user.id,
							users_user.name, 
							users_user.username, 
							users_user.thumbnail
						
						FROM users_connection
						INNER JOIN users_user 
						ON users_connection.to_user_id=users_user.id
						WHERE users_connection.from_user_id = %s 
						ORDER BY users_connection.id DESC LIMIT %s

					''', [user.pk, limit]
				)

			# Run the query starting from the offset context['max_id']
			else:
				
				self.cursor.execute(
					
					'''SELECT 

							users_connection.id AS cid,
							users_connection.from_user_id, 
							users_connection.to_user_id,
							users_user.id,
							users_user.name, 
							users_user.username, 
							users_user.thumbnail
						
						FROM users_connection
						INNER JOIN users_user 
						ON users_connection.to_user_id=users_user.id
						WHERE users_connection.from_user_id=%s AND users_connection.id < %s
						ORDER BY users_connection.id DESC LIMIT %s

					''', [user.pk, context[self.max_id], limit]
				)
				
		
		# Add the column is_following to indicate whether the logged in used is following
		# each user returned by the query.
		else:

			# Run the query without an offset if max_id is None
			if context[self.max_id] is None:
				
				self.cursor.execute(
					
					 '''SELECT

							followers.id,
							followers.cid,
							followers.name,
							followers.username, 
							followers.thumbnail,
							following.id AS is_following
						
						FROM (

							SELECT 
								users_connection.id AS cid,
								users_connection.from_user_id, 
								users_connection.to_user_id,
								users_user.id,
								users_user.name, 
								users_user.username, 
								users_user.thumbnail
							FROM users_connection
							INNER JOIN users_user 
							ON users_connection.to_user_id=users_user.id
							WHERE users_connection.from_user_id = %s 
							ORDER BY users_connection.id DESC LIMIT %s

						) AS followers LEFT JOIN users_connection AS following

							ON followers.to_user_id=following.to_user_id 
							AND following.from_user_id=%s 
							
					''', [user.pk, limit, self.request.user.pk]
				)
			
			# Run the query starting from the offset context['max_id']
			else:
				
				self.cursor.execute(
				
					 '''SELECT

							followers.id,
							followers.cid,
							followers.name,
							followers.username, 
							followers.thumbnail,
							following.id AS is_following
						
						FROM (

							SELECT 
								users_connection.id AS cid,
								users_connection.from_user_id, 
								users_connection.to_user_id,
								users_user.id,
								users_user.name, 
								users_user.username, 
								users_user.thumbnail
							FROM users_connection
							INNER JOIN users_user 
							ON users_connection.to_user_id=users_user.id
							WHERE users_connection.from_user_id = %s AND users_connection.id < %s
							ORDER BY users_connection.id DESC LIMIT %s

						) AS followers LEFT JOIN users_connection AS following

							ON followers.to_user_id=following.to_user_id 
							AND following.from_user_id=%s 
							
					''', [user.pk, limit, context[self.max_id], self.request.user.pk]
				)

		return dictfetchmany(self.cursor, per_page)

# END PROFILE PAGE VIEWS
# ----------------------------------------------------------------------------------------




# UPDATE PROFILE VIEWS
# ----------------------------------------------------------------------------------------

class ProfileUpdateView(LoginRequiredMixin, UloUpdateView):
	"""
	"""

	redirect_field_name = 'redirect_to'
	template_name = 'users/profile_update.html'
	form_class = ProfileUpdateForm

	def get_object(self):

		return self.request.user


	def get_success_url(self):

		return reverse('users:profile', args=(self.request.user.username,))


	def form_valid(self, form):

		if form.has_changed():
		
			form.save()
		
			messages.success(self.request, _('Profile updated.'))
		
		return super(ProfileUpdateView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class _ProfileUpdateImageView(UloFormView):
	
	redirect_field_name = 'redirect_to'
	form_class = ProfileUpdateImageForm


	def get_success_url(self):

		return reverse('users:profile', args=(self.request.user.username,))


	def get_form_kwargs(self):
		
		kwargs = super(_ProfileUpdateImageView, self).get_form_kwargs()
		
		if self.request.method in ('POST', 'PUT'):
		
			kwargs.update({
				
				'instance': self.request.user,
				# Required fields for FileUploadMixin.
				'request': self.request,
				'file_field': 'picture',
				'thumbnail_field': 'thumbnail'

			})

		return kwargs


	def ajax_valid(self, form):
		"""
		Add the option form error messages to the 'errors' object.
		"""

		data = super(_ProfileUpdateImageView, self).ajax_invalid(form)
		
		data['image_src'] = self.request.user.picture.url
		
		return data


	def form_valid(self, form):
		"""
		Save the post and all its options in an atomic block.
		"""

		if form.save() == None:

			messages.error(

				self.request,
				_('Sorry, it looks like we failed to update your photo. Please try again later.')

			)

			return super(_ProfileUpdateImageView, self).form_invalid(form)


		return super(_ProfileUpdateImageView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class ProfileUpdateImageView(UloOwnerUploadView):
	"""

	"""

	view_handler = _ProfileUpdateImageView
	form_class = ProfileUpdateImageForm
	model_class = get_user_model()


	def get(self, request, pk, *args, **kwargs):

		if request.is_ajax():
			
			return JsonResponse({}, status=403)

		return redirect( reverse('users:profile', args=(request.user.username,)) )


	def set_handlers(self, request):
		
		request.upload_handlers = [
			
			MediaUploadHandler(request=request, images=('picture',)),
			MemoryMediaUploadHandler(request=request, images=('picture',)),
			TemporaryMediaUploadHandler(request=request, images=('picture',))

		]

# ----------------------------------------------------------------------------------------

class ProfileDeleteImageView(UloOwnerRequiredMixin, UloView):
	"""

	"""

	model_class = get_user_model()


	def post(self, request, pk, *args, **kwargs):

		if request.is_ajax():

			status = 200


			if request.user.picture != DEFAULT_PROFILE_PICTURE:

				paths = request.user.picture.path, request.user.thumbnail.path

				request.user.picture = DEFAULT_PROFILE_PICTURE

				request.user.thumbnail = DEFAULT_PROFILE_THUMBNAIL


				try:

					with transaction.atomic():

						request.user.save()

						user_search.update_instance( request.user )

				except Exception:

					status = 400

					messages.error(

						request, 
						_('Sorry, it looks like we failed to delete your photo. Please try again later.')

					)


				if status == 200:

					try:

						
					
						# Remove the deleted files
						
						os.remove(paths[0])
						os.remove(paths[1])


						# -------------------- OR --------------------


						# Rename the deleted files
						
						# rename_pattern = r'\1__%s__deleted__' % request.user.str_id

						# name = re.sub(

						# 	r'(profile/\d+-\d+/)', 
						# 	rename_pattern,
						# 	request.user.picture.path

						# )

						# os.rename(request.user.picture.path, name)

						# name = re.sub(

						# 	r'(profile/\d+-\d+/)', 
						# 	rename_pattern, 
						# 	request.user.thumbnail.path

						# )

						# os.rename(request.user.thumbnail.path, name)

					except FileNotFoundError as e:

						pass


			return JsonResponse({

				'image_url': request.user.get_picture(),
				'messages': get_messages_json(request)

			}, status=status)


		return HttpResponseForbidden();
		

# END UPDATE PROFILE VIEWS
# ----------------------------------------------------------------------------------------




# FOLLOW/UNFOLLOW
# ----------------------------------------------------------------------------------------

class SameUserExpection(Exception):
	pass

# ----------------------------------------------------------------------------------------

class BaseConnectView(LoginRequiredMixin, UloView):
	"""
	Base class for follwing/unfollowing a user.
	"""

	redirect_field_name = 'redirect_to'


	def get_user(self, pk):
		"""
		Return the user being followed/unfollowed.
		"""
		try:
		
			return get_user_model().objects.only("id", "username", "followers_count").get(pk=pk)
		
		except get_user_model().DoesNotExist:
		
			raise Http404()


	def _counters(self, from_user, pk, value):
		"""
		Internal helper function to change the follower and following counter values.
		See inc_counters() and dec_counters().
		"""
		
		# update the counter value for the user being followed/unfollowed.
		if get_user_model().objects.filter(
			id=pk).update(followers_count=F('followers_count')-value) != 1:
			
			raise get_user_model().DoesNotExist
		
		# Update the counter value for the user doing the following/unfollowing.
		from_user.following_count = F('following_count') - value
		
		from_user.save()


	def inc_counters(self, from_user, to_user):
		"""
		Increment the followers_count value for the user being followed ('pk') and the 
		following_count value for the user doing the following ('user').
		"""

		self._counters(from_user, to_user, -1)


	def dec_counters(self, from_user, to_user):
		"""
		Decrement the followers_count value for the user being unfollowed ('pk') and the 
		following_count value for the user doing the unfollowing ('user').
		"""

		self._counters(from_user, to_user, 1)


	def connect(self, user, pk):
		"""
		Hook for subclasses to make a database entry or delete.
		"""

		raise NotImplementedError('Subclasses of BaseConnectView must define connect()')


	def reverse_url(self):
		"""
		Returns the url to give to the link if the request is successful. E.g. if the user
		makes a follow request return the unfollow url.
		"""

		raise NotImplementedError('Subclasses of BaseConnectView must define reverse_url()')


	def get(self, request, pk, *args, **kwargs):
		"""
		Handle the request to follow or unfollow a user.
		"""

		# Redirect the user if they are not logged in
		if request.user.is_authenticated()==False:

			return redirect( get_referer_path(request, redirect=True) )

		status, data = 200, {}

		try:

			# Execute the database query
			self.connect(request.user, pk)

		except (Connection.DoesNotExist, get_user_model().DoesNotExist, IntegrityError, 
			SameUserExpection, ValueError) as e:

			status = 403


		if request.is_ajax():

			return JsonResponse({

					'url': self.reverse_url(),
					'messages': get_messages_json(request)
				
				}, 
				
				status=status
			)

		# Return the user to the page they came from
		return redirect(get_referer_path(request))


	def post(self, request, pk, *args, **kwargs):
		
		raise Http404()

# ----------------------------------------------------------------------------------------

class FollowUserView(BaseConnectView):
	"""
	Add a database entry for a new follow.
	"""

	def reverse_url(self):
		"""
		Return the url that performs the reverse action (unfollow).
		"""

		return reverse('users:unfollow', args=(self.request.user.pk,))


	@transaction.atomic
	def connect(self, from_user, pk):
		"""
		@param from_user: user doing the following
		@param pk: id of the user being followed
		"""

		if from_user.str_id == pk:

			raise SameUserExpection()

		Connection.objects.create(from_user=from_user, to_user_id=pk)
		
		# Increment the counters
		self.inc_counters(from_user, pk)

# ----------------------------------------------------------------------------------------

class UnFollowUserView(BaseConnectView):
	"""
	Delete a database entry for an unfollow.
	"""

	def reverse_url(self):
		"""
		Return the url that performs the reverse action (follow).
		"""

		return reverse('users:follow', args=(self.request.user.pk,))


	@transaction.atomic
	def connect(self, from_user, pk):
		"""
		@param from_user: user doing the unfollowing
		@param pk: id of the user being unfollowed
		"""

		Connection.objects.get(from_user=from_user, to_user_id=pk).delete()

		# Decrement the counters
		self.dec_counters(from_user, pk)

# END FOLLOW/UNFOLLOW
# ----------------------------------------------------------------------------------------



