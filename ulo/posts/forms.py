# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# Standard library imports
from __future__ import unicode_literals

# Core django imports
from django import forms
from django.db import transaction
from django.utils.html import format_html, format_html_join
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

# Thrid party app imports

# Project imports
from .models import Post, PostOption, PostReport
from .search import post_search
from .utils import (
	CATEGORIES, COMMENT_SETTINGS, POST_ISSUES, IMAGE, VIDEO
)
from .widgets import ContentEditable, List
from ulo.fields import UloPIPField
from ulo.forms import UloModelForm, UloSecureModelForm
from ulo.formmixins import FileUploadMixin

# ----------------------------------------------------------------------------------------




# POST FORMS
# ----------------------------------------------------------------------------------------

class PostForm(FileUploadMixin, UloModelForm):
	"""
	Displays a form to create a post.
	"""

	# Add an extra file input field to the form for video uploads.
	# video = forms.FileField(
	
	# 	required=False,
		
	# 	label=_('Add a video'),

	# 	widget=forms.ClearableFileInput(

	# 		attrs={

	# 			'accept':'video/mp4,video/ogg,video/webm',
	# 			'capture': 'capture',
	# 			'class':'file'

	# 		}

	# 	)
	
	# )

	# Add an extra file input field to the form for editor uploads.
	editor_file0 = forms.FileField(
	
		required=False,
		
		widget=forms.ClearableFileInput(

			attrs={
			
				'accept': 'image/gif,image/jpeg,image/jpg,image/png, video/mp4,video/ogg,video/webm',
				'multiple': 'multiple',
				'capture': 'capture',
				'class': 'file',
				'required': 'required'
			
			}
		
		)
	
	)

	class Meta:

		model = Post
		
		fields = (

			'title', 'category', 'description', 'comment_settings', 'file0', 'thumbnail',
			'views', 'duration', 'thumbnail_time'

		)
		
		widgets = {

			# 'title': ContentEditable(attrs={
			#
			# 	'autofocus':'autofocus', 
			# 	'placeholder':'Title',
			# 	'class': 'text_input'
			#
			# }),

			'title': forms.TextInput(attrs={
			
				'autofocus':'autofocus', 
				'class': 'box_border',
				'placeholder':'Title',
				'autocomplete':'off',
				'required': 'required'
			
			}),

			'file0': forms.ClearableFileInput(attrs={
			
				'accept': 'image/gif,image/jpeg,image/jpg,image/png, video/mp4, video/ogg, video/webm',
				'multiple': 'multiple',
				'class': 'box_border',
				'capture': 'capture',
				'class': 'file'
			
			}),
			
			'category': List(choices=CATEGORIES),
			
			'comment_settings': List(choices=COMMENT_SETTINGS),
			
			'description': forms.Textarea(attrs={
			
				'class': 'box_border',
				'placeholder': _("What's your post about?")
			
			}),

		}
		
	def __init__(self, *args, **kwargs):
		"""
		
		"""

		# FileUploadMixin thumbnail dimensions.
		# This will create a 108 X 108 thumbnail as images have an aspect ratio of 4:3.
		kwargs['thumbnail_size'] = (144, 144)
		
		# FileUploadMixin error message for an empty file.
		kwargs['upload_error'] = 'Please upload photos or a video.'
		
		# Database file type value - this value is popped by FileUploadMixin.
		self.media_type = kwargs.get('media_type')
		
		super(PostForm, self).__init__(*args, **kwargs)


	def add_post_creation_error(self):

		self.add_error(

			None,
			_('Sorry, it looks like we failed to create your post. Please try again later.')

		)


	def clean_views(self):
		
		if self.media_type == VIDEO:
			
			return 0

		return None


	def clean_duration(self):
		 
		if self.media_type == VIDEO:

			return self.cleaned_data['duration']

		return None


	def clean_thumbnail_time(self):
		 
		if self.media_type == VIDEO:

			return self.cleaned_data['thumbnail_time']

		return None


	def is_valid(self):
		"""
		
		Validate the media type and add an error to the file field if the type is
		invalid.
		
		"""
		
		if self.media_type not in (IMAGE, VIDEO):
			self.add_error(self.file_field, _('The file type is not supported.'))

		return super(PostForm, self).is_valid()


	def save(self, commit=True):
		"""
		
		Update the post instance to include the logged in user and the media type.
		
		"""
		
		self.instance.user = self.request.user

		self.instance.media_type = self.media_type

		return super(PostForm, self).save(commit)

# ----------------------------------------------------------------------------------------


class PostOptionForm(UloModelForm):
	"""
	Render the option fields as a single li element with data attributes representing
	the individual field values.
	"""

	options = (

		{'text': _('Love'), 'icon': 'icon_love', 'option_type': '1'},

		{'text': _('Like'), 'icon': 'icon_like', 'option_type': '2'},

		{'text': _('Dislike'), 'icon': 'icon_dislike', 'option_type': '3'}

	)

	text_attrs = [

		('type', 'text'), 
		
		('class', 'box_border'), 
		
		('placeholder', 'Text'),

		('data-ignore', 'true'), 

		('autocomplete', 'off')

	]

	icon_options = (

		{'cls': 'icon_love selected default', 'icon': 'icon_love'},
		
		{'cls': 'icon_like', 'icon': 'icon_like'},
		
		{'cls': 'icon_dislike', 'icon': 'icon_dislike'},
		
		{'cls': 'icon_smile', 'icon': 'icon_smile'},
		
		{'cls': 'icon_yes', 'icon': 'icon_yes'},
		
		{'cls': 'icon_no', 'icon': 'icon_no'},


		{'cls': 'blue', 'colour': '#0A7DE3'},

		{'cls': 'green', 'colour': '#418F1A'},

		{'cls': 'pink', 'colour': '#CF2BA4'},

		{'cls': 'orange', 'colour': '#EE5728'},

		{'cls': 'yellow', 'colour': '#E8D427'},

		{'cls': 'white', 'colour': '#FFFFFF'},

		{'cls': 'black', 'colour': '#000000'}

	)

	colour_attrs = (

		('type', 'color'), 
		
		('value', '#EFEFEF'),

		('class', 'cloak')

	)

	class Meta:

		model = PostOption
		
		fields = ('text', 'icon', 'colour', 'option_type')
		
		widgets = {
			
			'text': forms.TextInput(
			
				attrs={

					'autocomplete':'off',
					'placeholder': 'Text',
					'class': 'box_border',
					'data-ignore': 'true', # Ignore clicks. See base.js Menu class
			
				}
			
			),
		
		}


	def __init__(self, *args, prefix, **kwargs):
		"""
		"""

		self.selected = kwargs.pop('selected', '1')

		super(PostOptionForm, self).__init__(*args, prefix=prefix, **kwargs)


	def __str__(self):
		
		return self.as_ul()


	def icon_menu_id(self):
		
		return self.prefix + '_icons'


	def as_ul(self):

		output = [format_html('<ul id="{}" name="{}", class="options">', self.prefix, self.prefix)]

		for option in self.options:

			li_class = 'ellipsis selected' if option['option_type'] == self.selected else 'ellipsis'

			attrs = []

			for name, value in option.items():

				attrs.append(('data-'+name, value))

			output.append( 

				format_html(

					'<li class="{}" {}><span class="font_icon {}"></span>{}</li>', 
					li_class,
					format_html_join('', ' {}="{}"', attrs), 
					option.get('icon', ''),
					option.get('text', '')

				) 

			)

		output.append( 

			format_html(

				'<li class="text" data-option_type="4"> \
					<button type="button" data-toggle="{}" data-ignore="true" class="toggle_icons font_icon icon_love"></button> \
					<input{} name={}> \
				</li>', 
				self.icon_menu_id(),
				format_html_join('', ' {}="{}"', self.text_attrs),
				self.prefix

			) 

		)

		output.append('</ul>')

		return mark_safe('\n'.join(output))


	def icons(self):

		output = [format_html('<ul id="{}" class="icons box_border">', self.icon_menu_id())]

		for icon in self.icon_options:

			cls, key  = ('font_icon', 'icon') if 'icon' in icon else ('colour', 'colour')

			output.append( 

				format_html(

					'<li class="{} {}" data-{}="{}"></li>', 
					cls, icon['cls'], key, icon[key]

				) 

			)


		output.append( 

			format_html(

				'<li class="icon icon_colour_picker" data-colour="#EFEFEF"><input{}></li>', 
				format_html_join('', ' {}="{}"', self.colour_attrs)

			) 

		)

		output.append('</ul>')

		return mark_safe('\n'.join(output))


	def save(self, post, commit=True):
		"""
		Attempt to save the form if the option is required or it has changed.
		"""

		if self.empty_permitted == False or self.has_changed():
			
			self.instance.post = post

			return super(PostOptionForm, self).save(commit)

# ----------------------------------------------------------------------------------------

class PostUpdateForm(UloSecureModelForm):

	class Meta:
		
		model = Post
		
		fields = ('title', 'category', 'description', 'comment_settings')

		widgets = {

			'description': forms.Textarea()

		}

	def __init__(self, *arg, **kwargs):
		
		super(PostUpdateForm, self).__init__(*arg, **kwargs)
		
		# UloSecureModelForm requires self.instance to be a user object or self.user to be 
		# explicitly set to the user instance so it can check the user's password.
		self.user = self.instance.user

		for name, field in self.fields.items():

			field.widget.attrs.update({

				'class':'box_border',
				'autocomplete': 'off'

			})
		
		self.fields['password'].widget.attrs.update({'placeholder': 'Current password'})


	def add_post_update_error(self):

		self.add_error(

			None,
			_('Sorry, it looks like we failed to update your post. Please try again later.')

		)


	def save(self, commit=True):
		"""
		"""

		try:

			with transaction.atomic():

				post =  super(PostUpdateForm, self).save(commit=commit)

				if commit:

					post_search.update_instance( post )

		except Exception:

			self.add_post_update_error()

			post = None


		return post


# ----------------------------------------------------------------------------------------

class PostReportForm(UloModelForm):
	"""
	Form to report an issue with a post.
	"""

	# Indicates whether the form has been loaded within another page.
	pip = UloPIPField()

	issue = forms.ChoiceField(

		widget=forms.RadioSelect(),
		choices=POST_ISSUES,
		initial=0,
		help_text=_('What is wrong with this post?'),
		error_messages={'required': _('Let us know what the issue is.')}
	
	)


	class Meta:
		
		model = PostReport
		
		fields = ('issue', 'information')

		widgets = {
		
			'information': forms.Textarea(
				attrs={ 'class': 'box_border' }
			),
		}
		
		error_messages = {
		
			'information': {
				'max_length': _('Please explain the issue in %(limit_value)s characters or less.')
			},
		
		}


	def __init__(self, *args, **kwargs):

		# Post instance used to display a summary for non javascript requests.
		self.post = kwargs.pop('post', None)

		# Authenticated user's id.
		self.user_id = kwargs.pop('user_id', None)

		self.post_id = kwargs.pop('post_id', None)

		super(PostReportForm, self).__init__(*args, **kwargs)


	def save(self, commit=True):

		# Assign the user and post ids to the PostReport instance.
		self.instance.user_id = self.user_id
		
		self.instance.post_id = self.post_id
		
		return super(PostReportForm, self).save(commit=commit)

# ----------------------------------------------------------------------------------------



