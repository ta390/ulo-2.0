"""
Posts URL Configuration
"""

# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# Standard library imports
from __future__ import unicode_literals

# Core django imports
from django.conf.urls import include, url

# Thrid party app imports

# Project imports
from . import views

# ----------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------

urlpatterns = [

	# Post Form
	url(r'^$', views.PostView.as_view(), name='post'),
	# Image post handler
	url(r'^image/$', views.PostImageView.as_view(), name='image'),
	# Video post handler
	url(r'^video/$', views.PostVideoView.as_view(), name='video'),

	# Users Post
	url(r'^(?P<pk>[\d]+)/$', views.PostDetailView.as_view(), name='detail'),
	url(r'^(?P<pk>[\d]+)/update/$', views.PostUpdateView.as_view(), name='update'),
	url(r'^(?P<pk>[\d]+)/delete/$', views.PostDeleteView.as_view(), name='delete'),

	# Vote on a post
	url(r'^(?P<pk>[\d]+)/vote/$', views.PostVoteView.as_view(), name='vote'),

	# Comment on a post
	url(r'^(?P<post_id>[\d]+)/comment/$', views.PostCommentView.as_view(), name='comment'),
	url(r'^(?P<pk>[\d]+)/comment/load/$', views.PostCommentLoadView.as_view(), name='comment_detail'),


	# Post actions menu when JS is disabled
	url(r'^(?P<pk>[\d]+)/actions/$', views.PostActionsView.as_view(), name='actions'),
	
	# Report this post.
	url(r'^report/complete/$', views.PostReportCompleteView.as_view(), name='report_complete'),
	url(r'^(?P<pk>[\d]+)/report/$', views.PostReportView.as_view(), name='report'),

]

# ----------------------------------------------------------------------------------------