# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# Standard library imports
from __future__ import unicode_literals

# Core django imports
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.db import connection, IntegrityError, OperationalError, transaction
from django.db.models import F
from django.http import Http404, JsonResponse
from django.shortcuts import redirect
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import DeletionMixin

# Thrid party app imports

# Project imports
from .forms import PostForm, PostUpdateForm, PostOptionForm, PostReportForm
from .models import Post, PostOption, PostVote
from .search import post_search
from .utils import IMAGE, VIDEO, DISABLED
from comments.forms import CommentForm
from comments.models import Comment
from ulo.handlers import (
	MediaUploadHandler, MemoryMediaUploadHandler, TemporaryMediaUploadHandler
)
from ulo.sessions import LinkViewsSession
from ulo.utils import (
	dictfetchmany, get_messages_json, get_cid, get_referer_path, postfetchone, 
	validate_cid
)
from ulo.viewmixins import UloLoginRequiredMixin, UloOwnerRequiredMixin
from ulo.views import UloView, UloFormView, UloUpdateView, UloUploadView

# ----------------------------------------------------------------------------------------




# POST UPLOAD VIEWS
# ----------------------------------------------------------------------------------------

class PostAttrsMixin(object):
	"""
	Define the class attributes common PostView and _PostUploadView.
	"""
	redirect_field_name = 'redirect_to'
	template_name = 'posts/post.html'
	form_class = PostForm
	option_class = PostOptionForm

# ----------------------------------------------------------------------------------------

class PostView(PostAttrsMixin, LoginRequiredMixin, UloView):
	"""
	Display the post form to the user.
	"""

	def post(self, request, *args, **kwargs):

		messages.info(request, _('Enable javascript to make a post.'))
		
		return self.get(request, *args, **kwargs)


	def get_context_data(self, **kwargs):
		
		context = super(PostView, self).get_context_data(**kwargs)
		
		context.update({
		
			'form': self.form_class(), 
			'option0': self.option_class(prefix='option0'),
			'option1': self.option_class(prefix='option1', selected='2')
		
		})
		
		return context

# ----------------------------------------------------------------------------------------

class _PostUploadView(PostAttrsMixin, LoginRequiredMixin, UloFormView):
	"""
	Handle the post request of an image or video. Post requests are first sent to 
	PostImageView or PostVideoView. Once the upload handlers have been added the request 
	is passed to this view for processing.
	"""

	post_id = None

	media_type = None


	def __init__(self, **kwargs):
		
		# Get the media type. See PostImageView and PostVideoView view_args()
		self.media_type = kwargs.get('media_type')
		
		return super(_PostUploadView, self).__init__(**kwargs)


	def get_success_url(self):
		
		return reverse('posts:detail', args=(self.post_id,))


	def get_form_kwargs(self):
		
		kwargs = super(_PostUploadView, self).get_form_kwargs()
		
		if self.request.method in ('POST', 'PUT'):
		
			kwargs.update({
		
				'media_type': self.media_type,
				'request': self.request,
				'file_field': 'file0',
				'thumbnail_field': 'thumbnail'
		
			})
		
		return kwargs


	@cached_property
	def get_options(self):
		"""
		
		Return a tuple of option forms.
		
		"""

		return (

			self.option_class(prefix='option0', data=self.request.POST),
			self.option_class(prefix='option1', data=self.request.POST, empty_permitted=True)

		)


	def option_errors(self):
		"""
		
		Return a dictionary containing the first error found for each option field.
		
		E.g. {'option0': [error], 'option1': [error]}

		"""
		
		errors = {}
		
		messages = []

		for opt in self.get_options:
			
			for item in opt.errors.items():
				
				field, msg = item
				
				messages += msg
				
				break
		
			errors.update({opt.prefix: messages})

		return errors


	def ajax_invalid(self, form):
		"""

		Add the error messages for the option forms to the response.

		"""
		
		data = super(_PostUploadView, self).ajax_invalid(form)
		
		data['errors'].update(self.option_errors())
		
		return data


	def form_valid(self, form):
		"""
		
		Save the new post and its options.

		"""

		# Get the option forms.
		option0, option1 = self.get_options

		# If the option forms are valid save all components of a post in one go.
		if option0.is_valid() and option1.is_valid():	
			
			try:

				with transaction.atomic():
				
					post = form.save()
					option0.save(post=post)
					option1.save(post=post)
				
					# Increment the counter that records the number of posts a user has.
					self.request.user.posts_count = F('posts_count')+1
					self.request.user.save()

					post_search.index_instance( post )


				# Set post_id for use by get_success_url()
				self.post_id = post.id
				
				return super(_PostUploadView, self).form_valid(form)

			except Exception as e:

				print(e)

				form.add_post_creation_error()


		# Else display the option form errors to the user.
		return self.form_invalid(form)

# ----------------------------------------------------------------------------------------

class PostMediaAttrsMixin(object):
	"""
	Define the class attributes common to PostImageView and PostVideoView.
	"""

	template_name = 'posts/post.html'
	view_handler = _PostUploadView
	form_class = PostForm


	def get_redirect_url(self):

		return reverse('posts:post')


	def get(self, request, *args, **kwargs):

		if request.is_ajax():

			return JsonResponse(

				{ 'messages': get_messages_json(request) },
				status = 403

			)

		return redirect( self.get_redirect_url() )
	
# ----------------------------------------------------------------------------------------

class PostImageView(PostMediaAttrsMixin, UloUploadView):
	"""
	NOTE: Make sure Django processes the files LAST! by making it the last element in the 
	QueryDict. Now if an error is raised because of the file the form data for all other
	fields will remain in the request object.
	"""

	view_args = {'media_type': IMAGE}

	def set_handlers(self, request):

		request.upload_handlers = [
			
			MediaUploadHandler(request=request, images=('file0',)),
			MemoryMediaUploadHandler(request=request, images=('file0',)),
			TemporaryMediaUploadHandler(request=request, images=('file0',))

		]

# ----------------------------------------------------------------------------------------

class PostVideoView(PostMediaAttrsMixin, UloUploadView):
	"""
	NOTE: Make sure Django processes the files LAST! by making it the last element in the 
	QueryDict. Now if an error is raised because of the file the form data for all other
	fields will remain in the request object.
	"""

	view_args = {'media_type': VIDEO}

	def set_handlers(self, request):

		request.upload_handlers = [
			
			MediaUploadHandler(request=request, videos=('file0',), images=('thumbnail',)),
			MemoryMediaUploadHandler(request=request, videos=('file0',), images=('thumbnail',)),
			TemporaryMediaUploadHandler(request=request, videos=('file0',), images=('thumbnail',))

		]


# END POST UPLOAD VIEWS
# ----------------------------------------------------------------------------------------

class PostDetailView(UloView):
	"""
	Render a post adding user specific information to the context for a logged in user.
	"""

	template_name = 'posts/post_detail.html'

	# Database cursor
	cursor = None


	def __init__(self, *args, **kwargs):
		
		self. cursor = connection.cursor()
		
		return super(PostDetailView, self).__init__(*args, **kwargs)


	def get(self, request, pk, *args, **kwargs):

		try:

			is_auth = request.user.is_authenticated()

			if is_auth:

				offset = 6

				self.cursor.execute(
					
					 '''SELECT

							posts_postvote.postoption_id AS vote_id,
					 		post.*

					 	FROM (

						 	SELECT

						 		posts_postoption.id AS option_id,
						 		posts_postoption.colour,
								posts_postoption.text,
								posts_postoption.icon,
								posts_postoption.count,
								post.*

						 	FROM (

							 	SELECT
									
									post.*,
									users_connection.id AS is_following
								
								FROM (

									SELECT

										posts_post.*,
										users_user.name,
										users_user.username,
										users_user.thumbnail AS user_thumbnail,
										users_user.followers_count
									
									FROM posts_post
									
									INNER JOIN users_user 
									
									ON posts_post.user_id=users_user.id
									
									WHERE posts_post.is_active=%s 
									AND posts_post.id=%s

								) AS post 

								LEFT JOIN users_connection

								ON post.user_id=users_connection.to_user_id 	
								AND users_connection.from_user_id=%s

							) AS post

							LEFT JOIN posts_postoption

							ON post.id=posts_postoption.post_id

						) AS post

						LEFT JOIN posts_postvote

						ON posts_postvote.postoption_id=post.option_id
						
						AND posts_postvote.user_id=%s
						
					''', [True, pk, request.user.id, request.user.id]
				
				)
			
			else:

				offset = 5

				self.cursor.execute(

					 '''SELECT

					 		posts_postoption.id AS option_id,
							posts_postoption.colour,
							posts_postoption.text,
							posts_postoption.icon,
							posts_postoption.count,
							post.*

					 	FROM (
		
							SELECT

								posts_post.*,
								users_user.name,
								users_user.username,
								users_user.thumbnail AS user_thumbnail,
								users_user.followers_count
							
							FROM posts_post
							
							INNER JOIN users_user 
							
							ON posts_post.user_id=users_user.id
							
							WHERE posts_post.is_active=%s 
							AND posts_post.id=%s

						) AS post

						LEFT JOIN posts_postoption

						ON post.id=posts_postoption.post_id
					
					''', [True, pk]
				
				)


			post = postfetchone(cursor=self.cursor, offset=offset)
		
			if post == None:

				raise Post.DoesNotExist()	


			total, colours = 0, []

			for option in post['options']:

				total += option['count']
				
				colours.append( option['colour'] )


		except Post.DoesNotExist:
			
			raise Http404()
			

		kwargs.update({

			'post':post, 
			'colours':colours,
			'count_total':total,

			'comments': Comment.objects.all(),
			'form': CommentForm(post_id=pk),
			
			'VIDEO': VIDEO,

			'comments_disabled': DISABLED,
			
			'timeline': request.GET.get('timeline', False),
			'is_own_post': (is_auth and post['user_id']==request.user.id)
		
		})

		return super(PostDetailView, self).get(request, pk, *args, **kwargs)

# ----------------------------------------------------------------------------------------

class PostUpdateView(UloOwnerRequiredMixin, UloUpdateView):
	"""
	"""

	redirect_field_name = 'redirect_to'
	template_name = 'posts/post_update.html'
	form_class = PostUpdateForm
	id_field = 'user_id'
	model_class = Post


	def get_object(self):

		return self.instance


	def get_context_data(self, **kwargs):

		context = super(PostUpdateView, self).get_context_data(**kwargs)

		context['pk'] = self.instance.id

		return context


	def get_success_url(self):
		
		return reverse('posts:detail', args={self.instance.id})


	def form_valid(self, form):

		if form.has_changed():

			if form.save() == None:

				return super(PostUpdateView, self).form_invalid(form)

			
			messages.success(self.request, _('Post updated.'))
		
		return super(PostUpdateView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class PostDeleteView(UloOwnerRequiredMixin, DeletionMixin, UloView):
	"""
	"""

	template_name = 'posts/post_delete.html'

	redirect_field_name = 'redirect_to'

	id_field = 'user_id'

	model_class = Post


	def get_success_url(self):

		return reverse('home')


	def get_object(self):

		return self.instance


	def get_context_data(self, **kwargs):
		
		context = super(PostDeleteView, self).get_context_data(**kwargs)
		
		context['post'] = self.get_object()

		return context


	@transaction.atomic
	def delete(self, request, *args, **kwargs):

		request.user.posts_count = F('posts_count') - 1

		request.user.save()

		doc_id = self.instance.id

		response = super(PostDeleteView, self).delete(request, *args, **kwargs)

		post_search.delete_instance( doc_id )

		# Do not return any html if the request is an ajax request.
		return (JsonResponse({}, status=200) if request.is_ajax() else response)

# ----------------------------------------------------------------------------------------

class PostVoteView(UloLoginRequiredMixin, UloView):
	"""
	Add or remove a user's vote from PostVote.
	"""

	def get_success_url(self):
		"""
		Redirect to the page that the user came from
		"""
		return get_referer_path(

			self.request, 
			error_path=reverse('posts:detail', args=(self.kwargs['pk'],))
		
		)


	def get(self, request, pk, *args, **kwargs):
		"""
		"""

		vote, voted, status = request.GET.get('vote'), request.GET.get('voted', ''), 200

		print(vote, voted)

		try:

			with transaction.atomic():

				if vote == voted:

					if PostOption.objects.filter(id=vote, post_id=pk).update(count=F('count')-1) != 1:
				
						raise PostOption.DoesNotExist()

					PostVote.objects.get(postoption_id=voted, user_id=request.user.pk).delete()


				elif voted != '':

					if PostOption.objects.filter(id=vote, post_id=pk).update(count=F('count')+1) != 1 or \
						PostOption.objects.filter(id=voted, post_id=pk).update(count=F('count')-1) != 1:
						
						raise PostOption.DoesNotExist()

					if PostVote.objects.filter(postoption_id=voted, user_id=request.user.pk).update(postoption_id=vote) != 1:

						raise PostVote.DoesNotExist()


				else:

					if PostOption.objects.filter(id=vote, post_id=pk).update(count=F('count')+1) != 1:
						
						raise PostOption.DoesNotExist()

					PostVote.objects.create(postoption_id=vote, user_id=request.user.pk, post_id=pk)


		except (IntegrityError, OperationalError, PostOption.DoesNotExist, PostVote.DoesNotExist, Exception) as e:

			# PostVote.DoesNotExists is thrown if vote==voted but the autheticated
			# user logged out and then in as a different user on a differenct tab.

			msg = 'Sorry, we cannot register your vote. Refresh the page and try again.'
			
			messages.error(self.request, _(msg))
			
			status = 403


		if request.is_ajax():

			return JsonResponse({'messages':get_messages_json(request)}, status=status)

		return redirect(self.get_success_url())

# ----------------------------------------------------------------------------------------

class PostReportView(LoginRequiredMixin, UloFormView):
	"""
	Render a form that allows the user to report an issue with a post.
	"""

	redirect_field_name = 'redirect_to'
	template_name = 'posts/post_report.html'
	form_class = PostReportForm


	def get_success_url(self):
		"""
		Redirect to a page that informs the user that their report has been received.
		"""
		
		LinkViewsSession(self.request).set('post_report', True)

		return reverse('posts:report_complete')
	

	def get_form_kwargs(self):
		"""
		Fill the form with all the information required to render the page.
		"""
		
		# Get the default kwargs
		form_kwargs = super(PostReportView, self).get_form_kwargs()

		try:

			# Get the post id
			pid = self.kwargs['pk']

			if self.request.is_ajax():

				if Post.objects.filter(id=pid).exists() == False:
					raise Post.DoesNotExist()

			else:

				# Add the post to the form to display a summary to the user
				form_kwargs['post'] = Post.objects.only('thumbnail', 'title', 'id').get(id=pid)


			if self.request.method in ('POST', 'PUT'):

				# Add the request to get the logged in user's id in the form.
				form_kwargs.update({

					'post_id': pid,
					'user_id': self.request.user.id 

				})

		except (KeyError, Post.DoesNotExist) as e:
			
			raise Http404()


		return form_kwargs


	def form_valid(self, form):
		"""
		Create the report and redirect to the complete page.
		"""
		
		form.save()
		
		self.force_html = form.cleaned_data['pip']
		
		return super(PostReportView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class PostReportCompleteView(LoginRequiredMixin, UloView):
	"""
	Display page informing the user that their report has been received.
	"""

	redirect_field_name = 'redirect_to'
	template_name = 'posts/post_report_complete.html'


	def get(self, request, *args, **kwargs):

		if LinkViewsSession(request).get('post_report'):
		
			return super(PostReportCompleteView, self).get(request, *args, **kwargs)
		
		raise Http404()

# ----------------------------------------------------------------------------------------

class PostActionsView(LoginRequiredMixin, UloView):
	"""
	Javascript Disabled.
	Rendered when a user does not have javascript enabled and tries to perform an action
	on a post (e.g. report an issue or share). The view renders a menu with links to each
	actions page.
	"""
	
	redirect_field_name = 'redirect_to'
	template_name = 'posts/post_actions.html'


	def get(self, request, pk, *args, **kwargs):

		try:
			
			post = Post.objects.only('title', 'thumbnail', 'user_id').get(pk=pk)
		
		except Post.DoesNotExist:
		
			raise Http404()


		kwargs.update({

			'post':post,
			'is_own_post': post.user_id==request.user.pk,
			'back_url': reverse('posts:detail', args=(pk,))
		
		})
		
		return super(PostActionsView, self).get(request, pk, *args, **kwargs)

# ----------------------------------------------------------------------------------------

class PostCommentView(LoginRequiredMixin, UloFormView):
	"""
	Make a comment to a post.
	"""
	
	redirect_field_name = 'redirect_to'
	template_name = 'posts/post_comment.html'
	form_class = CommentForm


	def get_success_url(self):
		"""
		Return to the post.
		"""
	
		return reverse('posts:detail', args=(self.kwargs['post_id'],))
	

	def get_form_kwargs(self):

		form_kwargs = super(PostCommentView, self).get_form_kwargs()

		try:

			pid = self.kwargs['post_id']

			# Validate the post id if uploaded via ajax
			if self.request.is_ajax():
				
				if Post.objects.filter(id=pid).exists() == False:
					raise Post.DoesNotExist()

			# Else render the page with minimal information about the post.
			else:

				form_kwargs['post'] = Post.objects.only('thumbnail', 'title').get(id=pid)


		except (KeyError, Post.DoesNotExist):

			raise Http404()
		

		if self.request.method in ('POST', 'PUT'):
			
			form_kwargs.update({'user_id': self.request.user.id})


		form_kwargs['post_id'] = pid

		return form_kwargs


	def form_valid(self, form):

		with transaction.atomic():
			
			comment = form.save()

			if form.post == None:

				Post.objects.filter(id=comment.post_id).update(comments_count=F('comments_count')+1)
			
			else:
			
				form.post.comments_count = F('comments_count')+1
			
				form.post.save()


		if self.request.is_ajax():

			return JsonResponse(
				{	
					'comments': [{
					
						'name': self.request.user.name,
						'username': self.request.user.username,
						'thumbnail': self.request.user.thumbnail.name,
						'id': comment.id,
						'comment': comment.comment,
						'user_id': self.request.user.id
					
					}]

				}, status=200)
		
		return super(PostCommentView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class PostCommentLoadView(UloView):
	"""

	Load the next set of comments for a post.

	"""

	# Database cursor
	cursor = None

	def __init__(self, *args, **kwargs):

		self.cursor = connection.cursor()
		
		return super(PostCommentLoadView, self).__init__(*args, **kwargs)


	def get(self, request, pk, *args, **kwargs):

		if request.is_ajax():

			per_page = 12

			limit = per_page+1
			
			max_id = validate_cid(request.GET.get('max_id'))

			if max_id==None:

				self.cursor.execute(

					 '''SELECT

					 		users_user.name,
					 		users_user.username,
							users_user.thumbnail,
							comments_comment.id,
							comments_comment.comment,
							comments_comment.user_id
						
						FROM comments_comment
						
						INNER JOIN users_user 
						
						ON comments_comment.user_id=users_user.id
						
						WHERE comments_comment.is_active=%s
						
						AND comments_comment.post_id=%s
						
						ORDER BY published DESC LIMIT %s
					
					''', [True, pk, limit]

				)

			else:
				
				self.cursor.execute(

					 '''SELECT

					 		users_user.name,
					 		users_user.username,
							users_user.thumbnail,
							comments_comment.id,
							comments_comment.comment,
							comments_comment.user_id
					
						FROM comments_comment
					
						INNER JOIN users_user 
					
						ON comments_comment.user_id=users_user.id
					
						WHERE comments_comment.is_active=%s
					
						AND comments_comment.post_id=%s
					
						AND comments_comment.id<%s
					
						ORDER BY published DESC LIMIT %s
					
					''', [True, pk, max_id, limit]

				)


			comments = dictfetchmany(self.cursor, per_page)

			has_next, cid = get_cid(comments, self.cursor, per_page, 'id')


			return JsonResponse({
				
				'comments':comments, 'has_next':has_next, 'max_id':cid
			
			}, status=200)


		raise Http404()

# ----------------------------------------------------------------------------------------



