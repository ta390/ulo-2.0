"""ulo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# Standard library imports

# Core django imports
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

# Thrid party app imports

# Project imports
from generics.views import HomeAuthView, NavigationView
from users.views import LoginView, LogoutView, SignUpView

# TO SERVE MEDIA FILES IN DEVELOPMENT ONLY
from django.conf.urls.static import static

# ----------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------

urlpatterns = [

	# TESTING!!!!
	url(r'^search/', include('search.urls', namespace='search')),


	# Landing Page
	url(r'^$', HomeAuthView.as_view(), name='home'),


	# User Posts (Post detail, Post form)
	url(r'^post/', include('posts.urls', namespace='posts')),
	# User Pages (Profile, Profile update)
	url(r'^user/', include('users.urls', namespace='users')),


	# Sign Up Page
	url(r'^signup/$', SignUpView.as_view(), name='signup'),
	# Login Page
	url(r'^login/(\?redirect_to=(?P<redirect_to>[^<>\'"@]*/))?$', LoginView.as_view(), name='login'),
	# Logout Url
	url(r'^logout/$', LogoutView.as_view(), name='logout'),

	
	# Accounts Settings
	url(r'^accounts/', include('accounts.urls', namespace='accounts')),
	# User Settings
	url(r'^settings/', include('settings.urls', namespace='settings')),


	# User Comments
	url(r'^comment/', include('comments.urls', namespace='comments')),


	# Admin web interface
	url(r'^admin/', include(admin.site.urls)),


	# Site navigation when js is disabled.
	url(r'^navigation/$', NavigationView.as_view(), name='navigation'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# ----------------------------------------------------------------------------------------



