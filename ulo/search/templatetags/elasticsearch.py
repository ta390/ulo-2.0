# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

from django import template
from datetime import datetime
from django.utils.dateparse import parse_datetime

# ----------------------------------------------------------------------------------------

register = template.Library()
# ----------------------------------------------------------------------------------------

@register.filter(is_safe=True, needs_autoescape=False, expects_localtime=False)
def get_id(result):
	return result['_id']

# ----------------------------------------------------------------------------------------

@register.filter(is_safe=True, needs_autoescape=False, expects_localtime=False)
def get_source(result):
	return result['_source']

# ----------------------------------------------------------------------------------------

@register.filter(is_safe=True, needs_autoescape=False, expects_localtime=False)
def index_source(result, key):
	return result['_source'][key]

# ----------------------------------------------------------------------------------------



