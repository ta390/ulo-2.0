# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# Standard library imports
from __future__ import unicode_literals
from io import BytesIO
from PIL import Image
from sys import getsizeof

# Core django imports
from django.contrib.auth import get_user_model
from django.core.exceptions import FieldDoesNotExist, ValidationError
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.forms import Form
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

# Thrid party app imports

# Project imports
from .fields import UloPIPField
from .handlers import get_error
from .utils import reserved_usernames
from posts.utils import IMAGE, VIDEO
from users.utils import DEFAULT_PROFILE_THUMBNAIL

# ----------------------------------------------------------------------------------------




# FORM MIXINS
# ----------------------------------------------------------------------------------------

class UloBaseFormMixin(object):

	def __init__(self, *args, **kwargs):
		"""
		"""
		kwargs.setdefault('auto_id', '%s')
		
		kwargs.setdefault('label_suffix', '')
		
		super(UloBaseFormMixin, self).__init__(*args, **kwargs)
		
		
	def http_referer(self):
		"""
		Add the hidden input field 'http_referer' to the form.
		"""
		
		value = self.initial.get('http_referer', '')
		
		return mark_safe('<input type="hidden" name="http_referer" value="'+value+'">')


# ----------------------------------------------------------------------------------------

class CleanUsernameMixin(object):
	"""
	Check that the username is not a reserved username. 
	
	Forms must defined a 'username' field.
	"""

	def clean_username(self):
		"""
		Check that the username is unique and return a normalised string (all lowercase).
		"""
		username = self.cleaned_data.get('username', '').lower()
		
		if username in reserved_usernames:
			
			raise ValidationError(
			
				_('This username has been taken.'), code='reserved_username',
			
			)

		return username

# ----------------------------------------------------------------------------------------

class PasswordRequiredMixin(object):
	"""
	Require the user to enter their current password. 
	
	Forms must define a 'password' field. If the form's instance is not a user instance 
	the form must also define a 'user' variable.
	"""

	# Fallback defined by the form if self.instance is not a User instance
	user = None

	# Check that the password entered matches the user's current password.
	def clean_password(self):
		
		password = self.cleaned_data.get('password', '')

		if password=='':

			raise ValidationError(
			
				_('Please enter your current password.'),
				code='required',
			
			)


		user = self.instance if hasattr(self.instance, 'check_password') else self.user

		if not user.check_password(password):

			raise ValidationError(

				_('Password incorrect.'),
				code='password_incorrect',

			)


		return self.initial.get('password', '')

# ----------------------------------------------------------------------------------------

class ChangeImageMixin(object):
	"""
	Mixin to add helper functions when removing an old image.
	"""

	# name of the image field
	image_field = None


	def __init__(self, *args, **kwargs):
		self.image_field = kwargs.pop('image_field', None)
		super(ChangeImageMixin, self).__init__(*args, **kwargs)


	def get_default(self, fail_silently=True):
		"""
		Return the default value given to an image when it is not set or None.
		@param fail_silently: boolean to raise or suppress exceptions.
		"""
		try:

			return self.Meta.model._meta.get_field(self.image_field).get_default()
		
		except (FieldDoesNotExist, AttributeError) as e:

			if fail_silently: return None
			raise e


	def get_attributes(self, image, fail_silently=True):
		"""
		Return the parameters for remove image. The image url, the full path of
		the image and the default url given to all model instances when not set.
		@param image: image field.
		@param fail_silently: boolean to raise or suppress exceptions.
		"""
		try:

			return image.url, image.path, self.get_default(fail_silently)

		except (FieldDoesNotExist, AttributeError, ValueError) as e:

			if fail_silently: return None, None, None
			raise e


	def get_original_image(self, instance):
		"""
		Return the image field from the database.
		@param instance: model instance.
		"""
		try:

			return getattr(self.Meta.model.objects.get(pk=instance.pk), self.image_field)
		
		except (self.Meta.model.DoesNotExist, AttributeError, ValueError):
			
			return None


	def remove_image(self, url, path, default, fail_silently=True):
		"""
		Remove a file if the url is not equal to the default url which can be used by 
		many instances.
		@param url: url of the image to delete.
		@param path: full system path used to remove the file with.
		@param default: default url given to all users with no image set (get_default()).
		"""
		try:

			if not default or default != url:
				import os
				os.remove(path)

		except (FileNotFoundError, TypeError) as e:
		
			if fail_silently: return None
			raise e

# ----------------------------------------------------------------------------------------

class FileUploadMixin(object):
	"""
	Validate the uploaded file against the custom upload handlers and generate a thumbnail 
	if needed. The name of the thumbnail will be the name of the uploaded file with the 
	suffix '_thumb'.
	"""

	file_upload_errors = {

		'error': _('We could not process your file.')

	}

	def __init__(self, *args, **kwargs):
		"""

		"""

		self.request = kwargs.pop('request', None)

		# File field name.
		self.file_field = kwargs.pop('file_field', None)
		
		# Thumbnail field name.
		self.thumbnail_field = kwargs.pop('thumbnail_field', None)


		# Set True if the image is being changed to force a thumbnail change.
		self.change = kwargs.pop('change_image', False)

		# If True create a thumbnail with a 1:1 aspect ratio.
		self.square = kwargs.pop('square', True)
		
		# Maximum width and height of the thumbnail.
		self.size = kwargs.pop('thumbnail_size', (60,60))

		# Database media type value - used to identify the type of file being uploaded.
		self.media_type = kwargs.pop('media_type', IMAGE)

		# Error display when no files were uploaded.
		self.file_upload_errors['upload'] = _(kwargs.pop('upload_error', 'Please upload a file.'))
			
		
		super(FileUploadMixin, self).__init__(*args, **kwargs)


	def delete_errors(self, field_name):
		"""

		Delete all form errors for a given field.
		
		@param field_name: Form field name.
		
		"""

		try: 
		
			del self.errors[field_name] 
		
		except KeyError: 
		
			pass


	def clean(self):
		"""
		
		Add a thumbnail image to the form's data.
		
		"""
		cleaned_data = super(FileUploadMixin, self).clean()
		
		self.add_thumbnail(cleaned_data)
		
		return cleaned_data


	def is_valid(self):
		"""
		
		Validate the files uploaded and add error messages to the form for failed
		uploads. Add a thumbnail to the form if one was not uploaded.
		
		"""

		# Get any errors set on the request object by the custom upload handlers.
		upload_error = get_error(self.request)

		# If the upload handler set an error or no files have been uploaded 
		# set an error on the form.
		if upload_error!=None or self.files=={}:
			
			# Delete all errors currently associated with the file field.
			self.delete_errors(self.file_field)
			
			# Add the upload error to the file field.
			self.add_error(

				self.file_field, 
				upload_error or self.file_upload_errors['upload']

			)

		# If the file field is valid but he thumbnail field is not, set a generic upload
		# error message on the file field to inform the user of the issue.
		if self.has_error(self.file_field)==False and self.has_error(self.thumbnail_field)==True:

				self.add_error(self.file_field, self.file_upload_errors['error'])


		return super(FileUploadMixin, self).is_valid()


	def square_image(self, img):
		"""
		
		Return the image Cropped to a 1:1 aspect ratio.

		@param img: PIL image file.
		
		"""

		if img.width != img.height:

			# Save the current format to restore it after the crop.
			format = img.format
							
			if img.width > img.height:

				diff = img.width-img.height
				
				# Calculate the left, top, bottom and right offsets.
				l, t, b = round(diff*0.5), 0, img.height
				r = img.width-(diff-l)

			else:

				diff = img.height-img.width
				
				# Calculate the left, top, right and bottom offsets.
				l, t, r = 0, round(diff*0.5), img.width
				b = img.height-(diff-t)

			
			img = img.crop((l,t,r,b))
			
			# Restore the original image format.
			img.format = format


		return img


	def img_to_mem(self, img, ufile, field_name, name=None):
		"""

		Return the PIL image file as an in memory file.

		@param img: PIL image file.
		@param ufile: Uploaded image file.
		@param field_name: Uploaded file's form field name.
		@param name: Optional name to give the new file - defaults to ufile's name.

		"""
		io = BytesIO()
		
		img.save(io, img.format)
		
		io.seek(0)

		return InMemoryUploadedFile(
					
			file=io,
			field_name=field_name,
			name= ufile.name if name==None else name,
			size=getsizeof(io),
			charset=ufile.charset,
			content_type=ufile.content_type
					
		)

	def add_thumbnail(self, cleaned_data):
		"""
		
		Create a thumbnail from the image file and add it to the form.

		NOTE: PIL's thumbnail function preserves the aspect ratio.

		@param cleaned_data: The form's cleaned_data dictionary.
		
		"""
		try:

			ufile = None

			# Do nothing if the uploaded file field has an error.
			if self.has_error(self.file_field)==False:

				# Get the uploaded file.
				ufile = cleaned_data.get(self.file_field)

				if ufile != None:

					# Get the thumbnail image
					thumb = cleaned_data.get(self.thumbnail_field)


					try:

						print('THUMB', thumb != DEFAULT_PROFILE_THUMBNAIL)

					except:

						pass

					# Normalise the thumnail image name ('ufile_name'_thumb).
					name, dot, ext = ufile.name.rpartition('.')
					name = name+'_thumb'+dot+(ext if self.media_type==IMAGE else 'jpg')

					# If there is an existing thumbnail update the name.
					if self.change==False and thumb!=None:
							
						ufile.seek(0)

						thumb.name = name

					# Create the thumbnail from the uploaded file.
					else:

						img = Image.open(ufile)

						if self.square:
		
							img = self.square_image(img)


						# Create a thumbnail image.
						img.thumbnail(self.size, Image.BICUBIC)

						# Add it to the form.
						cleaned_data[self.thumbnail_field] = self.img_to_mem(

							img, ufile, self.thumbnail_field, name

						)
						
						# Delete any errors associated with the thumbnail field.
						self.delete_errors(self.thumbnail_field)


		# Raised by Image.open() or Image.save() or seek()
		except (KeyError, IOError, ValueError) as e:
			
			# NOTE: If e is an empty string img.format is probably None or ''

			if ufile: 
				ufile.close()

			# Add the generic error message to the file field.
			self.add_error(self.file_field, self.file_upload_errors['error'])


# ----------------------------------------------------------------------------------------



