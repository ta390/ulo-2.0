# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# Standard library imports
from __future__ import unicode_literals

# Core django imports
from django.conf import settings

# Thrid party app imports

# Project imports
from search.utils import search_filter_keys

# ----------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------

BASE_TEMPLATE_KEY = 'base_template'
BASE_TEMPLATE_NAME = 'base.html'
BASE_AJAX_TEMPLATE_NAME = 'base_ajax.html'

# ----------------------------------------------------------------------------------------

SEARCH_FILTERS_KEY = 'search_filters'
SEARCH_FILTER_VALUE_KEY = 'search_filter_value'

# ----------------------------------------------------------------------------------------

def base_template(request):
	
	template_name = BASE_AJAX_TEMPLATE_NAME if request.is_ajax() else BASE_TEMPLATE_NAME
	
	return {

		'site_name': settings.SITE_NAME,
		BASE_TEMPLATE_KEY: template_name, 
		SEARCH_FILTERS_KEY: search_filter_keys,
		SEARCH_FILTER_VALUE_KEY: search_filter_keys.get(request.GET.get('filter'), "")

	}

# ----------------------------------------------------------------------------------------

