# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

from django import template
from django.conf import settings

from users.utils import DEFAULT_PROFILE_PICTURE, DEFAULT_PROFILE_THUMBNAIL

# ----------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------

register = template.Library()

# ----------------------------------------------------------------------------------------

@register.filter(is_safe=True, needs_autoescape=False, expects_localtime=False)
def get_picture(value):
	
	return settings.MEDIA_URL + (DEFAULT_PROFILE_PICTURE if value == '' else + value)

# ----------------------------------------------------------------------------------------

@register.filter(is_safe=True, needs_autoescape=False, expects_localtime=False)
def get_thumbnail(value):

	return settings.MEDIA_URL + (DEFAULT_PROFILE_THUMBNAIL if value == '' else  value)

# ----------------------------------------------------------------------------------------