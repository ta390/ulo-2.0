/* Profile Page Javascript File */
/* Dependencies: JQuery, Base.js, User.js */

/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";


/* ------------------------------------------------------------------------------------ */

	/*
		Profile page tabs.
	*/
	function Tabs(){

		Ulo.checkDependencies("Session");
		// Ulo.checkTempDependencies("setCounters");

		/* Follow/Unfollow user's */
		// this.connect = new Connect();

		this.jqxhr = null;
		this.active_class = "active";
		this.next_page_id = "next_page";

		this.regexp_param = /(max_id=)(\d*)(\s*|&|$)/;
		this.post_url = "/post/000/";


		/* Tabs setup */

		var tabs = getElement("profile_tabs").getElementsByTagName("a"),
		container = getElement("profile"),
		next_page = this.getNextPage();
			
		for(var i=0; i<tabs.length; ++i){

			if(hasClass(tabs[i], this.active_class)){

				/*
					Create the next page button if one was not rendered with the html.
				*/
				if(next_page===null){

					/*
						If the button was not rendered then this tab has no more 
						results. 
					*/
					tabs[i].next_page = false;

					next_page = container.appendChild(
					
						makeElement("a", {

							"id":this.next_page_id, 
							"class":Ulo.cls.hide, 
							"href":"#"
							
						}, "Next Page")
					
					);

				} else{

					tabs[i].next_page = true;

					/*
						Update the tab's url to match the button's url which will contain 
						the max_id as a parameter if the tab rendered any results.
					*/
					this.setHref(tabs[i], this.getHref(next_page));

				}

				

				/* 
					Assign the active tab to the class 
				*/
				this.active_tab = tabs[i];

			}

			/* Add event to change the tabs */
			$(tabs[i]).on(Ulo.evts.click, {self:this}, this.selectTab);

		}

		if(this.active_tab===undefined){

			throw Error("No active tab.");

		}

		/* END Tabs setup */


		this.username = container.getAttribute("data-username");

		/* Store the profile user's id */
		this.user_id = parseInt(container.getAttribute("data-user-id"));

		/* Add event to get the next set of tab results */
		$(next_page).on(Ulo.evts.click, {self:this}, this.getContent);

	}

	Tabs.prototype = {

		constructor: Tabs,
		
		/* ---------------------------------------------------------------------------- */
		/*
			Return the button used to fetch more results.
		*/
		getNextPage: function(){
			return getElement(this.next_page_id);
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Show the button if the tabs has more results else hide it.
			
			@param anchor: tab element.
		*/
		displayNextPage: function(anchor){
			(anchor.next_page ? removeClass : addClass)(this.getNextPage(), Ulo.cls.hide);
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the container that holds the tab's results.
			
			@param anchor: tab element.
		*/    
		getTabResults: function(anchor){
			return getElement(anchor.id+"_tab");
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Create a container to append tab results to.
			
			@param anchor: tab element.
		*/
		createTabContainer: function(anchor){
			getElement("tab_content")
				.appendChild(makeElement("div", {"id":anchor.id+"_tab"}));
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Set the href value of the anchor (tab) element.
			
			@param anchor: tab element.
			@param href: href value.
		*/
		setHref: function(anchor, href){
			anchor.setAttribute("href", href);
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the href with the url parameter "max_id".
			
			@param anchor: tab element.
		*/
		getHref: function(anchor){

			var href = anchor.getAttribute("href") || "";
			
			if(/max_id=/.test(href)===false){

				href += (/\/$/.test(href) ? "?" : "&") + "max_id=";

			}

			return href;
			
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the href with no url parameter.
			
			@param anchor: tab element.
		*/
		getRawHref: function(anchor){

			return (anchor.getAttribute("href") || "").replace(/[?].*$/, "");

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Update the value of the "max_id" url parameter on the next page button and
			the active tab.
			
			@param anchor: tab element.
			@param max_id: database cursor.
		*/
		setParam: function(anchor, max_id){

			var href = anchor.getAttribute("href");
			href = href.replace(this.regexp_param, "$1"+max_id+"$3");

			this.setHref(anchor, href);
			this.setHref(this.active_tab, href);

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Reutrn the value of the url parameter max_id or "" if the anchor (tab)
			element does not have a max_id parameter.

			@param anchor: tab element.
		*/
		getParam: function(anchor){

			var r = this.regexp_param.exec(anchor.getAttribute("href"));
			return r===null ? "" : r[2];

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Event handler to switch between the tabs.
		*/
		selectTab: function(e){

			e.preventDefault(e);

			var self = e.data.self;

			if(self.active_tab !== e.currentTarget){

				/*
					Remove the "active" class from the current tab and hide its results.
				*/
				removeClass(self.active_tab, self.active_class);
				addClass(self.getTabResults(self.active_tab), Ulo.cls.hide);

				/*
					Add the "active" class to the selected tab.
				*/
				self.active_tab=addClass(e.currentTarget, self.active_class);

				/*
					Set the tab's url on the next page button.
				*/
				self.setHref(self.getNextPage(), self.getHref(self.active_tab));

				/*
					Update the page url to the tab's url (excluding parameters).
				*/
				updateURL(self.getRawHref(self.active_tab));

				/*
					Get the tab's results container. 
				*/
				var container = self.getTabResults(self.active_tab);
				/*
					If the tab has no container create one and make a request for its
					results.
				*/
				if(container===null){

					self.createTabContainer(self.active_tab);
					/*
						Modify the event's target to be the next page button.
					*/
					e.target=e.currentTarget=self.getNextPage();
					/* 
						Add a data attribute to indicate that this is the first request 
						for this tab. 
					*/
					e.data.initial_request = true;
					self.getContent.call(e.target, e);

				} 
				/*
					Else show the container and the next page button if the tab has
					more results.
				*/
				else{

					removeClass(container, Ulo.cls.hide);
					self.displayNextPage(self.active_tab);

				}
				
				

			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Get the next set of results for the current tab and append each result to 
			it's container.
		*/
		getContent: function(e){

			e.preventDefault();

			var self=e.data.self;

			/*
				Request the results only if the tab has more results.
			*/
			if(self.jqxhr===null && self.active_tab.next_page!==false){

				self.jqxhr = $.ajax({

						type: "GET", 
						url: this.getAttribute("href")+"&tab=true", 
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/*
							Update the max_id parameter of the next page button.
						*/
						self.setParam(e.currentTarget, data.max_id||"");
						/*
							Update the tab's next_page boolean value.
						*/
						self.active_tab.next_page=data.next_page;
						/*
							Hide the next page button if the tab has no more results.
						*/
						self.displayNextPage(self.active_tab);

						/*
							Tab's results container, 
							Boolean - true if no results were returned.
						*/
						var container = self.getTabResults(self.active_tab),
						no_data = data.content===undefined || data.content.length===0;
						
						/*
							Prepare the container if this is the first request.
						*/
						if(e.data.initial_request===true){

							emptyElement(container);

							if(data.count!==undefined){

								// setCounters(data.count, self.active_tab);

							}

							if(no_data){

								container.appendChild( self.empty(self.active_tab) );

							}

						}

						/*
							Run the tab's function to process the results.
						*/
						if(no_data===false){

							self[self.active_tab.id](container, data.content);	

						}

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr){
						
						requestMessages(xhr);
					
					})
					
					.always(function(){
					
						self.jqxhr = null;
				
				});
			}
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Display a message if the tab has no results.
		*/
		empty: function(){

			var container = makeElement("div", {"class": "empty"}),
			content = document.createElement("p"),
			id = this.active_tab.id,
			is_owner = Ulo.Session.isOwner(this.user_id),
			text = (is_owner ? "You have " : "@"+this.username+" has ");

			if(id==="posts"){

				text += " not made any posts.";

			} else if(id==="followers"){

				text += " no followers.";

			} else if(id==="following"){

				text += " not followed anyone.";

			} else{

				text = "No results.";

			}
			
			container.appendChild(content).appendChild(document.createTextNode(text));

			return container;

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Append each post to the posts tab container.

			@param container: posts tab container.
			@param data: array of posts.
		*/
		posts: function(container, data){

			var now = new Date().getTime(), url, post, item, parent, child;

			for(var i=0; i<data.length; ++i){

				url = this.post_url.replace("000", data[i].cid);

				/* Post containers */
				post = makeElement("div", {"class": "post box_border"});
				item = post.appendChild(makeElement("div", {"class": "item"}));
				/* END Post containers */

				/* Image field */
				parent = makeElement("a", {
					"class": "thumbnail", 
					"href": url,
					"data-apl": "true",
					"title": "Post Detail"
				});
				child = makeElement("img", {"src": Ulo.MEDIA_URL+data[i].file0});
				item.appendChild(parent).appendChild(child);
				/* END Image field */

				/* Data fields container */
				parent = makeElement("div", {"class": "fields"});
				item.appendChild(parent);
				/* END Data fields container */

				/* Title field */
				child = makeElement("a", {
					"class": "title ellipsis bold", 
					"href": url,
					"data-apl": "true",
					"title": "Post Detail"
				}, data[i].title);
				parent.appendChild(child);
				/* END Title field */

				/* Stats container */
				child = makeElement("div", {"class": "stats"});
				parent = parent.appendChild(child);
				/* END Stats container */

				/* Published field */
				child = makeElement("span", {"class": "published"}, elapsedTime(now, data[i].published));
				parent.appendChild(child);
				/* END Published field */

				/* View count field */
				child = makeElement("span", {"class": "views"}, data[i].views+" views");
				parent.appendChild(child);
				/* END View count field */

				container.appendChild(post);
				registerLinks(post);

			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Append each connection (user) to the followers or following.

			@param container: followers or following tab container.
			@param data: array of users.
			@param is_following: boolean - true if the user is following all users.
		*/
		connection: function(container, data, is_following){

			var type, connection, parent, child, 
			auth_id=Ulo.Session.get()[Ulo.Session.AUTH_NAME];


			for(var i=0; i<data.length; ++i){

				/* Type of link to create */
				type = (is_following || data[i].is_following) ? "unfollow" : "follow";

				/* Connection container */
				connection = makeElement("div", {"class": "connection box_border"});
				/* END Connection container */

				/* Profile picture */
				parent = connection.appendChild(
					makeElement("img", {"src": Ulo.MEDIA_URL+data[i].picture})
				);
				/* END Profile picture */

				/* User fields container */
				parent = connection.appendChild(makeElement("div", {"class": "fields"}));
				/* END User fields container */

				/* Name field */
				child = parent.appendChild(
					makeElement("h3", {"class": "name ellipsis"}, data[i].name)
				);
				/* END Name field */

				/* Username field */
				child = parent.appendChild(
					makeElement("div", {"class": "username ellipsis"}, data[i].username)
				);
				/* END Username field */

				/*
					If the user (data) is not the authenticated user add a link to 
					follow or unfollow the user.
				*/
				if(auth_id != data[i].id){

					/* Follow/Unfollow link */
					parent = connection.appendChild(document.createElement("a"));
					/* Set the inner html for the link and register click events */
					this.connect.registerCreate(parent, type, data[i].id)

				}

				container.appendChild(connection);
				registerLinks(connection);
			}
		
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Append each follower to the followers tab.
			
			@param container: followers tab container.
			@param data: array of users.
		*/
		followers: function(container, data){

			return this.connection(container, data, false);

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Append each following user to the following tab.

			@param container: following tab container.
			@param data: array of users.
		*/
		following: function(container, data){

			return this.connection(container, data,  Ulo.Session.isOwner(this.user_id));

		}

	}




	/* -------------------------------------------------------------------------------- */
	
	function ProfilePicture(){

		var menu = getElement("change_picture");

		if(menu !== null){

			this.FileUpload = new FileUpload(menu, this.changeImage.bind(this), {});
		
			menus(getElement("toggle_change_picture"), "height");

			this.DEFAULT_IMAGE_URL = Ulo.MEDIA_URL + "default/profile.jpg";

			this.deleteButton();

		}

	}

	ProfilePicture.prototype = {

		constructor: ProfilePicture,

		/* -------------------------------------------------------------------------------- */

		getPictureURL: function(url){

			return getElement("pp").src;

		},

		/* -------------------------------------------------------------------------------- */

		setPictureURL: function(url){

			if(url){

				getElement("pp").src = url;

			}

		},

		/* -------------------------------------------------------------------------------- */

		hasPicture: function(){

			return new RegExp(this.DEFAULT_IMAGE_URL+"$").test(this.getPictureURL()) === false

		},

		/* -------------------------------------------------------------------------------- */

		deleteButton: function(){

			var button = getElement("delete_photo");

			if( this.hasPicture() && button === null){

				button = makeElement("li");

				button.appendChild( 

					makeElement(

						"button", 
						{id: "delete_photo", type: "button"}, 
						"Delete photo"

					)

				);

				getElement("change_picture").appendChild(button);

				$(button).on(Ulo.evts.click, {self: this}, this.deleteImage);

			} else if( button !== null){

				removeElements( button.parentNode );

			}

		},

		/* -------------------------------------------------------------------------------- */
		
		changeImage: function(e){

			var canvas = this.FileUpload.Editor.getCanvas(false, 320, 320);

			if(canvas === null || canvas.failed){

				// ERROR

			}

			else{

				var self = this;

				this.FileUpload.close();

				canvas.toBlob(function(blob){

					var data = new FormData();

					var token = Ulo.Session.get()[Ulo.Session.TOKEN_NAME];
					
					data.append(Ulo.Session.CSRF_TOKEN_NAME, token);
					
					data.append("picture", blob);

					$.ajax({

							type: "POST", 
							url: "/user/update/image/",
							data: data,
							cache: false,
							processData: false,
							contentType: false,
							statusCode: requestCodes

						})

						/* Params: server data, status code, xhr */
						.done(function(data, sc, xhr){

							self.setPictureURL(data.image_url);

							self.deleteButton();

						})

						/* Params: xhr, status code, error type */
						.fail(function(xhr){
							
							xhrErrorMessage(xhr);
						
						})
						
						.always(function(){
						
						
					});

				}, 'image/jpeg', 95);

			}

		},

		/* -------------------------------------------------------------------------------- */

		deleteImage: function(e){

			var self = e.data.self;

			if( self.hasPicture() ){

				var token = Ulo.Session.get()[Ulo.Session.TOKEN_NAME];

				$.ajax({

						type: "POST", 
						url: "/user/delete/image/",
						data: Ulo.Session.CSRF_TOKEN_NAME + "=" + token,
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						self.setPictureURL(data.image_url);

						self.deleteButton();

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr){
						
						xhrErrorMessage(xhr);
					
					})
					
					.always(function(){
					
					
				});

			}

		}

	}

	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */

	$(function(){

		new Tabs();
		new ProfilePicture();

	})

/* ------------------------------------------------------------------------------------ */
}());



