/* Profile Page Javascript File */
/* Dependencies: JQuery, Base.js */

"use strict";

/* ------------------------------------------------------------------------------------ */

/*
	Perform ajax requests for follow/unfollow links marked with the data attribute 
	data-ac (ajax connect). The value of the attribute must be "follow" or "unfollow".

	The function creates/updates links with the format:

		<a data-ac="'type'" href="user/'type'/'user_id'">
			<span class="icon icon_'type'"></span>'type'
		</a>

		where 'type' is the string "follow" or "unfollow"
		and 'user_id' is the primary key of the user to follow or unfollow

	@param callback: optional callback function run each time a follow or unfollow
		request is made successfully.
	@param thisArg: option this argument for the callback function.

*/
function Connect(callback){

	/* Optional callback function */
	this.callback = callback;

	/* Current request */
	this.jqxhr = null;

	/* Type values */
	this.follow = "follow";
	this.unfollow = "unfollow";

	/* Register all anchor elements with the attribute data-ac on the page */
	this.register();

}
Connect.prototype = {

	constructor: Connect,

	/*
		Register the event handler connectHandler() on an anchor element.
		@param aOrC: optional anchor element or context
	*/
	register: function(aOrC){

		(aOrC!==undefined && aOrC.nodeName==="A" ? $(aOrC) : $("a[data-ac]", aOrC))
			.on(Ulo.evts.click, {self:this}, this.connectHandler);
	
	},
	/*
		Create the contents of a new anchor element and register the event handler 
		connectHandler() on an anchor element.
		@param a: anchor element
		@param type: "follow" or "unfollow";
		@param id: primary key of the user being followed or unfollowed.
	*/
	registerCreate: function(a, type, id){
		this.create(a, type, "/user/"+type+"/"+id+"/");
		this.register(a);
	},
	/*
		Disable the anchor element.
		@param a: anchor element
	*/
	disableLink: function(a){
		a.disabled=true;
	},
	/*
		Enable the anchor element and set the jqxhr value to null.
		@param a: anchor element
	*/
	enableLink: function(a){
		a.disabled=false;
		this.jqxhr = null;
	},
	/*
		Set the href, url and inner html of the anchor element.
		@param a: anchor element
		@param type: "follow" or "unfollow";
		@param url: href url for the link.
	*/
	create: function(a, type, url){

		/* Set the data-ac and href values */
		a.setAttribute("data-ac", type);
		a.setAttribute("href", url);

		/* Create the contents for the link */
		var icon = makeElement("span", {"class": "icon icon_"+type})
		var text = document.createTextNode(type.charAt(0).toUpperCase()+type.slice(1));

		/* Set the contents for the link */
		appendElements(emptyElement(a), [icon, text]);
	},
	/*
		Event handler to make an ajax request for a link and update the
		link's attribtues and contents if the request is successful.
	*/
	connectHandler: function(e){

		var self = e.data.self;

		if(Ulo.PIP.loginRequired()===false && self.jqxhr === null){

			self.disableLink(e.currentTarget);

			var url = e.currentTarget.getAttribute("href");

			self.jqxhr = $.ajax({

					type:"GET", 
					url:url, 
					statusCode:requestCodes
				
				})

				/* Params: server data, status code, xhr */
				.done(function(data, sc, xhr){

					/*
					*/
					if(Ulo.Session.hasChanged(xhr)){

						Ulo.PIP.updatePage(xhr, data);
					
					}

					/* 
					*/
					else if(data.html!==undefined){

						Ulo.PIP.displayPage(xhr, data);

					}

					/*
					*/
					else{

						/* Get the current link type "follow" or "unfollow" */
						var type = e.currentTarget.getAttribute("data-ac");
						type = (type===self.follow)? self.unfollow : self.follow;

						/* Create the new url */
						url = url.replace(/(follow|unfollow)/, type);
						/* Set the href and inner HTLM of the link */
						self.create(e.currentTarget, type, url);

						/* Run the callback function if one has been defined */
						self.callback!==undefined && self.callback(e.currentTarget, type);

					}

				})

				/* Params: xhr, status code, error type */
				.fail(function(xhr){

					console.log(xhr);
					requestMessages(xhr);
					
				})
			
				.always(function(){

					self.enableLink(e.currentTarget);
			
			});

		}

		e.preventDefault();

	}

}
/* ------------------------------------------------------------------------------------ */
/*
	Return the abbreviation of "value" as a string in the format "Ns" where "N" 
	is a value between 0 - 100 and "s" is the symbol that represents the value.

	@param value: int or float < 10^18 (Q, Quintillion)

	SYMBOL TABLE:

		10^3 	'K, Thousand'
		10^6 	'M, Million'
		10^9 	'B, Billion'
		10^12 	't, Trillion'
		10^15 	'q, Quadrillion'
		10^18 	'Q, Quintillion'
		...

	NOTE: This is a copy of the abbreviate function implemented in the template 
	tags file ulo_humanise.py in the ulo app.
*/
function abbreviate(value){
	
	var unit = 1
	var mult = 1000
	var max_value = 1000
	var abbr = ["", "K", "M", "B", "T", "Q"];

	try{

		for(var i=0; i<abbr.length; ++i){
		
			if(value < max_value){
				return Math.floor(value/unit)+abbr[i];
			}

			unit *= mult
			max_value *= mult
		}

		throw new Error("The value passed is too large");

	} catch(e){

		return "";
	
	}
		
}
/* ------------------------------------------------------------------------------------ */
/*
	Return a date in the format "Month Day, Year" or an empty string.
	
	@param date: DateTime string.

	NOTE: This is a copy of the short_date function implemented in the template 
	tags file ulo_humanise.py in the ulo app.
*/
function shortDate(date){
	
	try{

		date = new Date(date)

		return this.months[date.getMonth()]+" "+date.getDate()+", "+date.getFullYear();
	
	} catch(e){
	
		return "";
	
	}

}
/* ------------------------------------------------------------------------------------ */
/*
	Return an abbreviated time period in the format "Nt" where N is a number and 
	t is a period of time ("y" - years, "w" - weeks, "d" - days, "h" - hours, 
	"m" - minutes and "s" - seconds).
	
	@param now: epoch milliseconds.
	@param date: DateTime string.

	NOTE: This is a copy of the elapsed_time function implemented in the template 
	tags file ulo_humanise.py in the ulo app.
*/
function elapsedTime(now, date){

	try{

		date = new Date(date);
		// var timeOffset = now.getTimezoneOffset();
		/* Seconds */
		var secs = (now-date.getTime())/1000;
		/* Days */
		var days = Math.floor(secs/86400);


		/* years */
		if(days > 364){
			return Math.floor(days/365)+"y";
		}
			
		/* weeks */
		if(days > 27){
			return Math.floor(days/7)+"w";
		}

		/* days */
		if(days > 0){
			return days+"d";
		}
		
		/* hours */
		var t = secs/3600
		if(t > 0){
			return Math.floor(t)+"h";
		}

		/* minutes */
		t = (secs % 3600)
		if(t > 0){
			return Math.floor(t)+"m";
		}

		/* seconds */
		t = secs % 60
		if(t > 0){
			return Math.floor(t)+"s";
		}

		return "now";

	} catch(e){
	
		return "";
	
	}

}
/* ------------------------------------------------------------------------------------ */

function setCounters(value, context){

	var counters = [$(".full_count", context), $(".abbr_count", context)];

	for(var i=0; i<counters.length && counters[i]; ++i){

		for(var j=0; j<counters[i].length; ++j){

			emptyElement(counters[i][j]).appendChild(
				document.createTextNode(i===0 ? value : abbreviate(value))
			);

		}
	}

}

/* ------------------------------------------------------------------------------------ */

function updateCounters(container, inc){

	var value = NaN;

	if(container!==null){

		var full_counts = $(".full_count", container);

		if(full_counts.length > 0){

			value = parseInt(
				full_counts[0].textContent||full_counts[0].innerText
			)+inc;

			if(value===value && inc!==0){

				var abbr_counts = $(".abbr_count", container);

				for(var i=0; i<abbr_counts.length; ++i){
					emptyElement(abbr_counts[i])
						.appendChild(document.createTextNode(abbreviate(value)));
				}

				for(var i=0; i<full_counts.length; ++i){
					emptyElement(full_counts[i])
						.appendChild(document.createTextNode(value));
				}

			}

		}

	}

	return value;

}

/* ------------------------------------------------------------------------------------ */

