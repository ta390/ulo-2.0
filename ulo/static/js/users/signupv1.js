/* Sign Up Form javascript file */
/* Dependencies: jQuery, base.js */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";


	/*
		******************************************************************************************
		FORM VALIDATION CLASS
		******************************************************************************************


		Manage form validation, displaying error messages and help text to the user as they 
		fill in the form.


		STRCUTURE OF THE VALIDATION OBJECT: The validation object is used to store field
		specific information including the current state of the field. It is grouped into the 
		field types: input, select and multi_select. 

		The basci structure of each object within this.input and this.select:
			
			'field1_id'{
				...
				...
			},
			'field2_id'{
				...
				...
			},

		The basic structure of each object within multi_select is:

			'parentNode1_id':{
				...
				has_errors: { select1_id: true, select2_id: true, select3_id: true },
			}
			'parentNode2_id':{
				...
				has_errors: { select1_id: true, select2_id: true, select3_id: true },
			}

			INPUT FIELDS: THE KEY MUST BE THE INPUT FIELD'S ID.
			@param error: stores the current error message or null if no errors.
			@param help_text: stores the optional help text displayed when an input field is 
			empty and in focus.
			@param regex: an array of regular expressions to validate the field's value.
			@param errors: an array of error messages displayed when a regexp returns false.
			Each regexp must have an error message at the same index position.
			@param timeout_id: an optional variable used to store the return value of a 
			setTimeout function call. If present validation will run after N seconds as 
			specified by validateTimeout().

			SINGLE SELECT FIELDS: THE KEY MUST BE THE SELECT FIELD'S ID.
			@param has_error: boolean/flag to indicate that the field has an error.
			@param error: the error message to display when the field has an error.

			MULTI SELECT FIELDS: THE KEY MUST BE THE PARENT NODE FOR ALL SELECT FIELDS.
			The parameters are the same as single select fields above plus...
			@param has_errors: an object where each key is the id of the individual select
			fields. The value is a boolean indicating whether that particular select field 
			has an error (see multi_select basic structure above).

			ALL FIELDS:
			@param validator: optional function that is run when a form is submitted. The
			field will be passed to the function when called. For multi_select fields an
			array containing the select fields is passed.


		ERROR REPORTING: Fields are given the class 'error_class' when they have an error.
		An error message is also given at the top of the form for the field in focus.

			INPUT FIELD ERROR DEFINITION: 
			An input field has an error if it contains a value which is not valid. An empty
			string "" is not condidered an error, help text is displayed when a field is
			empty (help text is optional).

			SELECT FIELD ERROR DEFINITION: 
			A select field has an error if the empty-value/placeholder-value is given as
			the selection. Errors are only displayed once an initial selection has been made.
			Multi select errors are displayed once the user navigates away from a multi 
			select field.

		@param error_class: string of class names to add to the input/select field when it
		has an error.
		@param event_type: the event used to trigger the validation handler on input fields.

	*/	
	function FormValidation(error_class, event_type){

		/* FORM */
		this.form = $("#signup_form").on("submit", {self: this}, this.submitForm);
		
		/* USERNAME AVAILABILITY CLASS */
		this.Username = new UsernameAvailability(error_class, event_type);

		/* AJAX REQUEST */
		this.jqxhr = null;


		/* IDS AND CLASSES */
		/* submit button id */
		this.submit_btn = $("#id_submit", this.form);
		/* id of the div used to display error messages to the user */
		this.info_div = $("#form_info");
		/* class name added to a field when it has an error */
		this.error_class = error_class;
		/* class name added to the span inside info_div when help text is being displayed */
		this.help_class = "help_text";


		/* TEXT DISPLAYED AT THE TOP OF THE FORM WHEN THERE ARE NO ERRORS */
		this.text = "It's free!";


		/* REGEXPS */
		this.regexp_empty_input = /^\s*$/;
		this.regexp_empty_select = /^\s*$|^0$/;


		/* VALIDATION OBJECT */
		/* input fields */
		this.input = {
			"id_name": {
				error: null,
				help_text: null,
				regexp: [
					/^[^@#%^<>(){}=`"*~;:,.+_|&±§?!\[\]\/\\]+$/,
					/^.{1,30}$/
				],
				errors: [
					"Your name cannot contain special characters.",
					"Your name cannot be more than 30 characters long."
				]
			},
			"id_username": {
				error: null,
				help_text: null,
				regexp: [
					/^\w+$/,
					/^.{1,30}$/
				],
				errors: [
					"Your username must consist of letters, numbers and underscores.",
					"Your username cannot be more than 30 characters long."
				],
				validator: this.UsernameAvailability,
			},
			"id_email": {
				error: null,
				help_text: null,
				regexp: [
					/^(?=.{3,256}$)[^@\s]+@[^@\s\.]+(\.[^@\s\.]+)+$/,
					// /^([^@\s]+(@([^@\s\.]+((\.([^@\s\.]+))+?)?)?)?)?$/,
				],
				errors: [
					"Please enter a valid email address."
				],
				timeout_id: null,
			},
			"id_password": {
				error: null,
				help_text: "Your password must combine letters, numbers or punctuation.",
				regexp: [
					/^.{6,}$/,
					/^.{6,128}$/,
					/^(?![a-zA-Z]+$)/,
					/^(?!\d+$)/,
					/^(?!(.)\1+$|password(?:\d{0,2}|(\d)\2{1,})$)/,
				],
				errors: [
					"Your password must be at least 6 characters.",
					"Your password must be less than 128 characters.",
					"Your password cannot contain letters only.",
					"Your password cannot contain numbers only.",
					"Please choose a more secure password.",
				]
			}
		}
		/* single select fields */
		this.select = {
			"id_gender": {
				has_error: false,
				error: "Please select a gender.",
			},
		}
		/* multi select fields */
		this.multi_select = {
			/* NOTE: If any ids change ("id_dob", "id_dob_0", ...) then update ageVerification */
			"id_dob": {
				has_error: false,
				has_errors: {id_dob_0:true, id_dob_1:true, id_dob_2:true},
				error: "Please enter your date of birth.",
				validator: this.verifyAge,
			},
		}
		/* END VALIDATION OBJECT */


		/* ADD INPUT FIELD EVENTS */
		var useValidate = (event_type === "change");
		for(var id in this.input){
			var eventHandler = (useValidate || this.input[id].timeout_id === undefined) ? 
									this.validate : this.validateTimeout;

			$("#"+id).on(event_type, {self: this}, eventHandler)
				.on("focus", {self: this}, this.information);
		}

		/* ADD SINGLE SELECT FIELD EVENTS */
		for(var id in this.select){
			$("#"+id).on("change", {self: this}, this.validateSelect)
				.on("focus", {self: this}, this.informationSelect);
		}

		/* ADD MULTI SELECT FIELD EVENTS */
		for(var id in this.multi_select){
			$("#"+id).children().on("change", {self: this}, this.validateMultiSelect)
				.on("blur", {self: this}, this.validateMultiSelectOnBlur)
				.on("focus", {self: this}, this.informationMultiSelect)
		}
	}
	FormValidation.prototype = {

		constructor: FormValidation,

		/* INPUT FIELD VALIDATION */
		/* ------------------------------------------------------------------------------------ */
		/*
			ON INPUT OR CHANGE: validate an input field and display any errors back to the user. 
			If no errors remove any existing errors being displayed by the form.
		*/
		validate: function(e){

			var self = e.data.self;
			var error = self.hasError(this);

			if(error === null){
				self.removeMsg();
			} else{
				var classes = (this.value === "") ? self.help_class : self.error_class;
				self.displayMsg(error, classes);
			}
		},
		/*
			ON INPUT: run validate() after a timeout - this gives the user time to type their 
			input before displaying error messages.
		*/
		validateTimeout: function(e){

			var self = e.data.self;
			clearTimeout(self.input[this.id].timeout_id);
			$(this).removeClass(self.error_class);
			
			self.input[this.id].timeout_id = setTimeout(	
				function(){ self.validate.call(e.target, e) }, 600
			);
		},
		/*
			ON FOCUS: Display any existing errors or help text for the field in focus.
		*/
		information: function(e){

			var self = e.data.self;

			var info = self.input[this.id].error;
			if(info){
				return self.displayMsg(info, self.error_class);
			}

			info = !this.value && self.input[this.id].help_text;
			if(info){
				return self.displayMsg(info, self.help_class);
			}

			self.removeMsg();
	
		},
		/* END INPUT FIELD VALIDATION */
		/* ------------------------------------------------------------------------------------ */


		/* SELECT FIELD VALIDATION */
		/* ------------------------------------------------------------------------------------ */
		/*
			ON CHANGE (single select fields): check that a valid selection was made or display 
			an error message back to the user.
		*/
		validateSelect: function(e){

			var self = e.data.self;

			/* if invalid selection... */
			if(self.hasSelectError(this) === true){
				self.displayMsg(self.select[this.id].error, self.error_class);

			/* else remove any previous errors */
			} else{

				$(this).removeClass(self.error_class);
				self.select[this.id].has_error = false;
				self.removeMsg();
			}
		},
		/*
			ON CHANGE (multi select fields): check that all selections for a multi select field 
			are valid and set the flag has_error accordingly. Remove the error class if a field 
			has no errors (i.e. no selections have ever been made or the field is partially 
			complete). Error messages are displayed by validateMultiSelectOnBlur.
		*/
		validateMultiSelect: function(e){

			var self = e.data.self;
			var obj = self.multi_select[this.parentNode.id];

			obj.has_errors[this.id] = self.regexp_empty_select.test(this.value);
	
			for(var i in obj.has_errors){
				if(obj.has_errors[i] === true){
					obj.has_error = true;
					return;
				}
			}

			obj.has_error = false;
			$(this.parentNode).removeClass(self.error_class);
		},

		// _birthdayMessage: function(){
		// 	var d = document.getElementById("id_dob_0").value;
		// 	var m = document.getElementById("id_dob_1").value;
		// 	var date = new Date();

		// 	if(d==date.getDate() && m==(date.getMonth()+1)){
		// 		this.text = "Happy Birthday!&nbsp;&#x1F382;";
		// 	} else{
		// 		this.text = "It's free!";
		// 	}
		// },
		/*
			ON BLUR (multi select fields): when a user navigates away from a multi select field
			add the error class to the parent if the multi select field has an error.
		*/
		validateMultiSelectOnBlur: function(e){

			var self = e.data.self;
			var obj = self.multi_select[this.parentNode.id]

			/* if the related target is not one of the multi select fields... */
			if(!e.relatedTarget || obj.has_errors[e.relatedTarget.id] === undefined){
				/* if the object has an error, set the error class on the field's parent */
				if(obj.has_error === true){
					$(this.parentNode).addClass(self.error_class);
				}
			} 
		},
		/*
			HELPER (all select fields): display or remove an error message depending
			on the error status of a select/multi-select field.
			@param object: name of the validation object (e.g. "select" or "multi_select").
			@param id: id of the field.
		*/
		_informationSelect: function(object, id){
			
			if(this[object][id].has_error === true){
				this.displayMsg(this[object][id].error, this.error_class);
			} else{
				this.removeMsg();
			}
		},
		/*
			ON FOCUS (single select fields): display or remove an error message depending
			on the error status of a select field.
		*/
		informationSelect: function(e){
			
			e.data.self._informationSelect("select", this.id);
		},
		/*
			ON FOCUS (multi select fields): display or remove an error message depending
			on the error status of a select field.
		*/
		informationMultiSelect: function(e){
			
			e.data.self._informationSelect("multi_select", this.parentNode.id);
		},

		/* END SELECT FIELD VALIDATION */
		/* ------------------------------------------------------------------------------------ */


		/* INPUT FIELD ERROR HANDLERS */
		/* ------------------------------------------------------------------------------------ */
		/*
			HELPER: run the regex validators for a field and return an error message as soon as a 
			regexp fails. Return null if no errors were found.
			@param field: javascript input field.
		*/
		_hasError: function(field){
			
			var regexp = this.input[field.id].regexp;
			for(var i in regexp){
				if(regexp[i].test(field.value) === false){
					/* returns the error message for a regexp */
					return this.addError(field, this.input[field.id].errors[i]);
				}
			}

			/* return null */
			return this.removeError(field, null);
		},
		/*
			Check if the input field has an error and return an error message or null. An empty 
			string is not displayed to the user as an error, help text is rendered in this 
			scenario if provided by the field.
			@param field: javascript input field.
		*/
		hasError: function(field){

			if(field.value===""){
				/* returns help_text or null */
				return this.removeError(field, this.input[field.id].help_text);
			}
			/* returns an error message or null */
			return this._hasError(field);
		},
		/*
			Add an error message to the field's error variable (this.input['field_id'].error) and 
			add the error class to the input field. Return the error message (msg).
			@param field: javascript input field.
			@param msg: error message.
		*/
		addError: function(field, msg){
			this.input[field.id].error = msg;
			$(field).addClass(this.error_class);
			return msg;			
		},
		/*
			Remove the error class from an input field and set the error value using 'msg'.
			Return the value of msg.
			@param field: javascript input field.
			@param msg: error value (usually null or help text).
		*/
		removeError: function(field, msg){
			this.input[field.id].error = msg;
			$(field).removeClass(this.error_class);
			return msg;	
		},
		/* END INPUT FIELD ERROR HANDLERS */
		/* ------------------------------------------------------------------------------------ */


		/* SELECT FIELD ERROR HANDLERS */
		/* ------------------------------------------------------------------------------------ */
		/*
			Return true if a select field has no value or false otherwise. Call addSelectError
			or addMulitSelectError to set the has_error flag and apply the error styling.
			@param field: select field.
			@param parent: option javascript element, if provided it is assumed to be the parent 
			container of a multi select field.
		*/
		hasSelectError: function(field, parent){
			if(this.regexp_empty_select.test(field.value)){
				if(parent === undefined){
					this.addSelectError(field);
				} else{
					this.addMultiSelectError(parent);
				}		
				return true;
			}
			return false;
		},
		/*
			HELPER: add the error class to a select field and set the has_error flag to true. If 
			an error message is given overwrite the objects default error message.
			@param field: field to add the error class to.
			@param object: name of the select object "select" or "multi_select".
			@param error: optional error message. 
		*/
		_addSelectError: function(field, object, error){
			$(field).addClass(this.error_class);
			this[object][field.id].has_error = true;
			if(error !== undefined){ this[object][field.id].error = error; }	
		},
		/*
			Set the has_error flag and styling for a single select field.
		*/
		addSelectError: function(field, error){
			return this._addSelectError(field, "select", error)
		},
		/*
			Set the has_error flag and styling for a multi select field.
		*/
		addMultiSelectError: function(field, error){
			return this._addSelectError(field, "multi_select", error)
		},

		/* END SELECT FIELD ERROR HANDLERS */
		/* ------------------------------------------------------------------------------------ */


		/* FORM SUBMISSION FUNCTIONS */
		/* ------------------------------------------------------------------------------------ */
		/*
			Enable the form's submit button.
		*/
		enableSubmit: function(){
			this.submit_btn.removeAttr("disabled");
		},
		/*
			Disable the form's submit button.
		*/
		disableSubmit: function(){
			this.submit_btn.attr("disabled", "disabled");
		},
		/*
			Run the field's validator function and return its value. If the field does not have a 
			validator function return 0 - validator functions must return 0 or 1;
		*/
		fieldValidator: function(obj, field){
			return (obj.validator===undefined) ? 0 : obj.validator.call(this, field);
		},
		/*
			Return the number of input fields that contain errors and add error messages and
			styling to each field that has an error.
		*/
		inputErrors: function(){

			var error_count = 0;
			for(var id in this.input){

				/* if it already has an error increment error count */
				if(this.input[id].error !== null){
					error_count++;

				/* else run a validation check on the field and perform any error handling */
				} else{
					var field = document.getElementById(id);
					/* if empty set a default error or get error returned by _hasError */
					var error = this.regexp_empty_input.test(field.value) && "This field is required." || 
									this._hasError(field)
					if(error){
						error_count++
						this.addError(field, error);
					} else{

						/* if the field has no errors run its validator function */
						error_count += this.fieldValidator(this.input[id], field);
					}
				}
			}

			return error_count;
		},
		/*
			Return the number of selct fields that contain errors and add error messages and
			styling to each field.
		*/
		selectErrors: function(){

			var error_count = 0;
			for(var id in this.select){

				var field = document.getElementById(id);

				/* if the field is empty add error and increment error_count */
				if(this.hasSelectError(field)){
					error_count++;
				} else{
					error_count += this.fieldValidator(this.select[id], field);
				}
			}
			return error_count;
		},
		/*
			Return the number of multi select fields that contain errors and add error messages
			and styling to each field.
		*/
		multiSelectErrors: function(){

			var error_count = 0;
			for(var id in this.multi_select){

				/* get each individual select field */
				var selects = document.getElementById(id).childNodes;
				var length = selects.length;

				for(var i = 0; i < length; i++){
					/* if any field is empty add error and increment error_count */
					if(this.hasSelectError(selects[i], selects[i].parentNode)){
						error_count++;
						break;
					}
				}

				if(this.multi_select[id].has_error === false){
					error_count += this.fieldValidator(this.multi_select[id], selects);
				}
			}
			return error_count;
		},

		/* FIELD VALIDATOR FUNCTIONS */
		/*
			HELPER: Check that the selected day/month combination is valid and return true if it
			is and false if not.
			@param d: day
			@param m: month
			@param y: year
		*/
		_isDateValid: function(d, m, y){
			if(m > 0){ 	
				var feb = ((!(y % 4) && y % 100) || !(y % 400)) ? 29 : 28;
				var month_days = [31,feb,31,30,31,30,31,31,30,31,30,31];
				return (d <= month_days[m-1]);
			}
			return false;
		},
		/*
			Add an error message and styling to the field and return 1.
			@param field: javascript field.
			@param error: optional error message.
		*/
		_verifyAgeError: function(field, error){
			this.addMultiSelectError(field, error || "Please enter a valid date.");
			return 1;
		},
		/*
			VALIDATOR FUNCTION FOR id_dob:
			Check that the user's age is greater than or equal to the min_age set in the function
			Return 0 if the user is of age and 1 if not.
			@param selects: array containing the dob select fields.
		*/
		verifyAge: function(selects){

			var min_age = 13;
			
			/* ASSUMES DOB FIELD IDS ARE IN LEXICAL ORDER - D.ID < M.ID < Y.ID */
			selects = $(selects).sort(function(a,b){ return a.id > b.id; });

			var d = selects[0].value;
			var m = selects[1].value;
			var y = selects[2].value;

			if(this._isDateValid(d, m, y)){
				
				var today = new Date();
				var dob = new Date(y, m-1, d);
				var age = today.getFullYear() - dob.getFullYear();
				
				m = today.getMonth() - dob.getMonth();
				if(m < 0 || (m === 0 && today.getDate() < dob.getDate())){ 
					--age; 
				}

				if(age < min_age){
					return this._verifyAgeError(
						selects[0].parentNode, 
						"Sorry, we require all our users to be above "+min_age+" years old."
					)
				}
			} else{
				return this._verifyAgeError(selects[0].parentNode);
			}

			return 0;

		},
		/*
			VALIDATOR FUNCTION FOR id_username:
			Check that the username entered by the user is available and return 0 if
			available and 1 if not.
			@param username: javascript username field.
		*/
		UsernameAvailability: function(username){

			if(this.Username.isUsernameUnavailable()){
				this.addError(username, "This username has been taken.");
				return 1;
			}

			return 0;
		},
		/* END FIELD VALIDATOR FUNCTIONS */
		
		/*
			Run validation checks on all form fields and return true if the form has no errors
			and false if not.
		*/
		isFormValidated: function(){

			var error_count = 0;
		
			error_count += this.inputErrors();
			error_count += this.selectErrors();
			error_count += this.multiSelectErrors();

			return error_count === 0;
		},
		/*
			Add 'error' or a generic error message to the form.
			@param error: optional error message.
		*/
		addFormError: function(error){
			this.displayMsg(error || "The form contains errors.", this.error_class);
		},
		/*
			Add a server side validation error to the form.
			@param name: name of the field.
			@param error: error message.
		*/
		addServerError: function(name, error){

			var id = "id_"+name;
			var field = document.getElementById(id);

			/*
				Assume that the server side error is for an input field and call addError.
				If an exception is raise try addSelectError, the addMultiSelectError.
				Non field specific errors are handled by the ajax request.
			*/
			try{
				this.addError(field, error);
			} catch(e){
				try{
					this.addSelectError(field, error);
				} catch(e){
					try{
						this.addMultiSelectError(field, error);
					} catch(e){}
				}
			}

		},
		/*
			Submit the form and redirect the page if successful or display error messages
			back to the user.
		*/
		submitForm: function(e){

			e.preventDefault();
			
			var self = e.data.self;
			
			if(self.jqxhr !== null){
				return;
			}

			self.disableSubmit();
			if(self.isFormValidated()){

				var request = {
					type: "POST",
					url: $(this).attr("action"),
					data: $(this).serialize(),
					cache:false,
				}

				self.jqxhr = $.ajax(request)
					/* params: server data, status code, xhr */
					.done(function(data){
						
						window.location.replace(data.url);

					})
					/* params: xhr, status code, error type */
					.fail(function(xhr){

						$.each(xhr.responseJSON.errors, function(key, error){
							self.addServerError(key, error);
						})

						var form_error = xhr.responseJSON.__all__;
						self.addFormError(form_error);
					})
					.always(function(){
						self.jqxhr = null;
						self.enableSubmit();
				});

			} else{
				self.enableSubmit();
			}

			
		},


		/* GENERIC HELPER FUNCTIONS */
		/* ------------------------------------------------------------------------------------ */
		/*
			Attach a message to the info_div.
			@param msg: string.
			@param classes: string of class names to add to the span.
		*/
		displayMsg: function(msg, classes){
			
			this.info_div.empty().append(
				"<span class='icon "+classes+"'></span><span class='nonfield'>"+msg+"</span>"
			);
		},
		/*
			Remove a message to the info_div.
		*/
		removeMsg: function(){
			this.info_div.empty().append("<span>"+this.text+"</span>");
		},

		/* END GENERIC HELPER FUNCTIONS */
		/* ------------------------------------------------------------------------------------ */
	}




	/*
		******************************************************************************************
		USERNAME AVAILABILITY CLASS
		******************************************************************************************

		Check that the username entered is available and display an icon indicating its 
		availability.

		NOTE: jQuery will execute events in the order they were assigned so always initialise
		this class after FormValidation so that "on input" is fired after FormValidation's 
		"on input".

		@param error_class: name of the class used by FormValidation when setting an error on a
		field.
		@param event_type: event to trigger a lookup ("input" or "change").
	*/
	function UsernameAvailability(error_class, event_type){

		/* handle to the username field */
		this.username = $("#id_username");
		/* handle to the field container where class names are added/removed */
		this.container = $("#username_field_container");
		/* handle to the span element used to display the availability text */
		this.icon_text = $(".availability_text", this.container);


		/* the header name to retrieve information from */
		this.header = "username_exists";
		/* the request url */
		this.url = "/user/available/";

		/* name of the class given to an input field when it has an error */
		this.error_class = error_class;
		/* setTimeout return value - used to cancel a timeout function */
		this.timeout_id = null;
		/* current request object - used to abort requests */
		this.jqxhr = null;

		/* CHECK AVAILABILITY */
		var eventHandler = (event_type === "input") ? this.timeout : this.noTimeout;
		this.username.on(event_type, {self: this}, eventHandler);

		/* TOGGLE AVAILABILITY TEXT DIPLAY */
		$(".availability_icon", this.container).on("click", {self: this}, this.toggleIconText);

	}
	UsernameAvailability.prototype = {
		
		constructor: UsernameAvailability,

		/* AJAX REQUEST */
		/* ------------------------------------------------------------------------------------ */
		/*
			Send an ajax HEAD request to check the availability of a username. Set an
			icon on the input field to indicate its status. The icon is also a button
			used to toggle text.
			@param username: username field value.
		*/
		is_available: function(username){

			var self = this;

			if(self.jqxhr === null){
			
				var request = {
					type: "HEAD",
					url: self.url+username,
					cache:false,
				}
				self.jqxhr = $.ajax(request)
					.done(function(_, __, xhr){

						if(xhr.getResponseHeader(self.header) === "False"){
							self.addAvailableIcon();
						} else{
							self.addUnAvailableIcon();
						}

					})
					.fail(function(xhr){
						self.removeIcons();
					})
					.always(function(xhr){
						self.jqxhr = null;
				});
			}
		},

		/*
			Called each time the username value is change.
			Abort the current request and return true if a new request should be made and false 
			if not. A request will be made if the field has no errors and is not empty.
			@param field: javascript username field.
		*/
		runRequest: function(field){
			
			this.abort();

			if(field.value === "" || this.username.hasClass(this.error_class)){	
				return false;
			}
			return true;
		},

		/*
			Cancel the current request.
		*/
		abort: function(){
			if(this.jqxhr !== null){
				this.jqxhr.abort();
			}
			this.removeIcons();
		},

		/* END AJAX REQUEST */
		/* ------------------------------------------------------------------------------------ */


		/* ICON/TEXT DISPLAY */
		/* ------------------------------------------------------------------------------------ */
		/*
			Add a tick icon to indicate that the username is available and update the icon_text
			element which is hidden initially and can be toggles by the user.
		*/
		addAvailableIcon: function(){
			this.icon_text.empty().append("username available");
			this.container.addClass("availability").removeClass("cross_icon");
		},
		/*
			Add a cross icon to indicate that the username is unavailable and update the icon_text
			element which is hidden initially and can be toggles by the user.
		*/
		addUnAvailableIcon: function(){
			this.icon_text.empty().append("username unavailable");
			this.container.addClass("availability cross_icon");
		},
		/*
			Remove the current icon and any text.
		*/
		removeIcons: function(){
			this.icon_text.empty().addClass("hide");
			this.container.removeClass("availability cross_icon");
		},
		/*
			Toggle the display of the icon text which informs the user of the availability 
			status of thier chosen username.
		*/
		toggleIconText: function(e){
			e.data.self.icon_text.toggleClass("hide");
		},
		/*
			Return true if the username has been taken.
		*/
		isUsernameUnavailable: function(){
			return this.container.hasClass("cross_icon")
		},

		/* END ICON/TEXT DISPLAY */
		/* ------------------------------------------------------------------------------------ */


		/* EVENT HANDLERS */
		/* ------------------------------------------------------------------------------------ */
		/*
			Run a request to check the username availability once the time has elapsed (used by 
			"input" event).
		*/
		timeout: function(e){

			var self = e.data.self;
			clearTimeout(self.timeout_id);

			if(self.runRequest(this) === true){
				self.timeout_id = setTimeout(
					function(){ self.is_available(e.target.value) }, 1000
				);
			}
		},
		/*
			Run a request to check the username availability immediately (used by "change" event).
		*/
		noTimeout: function(e){
			
			if(e.data.self.runRequest(this) === true){
				e.data.self.is_available(this.value);
			}
		}

		/* END EVENT HANDLERS */
		/* ------------------------------------------------------------------------------------ */
	}


	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */
	$(function(){
		try{
	
			var event_type = (isEventSupported("input") === true) ? "input" : "change";
			new FormValidation("error", event_type);

		} catch(e){
			debug(e);
		}
	});

/* ------------------------------------------------------------------------------------ */
}());

