/* Sign Up Form javascript file */
/* Dependencies: JQuery, Base.js */


/* ------------------------------------------------------------------------------------ */

(function () {
"use strict";

	/*
		**********************************************************************************
		FORM VALIDATION CLASS
		**********************************************************************************

		Manage form validation, displaying error messages to the user after they submit 
		the form.
	*/	
	function LoginForm(){

		Ulo.checkDependencies("Session", "PIP");

		/* REGEX VALIDATORS */
		this.input = {

			"login_email": {
				regexp: [
					/^(?!\s*$)/,
					/^(?=.{3,256}$)[^@\s]+@[^@\s\.]+(\.[^@\s\.]+)+$/
				],
			},
			"login_password": {
				regexp: [
					/^(?!\s*$)/,
					/^.{6,}$/,
					/^.{6,128}$/,
					/^(?![a-zA-Z]+$)/,
					/^(?!\d+$)/,
					/^(?!(.)\1+$|password(?:\d{0,2}|(\d)\2{1,})$)/
				],
			}
		}

		this.jqxhr = null;
		this.auto_submit = true;
		$(getElement("login_form")).on("submit", {self: this}, this.submitForm);

	}
	LoginForm.prototype = {

		constructor: LoginForm,

		/* ---------------------------------------------------------------------------- */
		/*
			Return the default error message.
		*/
		defaultErrMsg: function(){
			return "Login failed. Please try again later."
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Enable the form's submit button.
		*/
		enableSubmit: function(){
			var submit = getElement("login_submit");
			if(submit!==null){ submit.disabled=false; }
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Disable the form's submit button.
		*/
		disableSubmit: function(){
			var submit = getElement("login_submit");
			if(submit!==null){ submit.disabled=true; }
		},
		/* ---------------------------------------------------------------------------- */
		/*
			
		*/
		clearPassword: function(){
			var pw = getElement("login_password");
			if(pw!==null){ pw.value=""; }
		},
		/* ---------------------------------------------------------------------------- */
		/*

		*/
		setRedirectTo: function(form){

			if(Ulo.PIP.isPIP(form)){
				var r = getElement("login_redirect_to");
				if(r!==null){ r.value = Ulo.getURL(); }
			}
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Add an error message to the form. If no error message was provided use
			a fallback message.
			@param error: error message.
		*/
		addFormError: function(error){
			
			error = error || this.defaultErrMsg();

			var container = getElement("login_errors");

			if(container===null){

				var icon = makeElement("span", {"class": "icon icon_error"});
				container = makeElement("div", {"id": "login_errors"});
				appendElements(getElement("login_form_help_text"), [icon, container]);

			}

			emptyElement(container).appendChild(makeElement("p", {"class":"error"}, error));

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Run all regex validators on the form fields and return true if all succeed
			and false if not. Display an error message if it fails.
		*/
		isValid: function(){
			
			for(var id in this.input){

				var value = getElement(id).value;
				var regexp = this.input[id].regexp;

				for(var i in regexp){
					if(regexp[i].test(value) === false){

						var self = this;
						setTimeout(function(){

							self.addFormError("The email address and password do not match.");
							self.enableSubmit();

						}, 500)
						
						return false;
					}
				}
			}

			return true;
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Submit the form and redirect the page if successful or display an error 
			message back to the user.
		*/
		submitForm: function(e){

			var self=e.data.self, form=this;

			e.preventDefault();

			if(self.jqxhr===null && self.isValid()){

				self.setRedirectTo(this);

				self.disableSubmit();

				self.jqxhr = $.ajax({

						type: "POST",
						url: $(this).attr("action"),
						data: $(this).serialize(),
						cache:false,
						statusCode: requestCodes,
					
					})
					
					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/* If the login view returned the html of the new page render it */
						if(data.html!==undefined){

							/* Check that the page class is valid. */
							var isPageValid = Ulo.checkDependencies(true, "Page");

							/* Update the page only if the url can be updated too. */
							if(isPageValid || Ulo.getURL()===data.url){

								Ulo.PageContent.removeFiles(getElement("login"), true);
								
								var main = getMain();
								Ulo.PageContent.change(main, main.id, data.html, xhr, true)

									.done(function(){

										isPageValid && Ulo.Page.updateHistory(data.url);
										Ulo.PIP.modal.close();

									})
									.fail(function(){

										Ulo.replacePage(data.url);

								});

								/* Return to avoid executing Ulo.replacePage */
								return true;

							}

						}
						
						/* If the page cannot be updated then navigate to the new url */
						Ulo.replacePage(data.url);							

					})
					/* Params: xhr, status code, error type */
					.fail(function(xhr){

						try{
							/* 
								Display the error to the user. The log in form will always 
								return non_field error messages with the key "__all__".
							*/
							self.addFormError(xhr.responseJSON.errors.__all__);

						} catch(ex){

							var csrf = Ulo.Session.csrfExpired(xhr, true);

							if(self.auto_submit===true && csrf.expired===true && csrf.isAuth===false){

								Ulo.Session.setCsrfTokens(xhr.getResponseHeader(Ulo.Session.TOKEN_NAME), form);
								xhr.statusCode = function(){}
								self.auto_submit = false;
								self.jqxhr = null;
								$(form).submit();
								
							} else{

								/* If the login redirect produces a 404 remove the form */
								if(xhr.status===404){

									Ulo.PageContent.removeFiles(getElement("login"), true);

								}

								/* Display a generic error message is no error was displayed */
								requestMessages(xhr, self.defaultErrMsg());

							}

						}

						self.clearPassword();

					})

					.always(function(){

						self.jqxhr = null;
						self.enableSubmit();

				});
			}
		}
	}


	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */
	$(function(){
		try{

			new LoginForm();

		} catch(e){

			debug(e);
		
		}
	});

/* ------------------------------------------------------------------------------------ */
}());

