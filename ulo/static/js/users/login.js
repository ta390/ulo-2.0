/* Sign Up Form javascript file */
/* Dependencies: JQuery, Base.js */


/* ------------------------------------------------------------------------------------ */

(function () {
"use strict";

	/*
		**********************************************************************************
		FORM VALIDATION CLASS
		**********************************************************************************

		Manage form validation, displaying error messages to the user after they submit 
		the form.
	*/	
	function LoginForm(){

		Ulo.checkDependencies("Session", "Pip");

		/* Regular expression to check that an input field is not empty */
		this.not_empty = /^(?![\s]*$)/;

		/* REGEX VALIDATORS */
		this.validators = {

			"login_email": {
					
				regexp: [
				
					this.not_empty,
					/^(?=.{3,255}$)[^@\s]+@[^@\s\.]+(\.[^@\s\.]+)+$/
				
				],
			
			},
			
			"login_password": {
				
				regexp: [
				
					this.not_empty,
					/^.{6,}$/,
					/^.{0,128}$/,
					/^(?![a-zA-Z]+$)/,
					/^(?!\d+$)/,
					/^(?!(.)\1+$|password(?:\d{0,2}|(\d)\2{1,})$)/
				
				],
			
			},
				
		}

		this.jqxhr = null;

		$(getElement("login_form")).on("submit", {self: this}, this.submit);

	}
	LoginForm.prototype = {

		constructor: LoginForm,

		/* ---------------------------------------------------------------------------- */
		/*
			Return the default error message.
		*/
		defaultErrMsg: function(){
		
			return "Login failed. Please try again later."
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Enable the form's submit button.
		*/
		setSubmitDisabled: function(disable){
		
			var submit = getElement("login_submit");
			
			if(submit!==null){

				submit.disabled = disable;

				(disable ? addClass : removeClass)(submit, "disabled");

			}
		
		},

		/* ---------------------------------------------------------------------------- */
		/*
			
		*/
		clearPassword: function(){

			var password = getElement("login_password");

			if(password!==null){

				password.value=""; 

			}
		
		},
		/* ---------------------------------------------------------------------------- */
		/*

		*/
		setPipRedirect: function(form){

			if(Ulo.Pip.isPip(form)){

				var redirect = getElement("login_redirect_to");

				if(redirect!==null){

					redirect.value = Ulo.getURL();

				}
			
			}
		
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return the field's error container.

			@param name: field name.
		*/
		getField: function(name){

			return getElement( "login_" + name + "_container" );

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return the form's error container.

			@param form: form.
			@param create: boolean - if true create the element if it does not exist and
				prepend it to the form.
		*/
		getNonField: function(form){

			var id = "login_form_errors",
			container = getElement(id);

			if(container===null){

				container = makeElement("div", {"id": id, "class": "form_errors"});

				form.insertBefore(container, form.firstChild);

			}

			return container;
		
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Remove all messages.

			@param context: option context to narrow the scope.
		*/
		removeText: function(context){

			$("p.validation", context).remove();

		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Add all messages.

			@param container: container to add messages to.
			@param messages: string or array of strings.
			@param cls: optional class set on the <p> element - defaults to "error".
		*/
		addText: function(container, messages, cls){

			if(container !== null){

				if( Array.isArray(messages)===false ){

					messages = [messages];

				}

				for(var i=0; i<messages.length; ++i){

					if(messages[i]){

						container.insertBefore( 

							makeElement("p", {

								"class": "validation " + (cls===undefined ? "error" : cls)

							}, messages[i]),

							container.firstChild

						);

					}

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Add all server error messages.

			@param form: form.
			@param xhr: XMLHttpRequest.
		*/
		addServerErrors: function(form, xhr){
			
			this.removeText(form);

			if( xhr.responseJSON!==undefined && xhr.responseJSON.errors!==undefined ){

				for(var n in xhr.responseJSON.errors){

					this.addText(
						
						n==="__all__" ? this.getNonField(form) : this.getField(n),
						xhr.responseJSON.errors[n]

					);

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Run all regex validators on the form fields and return true if all succeed
			and false if not. Display an error message if it fails.
		*/
		isValid: function(form){

			for(var id in this.validators){

				var value = getElement(id).value;
				
				var regexp = this.validators[id].regexp;

				for(var i in regexp){

					if(regexp[i].test(value) === false){

						var self = this;

						setTimeout(function(){
							
							self.removeText(form);

							self.addText(

								self.getNonField(form),
								"The email address and password do not match."

							);
							
							self.setSubmitDisabled(false);

						}, 500)
						
						return false;

					}
				}
			}

			return true;
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Submit the form and redirect the page if successful or display an error 
			message back to the user.
		*/
		submit: function(e){

			e.preventDefault();

			var self=e.data.self, form=this;

			if(self.jqxhr===null){


				self.setSubmitDisabled(true);


				if(self.isValid(form)){

				

					self.setPipRedirect(this);

					self.jqxhr = $.ajax({

							type: "POST",
							url: this.getAttribute("action"),
							data: $(this).serialize(),
							cache:false,
							statusCode: requestCodes,
						
						})
						
						/* Params: server data, status code, xhr */
						.done(function(data, sc, xhr){

							if(data.html && Ulo.Pip.isPip(form) && Ulo.checkDependencies(true, "Page")){

								Ulo.PageContent.removeFiles(getElement("login"));
									
								var main = getMain();
								
								Ulo.PageContent.change(main, xhr, true)

									.done(function(){

										Ulo.Page.updateHistory(data.url);

										Ulo.Pip.close();

									})

									.fail(function(){

										Ulo.replacePage(data.url);

								});

							} else{

								Ulo.replacePage(data.url);

							}					

						})

						/* Params: xhr, status code, error type */
						.fail(function(xhr){

							/*
								If the login redirect produces a 404 refresh the page as 
								the user will have been logged in.
							*/
							if(Ulo.Pip.isPip(form) && xhr.status===404){

								/* Suppress the 404 error message */
								xhr.statusCode = function(){}

								/* Refresh the page */
								Ulo.replacePage();

							} else{

								self.clearPassword();

								self.addServerErrors(form, xhr);

								self.setSubmitDisabled( Ulo.Session.csrfExpired(xhr) );

							}
							
						})

						.always(function(){

							self.jqxhr = null;

					});

				}
			}
		}
	}


	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */
	$(function(){
		try{

			new LoginForm();

		} catch(e){

			debug(e);
		
		}
	});

/* ------------------------------------------------------------------------------------ */
}());

