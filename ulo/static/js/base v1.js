/* Site wide javascript file */

/* Dependencies: JQuery */


"use strict";


/* GLOBAL ERROR HANDLER */
/* ------------------------------------------------------------------------------------ */

window.onerror = function(message, source, lineno, colno, error){
	console.log(message, source, lineno, error);
	return false;
}

/* ------------------------------------------------------------------------------------ */




/* POLYFILLERS */
/* ------------------------------------------------------------------------------------ */

/* Add the bind method if not supported */
if (!Function.prototype.bind){

	Function.prototype.bind = function(oThis) {
		
		if(typeof this !== 'function'){
			throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
		}

		var a = Array.prototype.slice.call(arguments, 1),
		FtoB = this,
		FN = function() {},
		FB = function() {
			return FtoB.apply(this instanceof FN
				? this
				: oThis,
				a.concat(Array.prototype.slice.call(arguments)));
		};

		if (this.prototype) {
			// Function.prototype doesn't have a prototype property
			FN.prototype = this.prototype; 
		}
		FB.prototype = new FN();

		return FB;
	};

}

/* ------------------------------------------------------------------------------------ */

/* Bind arguments to the class constructor */
Function.prototype.bindCls = function(){

	/* Remove the required arguments from the list */
	var args = Array.prototype.slice.call(arguments, 3);

	/* Bind the arguments */
	function F(){
		return this.constructor.apply(this,
			args.concat(Array.prototype.slice.call(arguments)));
	}

	F.prototype = this.prototype;

	return F;
}

/* END POLYFILLERS */
/* ------------------------------------------------------------------------------------ */




/* GLOBAL VARIABLES */
/* ------------------------------------------------------------------------------------ */

/* Unicode whitespace regexp */
var regexp_unicode_ws = /^[\u0009\u0020\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u200B\u200C\u200D\u202F\u205F\u2060\u3000\uFEFF]*$/

/* END GLOBAL VARIABLES */
/* ------------------------------------------------------------------------------------ */




/* GLOBAL CLASSES */
/* ------------------------------------------------------------------------------------ */

window.Ulo = {

	/* Current ajax request - If null the function is free to perform its request */
	jqxhr: null,

	TEMP: {},

	media: {

		VIDEO: 2,
		IMAGE: 1
	},

	evts: {

		click: "click"
	
	},

	ids: {
	
		main: "main_content"
	
	},

	cls: {
	
		hide: "hide",
		show: "show",
		hidden: "hidden",
		disabled: "disabled",
		modal: "modal",
		modal_open: "modal_open"
	
	},

	MEDIA_URL: "/media/",

	/* -------------------------------------------------------------------------------- */
	
	getURL: function(){
	
		return location.pathname+location.search;
	
	},
	
	/* -------------------------------------------------------------------------------- */
	
	replacePage: function(url){
	
		window.location.replace(url || location.pathname+location.search);
	
	},
	
	/* -------------------------------------------------------------------------------- */


	/* CLASSES */
	/* -------------------------------------------------------------------------------- */
	/*
		Create an instance of the class and store a global reference to it
		
		@param Cls: Class
		@param name: Fallback class name if Class.name is not a property.
		@param tmp: Boolean - if true remove the class when the page unloads.
		
		@params 3...N: arguments passed to the class constructor.
	*/
	newClass: function(Cls, name, tmp){

		try{

			if(Cls.name===undefined){

				Cls.name=name;

			}

			var obj = (tmp===true) ? this.TEMP : this;

			obj[Cls.name] = (arguments.length > 3) ? 
				new (Cls.constructor.bindCls.apply(Cls, arguments)) : new Cls();

		} catch(e){

			debug(e);

		}

	},
	
	/* -------------------------------------------------------------------------------- */
	/*
		Store a global reference to a class.
		@param Cls: class
		@param name: class name
		@param tmp: boolean - if true remove the reference when the page unloads.
	*/
	refClass: function(Cls, name, tmp){

		if(Cls.name===undefined){

			Cls.name=name;

		}
		
		var obj = (tmp===true) ? this.TEMP : this;
		
		obj[Cls.name] = Cls;

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove a reference to a class.
		@param Cls: class
	*/	
	delClass: function(Cls){

		delete this[Cls.name];

	},
	/* -------------------------------------------------------------------------------- */	
	/*
		Check that the class names passed as arguments are referenced in the Global 
		(window) or temporary (this.TEMP) namespace.

		@param fail_silently: optional boolean to prevent an exception from being thrown.
		@params 0..N: class names (e.g. "Page", "Menu");.
	*/
	checkTempDependencies: function(fail_silently){

		var i = typeof arguments[0]==="boolean" ? 1 : 0;

		for(; i<arguments.length; ++i){

			if(window[arguments[i]]===undefined && this.TEMP[arguments[i]]===undefined){
	
				if(arguments[0]===true){

					return false;

				}
				
				throw new Error("Global dependency missing: " + arguments[i]);

			}

		}

		return true;

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Check that the class names passed as arguments (args) are referenced in the Ulo
		namespace. Thrown an exception or return false if the class has not been defined
		and return true otherwise.

		@param type: typeof value to check the class name against.
		@param name: name of the check performed ("dependency" or "reference").
		@param args: array of class names. A boolean value can be passed as the first
			argument - if true the function will return false instead of throwing an
			exception.
	*/
	_checkUloCls: function(type, name, args){

		var i = typeof args[0]==="boolean" ? 1 : 0;

		for(; i<args.length; ++i){

			if(this[args[i]]===undefined || typeof this[args[i]]!==type){

				if(args[0]===true){

					return false;

				}
				
				throw new Error("Missing "+name+": "+args[i]);

			}

		}

		return true;

	},
	/* -------------------------------------------------------------------------------- */	
	/*
		Check that all classes have instances defined in the Ulo namespace.
		
		@param fail_silently: optional boolean to prevent an exception from being thrown.
		@params 0..N: class names (e.g. "Page", "Menu");
	*/
	checkDependencies: function(fail_silently){
	
		return this._checkUloCls("object", "dependency", arguments);
	
	},
	
	/* -------------------------------------------------------------------------------- */
	/*
		Check that all classes have references defined in the Ulo namespace.
		@param fail_silently: optional boolean to prevent an exception from being thrown.
		@params 0..N: class names (e.g. "Page", "Menu");
	*/
	checkReferences: function(fail_silently){
	
		return this._checkUloCls("function", "reference", arguments);
	
	}
	
	/* END CLASSES */
	/* -------------------------------------------------------------------------------- */

}

/* END GLOBAL CLASSES */
/* -------------------------------------------------------------------------------- */




/* DEBUG */
/* ------------------------------------------------------------------------------------ */

function debug(e){
	
	e = "DEBUG: "+e

	console.log(e);
	// alert(e);

}

/* END DEBUG */
/* ------------------------------------------------------------------------------------ */




/* JAVASCRIPT HELPER FUNCTIONS */
/* ------------------------------------------------------------------------------------ */
/*
	Wrapper for document.getElementById
*/
function getElement(id){

	return document.getElementById(id);

}

/* ------------------------------------------------------------------------------------ */
/*
	Return <main>
*/
function getMain(){

	return document.getElementById(Ulo.ids.main);

}

/* ------------------------------------------------------------------------------------ */
/*
	Return the html as a document fragment.

	@param html: String of html.
*/
function createDocFrag(html){

	var d = document.createDocumentFragment();
	
	d.appendChild(document.createElement("div")).innerHTML=html;
	
	return d;

}

/* ------------------------------------------------------------------------------------ */
/*
	Return an element contained in a document fragment. IE does not recognise
	getElementByID consistently.

	@param frag: Document fragment.
	@param id: Element id.
*/
function getFragElement(frag, id){

	if(frag.getElementById===undefined){
	
		return frag.querySelector("#"+id);
	
	}
	
	return frag.getElementById(id);

}

/* ------------------------------------------------------------------------------------ */
/*
	Create and return a new DOM element.

	@param tag: Element tag name.
	@param attrs: Object of attributes {'name':value, 'name':value}
	@param text: Optional text to add to the element.
*/
function makeElement(tag, attrs, text){

	var e = document.createElement(tag);
	
	for(var n in attrs){ e.setAttribute(n, attrs[n]); }
	
	text!==undefined && e.appendChild(document.createTextNode(text));
	
	return e;

}

/* ------------------------------------------------------------------------------------ */
/*
	Return a regexp to find a class name in a string.

	@param class_name: Name of the class.
*/
function classRegExp(class_name){

	return new RegExp("(^|\\s+)"+class_name+"(\\s+|$)");

}

/* ------------------------------------------------------------------------------------ */
/*
	Return true if a javascript element has the class "class_name" and false 
	otherwise.

	@param element: Javascript element.
	@param class_name: Name of the class.
*/
function hasClass(element, class_name){

	return classRegExp(class_name).test(element.className);

}

/* ------------------------------------------------------------------------------------ */
/*
	Add a class to a javascript element.

	@param element: Javascript element.
	@param class_name: Name of the class.
*/
function _addClass(element, class_name){

	element.className += (element.className==="" ? "" : " ") + class_name;

}

/* ------------------------------------------------------------------------------------ */
/*
	Add a class to a javascript element if it does not exist. Return the original 
	element.
	
	@param element: Javascript element.
	@param class_name: Name of the class.
*/
function addClass(element, class_name){
	
	try{
	
		if(classRegExp(class_name).test(element.className) === false){
	
			_addClass(element, class_name);
	
		}
	
	}catch(e){}
	
	return element;

}

/* ------------------------------------------------------------------------------------ */
/*
	Remove a class from a javascript element. Return the original element.
	
	@param element: Javascript element.
	@param class_name: Name of the class.
	@param replace: Replace "class_name" with the value "replace".
*/
function removeClass(element, class_name, replace){
	
	try{
	
		replace = (replace === undefined) ? "$2" : "$1"+replace+"$2";
	
		element.className = element.className.replace
			(classRegExp(class_name), replace).replace(/^\s/, "");
	
	} catch(e){}

	return element;

}

/* ------------------------------------------------------------------------------------ */
/*
	Toggle the class on a javascript element. Return true if the class was added and false 
	if it was removed.
	
	@param element: Javascript element.
	@param class_name: Name of the class.
*/
function toggleClass(element, class_name){
	
	if(classRegExp(class_name).test(element.className)){
	
		removeClass(element, class_name);
		return false;
	
	} else{
	
		_addClass(element, class_name);
		return true;
	
	}

}

/* ------------------------------------------------------------------------------------ */
/*
	Replace one javascript element with another. Return the new element.

	@param new_elem: Element to replace "old_elem".
	@param old_elem: Element to be replaced by "new_elem".
*/
function replaceWith(new_elem, old_elem){

	old_elem.parentNode.replaceChild(new_elem, old_elem);
	return new_elem;

}

/* ------------------------------------------------------------------------------------ */
/*
	Remove all child nodes of an element.

	@param element: Javascript element.
*/
function emptyElement(element){

	while(element.firstChild){

		element.removeChild(element.firstChild);

	}

	return element;

}

/* ------------------------------------------------------------------------------------ */
/*
	Remove all javascript elements passed to the function.

	@param *: DOM elements.
*/
function removeElements(){
	
	for(var i=0; i<arguments.length; i++){
	
		if(arguments[i]){
	
			arguments[i].parentNode.removeChild(arguments[i]);
	
		}
	
	}

}

/* ------------------------------------------------------------------------------------ */
/*
	Append all elements in the array "array" to "element".

	@param element: Javascript element.
	@param array: Array of elements.
	@param clone: If true shallow clone the element.
*/
function appendElements(element, array, clone){

	var length = array.length;

	for(var i=0; i<array.length; i++){

		try{
		
			element.appendChild( (clone===true) ? array[i].cloneNode(false) : array[i]);
		
		} catch(e){}

		if(length !== array.length){
		
			length = array.length;
		
			--i;
		
		}

	}
}

/* ------------------------------------------------------------------------------------ */
/*
	Return true if "child" is a decendant of "parent" and false otherwise.

	@param parent: Parent element that contains child.
	@param child: Element to find.
	@param levels: Max number of predecessors.
*/
function isDescendant(parent, child, levels){
	
	var node = child.parentNode;
	
	for(; node !== null && levels > 0; --levels){
		
		if(node===parent){
		
			return true;
		
		}
		
		node = node.parentNode;
	
	}
	
	return false;

}

/* ------------------------------------------------------------------------------------ */
/* 
	Return a class instance that inherits "prototype".

	@param prototype: Object.
*/
function inherit(prototype){

	/* Dummy constructor function */
	function f(){}

	/* Assign to it the prototype to inherit */
	f.prototype = prototype;

	/* Return 'subclass' */
	return new f();

}

/* ------------------------------------------------------------------------------------ */
/*
	Return true if the browser is an IE browser lte "no".

	@param no: IE version number 8, 9, or 10
*/
function isIeLte(no){

	if(no === 8){
	
		return (document.all && !document.addEventListener);
	
	} else if (no === 9){
	
		return (document.all && !window.atob);
	
	} else if(no === 10){
	
		return !!(document.all)
	
	}
	
	throw new "IE Version Support only tests for 8, 9, and 10";
}

/* ------------------------------------------------------------------------------------ */
/*
	Return true if the browser supports a particular feature, else return false.

	@param attr: Name of the attribute to test for (e.g. mulitple)
	@param element: Element or Element tag name.
*/
function isAttributeSupported(attr, element){

	if(element.nodeType===undefined){

		element = document.createElement(element); 

	}

	return element[attr] !== undefined;

}

/* ------------------------------------------------------------------------------------ */
/*
	Return true if the event is supports.

	@param name: Event name.
	@param element: Optional element to test the event against.

	http://perfectionkills.com/detecting-event-support-without-browser-sniffing/
*/
var isEventSupported = (function(){
	
	var TAGNAMES = {
		"select":"input",
		"change":"input",
		"input": "input",
		"submit":"form",
		"reset":"form",
		"error":"img",
		"load":"img",
		"abort":"img"
	}
	
	function isEventSupported(name, element) {
		
		if(element===undefined || element.nodeType===undefined){

			element = document.createElement(element || TAGNAMES[name] || "div");
		
		}
		
		name = "on" + name;

		var isSupported = (name in element);
		
		if (!isSupported) {
		
			element.setAttribute(name, "return;");
		
			isSupported = typeof element[name] == "function";
		
		}
		
		element = null;

		return isSupported;
	
	}
	
	return isEventSupported;

})();

/* ------------------------------------------------------------------------------------ */
/* 
	Functions to round the value n to the nearest m
*/
function roundTo(n, m){

	/* Round to the nearest m */
	return Math.round(n/m)*m;

}

function roundUpTo(n, m){

	/* Round up to the nearest m */
	return Math.ceil(n/m)*m;

}

function roundDownTo(n, m){

	/* Round down to the nearest m */
	return Math.floor(n/m)*m;

}

/* ------------------------------------------------------------------------------------ */
/* 
	(5-Feb-16)
	https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/encodeURI.
	
*/
function fixedEncodeURI(str){

    return encodeURI(str).replace(/%5B/g, '[').replace(/%5D/g, ']');

}

/* ------------------------------------------------------------------------------------ */
/*
	(5-Feb-16)
	https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/
		encodeURIComponent.
*/
function fixedEncodeURIComponent(str){

	return encodeURIComponent(str).replace(/[!'()*]/g, function(c){

		return '%' + c.charCodeAt(0).toString(16);

	});

}

/* ------------------------------------------------------------------------------------ */

function xhrErrorMessage(xhr, msg){

	if(xhr.responseJSON!==undefined && xhr.responseJSON.messages!==undefined &&
		xhr.responseJSON.messages.length!==0){

		msg = xhr.responseJSON.messages;

	}

	messages(msg);

}

/* ------------------------------------------------------------------------------------ */
/* 
	Display flash message for the error code on a failed ajax request.
*/
var requestCodes = {

	500: function(xhr){ 

		xhrErrorMessage(xhr, "Sorry, the server did not send anything back.");

	},
	
	403: function(xhr){

		xhrErrorMessage(xhr, "Sorry, your request has been denied.");

	},
	
	404: function(xhr){

		xhrErrorMessage(xhr, "Sorry, we could not find the page you were looking for.");

	}

}

/* ------------------------------------------------------------------------------------ */
/*
	Global function used to access the addMessage function. The function returns true if 
	addMessage() was executed successfully and false if not.
*/
function messages(m, p, a, t){

	if(Ulo.Message!==undefined && m!==undefined){
		
		if(m.constructor === String){
		
			m = [{message: m}];
		
		}
		
		return Ulo.Message.addMessages(m, p, a, t);
	}

	return false;
}

/* ------------------------------------------------------------------------------------ */
/*
	Wrapper for the messages function that only displays a message if the request has
	not been given a default message.
*/
function requestMessages(xhr, m, p, a, t){
	
	if(requestCodes[xhr.status]===undefined){

		return messages(m||"Something has gone wrong. Please try again later.",p,a,t);
	
	}
	
	return false;
}

/* ------------------------------------------------------------------------------------ */
/*
	Global function used to add or remove events for the Menu class. The function returns 
	true if the method was executed successfully and false if not.
*/
function menus(b, d, c, cb, p){
	
	if(Ulo.Menu !== undefined){

		if(arguments.length > 1){
			
			return Ulo.Menu.add.apply(Ulo.Menu, arguments);

		} else{
			
			return Ulo.Menu.remove.apply(Ulo.Menu, arguments);
		
		}
	}

	return false;
}

/* ------------------------------------------------------------------------------------ */

function registerLinks(context){

	Ulo.Page!==undefined && Ulo.Page.register(context);

}

/* ------------------------------------------------------------------------------------ */
/*
	Global function used to access the registerElement function of the Page class. The 
	function returns the element passed in as an argument.
*/
function registerPageLink(element){

	if(Ulo.Page!==undefined){

		return Ulo.Page.registerElement(element);

	}

	return element;

}

/* ------------------------------------------------------------------------------------ */
/*
	Global function used to access the updateHistory function of the Page class. The 
	function returns true if the method was executed successfully and false if not.
*/
function updateURL(url, push_state){

	if(Ulo.Page!==undefined){
	
		Ulo.Page.updateHistory(url, push_state)
	
		return true;
	
	}
	
	return false

}

/* ------------------------------------------------------------------------------------ */

function getPage(url, push_state, done, fail){

	return Ulo.Page.getPage(url, push_state, done, fail)

}

/* ------------------------------------------------------------------------------------ */
/*
	Global function to check that the Page class is valid.
*/
function isPageValid(){

	return Ulo.Page!==undefined;

}




/* ------------------------------------------------------------------------------------ */

(function () {

	/* MESSAGE */
	/* -------------------------------------------------------------------------------- */
	/*
		Disaply a message at the top of the page.
	*/
	function Message(){

		var button = getElement("messages").appendChild(

			makeElement("button", {
			
				"id": "close_messages",
				"type": "button"

			})

		);

		button.appendChild( makeElement("span", {"class": "icon icon_cross"}) );

		$(button).on(Ulo.evts.click, {self: this}, this.closeHandler);
		removeClass(this.getContainer(), "block");
		
	}
	Message.prototype = {

		constructor: Message,

		/* ---------------------------------------------------------------------------- */
		/*
			Return the outermost container.
		*/
		getContainer: function(){
			return getElement("messages_container");
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the messages container.
		*/
		getBox: function(){
			return getElement("message_box");
		},
		/*
			Hide the messages container and clear the interval.
		*/
		close: function(){
			clearInterval(this.interval);
			emptyElement(this.getBox());
			removeClass(this.getContainer(), Ulo.cls.show);
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Event handler for close().
		*/
		closeHandler: function(e){
			e.data.self.close();
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Interval to clear the messages after "time"
			@param time: optional time in milliseconds.
		*/
		timeout: function(time){
			var self = this;
			clearInterval(this.interval);
			this.interval = setInterval(function(){ self.close(); }, time || 3000);
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Append messages to the message box. Return true if the function executed
			successfully and false if not.

			@param messages: array of objects containing the properties 'message' and the
				option property 'tags'. 'message' is the text to display and 'tags' is a
				string of class names to give the message.
			@param persist: optional boolean - if true the message will not be cleared are 
				'time' else it will.
			@param append: optional boolean - if true the new messages will be appended to
				the current contents of the message box.
			@param time: optional time - the message will be removed after 'time' 
				milliseconds, the default value is 3000

		*/
		addMessages: function(messages, persist, append, time){

			try{

				if(messages.length!==0){

					var msg_box = this.getBox();

					/* Clear the current contents if append is not true */
					if(append !== true){

						emptyElement(msg_box);
					
					}

					/* Append each message to the message box */
					for(var i in messages){

						var p = document.createElement("p");
						
						p.appendChild( document.createTextNode(messages[i].message || "") );

						if(messages[i].tags){ 
							p.setAttribute("class", messages[i].tags);
						}

						msg_box.appendChild(p);

					}

					/* Set an interval to remove the messages if persist is not true */
					if(persist !== true){

						this.timeout();
						
					}

					/* Display the messages container */
					addClass(this.getContainer(), Ulo.cls.show);
				}

				return true;

			} catch(e){

				debug(e);
				return false;
			}

		},

	}

	/************************************************************************************/

	Ulo.newClass(Message, "Message");

	/************************************************************************************/

	/* END MESSAGE */
	/* -------------------------------------------------------------------------------- */




	/* MENU */
	/* -------------------------------------------------------------------------------- */
	/*
		Manage the opening and closing of menus. The class registers a button that toggles
		the display of a menu. The menu is displayed by changing its width or height value
		and therefore must have a css value of overflow: hidden. Attributes set on the button 
		control its behaviour.

		Attributes (set on the button):
			
			data-toggle: id of the menu to toggle.
			
			data-area: 
				
				"local" - toggle the menu using the dedicated button only.
				
				"global" - show the menu when the button is clicked and close the menu when
					a click occurs outside the menu.
				
				no value - show the menu when the button is clicked and close the menu with 
					the next click.

		Attributes (set on children of the button):

			data-ignore:

				"true" - ignore the click event on this child element. This will not ignore
				click events events bound to this element directly (i.e. the currentElement).

	*/
	function Menu(){

		/* Initialise list to keep track of open menus */
		document.menus = [];

		/* Capitalised dimension names, attributes and class names */
		this.data = {

			width: "Width",
			height: "Height",
			area: "data-area",
			ignore: "data-ignore",
			toggle: "data-toggle",
			btn_class: "menu_open",

		};

	}

	Menu.prototype = {

		constructor: Menu,

		stopProp: function(){

			if(document.menus.length!==0 && document.menus[0]===null){

				document.menus.shift();

			}

		},

		/*
			Register click events on the button to toggle the menu.

			@param btn: btn element or id.
			@param dimmension: dimension to control "width" or "height".
			@param menu_class: (optional) class name to set on the menu when displayed.
			@param callback: (optional) function to call each time the menu is open or
				closed. The function is passed the following parameters.
				@param closed: true if the menu is closed and false if not.
				@param button: button element.
				@param menu: menu element.
				@param target: the current target element.
			@param preventDefault: (optional) boolean = true if the event handler should 
				call preventDefault().
		*/
		add: function(btn, dimension, menu_class, callback, preventDefault){

			if(btn){

				var data = {

					self: this,
					dimension: dimension,
					menu_class: menu_class,
					callback: callback,
					preventDefault: preventDefault || false

				}

				$((btn.nodeType === undefined) ? getElement(btn) : btn)
					.off(Ulo.evts.click, this.toggle).on(Ulo.evts.click, data, this.toggle);
			
			}
		}, 
		/*
			Unregister click events on the button to toggle the menu.
			@param btn: btn element or id.
		*/
		remove: function(btn){
			
			if(btn){
			
				$((btn.nodeType === undefined) ? getElement(btn) : btn)
					.off(Ulo.evts.click, this.toggle);

			}
		},
		/*

		*/
		queueData: function(data, class_only){
			
			/* if this is the first menu opened then add the document click event */
			if(document.menus.length === 0){
				/* 
					intialise the first element to null so that it can be ignored 
					when this call is propagated.
				*/
				document.menus = [null];
				
				$(document).on(Ulo.evts.click, this.close);

			}

			data.class_only = class_only;

			/* add the data object to the list */
			document.menus.push(data);


		},
		/*
			Toggle the display of the menu.

			@param value: value to change the dimension ("width" or "height") to.
		*/
		setDimension: function(data, value, target){

			/* Toggle the class on the menu if one was provided */
			if(data.menu_class){

				toggleClass(data.menu, data.menu_class);

			}

			/* Set the dimension relying on css transitions for animation */
			if(data.class_only!==true){

				data.menu.style[data.dimension] = value+"px";

			}

			/* Call the callback function if one was provided */
			if(data.callback){
				
				data.callback(value===0, data.button, data.menu, target);
			
			}

		},
		/*
			Document level event handler used to close open menus.
		*/
		close: function(e){

			/* get the first menu in the array */
			var data = document.menus.shift();

			if(data){

				var self = data.self;

				/* 
					if the value of the attribute data-area is "global" and the target is 
					the menu itself or one of its children then ignore the event.
				*/
				if(data.area === "global" && 
					(data.menu === e.target || isDescendant(data.menu, e.target, 3))){

					/* push the menu onto the front of the array */
					document.menus.unshift(data);

				/*
					else remove the "menu_open" class from the button and close the menu
				*/
				} else{

					removeClass(data.button, self.data.btn_class);

					if(document.menus.length === 0){
						
						$(document).off(Ulo.evts.click, self.close);
					
					}

					self.setDimension(data, 0, e.target);

				}
			}
		},
		/*
			Button event handler used to toggle the menu when clicked.
		*/
		toggle: function(e){

			var self = e.data.self;
			var info = self.data;

			if(e.data.preventDefault===true){
			
				e.preventDefault();
			
			}

			/* 
				ignore click events on child elements that have set the attribute 
				"data-ignore" to true.
			*/
			if(e.target.getAttribute(info.ignore)==="true" && e.currentTarget!==e.target){

				return true;

			}

			/* get the element to toggle */
			var menu = getElement(e.currentTarget.getAttribute(info.toggle));

			if(menu!==null){

				/* read menu's current width or height value */
				var value = menu["client"+info[e.data.dimension]];

				/* toggle its current value */
				if(value === 0){

					value = menu["scroll"+info[e.data.dimension]];
					
					/*
						If element.scrollWidth/Height is undefined show the menu
						to read its width or height
					*/
					if(value===undefined){
						
						var style = menu.style[e.data.dimension];
						menu.style[e.data.dimension] = "auto";
						value =  menu["client"+info[e.data.dimension]];
						menu.style[e.data.dimension] = style;

					}

					addClass(e.currentTarget, info.btn_class);
				
				} else{

					value = 0;
					removeClass(e.currentTarget, info.btn_class);
				
				}				

				/* get the value of the attribute data-area */
				var area = e.currentTarget.getAttribute(info.area);

				/* add extra information to the data object */
				e.data.area = area;
				e.data.menu = menu;
				e.data.button = e.currentTarget;

				/* if "local" then toggle the menu using the dedicated button only */
				if(area === "local"){

					self.setDimension(e.data, value, e.target);
				
				/* else if the menu has been opened set a document event to close it */
				} else if(value !== 0){

					self.queueData(e.data, false);
					
					self.setDimension(e.data, value, e.target);

				}
			}
		}
	}

	/************************************************************************************/

	Ulo.newClass(Menu, "Menu");

	/************************************************************************************/

	/* END MENU */
	/* -------------------------------------------------------------------------------- */




	/* SESSION */
	/* -------------------------------------------------------------------------------- */

	function Session(){

		this.CSRF_TOKEN_NAME = "csrfmiddlewaretoken";
		
		this.TOKEN_NAME = "token-id";
		
		this.AUTH_NAME = "auth-id";
		
		this.nav_id = "nav_ul";

	}
	Session.prototype = {

		constructor: Session,

		/* ---------------------------------------------------------------------------- */

		isOwner: function(id){

			var id_ = getMain().getAttribute("data-"+this.AUTH_NAME);
			
			return id_!=="" && id_!==null && id_==id;

		},

		/* ---------------------------------------------------------------------------- */

		isAuthenticated: function(id){

			if(id===undefined){ id=getMain().getAttribute("data-"+this.AUTH_NAME); }
			
			return id!=="" && id!==null;

		},

		/* ---------------------------------------------------------------------------- */

		getAuthHeader: function(xhr){

			var a = xhr.getResponseHeader(this.AUTH_NAME);
			
			return (a==="None") ? null : a;

		},

		/* ---------------------------------------------------------------------------- */

		hasChanged: function(xhr, main){

			var data = this.get(main);

			return data[this.AUTH_NAME] != this.getAuthHeader(xhr) ||
				data[this.TOKEN_NAME] !== xhr.getResponseHeader(this.TOKEN_NAME);
				

		},

		/* ---------------------------------------------------------------------------- */

		setCsrfTokens: function(token, context){

			if(token){

				var tokens = $("input[name='"+this.CSRF_TOKEN_NAME+"']", context);

				for(var i=0; i<tokens.length; ++i){

					tokens[i].value = token;

				}

			}
			
		},

		/* ---------------------------------------------------------------------------- */
		/*
			@param xhr: XMLHttpRequest.
		*/
		csrfExpired: function(xhr, verbose){

			var d = xhr.responseJSON!==undefined && xhr.responseJSON.csrf_error===true;

			if(verbose===true){

				d = {expired: d};

				var id = this.getAuthHeader(xhr);
				
				d.isAuth = 	this.isAuthenticated(id);
				
				d.isOwner = this.isOwner(id)				

			}

			return d;

		},

		/* ---------------------------------------------------------------------------- */

		set: function(xhr){

			var auth = this.getAuthHeader(xhr);
			var token = xhr.getResponseHeader(this.TOKEN_NAME);

			var m = getMain(), d={};

			if(auth===null){

				m.removeAttribute("data-"+this.AUTH_NAME);
			
			} else{
			
				m.setAttribute("data-"+this.AUTH_NAME, auth);
			
			}

			m.setAttribute("data-"+this.TOKEN_NAME, token);

		},

		/* ---------------------------------------------------------------------------- */

		get: function(xhrOrMain){
		
			var d={};

			if(xhrOrMain===undefined){

				xhrOrMain = getMain();

			}

			if(xhrOrMain.getResponseHeader===undefined){

				d[this.AUTH_NAME] = xhrOrMain.getAttribute("data-"+this.AUTH_NAME) || null;
				d[this.TOKEN_NAME] = xhrOrMain.getAttribute("data-"+this.TOKEN_NAME) || null;

			} else{

				d[this.AUTH_NAME] = this.getAuthHeader(xhrOrMain);
				d[this.TOKEN_NAME] = xhrOrMain.getResponseHeader(this.TOKEN_NAME);

			}
			
			return d;

		},

		/* ---------------------------------------------------------------------------- */

		update: function(xhr, main, fragment){

			if(this.hasChanged(xhr, main)){

				console.log("SESSION CHANGED!");

				this.setCsrfTokens(xhr.getResponseHeader(this.TOKEN_NAME));

				var nav = getFragElement(fragment, this.nav_id);

				if(nav!==null){

					replaceWith(nav, getElement(this.nav_id));
					
					registerLinks(nav);

				}

			}

			Ulo.Session.set(xhr);
		},

	}

	/************************************************************************************/

	Ulo.newClass(Session, "Session");
	
	/************************************************************************************/

	/* END SESSION */
	/* -------------------------------------------------------------------------------- */




	/* Pip */
	/* -------------------------------------------------------------------------------- */
	
	function Pip(){

		this.elements = [];

		this.callbacks = [];

		this.jqxhr = null;

	}

	Pip.prototype = {

		constructor: Pip,

		/* ---------------------------------------------------------------------------- */

		isPip: function(context){

			var pip = $("input[name='pip']", context)[0];

			return pip!==undefined && pip.value==="true";
		
		},

		/* ---------------------------------------------------------------------------- */

		setPip: function(context){

			var pips = $("input[name='pip']", context);

			for(var i=0; i<pips.length; ++i){

				pips[i].value=true;
			
			}
	
		},

		/* ---------------------------------------------------------------------------- */

		updatePage: function(xhr, data, page_id, removeOnClose, callback){
			
			if(Ulo.checkDependencies(true, "Page")){


				Ulo.Page.preventUnload();

				/* 
					Refresh the current page before displaying the requested page in the 
					modal. 
				*/
				Ulo.Page.getPage(
					
					Ulo.getURL(), 
					false,
					this.displayPage.bind(this, xhr, data, page_id, removeOnClose, callback)
				
				);
			
			} else{

				Ulo.replacePage();

			}

		},

		/* ---------------------------------------------------------------------------- */

		displayPage: function(xhr, data, page_id, removeOnClose, callback){

			if(data.html !== undefined){

				var self = this,
				frag = createDocFrag(data.html),
				content = getFragElement(frag, Ulo.ids.main);

				this.setPip(content);

				Ulo.PageContent.changeContent(this.get(), content, frag, xhr, false)

					.done(function(){

						self.open( getElement(page_id), removeOnClose, callback );

						getElement("post_detail").setAttribute("data-close-pip", "true");

					})
					
					.fail(function(){

						self.close();

						Ulo.PageContent.removeFiles( getElement(page_id), true );

						messages("Sorry, we could not load the page you requested.");

					})

					.always(function(){

						self.jqxhr = null;
						
				});

			}

		},

		/* ---------------------------------------------------------------------------- */

		getPage: function(url, page_id, callback){

			var self = this;

			/* Make a request for the page */
			self.jqxhr = $.ajax({

					type:"GET",
					url: url, 
					cache:true,
					statusCode: requestCodes,
					
				})

				/* Params: server data, status code, xhr */
				.done(function(data, sc, xhr){

					if(Ulo.Session.hasChanged(xhr)){

						self.updatePage(xhr, data, page_id, callback);

					}

					else{

						self.displayPage(xhr, data, page_id, callback);

					}

				})

				/* Params: xhr, status code, error type */
				.fail(function(xhr){
					
					requestMessages(xhr, "Sorry, we could not load the page you requested.");
			
				})

				.always(function(){

					self.jqxhr = null;

			});

		},

		/* -------------------------------------------------------------------------------- */

		get: function(create){

			var id = "pip_modal";

			var modal = getElement(id);

			if(create !== false && modal === null){

				modal = makeElement("div", {

					"id": id, 
					"class": "modal " + Ulo.cls.hide,

				});

				getMain().appendChild(modal);

			}

			return modal;

		},

		/* -------------------------------------------------------------------------------- */

		login: function(){

			return getElement("login");

		},

		/* -------------------------------------------------------------------------------- */

		open: function(element, removeOnClose, callback){

			var login = this.login();

			var modal = this.get();

			if(login){

				element = login;

				removeOnClose = false;

				var fields = [getElement("login_email"), getElement("login_password")];
			
				for(var i=0; i<fields.length; ++i){
					
					if(fields[i] !== null){
						
						fields[i].value="";

					}

				}

			}

			if(element){

				this.hideNodes(modal);

				if(removeOnClose === true){

					this.elements.push( element );

				}

				modal.appendChild( removeClass(element, Ulo.cls.hide) );

			}

			if(hasClass(modal, Ulo.cls.hide)){

				removeClass(modal, Ulo.cls.hide);

				addClass(document.body, Ulo.cls.modal_open);

				$(modal).on(Ulo.evts.click, {self: this}, this.close);


			}


			/* Run callbacks at the end. */

			if(callback !== undefined){
				
				this.callbacks.push( callback );

			}

			for(var i=0; i<this.callbacks.length; ++i){

				this.callbacks[i]( true );

			}

		},

		/* -------------------------------------------------------------------------------- */

		close: function(e){

			var self, modal, close_modal;

			if(e === undefined){

				self = this;
				modal = self.get();
				close_modal = true;

			} else{

				self = e.data.self;
				modal = self.get();
				close_modal = e.target === modal;

			}

			if(close_modal || e.target.getAttribute("data-close-pip") === "true"){

				for(var i=0; i<self.callbacks.length; ++i){

					self.callbacks[i]( false );

				}

				for(var i=0; i<self.elements.length; ++i){

					Ulo.PageContent.removeFiles(self.elements[i], true);

				}

				
				self.callbacks.length = self.elements.length = 0;

				$(modal).off(Ulo.evts.click, self.close);

				removeClass(document.body, Ulo.cls.modal_open);

				addClass(modal, Ulo.cls.hide);

				self.hideNodes(modal);				

			}

		},

		/* -------------------------------------------------------------------------------- */

		unload: function(e){

			var modal = this.get(false);

			if(modal !== null){

				$( modal ).trigger(Ulo.evts.click);

				var elements = modal.childNodes;

				for(var i=0; i<elements.length; ++i){

					Ulo.PageContent.removeFiles( elements[i] );

				}

			}

		},

		/* -------------------------------------------------------------------------------- */

		hideNodes: function(modal){

			for(var i=0; i<modal.childNodes.length; ++i){

				addClass(modal.childNodes[i], Ulo.cls.hide);

			}

		}

	}

	/************************************************************************************/

	Ulo.newClass(Pip, "Pip");

	/************************************************************************************/
	
	/* -------------------------------------------------------------------------------- */
	/* END Pip */




	/* CHANNGE PAGE */
	/* -------------------------------------------------------------------------------- */
	/*
		Change/append a new page with/to the current page. The page elements to update are 
		the meta tags, css files, title, main content and javascript files that are 
		specific to each page.
	*/
	function PageContent(){

		this.REF_CTR = "ref_ctr";
		this.META = "meta_files";
		this.CSS = "css_files";
		this.JS = "js_files";

		var main_content = getMain();
		main_content[this.META] = $("meta.meta_tag");
		main_content[this.CSS] = $("link.css_file");
		main_content[this.JS] = $("script.js_file");

		/* Browser support booleans */
		this.isLinkOnLoadSupported = isEventSupported("load", "link");
		var script = document.createElement("script");
		this.isAsyncSupported = isAttributeSupported("async", script);
		this.isIELt10 = this.isAsyncSupported===false && script.readyState;

	}

	PageContent.prototype = {

		constructor: PageContent,

		/* ---------------------------------------------------------------------------- */
		
		cloneElement: function(elem){

			var el = document.createElement(elem.nodeName);
				
			for(var i=0; i<elem.attributes.length; ++i){
				
				el.setAttribute(elem.attributes[i].name, elem.attributes[i].value);
				
			}
			
			return el;
		},

		/* ---------------------------------------------------------------------------- */

		unWrap: function(content){

			if(content){

				var nodes = content.childNodes;

				for(var i=0; i<nodes.length; ++i){

					if(nodes[i].nodeType===nodes[i].ELEMENT_NODE){

						return nodes[i];

					}

				}

			}

			return content;
		},

		/* ---------------------------------------------------------------------------- */

		fileExists: function(files, file, attr, idx){

			for(; idx >= 0; --idx){

				if(files[idx][attr]===file[attr]){
					
					files[idx][this.REF_CTR] = (files[idx][this.REF_CTR] || 0) + 1;

					return true;

				}

			}

			return false;
		},

		/* ---------------------------------------------------------------------------- */

		removeFiles: function(content, all){

			if(content){

				var files = [content[this.META], content[this.CSS], content[this.JS]];

				for(var i=0; i<files.length; ++i){

					if(files[i] !== undefined){

						for(var j=0; j<files[i].length; ++j){
							
							if(!files[i][j][this.REF_CTR] && files[i][j].parentNode!==null){

								files[i][j].parentNode.removeChild(files[i][j]);
								
							} else{

								--files[i][j][this.REF_CTR];

								console.log( 'count: ', files[i][j][this.REF_CTR] )

							}
								
						}

					}
					
				}

				if(all===true && content.parentNode){

					content.parentNode.removeChild(content);

				}

			}
			
		},

		/* ---------------------------------------------------------------------------- */

		setHandlers: function(elem, deferred, handler, evt){

			++deferred.loading;
			$(elem).on(evt||"load", {self:this, deferred:deferred}, handler||this.resolveHandler);
			$(elem).on("error", {self:this, deferred:deferred}, this.rejectHandler);

		},

		/* ---------------------------------------------------------------------------- */

		resolveHandler: function(e){

			if(--e.data.deferred.loading <= 0){
				e.data.deferred.resolve();
			}

		},

		/* ---------------------------------------------------------------------------- */

		/*
			Call the reject function for the deferred object.
		*/
		rejectHandler: function(e){

			e.data.self.resolveHandler(e)
			//e.data.deferred.reject();
		},


		/* META */
		/* ---------------------------------------------------------------------------- */

		metaResolve: function(container, content, fragment){

			var meta = getFragElement(fragment, "page_meta").childNodes;

			for(var i=0; i<meta.length; ++i){

				if(meta[i].nodeType === meta[i].ELEMENT_NODE){
					
					content[this.META].push( container.appendChild(meta[i]) );
				
				}

			}
		},

		/* CSS */
		/* ---------------------------------------------------------------------------- */

		cssHandler: function(e){

			this.disabled=true;
			e.data.self.resolveHandler(e);

		},

		/* ---------------------------------------------------------------------------- */

		cssLoad: function(container, content, fragment, deferred, replace){

			var links = getFragElement(fragment, "page_css").childNodes;
			var curr_links = container.getElementsByTagName("link");

			for(var i=0, idx=curr_links.length-1, elem; i<links.length; ++i){

				if(links[i].nodeType===links[i].ELEMENT_NODE && (replace===true ||
					this.fileExists(curr_links, links[i], "href", idx)===false)){

					if(this.isLinkOnLoadSupported){

						this.setHandlers(links[i], deferred, this.cssHandler);

					} else{

						links[i].disabled = true;

					}
					
					content[this.CSS].push( container.appendChild(links[i]) );
					
				}
			}

		},

		/* ---------------------------------------------------------------------------- */

		cssResolve: function(content){
			
			for(var i=0; i<content[this.CSS].length; ++i){
			
				content[this.CSS][i].disabled=false;
			
			}
			
		},
		

		/* SCRIPTS */
		/* ---------------------------------------------------------------------------- */

		ieHandler: function(e){

			var self = e.data.self;

			/*
				Execute as many loaded scripts as possible in order by adding them to 
				the DOM.
			*/
			while(self.scriptsQueue.length>0 && 
					(self.scriptsQueue[0].readyState==="loaded" 
						|| self.scriptsQueue[0].readyState==="complete")){
			
				/* Pop the script at the front of the array */
				var script = self.scriptsQueue.shift();
				
				/* Avoid future loading events from this script (eg, if src changes) */
				script.onreadystatechange = null;
				
				/* Can't just appendChild, old IE bug if element isn't closed */
				document.body.insertBefore(script, document.body.lastChild);

				/* If all scripts have been loaded call resolve() */
				self.resolveHandler(e);
	
			}	
		
		},
		/* ---------------------------------------------------------------------------- */
		/*
			onload event handler to load scripts in order when the browser does not 
			support the "async" attribute and is not an IE browser.
		*/
		syncHandler: function(e){

			var self = e.data.self;
			
			/* Get the script at the front of the array */
			var script = self.scriptsQueue.shift();

			/* Load the script */
			script && document.body.appendChild(script);

			/* If all scripts have been loaded call resolve() */
			self.resolveHandler(e);

		},
		
		/* ---------------------------------------------------------------------------- */
		
		jsLoad: function(content, fragment, deferred, replace){

			var scripts = getFragElement(fragment, "page_js").childNodes;
			var curr_scripts = document.body.getElementsByTagName("script");

			for(var i=0, script, idx=curr_scripts.length-1; i<scripts.length; ++i){

				if(scripts[i].nodeType === scripts[i].ELEMENT_NODE && (replace===true ||
					this.fileExists(curr_scripts, scripts[i], "src", idx)===false)){

					/* Node.cloneNode() does not work */
					script = this.cloneElement( scripts[i] );
					content[this.JS].push( script );

					/* Modern browsers */
					if(this.isAsyncSupported){

						script.async = false;
						/*
							Add onload events to trigger the deferred object's resolve 
							function when all scripts have been loaded.
						*/
						this.setHandlers(script, deferred);
						/*
							Append each script relying on "async=false" to load each file 
							asynchronously and in order.
						*/
						document.body.appendChild(script);
						
					/* IE lt 10 */
					} else if(this.isIELt10){

						/* Remove src before adding the event handler */
						var src = script.src;
						script.src = null;

						/* Add event handler to execute loaded scripts in order. */
						this.setHandlers(script, deferred, this.ieHandler, "readystatechange");

						/* 
							Set src AFTER adding onreadystatechange listener so that 
							loaded events for cached scripts are not missed.

							IE fetches the file as soon as source is set and only executes 
							the file once it is added to the DOM.
						*/
						script.src = src;


						this.scriptsQueue.push(script);

					
					/* No async attribute support - requires onload event support */
					} else{
						
						this.scriptsQueue.push(script);

						this.setHandlers(script, e.data.deferred, this.syncHandler);
						
						if(i+1===scripts.length){
						
							this.syncHandler({ data: {self:this, deferred:deferred} });

						}
					
					}

				}

			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Wrapper to create a document fragment and extract an element from the
			fragment before calling changeContent()

			@param container: Element to replace/append 'content' with/to.
			@param id: ID of the element to replace/append 'container' with/to.
			@param html: Html of the new page as a string.
			@param xhr: XMLHttpRequest.
			@param replace: If true replace container else if false append to it.
		*/
		change: function(container, id, html, xhr, replace){
			
			/* Create a document fragment from the html */
			html = createDocFrag(html);

			/* Get the content from the fragment */
			var content = getFragElement(html, id);

			return this.changeContent(container, content, html, xhr, replace);
		
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Replace or append the new page content to the container. Return a JQuery 
			deferred object to assign done, fail and always callback functions to.
			
			@param container: Element to replace/append 'content' with/to.
			
			@param content: Element to replace/append 'container' with/to.

			@param fragment: A document fragment containing the individual page elements.
				Each element is wrapped in a span element with the ids:
				
				Meta: 			"#page_meta"
				CSS: 			"#page_css"
				Title: 			"#page_title"
				Javascript: 	"#page_javascript"

				Each page specific meta tag, link element and script element must be 
				given the following class names:

				Meta: 			".meta_tag"
				CSS: 			".css_file"
				Javascript: 	".js_file"

			@param xhr: XMLHttpRequest.

			@param replace: Boolean - if true 'container' is replaced by 'content' and 
				all page specific elements (meta, css, title, javascript) are replaced 
				by the new elements found in the document fragment. If false the child 
				nodes of 'content' are appended to 'container' and all other page 
				specific elements (meta, css, javascript) are appended to the head or 
				body.
			
		*/
		changeContent: function(container, content, fragment, xhr, replace){

			var self = this,
			deferred = $.Deferred(),
			cssDeferred = $.Deferred(),
			head = document.getElementsByTagName("head")[0];

			if(container && content){

				if(replace===false){

					content = this.unWrap(content);

				}

				if(replace===true || getElement(content.id)===null){

					// var old_files = $("link.css_file, script.js_file, meta.meta_tag");

					cssDeferred.done(function(){

						if(replace){

							// for(var i=0; i<old_files.length; ++i){

							// 	removeElements( old_files[i] );

							// }

							Ulo.Pip.unload();

							self.removeFiles( container );

							
							


							/* Update the title */
							appendElements(
								emptyElement(head.getElementsByTagName("title")[0]),
								getFragElement(fragment, "page_title").childNodes
							);

							/* Replace the old content with the new content */
							replaceWith(content, container);

							Ulo.Session.update(xhr, container, fragment);

							if(Ulo.EmailConfirmation!==undefined){

								Ulo.EmailConfirmation.register();

							}

						} else{

							container.appendChild(content);

						}

						/* Enable the css files */
						self.cssResolve(content);
						self.metaResolve(head, content, fragment);

						self.jsLoad(content, fragment, deferred, replace);
						deferred.loading===0 && deferred.resolve();

					});

					cssDeferred.fail(function(){

						deferred.reject();

					});

					deferred.fail(function(){

						self.removeFiles(content);
						messages("The page did not load correctly. Please try again.");

					});

					deferred.loading=cssDeferred.loading=0;
					this.scriptsQueue=[];
					content[this.META]=[];
					content[this.CSS]=[];
					content[this.JS]=[];

					this.cssLoad(head, content, fragment, cssDeferred, replace);
					cssDeferred.loading===0 && cssDeferred.resolve();

					/* Register click events for ajax page load links */
					registerLinks(content);


				/*
					The page has already been loaded.
				*/
				} else{

					deferred.resolve();

				}

			} else{

				deferred.reject();

			}

			this.deferred=null;

			this.deferred=deferred;
			
			return deferred.promise();

		},



		/* HELPERS */
		/* ---------------------------------------------------------------------------- */
		/*
			Reduce the queues to empty arrays and remove all event handlers.
		*/
	// 	reset: function(){

	// 		var pending = [this.linksQueue, this.scriptsQueue];

	// 		for(var i=0; i<pending.length; ++i){
	// 			while(pending[i].length > 0){
	// 				var elem = pending[i].pop();
	// 				elem.onload=elem.onerror=elem.onreadystatechange=null; 
	// 				$(elem).off();
	// 			}
	// 		}

	// 		pending = null;

	// 	},
	// 	/*
	// 		Return a new element that is a copy of the "elem"
	// 		@param elem: element node.
	// 	*/
	// 	cloneElement: function(elem){

	// 		var el = null;

	// 		if(elem.nodeType===elem.ELEMENT_NODE){
	// 			el = document.createElement(elem.nodeName);
	// 			for(var i=0; i<elem.attributes.length; ++i){
	// 				el.setAttribute(elem.attributes[i].name, elem.attributes[i].value);
	// 			}
	// 		}
			
	// 		return el;
	// 	},
	// 	/*
	// 		Trigger the resolve() function of a deferred object if no files are being loaded.
			
	// 		@param deferred: JQuery deferred object.
	// 		@param instant: boolean - true if the function should be triggered instantly and
	// 			false if it should be triggered after a 500 millisecond delay.
	// 	*/
	// 	triggerResolve: function(deferred, instant){

	// 		if(deferred.loading<=0){
	// 			setTimeout(function(){
	// 				deferred.resolve();
	// 			}, instant ? 0 : 500);
	// 		}

	// 	},
	// 	/*
	// 		Set event handler to track the loading of links or scripts.
			
	// 		@param elem: link or script element.
	// 		@param deferred: JQuery deferred object.
	// 		@param handler: optional "load" event handler.
	// 		@param evt: optional event name to use in place of "load".
	// 	*/
	// 	setHandlers: function(elem, deferred, handler, evt){

	// 		++deferred.loading;
	// 		$(elem).on(evt||"load", {self:this, deferred:deferred}, handler||this.resolveHandler);
	// 		$(elem).on("error", {self:this, deferred:deferred}, this.rejectHandler);

	// 	},
	// 	/*
	// 		Decrement the loading counter set on the deferred object. If the counter falls to 
	// 		zero trigger the resolve function for the deferred object.
	// 	*/
	// 	resolveHandler: function(e){
	// 		if(--e.data.deferred.loading <= 0){
	// 			e.data.deferred.resolve();
	// 		}
	// 	},
	// 	/*
	// 		Call the resolve function for the deferred object.
	// 	*/
	// 	rejectHandler: function(e){
	// 		e.data.deferred.reject();
	// 	},
	// 	/* END HELPERS */
	// 	/* ---------------------------------------------------------------------------- */


	// 	/* LINK EVENT HANDLERS */
	// 	/* ---------------------------------------------------------------------------- */
	// 	/*
	// 		Load handler for links. Disable styles as they are loaded but before they are
	// 		applied.
	// 	*/
	// 	cssHandler: function(e){
	// 		this.disabled=true;
	// 		e.data.self.resolveHandler(e);
	// 	},
	// 	/* END LINK EVENT HANDLERS */
	// 	/* ---------------------------------------------------------------------------- */


	// 	/* SCRIPT EVENT HANDLERS */
	// 	/* ---------------------------------------------------------------------------- */
	// 	/*
	// 		onreadystatechange event handler to load async scripts in order for version of IE 
	// 		that do not support the "async" attribute.
	// 		See http://www.html5rocks.com/en/tutorials/speed/script-loading/
	// 	*/
	// 	ieHandler: function(e){

	// 		var self = e.data.self;

	// 		/*
	// 			Execute as many loaded scripts as possible in order by adding them to the DOM.
	// 		*/
	// 		while(self.scriptsQueue.length>0 && 
	// 				(self.scriptsQueue[0].readyState==="loaded" 
	// 					|| self.scriptsQueue[0].readyState==="complete")){
			
	// 			/* Pop the script at the front of the array */
	// 			var script = self.scriptsQueue.shift();
	// 			/* Avoid future loading events from this script (eg, if src changes) */
	// 			script.onreadystatechange = null;
	// 			/* Can't just appendChild, old IE bug if element isn't closed */
	// 			document.body.insertBefore(script, document.body.lastChild);
	
	// 		}

	// 		/* If all scripts have been loaded call resolve() */
	// 		self.resolveHandler(e);
		
	// 	},
	// 	/*
	// 		onload event handler to load scripts in order when the browser does not support
	// 		the "async" attribute and is not an IE browser.
	// 	*/
	// 	syncHandler: function(e){

	// 		var self = e.data.self;
	// 		/* Pop the script at the front of the array */
	// 		var script = self.scriptsQueue.shift();

	// 		if(script){
	// 			/* Add load event to call this function */
	// 			self.setHandlers(script, e.data.deferred, self.syncHandler);
	// 			/* Load the script */
	// 			document.body.appendChild(script);
	// 		}

	// 		/* If all scripts have been loaded call resolve() */
	// 		self.resolveHandler(e);

	// 	},
	// 	/* END SCRIPT EVENT HANDLERS */
	// 	/* ---------------------------------------------------------------------------- */
	// 	/*
	// 		Preload css files by adding them to the DOM and disabling them once they have been
	// 		loaded. Add all files being loaded to the linksQueue array.

	// 		@param container: head element.
	// 		@param fragment: document fragment containing the css files to load.
	// 		@param deferred: JQuery deferred object.
	// 	*/
	// 	cssPreloader: function(container, fragment, deferred){
			
	// 		var links = getFragElement(fragment, "page_css").childNodes;

	// 		for(var i=0, elem; i<links.length; ++i){
	// 			if(links[i].nodeType === links[i].ELEMENT_NODE){

	// 				if(this.isLinkOnLoadSupported){
	// 					this.linksQueue.push(links[i]);
	// 					this.setHandlers(links[i], deferred, this.cssHandler);
	// 				}
					
	// 				container.appendChild(links[i]);

	// 			}
	// 		}

	// 	},
	// 	/*
	// 		Preload javascript files by adding them to the DOM and disabled then once they 
	// 		have been loaded. Add all scripts to be loaded to the scriptsQueue array.
			
	// 		@param container: head element.
	// 		@param fragment: document fragment containing the javascript files to load.
	// 		@param deferred: JQuery deferred object.
	// 	*/
	// 	jsPreloader: function(container, fragment, deferred){

	// 		var scripts = getFragElement(fragment, "page_js").childNodes;
			
	// 		for(var i=0, src, elem; i<scripts.length; ++i){
	// 			if(scripts[i].nodeType === scripts[i].ELEMENT_NODE){

	// 				/* Preload the file - only supported by chrome */
	// 				elem = makeElement("link", {"href":scripts[i].src||"", "rel":"preload"});
	// 				container.appendChild(elem);

	// 				/*
	// 					Remove the source attribute for IE browsers below 10 before creating 
	// 					the new script. Source must be set after onreadystatechange. 
	// 					See jsLoader()
	// 				*/
	// 				if(this.isIELt10){
	// 					src = scripts[i].src
	// 					scripts[i].removeAttribute("src");
	// 				}

	// 				/* Create a new script element */
	// 				elem = this.cloneElement(scripts[i]);
	// 				/* Set _src for IE lt 10. See jsLoader() */
	// 				elem._src = src;
	// 				/* Set async for modern browsers. See jsLoader() */
	// 				elem.async = false;

	// 				this.scriptsQueue.push(elem);

	// 			}
	// 		}

	// 	},
	// 	/* ---------------------------------------------------------------------------- */
	// 	/*
	// 		Execute all css files added to the document by enabling the stylsheets.
	// 	*/
	// 	cssLoader: function(){
	// 		while(this.linksQueue.length>0){
	// 			this.linksQueue.pop().disabled=false;
	// 		}
	// 	},
	// 	/*
	// 		Load and execute all javascript files in order and asynchronously.
	// 		See http://www.html5rocks.com/en/tutorials/speed/script-loading/
	// 	*/
	// 	jsLoader: function(deferred){

	// 		for(var i=0, elem; i<this.scriptsQueue.length; ++i){

	// 			/* Modern browsers */
	// 			if(this.isAsyncSupported){
					
	// 				/*
	// 					Add onload events to trigger the deferred object's resolve function
	// 					when all scripts have been loaded.
	// 				*/
	// 				this.isScriptOnLoadSupported && this.setHandlers(this.scriptsQueue[i], deferred);
	// 				/*
	// 					Append each script relying on "async=false" to load each file 
	// 					asynchronously and in order.
	// 				*/
	// 				document.body.appendChild(this.scriptsQueue[i]);
					
	// 			/* IE lt 10 */
	// 			} else if(this.isIELt10){

	// 				/* Add handler to execute loaded scripts in order. */
	// 				this.setHandlers(this.scriptsQueue[i], deferred, this.ieHandler, "readystatechange");

	// 				/* 
	// 					Set src AFTER adding onreadystatechange listener so that loaded events
	// 					for cached scripts are not missed.

	// 					IE fetches the file as soon as source is set and only executes the
	// 					file once it is added to the DOM.
	// 				*/
	// 				this.scriptsQueue[i].src = this.scriptsQueue[i]._src;

				
	// 			/* No async attribute support - requires onload event support */
	// 			} else if(this.isScriptOnLoadSupported){
					
	// 				/* Dequeue the next file when the previous file loads */
	// 				this.syncHandler({data:{self:this, deferred:deferred}});
	// 				break;

	// 			/* Fallback to browser's default - DOES NOT GUARANTEE ASYNC & IN ORDER EXEC */
	// 			} else{
					
	// 				document.body.appendChild(this.scriptsQueue[i]);

	// 			}

	// 		}

	// 	},
	// 	/* ---------------------------------------------------------------------------- */
	// 	/*		
	// 		Update the current page with the new content. Return a JQuery deferred object
	// 		to assign done, fail and always callback functions.

	// 		@param curr_cont: Current content to append the new content to or replace it
	// 			with depending on the value of "replace".

	// 		@param new_cont: New content to append to current content or replace the current
	// 			content with depending on the value of "replace".

	// 		@param fragment: Document fragment. The document fragment must group the page 
	// 			elements (not including the main page content - #main_content) by wrapping 
	// 			them in containers using the ids:
				
	// 			Meta: 			"#page_meta"
	// 			CSS: 			"#page_css"
	// 			Title: 			"#page_title"
	// 			Javascript: 	"#page_javascript"

	// 			Each page specific meta tag, link element and script element must be given 
	// 			the following class names:

	// 			Meta: 			".meta_tag"
	// 			CSS: 			".css_file"
	// 			Javascript: 	".js_file"

	// 		@param replace: Boolean - if true curr_cont is replaced by new_cont and all page
	// 			specific elements (meta, css, title, javascript) are replaced by their new 
	// 			elements found in the document fragment. If false the child nodes of new_cont 
	// 			are appended to curr_cont and all other page specific elements (meta, css, 
	// 			javascript) are appended to their containers.
	// 	*/
	// 	change: function(curr_cont, new_cont, fragment, replace, full_load){


	// 		this.reset();


	// 		var elements = replace && $("meta.meta_tag, link.css_file, link[rel=preload], script.js_file");

	// 		var self = this, head = document.getElementsByTagName("head")[0];
	// 		var cssDeferred = $.Deferred(), jsDeferred = $.Deferred();
	// 		cssDeferred.loading = jsDeferred.loading = 0;

			
	// 		cssDeferred.done(function(){

	// 			if(replace){

	// 				/* Remove the olds elements */
	// 				elements.remove();

	// 				/* Update the title */
	// 				var title = emptyElement(head.getElementsByTagName("title")[0]);
	// 				appendElements(title, getFragElement(fragment, "page_title").childNodes);

	// 				/* Replace the old content with the new content */
	// 				replaceWith(new_cont, curr_cont);

	// 			} else{

	// 				appendElements(curr_cont, new_cont.childNodes);

	// 			}

	// 			/* Add the meta tags */
	// 			appendElements(head, getFragElement(fragment, "page_meta").childNodes);

	// 			/* Enable the css files */
	// 			self.cssLoader();
	// 			/* Load and execute the javascript files */
	// 			self.jsLoader(jsDeferred);
	// 			/* If there are no files loading resolve the deferred object */
	// 			self.triggerResolve(jsDeferred, this.isScriptOnLoadSupported);

	// 		});

	// 		/* Clear the queues if the process fails */
	// 		cssDeferred.fail(function(){ 

	// 			jsDeferred.reject(); 

	// 		});

	// 		jsDeferred.fail(function(){ 

	// 			self.reset();
	// 			replace && emptyElement(getMain());
	// 			messages("The page did not load correctly. Please try again.");


	// 		});


	// 		if(curr_cont && new_cont){
			
	// 			this.cssPreloader(head, fragment, cssDeferred);
	// 			this.jsPreloader(head, fragment, jsDeferred);

	// 			/* If there are no files loading resolve the deferred object */
	// 			this.triggerResolve(cssDeferred, this.isLinkOnLoadSupported);
			
	// 		} else{

	// 			jsDeferred.reject();
			
	// 		}

	// 		return (full_load ? jsDeferred : cssDeferred).promise();

	// 	},

	}
	/************************************************************************************/

	Ulo.newClass(PageContent, "PageContent");

	/************************************************************************************/

	/* END CHANNGE PAGE */
	/* -------------------------------------------------------------------------------- */




	// /*
	// 	Change/update the current page with the contents of the new page. The page elements
	// 	to update are the meta tags, css files, title, main content and javascript files that
	// 	are specific to each page. 
	// */
	// function ChangePage(){

	// 	/* Set the function used to track the loading of css files */
	// 	this.cssLoaded = isEventSupported("load", "link") ? this.elementLoaded : this.elementLoadedFallback;

	// }
	// ChangePage.prototype = {

	// 	constructor: ChangePage,

	// 	/* ---------------------------------------------------------------------------- */
	// 	/*
	// 		INTERNAL HELPER FUNCTIONS
	// 	*/
	// 	/* ---------------------------------------------------------------------------- */
	// 	cloneElement: function(elem){

	// 		var el = document.createElement(elem.nodeName);
			
	// 		for(var i=0; i<elem.attributes.length; ++i){
	// 			el.setAttribute(elem.attributes[i].name, elem.attributes[i].value);
	// 		}
			
	// 		return el;
	// 	},
	// 	/*
	// 		Resolve the deferred object (returned by changePageElements) once all css and 
	// 		javascript files have been loaded/preloaded.
			
	// 		@param element: link or script element.
	// 		@param deferred: JQuery deferred object.
	// 	*/
	// 	deferredResolve: function(element, deferred){

	// 		if(--this.counter<=0){
				
	// 			deferred.resolve();
				
	// 		}

	// 	},
	// 	/*
	// 		Reject the deferred object if an error occurs.

	// 		@param element: link or script element.
	// 		@param deferred: JQuery deferred object.
	// 	*/
	// 	deferredReject: function(element, deferred){

	// 		element.onload=element.onerror=null;
	// 		deferred.reject();

	// 	},
	// 	/*
	// 		Set onload and onerror event handlers to trigger the deferred objects resolve() 
	// 		or reject() methods.

	// 		@param element: link or script element.
	// 		@param deferred: JQuery deferred object.
	// 	*/
	// 	elementLoaded: function(element, deferred){

	// 		var self = this;
	// 		element.onload = function(e){ self.deferredResolve(element, deferred) }
	// 		element.onerror = function(e){ self.deferredReject(element, deferred) }

	// 	},
	// 	/*
	// 		Fallback method to detect when an element has loaded.

	// 		@param element: link or script element.
	// 		@param deferred: JQuery deferred object.
	// 	*/
	// 	elementLoadedFallback: function(element, deferred){

	// 		var self = this;
	// 		var img = document.createElement("img");
	// 		img.onerror = function(e){ self.deferredResolve(element, deferred) }
	// 		img.src = element.getAttribute("src") || element.getAttribute("href");
			
	// 	},
	// 	/*
	// 		Remove all elements in an array from the DOM. The function accepts any number
	// 		of one dimensional arrays passed to the function as separate arguments.

	// 		@param *: 1..N arrays of element nodes.
	// 	*/
	// 	removeArrayElements: function(){

	// 		for(var i=0; i<arguments.length; ++i){
	// 			for(var j=0; j<arguments[i].length; ++j){
	// 				if(arguments[i][j].parentNode !== null){
	// 					arguments[i][j].parentNode.removeChild(arguments[i][j]);
	// 				}
	// 			}
	// 		}

	// 	},
	// 	/*
	// 		Load link elements but prevent them from being applied to the page. Preload 
	// 		script elements without executing the files. Return an array containing the
	// 		new link or script element being loaded/preloaded.

	// 		@param container: element to append the link or script elements to.
	// 		@param nodes: array or link or script elements to load or preload.
	// 		@param tag: tag name of elements found in the nodes array ("link" or "script").
	// 		@param deferred: JQuery deferred object.
	// 	*/
	// 	pageElements: function(container, nodes, tag, deferred){

	// 		/* Loop variables */
	// 		var elem, elems=[], attrs, object, isLink=tag==="link";

	// 		for(var i=0; i<nodes.length; ++i){

	// 			if(nodes[i].nodeType === nodes[i].ELEMENT_NODE){

	// 				elem = this.cloneElement(nodes[i]);
	// 				this.elementLoaded(elem, deferred);
	// 				container.appendChild(elem);
	// 				elems.push(elem);

	// 			}

	// 		}

	// 		this.counter += elems.length;
	// 		nodes = null;
	// 		return elems;

	// 	},
	// 	// pageElements: function(container, nodes, tag, deferred){

	// 	// 	/* Loop variables */
	// 	// 	var elem, elems=[], attrs, object, isLink=tag==="link";

	// 	// 	for(var i=0; i<nodes.length; ++i){

	// 	// 		if(nodes[i].nodeType === nodes[i].ELEMENT_NODE){

	// 	// 			/*
	// 	// 			*/
	// 	// 			elem = document.createElement(tag);
	// 	// 			attrs = nodes[i].attributes;
	// 	// 			for(var j=0; j<attrs.length; ++j){
	// 	// 				elem.setAttribute(attrs[j].name, attrs[j].value);
	// 	// 			}

	// 	// 			/* Add the new element to the array returned by the function */
	// 	// 			elems.push(elem);

	// 	// 			if(isLink){
						
	// 	// 				/* Start loading the css files */
	// 	// 				this.cssLoaded(elem, deferred);
	// 	// 				container.appendChild(elem);

	// 	// 			} else{

	// 	// 				/* Maintain execution order without blocking */
	// 	// 				elem.setAttribute("async", false);

	// 	// 				try{

	// 	// 					/* Preload the scripts without executing the files */
	// 	// 					object = document.createElement("object");
	// 	// 					object.data = elem.getAttribute("src");
	// 	// 					object.width = object.height = 0;
							
	// 	// 					this.elementLoaded(object, deferred);
	// 	// 					container.appendChild(object);

	// 	// 				}catch(e){

	// 	// 					/* IE fallback */
	// 	// 					this.elementLoadedFallback(elem, deferred);
							
	// 	// 				}
					
	// 	// 			}

					

	// 	// 		}

	// 	// 	}

	// 	// 	this.counter += elems.length;
	// 	// 	nodes = null;
	// 	// 	return elems;

	// 	// },
	// 	/* ---------------------------------------------------------------------------- */
	// 	/*
	// 		END INTERNAL HELPER FUNCTIONS
	// 	*/
	// 	/* ---------------------------------------------------------------------------- */
	// 	/*
	// 		Update the current page with the new content. Return a JQuery deferred object
	// 		to assign done, fail and always callback functions.

	// 		@param curr_cont: Current content to append the new content to or replace it
	// 			with depending on the value of "replace".

	// 		@param new_cont: New content to append to current content or replace the current
	// 			content with depending on the value of "replace".

	// 		@param fragment: Document fragment. The document fragment must group the page 
	// 			elements (not including the main page content - #main_content) by wrapping 
	// 			them in containers using the ids:
				
	// 			Meta: 			"#page_meta"
	// 			CSS: 			"#page_css"
	// 			Title: 			"#page_title"
	// 			Javascript: 	"#page_javascript"

	// 			Each page specific meta tag, link element and script element must be given 
	// 			the following class names:

	// 			Meta: 			".meta_tag"
	// 			CSS: 			".css_file"
	// 			Javascript: 	".js_file"

	// 		@param replace: Boolean - if true curr_cont is replaced by new_cont and all page
	// 			specific elements (meta, css, title, javascript) are replaced by their new 
	// 			elements found in the document fragment. If false the child nodes of new_cont 
	// 			are appended to curr_cont and all other page specific elements (meta, css, 
	// 			javascript) are appended to their containers.
	// 	*/
	// 	changePageElements: function(curr_cont, new_cont, fragment, replace){

	// 		var self = this;
	// 		var nodes, links=[], scripts=[], deferred=$.Deferred();
	// 		var head = document.getElementsByTagName("head")[0];
	// 		/* Meta, CSS and Javascript files for the current page */
	// 		var elements = replace && $("meta.meta_tag, link.css_file, script.js_file");

	// 		/* Number of css and javascript files - incremented by pageElements */
	// 		this.counter = 0;

	// 		/* Load the new css files to the DOM but disable the stylesheets */
	// 		nodes = getFragElement(fragment, "page_css").childNodes
	// 		links = this.pageElements(head, nodes, "link", deferred);

	// 		/* Preload the javascript files without executing them */
	// 		nodes = getFragElement(fragment, "page_js").childNodes
	// 		// scripts = this.pageElements(head, nodes, "script", deferred);

	// 		/* If there are no files being loaded then resolve the deferred object */
	// 		if(this.counter===0){
	// 			deferred.resolve();	
	// 		}

	// 		deferred.done(function(){

	// 			if(replace){
				
	// 				/* Delete the meta, css and javascript files for the current page */
	// 				elements.remove();
	// 				/* Update the title */
	// 				var title = emptyElement(document.getElementsByTagName("title")[0]);
	// 				appendElements(title, getFragElement(fragment, "page_title").childNodes);
	// 				/* Replace the page contents */
	// 				replaceWith(new_cont, curr_cont);
				
	// 			} else{

	// 				/* Append the new page content */
	// 				appendElements(curr_cont, new_cont);

	// 			}

	// 			for(var i=0; i<nodes.length; ++i){
	// 				if(nodes[i].nodeType === nodes[i].ELEMENT_NODE){
	// 					var s = self.cloneElement(nodes[i]);
	// 					s.async = false;
	// 					head.appendChild( s );
	// 				}
	// 			}
				
	// 			/* Add the new meta data */
	// 			appendElements(head, getFragElement(fragment, "page_meta").childNodes);

	// 			/* Enable the new css files */
	// 			// for(var i=0; i<links.length; ++i){
	// 			// 	for(var j=0; j<document.styleSheets.length; ++j){
						
	// 			// 		if(document.styleSheets.item(j).ownerNode===links[i]){
						
	// 			// 			links[i] = document.styleSheets.item(j).disabled=true;
	// 			// 			break;

	// 			// 		}
						
	// 			// 	}
	// 			// }


	// 			/* Execute the new javascript files. */
	// 			// for(var i=0; i<scripts.length; ++i){
	// 			// 	document.body.appendChild(scripts[i]);
	// 			// }

	// 			/* Remove all object elements used to preload the javascript files */
	// 			self.removeArrayElements(document.getElementsByTagName("object"));

	// 		});


	// 		deferred.fail(function(){

	// 			/* Remove any of the new elements that may have been added to the document */
	// 			self.removeArrayElements(links, document.getElementsByTagName("object"));

	// 		});

	// 		return deferred;

	// 	},

	// }




	/* PAGE */
	/* -------------------------------------------------------------------------------- */
	/*
		Load a new page using an ajax GET request. The class depends on the HTML5 History 
		API to update the url and PageContent to update the content and load external 
		files.
	*/
	function Page(){

		Ulo.checkDependencies("PageContent");

		if(window.history && window.history.pushState){

			/* Params: state object, title, url (set to the current url if not specified) */
			history.replaceState({}, document.title, location.pathname+location.search);

			/* Handle backwards and forwards page navigation. */
			$(window).on("popstate", this.historyHandler);
			
			/* Ajax request handle */
			this.jqxhr = null;

			/* Register all anchor elements with the attribute data-apl */
			this.register();

		} else{

			throw Error("History API not supported.");
		}
		
	}
	Page.prototype = {
		
		constructor: Page,

		/* ---------------------------------------------------------------------------- */
		/*
			Register a click event on all <a> tags marked with the data attribute
			data-apl (ajax page loader). Return the optional context parameter.

			@param context: optional context passed to jQuery when searching for <a> tags.
				to narrow down the scope and stop multiple events being added to links.
		*/
		register: function(context){

			$("a[data-apl]", context).on(Ulo.evts.click, this.getPageHandler);
			
			return context;

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Register a click event on the element passed in as an argument. Return the 
			element.

			@param element: element node.
		*/
		registerElement: function(element){

			return $(element).on(Ulo.evts.click, this.getPageHandler);

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Update the contents of the page without performing a full page refresh when a 
			user moves backwards or forwards through their browser history.
		*/
		historyHandler: function(e){
		
			try{

				Ulo.Page.getPage(Ulo.getURL(), false);
				e.preventDefault();

			} catch(e){

				debug(e);
			
			}
		
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Event handler for getPage(). Retrieve and load the html found at the url.
		*/
		getPageHandler: function(e){
			
			try{

				Ulo.Page.getPage(e.currentTarget.href, true);
				e.preventDefault();

			} catch(e){

				debug(e);
			
			}
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Refresh the page using the value of the returned url (curr_url). Return true
			if the page has been refreshed and false if not.

			@param prev_url: the requested url
			@param curr_url: the returned url
			@param force: boolean to force a refresh.
		*/
		replacePage: function(prev_url, curr_url, force){

			if(force === true || /\/logout\/$/.test(prev_url)){
		
				Ulo.replacePage(curr_url);
		
				return true;
		
			}
		
			return false;
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Update the browser history if the url has changed.

			@param url: url returned by the GET request for a new page.
			@param push: boolean - true if the url should be added to the history
				instead of replacing the current entry.
		*/
		updateHistory: function(url, push){

			try{

				if(Ulo.getURL() !== url){

					history[push===true ? "pushState" : "replaceState"]({}, document.title, url);

				}

			} catch(e){}
		
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Abort the current request.
		*/
		abort: function(){

			this.jqxhr && this.jqxhr.abort();
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/*

		*/
		preventUnload: function(){

			this.prevent_unload = true;

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Get a html document and update swap the current page for the page returned.

			@param url: url of the html document.
			@param push: boolean - true if the url should be added to the history
				instead of replacing the current entry.
			@param done: option callback function, called when a request is successful.
			@param fail: option callback function, called when a request fails.
		*/
		getPage: function(url, push_state, done, fail){

			console.log(url);

			this.abort();

			if(url !== ""){

				var self = this;

				self.jqxhr = $.ajax({

						type: "GET", 
						url: url,
						cache: true,
						statusCode: requestCodes
					
					})
				
					.done(function(data, sc, xhr){

						/* If the page has not been refreshed then update the content */
						if(Ulo.Page.replacePage(url, data.url, false) === false){

							if(self.prevent_unload !== true){

								/* Remove all temporary classes from the global namespace */
								Ulo.TEMP = {};

							}

							/* Update url as the request may have been redirected */
							url = data.url;

							Ulo.Page.updateHistory(url, push_state);

							/* Trigger any unload events */
							// $(window).trigger('beforeunload');

							/* Update the contents of the page */
							var main = getMain();

							Ulo.PageContent.change(main, main.id, data.html, xhr, true)
								
								.done(function(){

									done!==undefined && done();

								})
								.fail(function(a,b,c){

									fail!==undefined && fail();

							});

						}

					})
					
					.fail(function(xhr){

						/*
							Try to render any error pages that may have been sent 
							from the server.
						*/
						var main = getMain();
						
						Ulo.PageContent.change(main, main.id, xhr.responseText, xhr, true)

						fail!==undefined && fail();

					})

					.always(function(){

						self.jqxhr = null;

						self.prevent_unload=false;
				
				});

				

			}
		}
	}

	/************************************************************************************/

	Ulo.newClass(Page, "Page");

	/************************************************************************************/

	/* END PAGE */
	/* -------------------------------------------------------------------------------- */


	/* AUTOCOMPLETE */
	/* -------------------------------------------------------------------------------- */
	
	function Autocomplete(prefix){

		this.timeout_id = null;

		this.autocompleteRequest = false;

		this.prefix = prefix===undefined ? '' : prefix+'_';

		$(this.getInput()).on("input", {self: this}, this.submit);
	}

	Autocomplete.prototype = {

		constructor: Autocomplete,

		/* ---------------------------------------------------------------------------- */

		selectSibling: function(next, selected, context){

			var data = next ? 

				["li:first", "firstChild", "nextSibling", "previousSibling"]
			
				: 
			
				["li:last", "lastChild", "previousSibling", "nextSibling"];


			if(selected !== null){

				var sibling = selected[data[2]];

				if(sibling === null){

					var ul = selected.parentNode;

					sibling = ul[data[2]] && ul[data[2]][data[1]] ||
						ul[data[3]] && ul[data[3]][data[1]] ||
						ul[data[1]];

				}

				removeClass(selected, "selected");

				selected = sibling;


			} else{

				selected = $(data[0], context)[0];
			}

			addClass(selected, "selected");

		},

		/* ---------------------------------------------------------------------------- */

		downShortcut: function(e){

			if(e.which === 40 && e.data.self.hasSuggestions === true){

				e.data.self.showSuggestions();

			}

		},

		/* ---------------------------------------------------------------------------- */

		shortcuts: function(e){

			var self = e.data.self;

			var container = self.getContainer();

			var selected = container.querySelector("li.selected");

			if(e.which === 13){

				if(selected !== null){

					$(selected.firstChild).trigger(Ulo.evts.click);

				}

			}

			else if(e.which === 40){

				self.selectSibling(true, selected, container);

			}

			else if(e.which === 38){

				self.selectSibling(false, selected, container);

			} 

			else{

				return true;

			}

			e.preventDefault();

		},

		/* ---------------------------------------------------------------------------- */

		enter: function(e){

			/* If the target element is not the UL itself: */
			if(e.target !== e.currentTarget){

				var container = e.data.self.getContainer();

				/* Get the current selection */
				var selected = container.querySelector("li.selected");

				/* Normalise the target to the li element */
				while(e.target !== null && e.target.nodeName !== "LI"){

					e.target = e.target.parentNode;

				}			

				/* If the target is not the current selection update the elements */
				if(selected !== e.target){

					removeClass(selected, "selected");

					addClass(e.target, "selected");

				}

			}

		},

		/* ---------------------------------------------------------------------------- */

		leave: function(e){

			var container = e.data.self.getContainer();

			removeClass(container.querySelector("li.selected"), "selected");

		},

		/* ---------------------------------------------------------------------------- */

		getElementPrefixed: function(id){

			return getElement(this.prefix+id);

		},

		/* ---------------------------------------------------------------------------- */

		getInput: function(){

			return this.getElementPrefixed("search_input");

		},

		/* ---------------------------------------------------------------------------- */

		getFilter: function(){

			return this.getElementPrefixed("search_filter");

		},

		/* ---------------------------------------------------------------------------- */

		getContainer: function(){

			return this.getElementPrefixed("search_autocomplete");

		},

		/* ---------------------------------------------------------------------------- */

		setInput: function(e){

			e.data.self.getInput().value = e.currentTarget.textContent || 
				e.currentTarget.innerHTML;

		},

		/* ---------------------------------------------------------------------------- */

		abort: function(){

			if(this.autocompleteRequest && Ulo.jqxhr!==null){

				this.autocompleteRequest = false;
				Ulo.jqxhr.abort();
				Ulo.jqxhr=null;

			}

			clearTimeout(this.timeout_id);

		},

		/* ---------------------------------------------------------------------------- */

		serialise: function(q){

			var value = $.trim(q.value);

			if(value === ""){

				return null;

			}

			return { "q": value, "filter":  this.getFilter().value }

		},

		/* ---------------------------------------------------------------------------- */

		user_suggestions: function(suggestions){

			var users = emptyElement(this.getElementPrefixed("user_suggestions"));

			if(suggestions!==undefined && suggestions.length!==0){

				var result, parent, child, i;

				for(i in suggestions){

					result = suggestions[i].payload;

					parent = makeElement("li", {"class": "suggestion"});

					child = makeElement("a", {

						"href": "/user/" + result.username + "/",
						"class": "user",
						"title": "Profile",
						"data-apl": "true"

					});

					registerPageLink(child);

					child.appendChild( makeElement("img", {"src": result.thumbnail}) );
					
					child.appendChild( makeElement("span", {

						"class": "bold name"

					}, result.name) );
					
					child.appendChild( makeElement("span", {

						"class": "username ellipsis"

					}, "@"+result.username) );

					users.appendChild(parent).appendChild(child);

				}

				return true;

			}

		},

		/* ---------------------------------------------------------------------------- */

		post_suggestions: function(suggestions){

			var posts = emptyElement(this.getElementPrefixed("post_suggestions"));

			if(suggestions!==undefined && suggestions.length!==0){

				var result, parent, child, i;

				for(i in suggestions){

					result = suggestions[i];

					parent = makeElement("li", {"class": "suggestion"});

					child = makeElement("a", {

						"href": "/search/?q=" + fixedEncodeURIComponent(result.text),
						"class": "post",
						"title": "Search",
						"data-apl": "true"

					}, result.text);

					registerPageLink(child);

					$(child).on(Ulo.evts.click, {self: this}, this.setInput);

					posts.appendChild(parent).appendChild(child);

				}

				return true;

			}

		},

		/* ---------------------------------------------------------------------------- */

		showSuggestions: function(e){

			var self = e===undefined ? this : e.data.self,
			container = self.getContainer();

			if(hasClass(container, Ulo.cls.show)===false){

				addClass(container, Ulo.cls.show);

				$(document).off("keydown", self.downShortcut);

				$(container.getElementsByTagName("ul"))
					.on("mouseover", {self: self}, self.enter)
					.on("mouseout", {self: self}, self.leave);

				$(document).on("keydown", {self: self}, self.shortcuts);
				
				$(document).on(Ulo.evts.click, {self: self}, self.hideSuggestions);

			}

		},

		/* ---------------------------------------------------------------------------- */

		isOutside: function(target){

			return target.id !== this.prefix+"search_input" &&
				isDescendant(getElement("toggle_search_filters"), target, 1)===false &&
				isDescendant(getElement("search_filters"), target, 2)===false;

		},

		/* ---------------------------------------------------------------------------- */

		hideSuggestions: function(e){

			var self = e===undefined ? this : e.data.self;

			/*
				If the function is called explicitly or the target of the click event is 
				not the input element then remove the suggestions.
			*/
			if(e===undefined || self.isOutside(e.target)){

				self.abort();

				var container = self.getContainer();
				
				removeClass(container, Ulo.cls.show);

				$(container.getElementsByTagName("ul"))
					.off("mouseover", self.enter).off("mouseout", self.leave);

				$(document).off("keydown", self.shortcuts);

				$(document).off(Ulo.evts.click, self.hideSuggestions);

				$(document).on("keydown", {self: self}, self.downShortcut);

			}

		},

		/* ---------------------------------------------------------------------------- */

		submit: function(e){

			var self, target, timeout;

			if(e===undefined){

				self = this;
				target = this.getInput();
				timeout = 0;

			} else{

				self = e.data.self;
				target = this;
				timeout = 300;

			}


			self.abort();

			var data = self.serialise(target);

			if(data !== null){

				self.timeout_id = setTimeout(function(){

					self.request( data );

				}, 300);

			} else{

				self.hideSuggestions();

				self.hasSuggestions = false;

			}

		},

		/* ---------------------------------------------------------------------------- */

		request: function(data){

			var self = this;

			Ulo.jqxhr = $.ajax({

					type: "GET",
					data: data,
					url: "/search/autocomplete/"
				
				})

				.done(function(data, sc, xhr){

					if(data !== null){

						self.hasSuggestions = false;

						if(self.post_suggestions(data.posts)){

							self.hasSuggestions = true;

						}
						
						if(self.user_suggestions(data.users)){

							self.hasSuggestions = true;

						}


						if(self.hasSuggestions){

							self.showSuggestions();

						} else{

							self.hideSuggestions();

						}

					}

				})

				.fail(function(xhr){

					console.log(xhr);

				})

				.always(function(){

					Ulo.jqxhr = null;

					this.autocompleteRequest = false;

			});

			this.autocompleteRequest = true;

		}

	}

	/************************************************************************************/

	Ulo.refClass(Autocomplete, "Autocomplete");

	/************************************************************************************/

	/* END AUTOCOMPLETE */
	/* -------------------------------------------------------------------------------- */




	/* NAV SEARCH */
	/* -------------------------------------------------------------------------------- */
	/*
		Manage the opening, closing and submission of the nav bar search form.
	*/
	function NavSearch(){

		this.filterSetup();

		this.autocomplete = new Ulo.Autocomplete("nav");

		this.hasPage = Ulo.checkDependencies(true, "Page");

		$(getElement("nav_search_form")).on("submit", {self: this}, this.submit);
		$(getElement("nav_search_close")).on(Ulo.evts.click, {self: this}, this.close);
	
	}
	
	NavSearch.prototype = {

		constructor: NavSearch,

		/* ---------------------------------------------------------------------------- */
		/*
			Register the search filter button and select the active filter (if any).
		*/
		filterSetup: function(){

			var filter_button = getElement("toggle_search_filters");

			removeClass(filter_button, "disabled");

			menus(filter_button, "height", false, this.selectFilter.bind(this));

			var menu = getElement(filter_button.getAttribute("data-toggle"));
			
			var li = $("li[data-value='" + getElement("nav_search_filter").value + "']", menu)[0];

			if(li !== undefined){

				addClass(filter_button.querySelector("span.icon"), li.getAttribute("data-icon"));
				
				addClass(li, "selected ");

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Select a filter in the menu.
		*/
		selectFilter: function(isClosed, button, menu, target){

			if(isClosed && isDescendant(menu, target, 2)){

				var icon_attr = "data-icon",
				cls = "selected",
				value = "";

				/* Get the current selection. */
				var selected = $("li.selected", menu)[0];

				var icon = button.querySelector("span.icon");

				/*
					
				*/
				if(selected !== undefined){

					removeClass(icon, selected.getAttribute(icon_attr));

				}

				/*
					If the filter has been selected (i.e. the class "selected" has been 
					added):
				*/
				if(toggleClass(target, cls)){

					
					addClass(icon, target.getAttribute(icon_attr));

					/* Get the filter value of the new selection. */
					value = target.getAttribute("data-value");
					
					/* Remove the "selected" class from the current selection. */
					removeClass(selected, cls);
				
				}

				/* Update the search form's filter element. */
				getElement("nav_search_filter").setAttribute("value", value);

				/* Run the autocomplete after updating the filter. */
				this.autocomplete.submit();				

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Close the search form.
		*/
		close: function(){
			
			removeClass(getElement("nav_search_wrapper"), "open");
		
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Refresh, open or submit the search page/form depending on its current state.
		*/
		submit: function(e){

			var self = e.data.self,
			input = getElement("nav_search_input"),
			container = getElement("nav_search_wrapper");

			/* Open the form if only the button is visible */
			if(container.clientWidth < 100){

				input.focus();
				addClass(container, "open");
				
			} 

			/* Submit the form if it has a value */
			else{

				var value = $.trim(input.value);

				if(value !== ""){

					/*
						Do not trigger e.preventDefault() if the page cannot be loaded 
						via an ajax call.
					*/
					if(self.hasPage===false){
			
						return true;
					
					}

					self.autocomplete.hideSuggestions();

					var url = this.getAttribute("action") + "?" + $(this).serialize();	
					
					Ulo.Page.getPage(url, true);

				}

			}
				

			e.preventDefault();

		},

	}

	/* END NAV SEARCH */
	/* -------------------------------------------------------------------------------- */




	/* EMAIL CONFIRMATION */
	/* -------------------------------------------------------------------------------- */

	function EmailConfirmation(){

		this.register();

	}

	EmailConfirmation.prototype = {

		constructor: EmailConfirmation,

		/* ---------------------------------------------------------------------------- */

		register: function(context){

			$(getElement("email_confirmation_form")).on("submit", this.submit);

		},

		/* ---------------------------------------------------------------------------- */

		submit: function(e){

			e.preventDefault();

			if(Ulo.jqxhr===null){

				Ulo.jqxhr = $.ajax({

						type: "GET", 
						url: this.getAttribute("action"), 
						cache: false
					
					})
				
					.done(function(data, sc, xhr){

						console.log(data);
						messages(data.messages);

					})
					
					.fail(function(xhr){

						console.log(xhr);
						xhrErrorMessage(xhr);

					})

					.always(function(){

						Ulo.jqxhr = null;
				
				});

			}
			
		}

	}

	/* END EMAIL CONFIRMATION */
	/* -------------------------------------------------------------------------------- */




	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */
	
	$(function(){

		try{

			try{
			
				/* Polyfiller to remove the 300ms delay on mobile browsers */
				// Origami.fastclick(document.body);
			
			} catch(e){}

			new NavSearch();

			new EmailConfirmation();

			/* Toggle nav sub menu */
			menus("toggle_nsm", "height", "open", false, true);


		} catch(e){

			debug(e);

		}

	});

/* ------------------------------------------------------------------------------------ */

}());



