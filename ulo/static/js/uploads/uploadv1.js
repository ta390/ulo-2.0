/* File Upload javascript file */
/* Dependencies: jQuery, base.js */


"use strict";


/****************************************************************************************/
/* GLOBAL HELPERS FOR FILEUPLOAD, EDITOR, SCALECANVAS AND MOVECANVAS */
/* ------------------------------------------------------------------------------------ */
/* 
	Boolean indicting whether the user is on a mobile device. 
*/
var isMobileDevice = (function(a){return(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))})(navigator.userAgent||navigator.vendor||window.opera)

/* ------------------------------------------------------------------------------------ */
/*
	Polyfill for Number.isFinite.
*/
Number.isFinite = Number.isFinite || function(value){
	return typeof value === "number" && isFinite(value);
}

/* ------------------------------------------------------------------------------------ */
/*
	Polyfill for Number.isNaN.
*/
Number.isNaN = Number.isNaN || function(value) {
    return value !== value;
}

/* ------------------------------------------------------------------------------------ */
/*
	Return the padding values of an element as an object.
	@param elem: javascript element.
*/
function padding(elem){
	try{
		var s = window.getComputedStyle(elem, null)
		return {
			left: parseFloat(s.getPropertyValue("padding-left")),
			right: parseFloat(s.getPropertyValue("padding-right")),
			top: parseFloat(s.getPropertyValue("padding-top")),
			bottom: parseFloat(s.getPropertyValue("padding-bottom"))
		}

	}catch(e){
		var e = $(elem);
		return {
			left: parseFloat(e.css("padding-left")),
			right: parseFloat(e.css("padding-right")),
			top: parseFloat(e.css("padding-top")),
			bottom: parseFloat(e.css("padding-bottom"))
		}
	}
}
/* ------------------------------------------------------------------------------------ */
/*
	Return the css left and right margins of the element as floats in an object.
	@param elem: javascript element.
*/
function margin(elem){
	try{

		var s = window.getComputedStyle(elem, null)
		return {
			left: parseFloat(s.getPropertyValue("margin-left")),
			right: parseFloat(s.getPropertyValue("margin-right")),
			top: parseFloat(s.getPropertyValue("margin-top")),
			bottom: parseFloat(s.getPropertyValue("margin-bottom"))
		};

	}catch(e){

		var e = $(elem);
		return {
			left:	parseFloat(e.css("margin-left")), 
			right:	parseFloat(e.css("margin-right")),
			top:	parseFloat(e.css("margin-top")), 
			bottom:	parseFloat(e.css("margin-bottom"))
		};
	}
}
/* ------------------------------------------------------------------------------------ */
/*
	Exception thrown when a known error occurs during file uploads.
	@param msg: error/information message displayed to the user.
*/
function FileUploadException(msg){
	this.msg = msg || "";
}

/* END GLOBAL HELPER FUNCTIONS FOR FILEUPLOAD, EDITOR, SCALECANVAS AND MOVECANVAS */
/****************************************************************************************/




/****************************************************************************************/
/* BASE CLASS - CREATE A DRAGGABLE ELEMENT */
/* ------------------------------------------------------------------------------------ */
/* 
	Create a draggable element.
	@param id: id of a relative or absolutely positioned html element.
	@param settings: an object of variables to add to the settings object.

	A draggable object can be moved within its bounding box defined by the
	css positions in settings. Override calcPosition to alter its behaviour.
*/
function Draggable(id, settings){
	try{
		/* events */
		this.start_events = "mousedown touchstart pointerdown";
		this.move_events = "mousemove touchmove pointermove";
		this.end_events = "mouseup touchend pointerup";
		/* end events */

		/* client x and y positions when a start event is triggered */
		this.X;
		this.Y;

		/* relative or absolute positioned html element */
		this.element = document.getElementById(id);

		/* default settings */
		this.settings = {
			/* css box boundaries */
			top: 0, right: 0, bottom: 0, left: 0,
			/* the permitted directions of movement */
			horizontal: true, vertical: true,
			/* position offset - see calcPosition() */
			offset: 0
		}
		for(var name in settings){
			/* override or add variables to the settings object */
			this.settings[name] = settings[name];
		}

	} catch(e){
		debug(e);
	}
}

Draggable.prototype = {
	
	constructor: Draggable,

	/*
		Register start events on the element.
	*/
	register: function(){
		$(this.element).off(this.start_events, this.startHandler)
			.on(this.start_events, {self: this}, this.startHandler)
	},
	/*
		Unregister start events on the element.
	*/
	_unregister: function(){
		$(this.element).off(this.start_events, this.startHandler);
	},
	/*

		Function to call when terminating the class. Default behaviour is to
		unregister all start events on the element. override this function to define
		custom behaviour.
	*/
	end: function(){
		this._unregister();
	},
	/*
		Set this.element
		@param elem: javascript element.
	*/
	setElement: function(elem){
		this.element = elem;
	},

	/* EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Set the starting postions (X and Y) and register the move and end events.
	*/
	startHandler: function(e){
		try{

			/* reference to the class */
			var self = e.data.self;

			/* get event start positions or throw an exception, see _getPosition */
			self.X = self._getPosition(e, "X") - this.offsetLeft;
			self.Y = self._getPosition(e, "Y") - this.offsetTop;

			/* add move and end events to the document */
			$(document).on(self.move_events, {self: self}, self.moveHandler);
			$(document).on(self.end_events, {self: self}, self.endHandler);

			/* event handled */
			e.stopPropagation();
			e.preventDefault();

		}catch(e){
			debug(e);
		}
	},
	/*
		Set the new left and top position of the element. See calcPosition.
	*/
	moveHandler: function(e){
		try{
			/* the current class */
			var self = e.data.self;

			/* if horizontal movements are enabled set the left postition */
			if(self.settings.horizontal){
				self.calcPosition(e, "X", "left", "right");
			}
			/* if vertical movements are enabled set the top postition */
			if(self.settings.vertical){
				self.calcPosition(e, "Y", "top", "bottom");
			}

			/* event handled */
			e.stopPropagation();
			e.preventDefault();

		}catch(e){
			debug(e);
		}
	},
	/*
		Remove the move and end events from the document.
	*/
	endHandler: function(e){
		try{
			/* the current class */
			var self = e.data.self;
			/* remove move and end events */
			$(document).off(self.move_events);
			$(document).off(self.end_events);
			/* event handled */
			e.stopPropagation();
			e.preventDefault();	
			
		}catch(e){
			debug(e);
		}
	},
	/* END EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */


	/* ELEMENT POSITIONING */
	/* -------------------------------------------------------------------------------- */
	/*
		Calculate the position of the element, constraining the element to its bounding
		box defined by the css positions in the settings object. Return the new position.
		@param e: the event object.
		@param xy: capitalised "X" or "Y" for axis of movement. 
		@param p1: first bounding position (string e.g. "left" or "top").
		@param p2: second bounding position (string e.g. "right" or "bottom") 
	*/
	calcPosition: function(e, xy, p1, p2){
		/* get current position offset from start position and any user defined offsets */
		var pos = this._getPosition(e, xy) - this[xy] - this.settings.offset;

		/* constrain to the edges of its bounding box */
		if(pos < this.settings[p1]){ 
			return this.settings[p1]; 
		} else if(pos > this.settings[p2]){ 
			return this.settings[p2]; 
		}

		return pos;
	},
	/* END ELEMENT POSITIONING */
	/* -------------------------------------------------------------------------------- */


	/* CLASS HELPER FUNCTIONS */
	/* -------------------------------------------------------------------------------- */
	/*
		Return the pixel value (pix) as a percentage.
		@param pix: pixel length.
		@param len: full length of the element to which pix is a section of.
		@param norm: boolean to indicate if the returned value should be normalised.
	*/
	pixelToPercent: function(pix, len, norm){
		var val = (norm) ? 1 : 100;
		return ((val*pix)/len);
	},
	/* END CLASS HELPER FUNCTIONS */
	/* -------------------------------------------------------------------------------- */


	/* INTERNAL HELPER FUNCTIONS */
	/* -------------------------------------------------------------------------------- */
	/*
		Return the on screen position of the current event or throw an exception.
		@params: e: the event object. 
		@param coord: the client coordinate (must be a capitalised string "X" or "Y") 
	*/
	_getPosition: function(e, coord){
		var pos;
		/* jQuery's normalised pageX/Y */
		if(pos = e["page"+coord]){
			return pos;
		/* raw event data with any scroll offset */
		} else if(pos = e.originalEvent["client"+coord]){
			var scroll = _getScrollOffsets()[coord];
			return pos + scroll;
		/* touch event */
		} else if(e.originalEvent.touches && 
				(pos=e.originalEvent.touches[0]["page"+coord])){
			return pos;
		}
		/* pointer event: REQUIRES TESTING */
		//} else if(e.originalEvent.currentPoint){
			//return e.originalEvent.currentPoint[coord.toLowerCase()];
		//}
		
		throw new Error("Start position was not found");
	},
	/* 
		Return the scroll X and Y offset positions.
		Javascript The Definitive Guide (6th Edition) pg: 391
	*/
	_getScrollOffsets: function(){
		/* All browsers except IE8 and before */
		if(window.pageXOffset != null){
			return {X: window.pageXOffset, Y: window.pageYOffset};
		}
		/* For IE or any browser in standard mode */
		var d = window.document;
		if(d.compatMode == "CSS1Compat"){
			return {X: d.documentElement.scrollLeft, Y: d.documentElement.scrollTop};
		}
		/* For browsers in Quirks mode */
		return {X: d.body.scrollLeft, Y: d.body.scrollTop};
	} 
	/* END INTERNAL HELPER FUNCTIONS */
	/* -------------------------------------------------------------------------------- */

}
/* END BASE CLASS - CREATE A DRAGGABLE ELEMENT */
/****************************************************************************************/




/****************************************************************************************/
/* PREDEFINED FILE PROCESSOR CLASSES */
/* ------------------------------------------------------------------------------------ */

/* SCALE ELEMENT - BASE CLASS DRAGGABLE */
/* ------------------------------------------------------------------------------------ */
/*
	Control the scaling of an element inside the edtior (See class Editor). The element
	is the canvas or video element's container referenced by this.container.
	@param crop: an object containing the width, height and padding of the editor's crop
		area.
	@param id: id of the button/element used to control scaling.
*/
function ScaleElement(crop){

	/* create a horizontal slider from the element #slider_button */
	Draggable.call(this, "image_slider", {"vertical": false});

	/* slider aesthetics - see calcPosition() */
	this.percent = document.getElementById("image_bar");

	/* scale multiplier */
	this.scale = 1;

	/* register the start events for the slider */
	this.register();
}
/* ------------------------------------------------------------------------------------ */
/* Base class Draggable */
ScaleElement.prototype = inherit(Draggable.prototype);
/* ------------------------------------------------------------------------------------ */
/* Override inhertied constructor */
ScaleElement.prototype.constructor = ScaleElement;
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Set this.container to the active element (this is called by the editor each time the 
	user clicks on an element in the editor).
	@param element: canvas or video container.
*/
ScaleElement.prototype.setElement = function(element){
	this.container = element;
	/* update the slider's position to match the scale of this element */ 
	this.setScale(element.scale);
};
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Calculate the position of the slider and pass its value as a percentage to 
	scaleElement().
	@params e,xy,p1,p2: same as base class.
*/
ScaleElement.prototype.calcPosition = function(e, xy, p1, p2){

	try{

		/* get the position in pixels */
		var pos = Draggable.prototype.calcPosition.call(this, e, xy, p1, p2);

		/* convert to a percent between 0 and 100 */
		pos = Math.round(this.pixelToPercent(pos, this.settings.right));
		
		/* scale the image or video */
		this.scaleElement(pos);
		
		/*
			set the position of the slider and the span element (this.percent) which 
			gives the slider bar a background colour.
		*/
		this.setScale(pos+"%");

	} catch(e){
		debug(e);
	}
};
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Store the current slider position as a property on the active element. This value is
	read by setElement to update the slider position each time a new element is selected
	in the Editor.
*/
ScaleElement.prototype.endHandler = function(e){
	/* call base class function */
	Draggable.prototype.endHandler.call(this, e);
	/* set the value of the slider's position on the active element */
	e.data.self.container.scale = e.data.self.element.style.left;
};
/* ------------------------------------------------------------------------------------ */
/*
	Scale the canvas or video container which gives the impression that the canvas or 
	video frame is being scaled on screen.
	@param percent: value between 0 and 100.
*/
ScaleElement.prototype.scaleElement = function(percent){

	try{

		percent = percent*this.scale + 100;

		/* get the base values for this active element */
		var base = this.container.base;
		/* 
			calculate the new width and height of the image, ensuring the values are 
			equal to or greater than the base width and height.
		*/
		var w = Math.max( roundDownTo(base.width*0.01*percent, 2), base.width );
		var h = Math.max( roundDownTo((base.height*0.01)*percent, 2), base.height );

		/* 
			calculate the left and top position of the image. Math.min ensures that 
			the top left corner of the image does not fall within the crop area. 
			Math.max ensures that the bottom right corner does not fall within the 
			crop area.
		*/
		var l = Math.max(
			/* maximum left value (0) */
			Math.min((parseInt(this.container.style.width)-w)*0.5+parseInt(this.container.style.left), 0),
			/* minimum left value (crop area width - image width) */
			this.box.width-w
		);
		var t = Math.max(
			/* maximum top value (0) */
			Math.min((parseInt(this.container.style.height)-h)*0.5+parseInt(this.container.style.top), 0),
			/* minimum top value (crop area height - image height) */
			this.box.height-h
		);

		/* Set the new values on the current element */
		this.container.style.width = w+"px";
		this.container.style.height = h+"px";
		this.container.style.left = l+"px";
		this.container.style.top = t+"px";

	} catch(e){

		debug(e);
	}
};
/* ------------------------------------------------------------------------------------ */
/*
	Update the dimensions of the element container.
	@param container: element's container (scale the container instead of the element).
	@param element: canvas or video element.
	@param crop: an object containing the width and height of an individual element's crop
		area.
*/
ScaleElement.prototype.update = function(container, element, crop){

	/* calc the base width and height of the image - i.e. dimensions when scale is 0% */
	var base = this.dimensions(element, crop.width, crop.height);

	/* set the width and height to their normal values - i.e. no scaling */
	container.style.width = base.width+"px";
	container.style.height = base.height+"px";

	/* centre the image inside its crop area. */
	container.style.left = Math.floor((base.width - crop.width) * -0.5) + "px";
	container.style.top = Math.floor((base.height - crop.height) * -0.5) + "px";

	/* store the base values on the container - accessed by ScaleElement() */
	container.base = base;
	/* set this container to be the active element */
	this.setElement(container);

	/* update the bounding box values - i.e. the clipping mask for each element */
	this.box = {width: crop.width, height: crop.height};

	/* if the element has been scaled then apply its scaling */
	if(container.scale !== undefined){
		this.scaleElement(parseFloat(container.scale));
	}

	/* 
		Set the max slider postition (i.e. the slider button's container length).
		This can change if the editor's view is toggled between portrait and landscape.
	*/
	this.settings.right = this.element.parentNode.clientWidth;

};
/* ------------------------------------------------------------------------------------ */
/*
	Return the scaled width and height of the element maintaining the original aspect 
	ratio. The element is scaled to fit its crop area (mw and mh).
	@param element: canvas or video element.
	@param mw: minimum width the image can be.
	@param mh: minimum height the image can be.
*/
ScaleElement.prototype.dimensions = function(element, mw, mh) {

	var w, h;

	if(element.videoWidth === undefined){
		w = element.width;
		h = element.height;
	} else{
		w = element.videoWidth;
		h = element.videoHeight;
	}

	var ratio = Math.max(mw/w, mh/h);
	
	if(isNaN(ratio) || w <= 0 || h <= 0 || mw <= 0 || mh <= 0){ 
		throw new Error("Invalid canvas or video dimensions");
	}

	return { 
		width: Math.max(w*ratio, mw), 
		height: Math.max(h*ratio, mh),
		ratio: h/w
	};
};
/* ------------------------------------------------------------------------------------ */
/*
	Set the position and styling of the slider element.
	@param percent: string between "0%" and "100%".
*/
ScaleElement.prototype.setScale = function(percent){
	this.element.style.left = this.percent.style.width = percent || "0%";
};
/* END SCALE ELEMENT - BASE CLASS DRAGGABLE */
/* ------------------------------------------------------------------------------------ */


/* MOVE ELEMENT - BASE CLASS DRAGGABLE */
/* ------------------------------------------------------------------------------------ */
/*
	Move the active element within its crop area. The active element is the container of
	the canvas or video element that has been selected by the user in the editor.
	@param crop: an object containing the width, height and padding of the Editor's crop
		area.
*/
function MoveElement(crop){
	/*
		Call the base class constructor omitting the element selector as this is set by 
		the editor when calling setElement for each file processor.
	*/
	Draggable.call(this, null);
}
/* ------------------------------------------------------------------------------------ */
/* Base class Draggable */
MoveElement.prototype = inherit(Draggable.prototype);
/* ------------------------------------------------------------------------------------ */
/* Override inhertied constructor */
MoveElement.prototype.constructor = MoveElement;
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Set the elements left or top values to control its postion. The element cannot fall
	within its crop area.
	@params e,xy,p1,p2: same as base class.
*/
MoveElement.prototype.calcPosition = function(e, xy, p1, p2){
	try{
		/* 
			get the width or height of the container depending on the direction of 
			movement.
		*/
		var wh = (xy==="X") ? this.element.clientWidth : this.element.clientHeight;

		/* calculate the difference between the crop area and the current w or h */
		var diff = this.settings[p2]-wh;

		/* get the elements position minus its left (X) or top (Y) padding */
		var pos = this._getPosition(e, xy) - this[xy] - this.element.padding[xy];

		/* ensure the container does not fall within its crop area */
		if(pos > this.settings[p1]){
			pos = this.settings[p1];
		}else if(pos < diff){ 
			pos = diff; 
		}

		/* set the containers new left or top position */
		this.element.style[p1] = pos+"px";
		
	} catch(e){
		debug(e);
	}

};
/* ------------------------------------------------------------------------------------ */
/*
	Update the bounding box values which define where the container can move.
	@param container: element's container (scale the container instead of the element).
	@param element: canvas or video element.
	@param crop: an object containing the width and height of an individual element's crop
		area.
*/
MoveElement.prototype.update = function(container, element, crop){

	/* update the bounding box values */
	this.settings.right = crop.width;
	this.settings.bottom = crop.height;

	/* store the left and top padding on the container - see calcPosition() */
	var p = padding(container.parentNode);
	container.padding = { X: p.left, Y: p.top };

	/* set this container to be the active element */
	this.setElement(container);
	/* register start events for this element */
	this.register();

};
/* END MOVE ELEMENT - BASE CLASS DRAGGABLE */
/* ------------------------------------------------------------------------------------ */


/* VIDEO THUMBNAIL - BASE CLASS DRAGGABLE */
/* ------------------------------------------------------------------------------------ */
/*
	Select a point in the video to use as the thumbnail image.
	@param crop: an object containing the width, height and padding of the editor's crop
		area.
*/
function VideoThumbnail(crop){

	/* create a horizontal slider from the element #video_slider */
	Draggable.call(this, "video_slider", {"vertical": false});

	/* store reference to the input element that displays the current time */
	this.current_time = getElement("video_time");

	/* register the start events for the slider */
	this.register();
}
/* ------------------------------------------------------------------------------------ */
/* Base class Draggable */
VideoThumbnail.prototype = inherit(Draggable.prototype);
/* ------------------------------------------------------------------------------------ */
/* Override inhertied constructor */
VideoThumbnail.prototype.constructor = VideoThumbnail;
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Add a simple animation over the video while the user is moving the slider.
*/
VideoThumbnail.prototype.startHandler = function(e){
	/* display the editor's animation div */
	removeClass(getElement("processing"), "hide");
	/* call base class function */
	Draggable.prototype.startHandler.call(this, e);
};
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Remove the animation added by the startHandler.
*/
/* ------------------------------------------------------------------------------------ */
VideoThumbnail.prototype.endHandler = function(e){
	/* hide the editor's animation div */
	addClass(getElement("processing"), "hide");
	/* call base class function */
	Draggable.prototype.endHandler.call(this, e);
};
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Register an event to update the slider position when the user enters a time in the
	input field.
*/
VideoThumbnail.prototype.register = function(){
	/* register event for the input element */
	var evt = isEventSupported("input") ? "input" : "change";
	$(this.current_time).off(evt, this.timeHandler)
		.on(evt, {self: this}, this.timeHandler);

	/* register event for the slider */
	Draggable.prototype.register.call(this);
};
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Set the container as the new active element and update the slider position to the
	current time of this video.
	@param element: video element's container.
*/
VideoThumbnail.prototype.setElement = function(element){
	
	this.container = element;
	this.video = element.video; /* See update() */
	
	this.element.style.left = this.secsToPercent( this.video.currentTime );
	this.current_time.value = this.secsToTime( this.video.currentTime );
};
/* ------------------------------------------------------------------------------------ */
/*
	OVERRIDE
	Set the slider position and current video time.
	@params e,xy,p1,p2: same as base class.
*/
VideoThumbnail.prototype.calcPosition = function(e, xy, p1, p2){

	try{

		/* get the slider position in pixels */
		var pos = Draggable.prototype.calcPosition.call(this, e, xy, p1, p2);
		/* convert it to a percentage between 0 and 100 */
		pos = Math.round(this.pixelToPercent(pos, this.settings.right));

		/* set the thumbnail slider position */
		this.element.style.left = pos+"%";

		/* set the current time on the video */
		this.video.currentTime = this.delta * pos;
		this.current_time.value = this.secsToTime( this.video.currentTime );

	} catch(e){

		debug(e);
	}
};
/* ------------------------------------------------------------------------------------ */
/*
	Convert seconds into a string percentage of the video's duration.
	@param secs: integer.
*/
VideoThumbnail.prototype.secsToPercent = function(secs){
	return (100*secs / this.video.duration)+"%";
}
/* ------------------------------------------------------------------------------------ */
/*
	Convert seconds into the time format h:m:s.
	@param secs: integer.
*/
VideoThumbnail.prototype.secsToTime = function(secs){

	/* minutes divisor */
	var md = secs % 3600;

	var h = Math.floor(secs / 3600);
	var m = Math.floor(md / 60);
	var s = Math.ceil(md % 60);

	return (h === 0 ? "" : h+":") + m+":" + (s < 10 ? "0"+s : s);
	
};
/* ------------------------------------------------------------------------------------ */
/*
	Convert time in the format h:m:s, m:s or s (where ":" can be any string) into seconds.
	Return null if time is empty or invalid.
	@param time: a string representing the time.
*/
VideoThumbnail.prototype.timeToSecs = function(time){

	var t=time.split(/[^\d]+/), secs=null;

	/* if the time entered is h:m:s or m:s or s... */ 
	if(t.length > 0 && t.length < 4){

		/* last index, hours minutes seconds multiplier, seconds in a minute */
		var idx = t.length - 1, m = 1, s = 60;

		/*
			if the time is in seconds (idx===0) or the minutes and seconds
			are less than 60 then the time is valid.
		*/
		if(idx === 0 || t[idx] < s && t[idx-1] < s ){
	
			/* convert the h:m:s time into seconds */
			for(var i=idx; i>=0; --i){

				/* ignore trailing characters */
				if(t[i] === ""){ break; }

				secs += t[i]*m;
				m *= s;
				
			}
		}
	} 

	return secs;
};
/* ------------------------------------------------------------------------------------ */
/*
	"input" or "change" event handler to update the thumbnail and slider position when the
	user enters a time in the input field.
*/
VideoThumbnail.prototype.timeHandler = function(e){

	var self = e.data.self;
	var secs = self.timeToSecs(this.value);

	if(secs !== null && secs <= self.video.duration){
		
		self.element.style.left = self.secsToPercent(secs);
		self.video.currentTime = secs;
	
	}
}
/* ------------------------------------------------------------------------------------ */
/*
	Store the container, video and time values of the video element for use throughout
	the class.
	@param container: video container (scale the container instead of the video).
	@param video: video element.
	@param crop: an object containing the width and height of an individual element's crop
		area
*/
VideoThumbnail.prototype.update = function(container, video, crop){

	/* store the video as a property on its container */
	container.video = video;

	/* set the container as the active element */
	this.setElement(container);

	/* store the value of a percent change */
	this.delta = video.duration / 100;

	/* Set the max right position of the slider */
	this.settings.right = this.element.parentNode.clientWidth;

};
/* END VIDEO THUMBNAIL - BASE CLASS DRAGGABLE */
/* ------------------------------------------------------------------------------------ */

/* END PREDEFINED FILE PROCESSOR CLASSES */
/****************************************************************************************/




/* BASE CLASS - EDITOR */
/* ------------------------------------------------------------------------------------ */
/*
	The class manages the display of uploaded files and relys on file processors
	defined by subclasses to specify how files are displayed and manipulated in the
	editor.


	FILE PROCESSOR CLASSES (see ScaleCanvas and MoveCanvas for examples):
	A file processor can be any class that defines an update(), setElement() and end() 
	function.

	**************************************************************************************
	function update(container, canvas, crop){ ... }
	**************************************************************************************
	update is called each time an element is added to or removed from the editor. Three
	parameters are passed to the function.
	
	@param container: The canvas container. The css stylsheet gives the canvas in the 
	editor a width and height of 100%. The container can therefore be used to control the
	on screen width and height of the canvas.

	@param canvas: the canvas element that hold the image data or video frame.

	@param crop: an object container the width, height and padding of the editor. The
	padding value does not change once the editor is open however the width or height is
	calculated as the editor's width or height divided by the number of elements in the 
	editor at that momemnt. The choice of width or height depends of the current 
	orientation of the editor window. See this.stacked


	**************************************************************************************
	function setElement(container){ ... }
	**************************************************************************************
	setElement is called each time a user clicks on an elements in the editor. One
	parameter is passed to the function.

	@param container: The canvas or video container that the user clicked on.

	**************************************************************************************
	function end(){ ... }
	**************************************************************************************
	end is called each time the editor is closed. It takes no arguments.



	@param max_img: maximum number of image files a user can upload.
	@param max_vid: maximum number of video files a user can upload.
	@param img_ar: optional object containing the width/height aspect ratio for images.
	@param vid_ar: optional object containing the width/height aspect ratio for videos.
*/
function Editor(max_img, max_vid, img_ar, vid_ar){

	/* file types - SAME AS FILEUPLOAD */
	this.image = "image";
	this.video = "video";
	/* button data attribute - SAME AS FILEUPLOAD */
	this.btn_data = "data-id";

	/* class names */
	this.c_view = "stacked";
	this.c_hide = "hide";
	this.c_open = "modal_open";
	this.c_hidden = "hidden";
	this.c_remove  = "remove";
	this.c_active = "active";
	this.c_disabled = "disabled";

	/* max number of images/videos a user can upload */
	this.max_img = max_img || 0;
	this.max_vid = max_vid || 0;

	/* aspect ratios for the file types */
	this.aspectRatio = {
		"image": img_ar || {width: 4, height: 3},
		"video": vid_ar || {width: 16, height: 9} // video thumbnail.
	}

	/* editor container */
	this.modal = getElement("editor");
	/* uploaded file container */
	this.content = getElement("editor_content");
	/* close editor button */
	this.cancel = getElement("editor_close");
	/* submit edited content button */
	this.submit = getElement("editor_submit");

	/* button to change the aspect ratio of the editor */
	this.view = getElement("editor_view");
	/* button to remove an element from the editor button */
	this.rmv_file = getElement("rmv_file");
	/* button to add a new file to the editor */
	this.add_file = getElement("add_file");


	/* set to true when an element has been fully loaded into the editor */
	this.stacked = false;
	this.isClosed = true;


	this.processors = [];
	this.assignProcessors();

	this.loading = 0;

}
Editor.prototype = {

	/* EVENTS */
	/* -------------------------------------------------------------------------------- */
	/*
		Register an event to an element.
		@param element: javascript element.
		@param handler: event handler.
		@param context: object to pass to the handler.
		@param attrs: object of attributes to add to the element.
	*/
	register: function(element, handler, context, attrs){
		for(var n in attrs){
			element.setAttribute(n, attrs[n]);
		}
		$(element).off(Ulo.evts.click, handler).on(Ulo.evts.click, context, handler);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Register an event on the close editor button.
		@param handler: event handler.
		@param context: object to pass to the handler.
		@param attrs: object of attributes to add to the element.
	*/
	registerClose: function(handler, context, attrs){
		this.register(this.cancel, handler, context, attrs);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Register an event on the submit editor content button.
		@param handler: event handler.
		@param context: object to pass to the handler.
		@param attrs: object of attributes to add to the element.
	*/
	registerSubmit: function(handler, context, attrs){
		this.register(this.submit, handler, context, attrs);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Register an event and display the button which toggles the view.
	*/
	registerView: function(){
		removeClass(this.view, this.c_hide);
		this.register(this.view, this.viewHandler, {self: this});
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Register the focusHandler on an element.
		@param element: javascript element.
	*/
	registerFocus: function(element){
		/* start events used by Draggable */
		var start_events = "mousedown touchstart pointerdown";
		$(element).off(start_events, this.focusHandler)
			.on(start_events, {self: this}, this.focusHandler);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Unregister an event on an element.
		@param element: javascript element.
		@param handler: event handler.
	*/
	unregister: function(element, handler){
		$(element).off(Ulo.evts.click, handler);
	},
	/*
		unregister all events on the close editor button.
	*/
	unregisterClose: function(){
		$(this.cancel).off();
	},
	/* -------------------------------------------------------------------------------- */
	/*
		unregister all events on the submit editor content button.
	*/
	unregisterSubmit: function(){
		$(this.submit).off();
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Unegister view event and hide the button.
	*/
	unregisterView: function(){
		addClass(this.view, this.c_hide);
		this.unregister(this.view, this.viewHandler);
	},
	/* END EVENTS */
	/* -------------------------------------------------------------------------------- */
	

	/* EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Close editor event handler.
	*/
	closeHandler: function(e){
		e.data.self.close();
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Set the class property active_element to the element in focus and call setElement 
		for each file processor to assign the element in focus.
	*/
	focusHandler: function(e){
		e.data.self.activateElement(e.currentTarget.parentNode, false);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Toggle the view of the editor between landscape and portrait.
	*/
	viewHandler: function(e){

		var self = e.data.self;

		if(self.type == self.image){

			/* toggle the class .stacked on the content div */
			self.stacked = toggleClass(self.content, self.c_view);

			/* swap the aspect ratio values */
			var ar = self.aspectRatio[self.type];
			self.aspectRatio[self.type] = {width: ar.height, height: ar.width};

			/* swap the crop area values */
			var w = self.crop.width;
			self.crop.width = self.crop.height;
			self.crop.height = w;

			/* set the new dimensions for the editor */
			self.content.style.width = self.crop.width+"px";
			self.content.style.height = self.crop.height+"px";

			self.update(true);
		}


	},
	/* -------------------------------------------------------------------------------- */
	/*
		Toggle the diplay of the button that removes an element from the editor. The
		button is toggled for the current element in focus.
	*/
	toggleRemove: function(e){

		var self = e.data.self;

		if(self.active_element){
			/*
				toggle the class remove on the active element to show/hide the button. If 
				the button is visible then add a document click event to hide it.
			*/
			if(toggleClass(self.active_element, self.c_remove)){

				/* the button that removes the current element */
				var btn = e.currentTarget;
				/* the current element */
				var elem = self.active_element;

				$(document).one(Ulo.evts.click, function(e){

					if(e.currentTarget !== btn){
						removeClass(elem, self.c_remove);
					}
				});
				
				/* prevent the event from bubbling */
				e.stopPropagation();
				e.preventDefault();
				
			}
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove an element from the editor and update the editor.
	*/
	removeElement: function(e){

		var self = e.data.self;

		removeElements(self.active_element);
		self.active_element = undefined;

		try{

			if(self.count() === 0){
				self.forceClose();
			} else{
				self.update(); 
			}
		
		} catch(e){ 
		
			/* update will thrown an exception if an error occurs */
			messages("Sorry, the editor has stopped working. Please try again."); 
		
		}

	},
	/* END EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */


	/* LOADING FILES */
	/* -------------------------------------------------------------------------------- */
	/*
		Disable the editor by displaying the loading animation over the content and
		disabling the controls. Increment this.loading to count the number of files
		loaded into the editor. (disable() is called by open()).
	*/
	disable: function(){

		++this.loading;

		/* display the processing animation */
		removeClass(getElement("processing"), this.c_hide);
		/* disable the editor buttons */
		addClass(getElement("editor_footer"), this.c_disabled);
	},
	/*
		Enable the editor if no more files are loading by removing the loading animation 
		placed over the content and footer. (enable() is called by add()).
	*/
	enable: function(checkPending){
		
		--this.loading;

		if(this.loading <= 0){
			/* reset just in case */
			this.loading = 0;
			/* hide the processing animation */
			addClass(getElement("processing"), this.c_hide);
			/* disable the editor buttons */
			removeClass(getElement("editor_footer"), this.c_disabled);
		}
		
	},
	/* END LOADING FILES */
	/* -------------------------------------------------------------------------------- */


	/* EDITOR WINDOW */
	/* -------------------------------------------------------------------------------- */
	/*
		Open the editor
		@param type: file type being added to the editor ("image" or "video")
	*/
	open: function(type){

		/* if this is the first time opening the editor */
		if(this.isClosed){

			this.loading = 0;

			/* store the file type being added to the editor */
			this.type = type;

			if(type === this.video){
				/* remove "image" class from the container */
				removeClass(this.modal, this.image);
				/* disabled the button that toggles the editor's view */
				this.unregisterView();
			} else{
				/* remove "video" class from the container */
				removeClass(this.modal, this.video);
				/* enable the button that toggle the editor's view */
				this.registerView();	
			}
			/* set the type as a class on the container */
			addClass(this.modal, type);


			/* register the editor's close handler */
			this.registerClose(this.closeHandler, {self: this});
			/* register event to remove the active element from the editor */
			this.register(this.rmv_file, this.toggleRemove, {self: this});


			/* set the value of this.crop */
			this.setCrop(type);
			/* set the dimensions of the editor */
			this.content.style.width = this.crop.width+"px";
			this.content.style.height = this.crop.height+"px";

			/* empty the editor's content */
			emptyElement(this.content);
			/* call the constructor of each file processor */
			this.startProcessors(this.crop, type);
		}

		/* disable editor interactions - calls to add() will enable editor interactions */
		this.disable();

		this.isClosed = false;
		this.show();

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Close the editor.
	*/
	close: function(){
		
		try{
			/* hide the editor window */
			this.hide();

			/* empty the editor's content */
			emptyElement(this.content);

			/* call end() for each file processor */
			this.endProcessors();

			/* remove all events added to the close and submit buttons */
			this.unregisterClose();
			this.unregisterSubmit();

			/* remove the events to toggle the view and remove a file */
			this.unregister(this.view, this.viewHandler);
			this.unregister(this.rmv_file, this.toggleRemove);

			this.isClosed = true;

		} catch(e){

			debug(e);
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Force the editor to close by triggering all close event handlers. (All handlers
		added using registerClose())
	*/
	forceClose: function(){
		$(this.cancel).trigger(Ulo.evts.click);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Add a new element to the editor and return a button that remove the element from
		the editor.
		@param element: image, video or canvas element.
		@param type: file type ("image" or "video");
	*/
	add: function(element, type){

		try{

			/* Convert an image to a canvas (return videos as they are). */
			var el = this.toCanvas(element, 4096);

			/* ADD A NEW ELEMENT TO THE EDITOR */
			/* create a clipping mask to crop each element individually */
			var mask = makeElement("div", {"class": "file_mask"});

			/* wrap the element in a div to control the on screen scaling */
			var container = makeElement("div", {"class": "file_container"});

			/* add an element that gives the user a visual clue to which element is active */
			var span = makeElement("span", {"class": "selected"});

			/* create a button that removes this element */
			var button = makeElement("button",{"class":"remove_file", "type":"button"});
			/* set the element's id as an attribite on the button so it can be found */
			button.setAttribute(this.btn_data, element.getAttribute("id"));


			/* create a span to display a trash can icon inside the button */
			var icon = makeElement("span", {"class": "icon trash"});

			/* append the elements together - el must be container's firstChild */
			mask.appendChild(container).appendChild(el); /* canvas or video element */
			mask.appendChild(button).appendChild(icon); /* remove file button */
			mask.appendChild(span); /* active element indicator */

			/* add the new element to the editor */
			this.content.appendChild(mask);
			/* END ADD A NEW ELEMENT TO THE EDITOR */


			/* register a click event on the button that will remove the element */
			this.register(button, this.removeElement, {self: this});
			/* register a click event on the container that will set it active */
			this.registerFocus(container);
			/* update the editor and its file processors with the new element */
			this.update();


			/*
				enable editor interaction if the image has finished loading. 
				See toCanvas() for the assignment of el.loading.
			*/
			if(el.loading !== true){
				
				this.enable();
			}
			
		

			return button;


		} catch(e){

			debug(e);

			if(!el || el.loading !== true){
				/* enable editor interactions */
				this.enable();
			}

			/* throw known errors */
			if(e instanceof FileUploadException){
				throw e;
			}

			/* else throw a generic error */
			throw new FileUploadException(
				"We could not open your "+(type||"file")+" in the editor, please try again."
			);

		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Update all elements in the editor after an element has been added or removed.
	*/
	update: function(){

		try{

			/* get all the elements in the editor */
			var nodes = this.content.childNodes;

			/* get the crop area dimensions of a single file */
			var crop = this.getCrop(nodes.length);

			var xy, wh;

			if(this.stacked){
				xy = "top";
				wh = "height";	
			} else{
				xy = "left";
				wh = "width";
			}

			/* update the dimensions of each element in the editor. */
			for(var i=0; i<nodes.length; ++i){

				/* element's clipping mask */
				var mask = nodes[i];
				/* element's container that controls scaling */
				var container = mask.firstChild;
				/* canvas or video element */
				var element = container.firstChild;

				/* update the dimensions of the clipping mask */
				mask.style.width = crop.width+"px";
				mask.style.height = crop.height+"px";
				mask.style.left = mask.style.top = null;
				/* set the value of left or top depending on the editor's orientation */
				mask.style[xy] = (i===0 ? 0:crop.padding.left) + i*crop[wh] + "px";

				/* run each element through the file processors */
				this.updateProcessors(container, element, crop);	

			}

			/* set the last element to be the active element */
			this.activateElement(nodes[nodes.length-1], false);
			/*
				display the button to remove individual elements if the editor has more 
				than one element (else have the user use the editor's close button).
			*/
			this.toggleDisplay(nodes.length>1, this.rmv_file);
			/*
				display the button to add more files to the editor if the max for the
				current file type has not been reached.
			*/
			this.toggleDisplay(this.maxFiles(this.type)>nodes.length, this.add_file);


		} catch(e){

			/*
				If the editor fails to update then trigger all events assigned to the 
				editor's close button.
			*/
			this.forceClose();
			throw e;
		}

	},
	/* EDITOR WINDOW */
	/* -------------------------------------------------------------------------------- */


	/* CANVAS SCALING */
	/* -------------------------------------------------------------------------------- */
	/*
		Scale the source image (src) into the destination image (dst).

		If isWorkerThread is true src and dst must be ArrayBuffers and the function will
		return the destination buffer (dst), else they must be Uint8ClampedArrays and 
		the function will not return a value.

		@param src: original image (ArrayBuffer or Uint8ClampedArray).
		@param sw: src width.
		@param sw: src height.
		@param dst: scaled image (ArrayBuffer or Uint8ClampedArray).
		@param dw: dst width.
		@param dw: dst height.
		@param scale: scale value (float).
		@param isWorkerThread: true if called from a worker and false if not.
	*/
	bilinear: function(src, sw, sh, dst, dw, dh, scale, isWorkerThread){

		/* Loop variables. */
		var iy_max = sh-1;
		var ix_max = sw-1;
		var i, ix,ix0,ix1,iy,iy0,iy1, p1,p2,p3,p4, w1,w2,w3,w4, dx,dx1,dy,dy1;

		/* Uint8ClampedArray is supported from IE 11, Uint8Array from IE 10. */
		if(isWorkerThread){
			src = new Uint8Array(src);
			dst = new Uint8Array(dst);
		}

		for(var y=0; y<dh; ++y){

			iy  = y / scale;
			iy0 = Math.floor(iy);
			iy1 = Math.ceil(iy);
			if(iy1 > iy_max){ iy1=iy_max; }

			for(var x=0; x<dw; ++x){
			
				ix  = x / scale;
				ix0 = Math.floor(ix);
				ix1 = Math.ceil(ix);
				if(ix1 > ix_max){ ix1=ix_max; }
				
				/* Calc the weights for each pixel. */
				dx = ix-ix0; 
				dy = iy-iy0;
				dx1 = 1.0-dx;
				dy1 = 1.0-dy;
				w1 = dx1 * dy1;
				w2 = dx * dy1;
				w3 = dx1 * dy;
				w4 = dx * dy;

				/* Index positions of the surrounding pixels. */
				p1 = (ix0 + sw * iy0) << 2;
				p2 = (ix1 + sw * iy0) << 2;
				p3 = (ix0 + sw * iy1) << 2;
				p4 = (ix1 + sw * iy1) << 2;

				i = (y*dw+x) << 2;

				dst[i]   = (src[p1] * w1 + src[p2] * w2 + src[p3] * w3 + src[p4] * w4); //& 0xff;
				dst[i+1] = (src[p1+1]*w1 + src[p2+1]*w2 + src[p3+1]*w3 + src[p4+1]*w4); //& 0xff;
				dst[i+2] = (src[p1+2]*w1 + src[p2+2]*w2 + src[p3+2]*w3 + src[p4+2]*w4); //& 0xff;
				dst[i+3] = (src[p1+3]*w1 + src[p2+3]*w2 + src[p3+3]*w3 + src[p4+3]*w4); //& 0xff;
			}
		}

		/*
			Return the destination array buffer if called from a web worker.
		*/
		if(isWorkerThread){
			return dst.buffer;
		}
		
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Scale the source image (src) into the destination image (dst).

		If isWorkerThread is true src and dst must be ArrayBuffers and the function will
		return the destination buffer (dst), else they must be Uint8ClampedArrays and 
		the function will not return a value.

		NOTE: CAN PRODUCE SPECS OF RGB PIXELS WHEN DOWN SAMPLING LARGE IMAGES TO SMALL
		SIZES.

		@param src: original image (ArrayBuffer or Uint8ClampedArray).
		@param sw: src width.
		@param sw: src height.
		@param dst: scaled image (ArrayBuffer or Uint8ClampedArray).
		@param dw: dst width.
		@param dw: dst height.
		@param scale: scale value (float).
		@param isWorkerThread: true if called from a worker and false if not.
	*/
	bicubic: function(src, sw, sh, dst, dw, dh, scale, isWorkerThread) {

		/* Loop variables */
		var dx, dy;
		var repeatX, repeatY;
		var r0, r1, r2, r3;
		var c0, c1, c2, c3;
		var v0, v1, v2, v3;
		var yv, y0, xv, x0, i;

		/* Uint8ClampedArray is supported from IE 11, Uint8Array from IE 10. */
		if(isWorkerThread){
			src = new Uint8Array(src);
			dst = new Uint8Array(dst);
		}
		
		for (var y=0; y < dh; ++y) {
			
			yv = y / scale;
			y0 = Math.floor(yv);

			repeatY = 0;
			if(y0 < 1){
				repeatY = -1;
			} else if(y0 > sh - 3){
				repeatY = y0 - (sh - 3); 
			}

			for (var x=0; x < dw; ++x) {
				
				xv = x / scale;
				x0 = Math.floor(xv);

				repeatX = 0;
				if(x0 < 1){
					repeatX = -1;
				} else if(x0 > sw - 3){
					repeatX = x0 - (sw - 3);
				}

				r1 = (y0 * sw + x0) << 2;
				r0 = repeatY < 0 ? r1 : ((y0-1) * sw + x0) << 2;
				r2 = repeatY > 1 ? r1 : ((y0+1) * sw + x0) << 2;
				r3 = repeatY > 0 ? r2 : ((y0+2) * sw + x0) << 2;

				c0 = repeatX < 0 ? 0 : -4;
				c2 = repeatX > 1 ? 0 : 4;
				c3 = repeatX > 0 ? c2 : 8;

				dx = xv - x0; 
				dy = yv - y0;
				i = (y*dw+x) << 2;

				/* rgba values: j===0 is r, j===1 is g, j===2 is b, j===3 is a */ 
				for(var j=0; j<4; ++r0, ++r1, ++r2, ++r3, ++i, ++j){

					v0 = 0.5*(src[r0+c2]-src[r0+c0]+(2*src[r0+c0]-5*src[r0]+4*src[r0+c2]-src[r0+c3]+
						(3*(src[r0]-src[r0+c2])+src[r0+c3]-src[r0+c0])*dx)*dx)*dx+src[r0];

					v1 = 0.5*(src[r1+c2]-src[r1+c0]+(2*src[r1+c0]-5*src[r1]+4*src[r1+c2]-src[r1+c3]+
						(3*(src[r1]-src[r1+c2])+src[r1+c3]-src[r1+c0])*dx)*dx)*dx+src[r1];

					v2 = 0.5*(src[r2+c2]-src[r2+c0]+(2*src[r2+c0]-5*src[r2]+4*src[r2+c2]-src[r2+c3]+
						(3*(src[r2]-src[r2+c2])+src[r2+c3]-src[r2+c0])*dx)*dx)*dx+src[r2];

					v3 = 0.5*(src[r3+c2]-src[r3+c0]+(2*src[r3+c0]-5*src[r3]+4*src[r3+c2]-src[r3+c3]+
						(3*(src[r3]-src[r3+c2])+src[r3+c3]-src[r3+c0])*dx)*dx)*dx+src[r3];

					dst[i] = 0.5*(v2-v0+(2*v0-5*v1+4*v2-v3+(3*(v1-v2)+v3-v0)*dy)*dy)*dy+v1;
				}

			}
		}
		
		/*
			Return the destination array buffer if called from a web worker.
		*/
		if(isWorkerThread){
			return dst.buffer;
		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Create an inline web worker for scaling functions and return the worker instance. 
		See scale() or scaleImageData().

		@param fn: function to call inside the worker's onmessage function. The function
			must return a MessagePort or ArrayBuffer.
		@param args: string of argument names or values passed to the function.
		@param errorCallback: function called if the worker thread throws an exception.
	*/
	createWorker: function(fn, args, errorCallback){

		/*
			Create a file like object using the Blob constructor. The onmessage function
			will return fn's return value as a transferable object so it must return a
			MessagePort or ArrayBuffer.
		*/
		var blob = new Blob([
			"onmessage = function(e){ ",
				"var d = (", fn.toString(), ")(", args, ");",
				"postMessage(d, [d]);",
			"}"
		]);
		
		/* Create a blob url passing the file to the worker constructor. */
		var url = window.URL.createObjectURL(blob);
		/* Create a worker instance. */
		var worker = new Worker(url);
		/* Revoke the blob url to free up memory. */
		window.URL.revokeObjectURL(url);

		/* Define a standard error handler for each worker created. */
		var self = this;
		worker.onerror = function(e){

			messages("Sorry, we encountered a problem while trying to edit your image.");
			
			worker.terminate();
			e.preventDefault();
			
			if(errorCallback){
				errorCallback.call(self);
			}

			debug(e);
		}

		/* Normalise postMessage. */
		worker.postMessage = worker.webkitPostMessage || worker.postMessage;

		return worker;

	},
	/* -------------------------------------------------------------------------------- */
	/* SCALE CANVAS IN ONE GO - VERSION ONE */
	/* -------------------------------------------------------------------------------- */
	/*
		Scale the canvas in place and return the thread used ("main" or "worker").

		NOTE: putImageData DOES NOT WORK CONSISTENTLY - USE drawImage IF SCALE STARTS
		RETURNING BLANK IMAGES.

		@param canvas: source image canvas.
		@param scale: scale value (float).
		@param mw: minimum scale width.
		@param mh: minimum scale height.
		@param scaleCallback: scale function: bilinear() or bicubic().
		@param workerCallback: called on completion of the worker thread and is passed the
			scaled canvas as its only argument.
		@param errorCallback: called if the worker throws an exception, takes no args.
	*/
	scale: function(canvas, scale, mw, mh, scaleCallback, workerCallback, errorCallback){

		try{
			/*
				Get the original image data - if the canvas is too large the browser may 
				throw an exception.
			*/
			var src = canvas.getContext("2d").getImageData(0,0,canvas.width,canvas.height);

			/*
				Create an ImageData instance to the scaled dimensions.
			*/
			var dst = canvas.getContext("2d").createImageData(
				Math.max(Math.round(canvas.width*scale), mw),
				Math.max(Math.round(canvas.height*scale), mh)
			);

			/*
				If dst is null the canvas dimensions are too large.
			*/
			if(dst === null){ 
				throw new Error(); 
			}

		} catch(e){

			throw new FileUploadException("The browser you are using does not support \
				editing of large images.");
		
		}

		/*
			Scale the canvas using a web worker if given a callback function.
		*/
		if(workerCallback && window.Worker){

			try{

				/* Create a web worker to run scaleCallback. */
				var args = "e.data.src, e.data.sw, e.data.sh, \
					e.data.dst, e.data.dw, e.data.dh, e.data.scale, true";
				var worker = this.createWorker(scaleCallback, args, errorCallback);

				/*
					Add onmessage handler to fill the original canvas with the scaled image 
					data and run the workerCallback function.
				*/
				var self = this;
				worker.onmessage = function(e){

					worker.terminate();

					/*
						If dst was transfered then its buffer will have been neutered so
						reassign it an empty ImageData object.
					*/
					if(dst.data.buffer.byteLength === 0){
						dst = canvas.getContext("2d").createImageData(dst.width,dst.height);
					}

					/* Set the array buffer on the scaled ImageData object. */
					dst.data.set(new Uint8Array(e.data));
					/* Set the original canvas dimensions to the scaled dimensions. */
					canvas.width=dst.width; canvas.height=dst.height;
					/* Add the data to the original canvas. */
					canvas.getContext("2d").putImageData(dst, 0, 0);
					dst = null;

					workerCallback.call(self, canvas);
					
				}

				/* Set the scaleCallback parameter values. See args above. */
				var data = {
					src:src.data.buffer, sw:src.width, sh:src.height,  
					dst:dst.data.buffer, dw:dst.width, dh:dst.height, 
					scale:scale
				};

				/* Send the data to the worker - src and dst are sent as transferable objects. */
				worker.postMessage(data, [data.src, data.dst]);

				return "worker";

			} catch(e){

				if(worker){
					worker.terminate();
				}

				/* Continue and attempt to perform the scale on the main thread. */

			}

		}

		/* Perform the scale on the main thread. */
		scaleCallback(src.data, src.width, src.height, dst.data, dst.width, dst.height, scale, false);
		
		/* 
			Set canvas to the scaled dimensions and add the scaled image data to the 
			original canvas.
		*/
		canvas.width = dst.width; canvas.height = dst.height;
		canvas.getContext("2d").putImageData(dst, 0, 0);
		dst = null;

		return "main";
	},
	/* -------------------------------------------------------------------------------- */
	/* END SCALE CANVAS IN ONE GO - VERSION ONE  */
	/* -------------------------------------------------------------------------------- */
	/* SCALE CANVAS IN CHUNKS - VERSION TWO
	/* -------------------------------------------------------------------------------- */
	/* 
		Fill an array with the x and y offset multipliers used to calculate the x and y 
		offset positions for each chunk of data.

		@param x: Number of chunks along the x axis.
		@param y: Number of chunks along the y axis.
		@param array: array to fill.
	*/
	chunkIndices: function(x, y, array){

		for(var i=0; i<y; ++i){
			for(var j=0; j<x; ++j){
				array.push({x:j, y:i});
			}
		}
		/*
			Chrome fix - The first call to putImageData is hit and miss, so run an 
			extra loop.
		 */
		 // if(array.length > 0){
		 // 	array.push(array[array.length-1]);
		 // }
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return the canvas containing the scale image data. If transfer is true place the
		destination canvas data in the src canvas. Clear the pixels of the unused canvas.

		@param src: source image canvas.
		@param dst: destination image canvas.
		@param transfer: true if dst should be placed in src.
	*/
	setImageData: function(src, dst, transfer){
		
		if(transfer){
			src.width=dst.width; src.height=dst.height;
			src.getContext("2d").drawImage(dst,0,0);
			dst.width=dst.height=0;
			return src;
		}

		src.width=src.height=0;
		return dst;
		
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Helper function for scaleImageData. Process a chunk of data from the source image.

		@param src: source image data object {canvas:src, x:x, y:y, w:w, h:h}.
		@param dst: destination image data object {canvas:dst, x:x, y:y, w:w, h:h}.
		@param index: object containing the x and y offset multipliers.
		@param scale: scale value (float).
		@param chunkSize: source image chunk size.
		@param chunkSizeScaled: destination image chunk size.
		@param object: worker instance or scale function.
		@param isWorker: true if object is a worker and false if a scale function.
	*/
	processImageData: function(src, dst, index, scale, chunkSize, chunkSizeScaled, object, isWorker){

		/* Build the arguments for the scale function */
		var args = [];

		/* Calculate the x and y offset for the original image. */
		var x = index.x*chunkSize;
		var y = index.y*chunkSize;
		/* Calculate the chunk's width and height for the original image. */
		var w = Math.min(src.w-x, chunkSize);
		var h = Math.min(src.h-y, chunkSize);

		/* Get a chunk of data from the original image to be scaled. */
		var src_chunk = src.canvas.getContext("2d").getImageData(src.x+x,src.y+y,w,h).data;
		args.push((isWorker ? src_chunk.buffer : src_chunk), w, h);

		/* Calculate the x and y offset for the scaled image. */
		x = index.x*chunkSizeScaled;
		y = index.y*chunkSizeScaled;
		/* Calculate the chunk's width and height for the scaled image. */
		w = Math.min(dst.w-x, chunkSizeScaled);
		h = Math.min(dst.h-y, chunkSizeScaled);

		/* Get a chunk from the destination image to place the scaled data in. */
		var dst_chunk = dst.canvas.getContext("2d").getImageData(dst.x+x,dst.y+y,w,h);
		args.push((isWorker ? dst_chunk.data.buffer : dst_chunk.data), w, h, scale, isWorker);


		if(isWorker){

			/* Send function parameters to the worker thread */
			object.postMessage(args, [args[0], args[3]]);
			/*
				Add the chunk and offset values to the dst data object so their values
				can be accessed by the caller function.
			*/
			dst.chunk = dst_chunk; dst.x_offset = dst.x+x; dst.y_offset = dst.y+y;
			/* Remove the src_chunk data */
			src_chunk = null;

		} else{

			if(scale === 1){
				dst_chunk = src_chunk;
			} else{
				object.apply(null, args);
			}
			
			/* Add the scaled data to the destination image */
			dst.canvas.getContext("2d").putImageData(dst_chunk, dst.x+x, dst.y+y);
			/* Remove the dst_chunk and src_chunk data */
			dst_chunk = src_chunk = null;
		
		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Scale a canvas in place or into a destination canvas if one is provided. 
		
		Syntax:
			scaleImageData(src, dst, chunkSize, scaleCallback, workerCallback, errorCallback)
			scaleImageData(src, scale, chunkSize, scaleCallback, workerCallback, errorCallback)

		src and dst are objects containing the information required to scaled the canvas.
		The object consists of 5 values in the form {canvas:canvas, x:x, y:y, w:w, h:h}.
			
			@param canvas: canvas element.
			@param x: x offset to read/write data to/from.
			@param y: y offset to read/write data to/from.
			@param w: width to read/write data to/from.
			@param h: height to read/write data to/from.
		
		@param src: source image data object.
		@param dst -or- scale: destination image data object -or- scale value.
		@param chunkSize: source image chunk size.
		@param scaleCallback: scale function: bilinear() or bicubic().
		@param workerCallback: called on completion of the worker thread and is passed the
			scaled canvas as its only argument.
		@param errorCallback: called if the worker throws an exception, takes no args.
	*/
	scaleImageData: function(src, dst, chunkSize, scaleCallback, workerCallback, errorCallback){

		/* Function variables. */
		var scale, index, indices=[], transfer;

		/* Check the canvas dimensions and set the scale values. */
		try{
			/*
				Attempt to read a single piece of data. If the canvas size is too large
				the browser will throw an exception.
			*/
			src.canvas.getContext("2d").getImageData(0,0,1,1);

			/* Set transfer - if true the scaled data in dst will be added to src at the end. */
			transfer = dst.canvas === undefined;

			/* Create a temporary canvas to scale the src.canvas into. */
			if(transfer){

				scale = dst;

				dst = {
					canvas:document.createElement("canvas"), 
					w:Math.round(src.w*scale),
					h:Math.round(src.h*scale),
					x:0,
					y:0,
				}
				
				dst.canvas.width = dst.w;
				dst.canvas.height = dst.h;

			/* Else validate the destination canvas and calculate its scale. */
			} else{

				/* Attempt to read some data */
				dst.canvas.getContext("2d").getImageData(0,0,1,1);
				/* Calculate the scale between src and dst */
				scale = Math.max(dst.w/src.w, dst.h/src.h);
				/* Throw an exception if scale is not a valid number */
				this.validateScale(scale);

			}

		} catch(e){
			
			debug(e);

			throw new FileUploadException("The browser you are using does not support \
				editing of large images.");
		
		}


		/* Calculate the chunkSize for the scaled image (dst) */
		var chunkSizeScaled = Math.round(chunkSize*scale);
		/* Fill indices with the x and y offset multipliers for each chunk */
		this.chunkIndices(Math.ceil(src.w/chunkSize), Math.ceil(src.h/chunkSize), indices);


		/*
			Scale each chunk using a web worker if given a callback function.
		*/
		if(workerCallback && window.Worker){

			try{

				/* Create a web worker to run scaleCallback(). */
				var args = "e.data[0], e.data[1], e.data[2], e.data[3], e.data[4], e.data[5], e.data[6], true";
				var worker = this.createWorker(scaleCallback, args, errorCallback);

				/*
					Set the data received on the dst canvas and process the next chunk.
					Once complete terminate the worker and run the callback function.
				*/
				var self = this;
				worker.onmessage = function(e){

					/*
						If dst_chunk was transfered then its buffer will have been neutered 
						so reassign it an empty ImageData object.
					*/
					if(dst.chunk.data.buffer.byteLength === 0){
						dst.chunk = dst.canvas.getContext("2d")
							.createImageData(dst.chunk.width, dst.chunk.height);
					}

					/* Put the scaled image data into the destination canvas. */
					dst.chunk.data.set(new Uint8Array(e.data));
					dst.canvas.getContext("2d")
						.putImageData(dst.chunk, dst.x_offset, dst.y_offset);

					/* Process the next chunk of data. */
					if( index=indices.pop() ){

						self.processImageData(src, dst, index, scale, chunkSize, 
							chunkSizeScaled, worker, true);

					/* End the process. */
					} else{
						
						worker.terminate();	

						/* Pass the canvas containing the scaled data to the function. */
						workerCallback.call(self, self.setImageData(src.canvas, dst.canvas, transfer));

						src = dst = null;

					}

				}

				/* Begin the process. */
				if( index=indices.pop() ){
					this.processImageData(src, dst, index, scale, chunkSize, 
						chunkSizeScaled, worker, true);
				}

				return "worker";

			} catch(e){

				debug(e);

				if(worker){
					worker.terminate();
				}

				/* Continue and attempt to perform the scale on the main thread. */

			}
		}

		/*
			Perform the scale of the main thread.
		*/
		while( index=indices.pop() ){
			this.processImageData(src, dst, index, scale, chunkSize, chunkSizeScaled, 
				scaleCallback, false);
		}

		this.setImageData(src.canvas, dst.canvas, transfer);

		return "main";

	},
	/* -------------------------------------------------------------------------------- */
	/* END SCALE CANVAS IN CHUNKS - VERSION TWO
	/* -------------------------------------------------------------------------------- */
	// /*
	// 	Return the scaleCallback function parameters for the next chunk of data or false
	// 	if there a no chunks to process.
		
	// 	@param src: source image canvas.
	// 	@param dst: destination image data object (ImageData instance).
	// 	@param indices: object containing the chunk's index values {x: x index, y: y index}.
	// 	@param buffer: destination image ArrayBuffer for worker threads.
	// 	@param scale: scale value (float).
	// 	@param chunkSize: pixel squared dimension for the original image (src)
	// 	@param chunkSizeScaled: pixel squared dimension for the scaled image (dst)
	// */
	// chunkData: function(src, dst, indices, chunkSize, chunkSizeScaled){

	// 	/* Get the x and y index mulitpliers for the next chunk */
	// 	var index = indices.shift();

	// 	if(index){

	// 		/* Calculate the x and y offset for the original image. */
	// 		var x = index.x*chunkSize;
	// 		var y = index.y*chunkSize;
	// 		/* Calculate the chunk's width and height for the original image. */
	// 		var w = Math.min(src.width-x, chunkSize);
	// 		var h = Math.min(src.height-y, chunkSize);

	// 		/* Get the chunk of data from the original image to be scaled. */
	// 		var src_chunk = src.getContext("2d").getImageData(x, y, w, h);

	// 		/* Calculate the x and y offset for the scaled image. */
	// 		x = index.x*chunkSizeScaled;
	// 		y = index.y*chunkSizeScaled;
	// 		/* Calculate the chunk's width and height for the scaled image. */
	// 		w = Math.min(dst.width-x, chunkSizeScaled);
	// 		h = Math.min(dst.height-y, chunkSizeScaled);

	// 		var dst_chunk = dst.getContext("2d").getImageData(x, y, w, h);

	// 		return {src:src_chunk, dst:dst_chunk, x:x, y:y};
			
	// 	}

	// 	/* Return false if there a no chunks to be processed */
	// 	return false;

	// },
	// /* -------------------------------------------------------------------------------- */
	// /*
	// 	Scale the canvas (src) in place and return the thread used ("main" or "worker").
	// 	The canvas is scaled in square blocks defined by chunkSize.

	// 	NOTE: DOES NOT PRODUCE THE BEST RESULTS WHEN UP SCALING SMALL IMAGES.

	// 	@param src: source image canvas.
	// 	@param scale: scale value (float).
	// 	@param mw: minimum scale width.
	// 	@param mh: minimum scale height.
	// 	@param chunkSize: pixels squared measurement to read data chunks.
	// 	@param scaleCallback: bilinear(), bicubic(), bilinearChunk() or bicubicChunk().
	// 	@param workerCallback: function called once the worker thread has finished. The
	// 		function is passed src.
	// 	@param errorCallback: function called if the worker thread throws an exception.

	// */
	// scaleChunk: function(src, scale, mw, mh, chunkSize, scaleCallback, workerCallback, errorCallback){

	// 	try{
	// 		/*
	// 			Attempt to read a single piece of data, if the canvas size is too large
	// 			the browser may throw an exception.
	// 		*/
	// 		src.getContext("2d").getImageData(0,0,1,1);

	// 		/*
	// 			Create an ImageData instance to place the scaled data in.
	// 		*/
	// 		var dst = src.getContext("2d").createImageData(
	// 			Math.max(Math.round(src.width*scale), mw),
	// 			Math.max(Math.round(src.height*scale), mh)
	// 		);

	// 		/*
	// 			If dst is null the canvas dimensions are too large.
	// 		*/
	// 		if(dst === null){ 
	// 			throw new Error(); 
	// 		}

	// 	} catch(e){

	// 		debug(e);
			
	// 		throw new FileUploadException("The browser you are using does not support \
	// 			editing of large images.");
		
	// 	}

	// 	var indices=[], data;
	// 	/* Number of blocks along the x axis */
	// 	var xb = Math.ceil(src.width/chunkSize);
	// 	/* Number of blocks along the y axis */
	// 	var yb = Math.ceil(src.height/chunkSize);
	// 	/* Calculate the chunkSize for the scaled image (dst) */
	// 	var chunkSizeScaled = Math.round(chunkSize*scale);

	// 	/* Fill indices with the x and y offset multipliers for each chunk */
	// 	this.chunkIndices(xb, yb, indices);

	// 	/*
	// 		Scale each chunk using a web worker if given a callback function.
	// 	*/
	// 	if(workerCallback && window.Worker){

	// 		try{

	// 			/* Create a web worker to run scaleCallback. */
	// 			var args = "e.data.src, e.data.sw, e.data.sh, e.data.dst, e.data.dw, \
	// 				e.data.x, e.data.y, e.data.cw, e.data.ch, e.data.scale, true";
	// 			var worker = this.createWorker(scaleCallback, args, errorCallback);

	// 			/*
	// 				Add onmessage handler to send the next chunk to the web worker. Fill the original 
	// 				canvas with the scaled image data and run the workerCallback when there are
	// 				no more chunks of data.
	// 			*/
	// 			var self=this;
	// 			worker.onmessage = function(e){

	// 				/* Get the scaleCallback parameters for the next chunk */
	// 				data = self.chunkData(src, dst, indices, e.data, scale, chunkSize, chunkSizeScaled);

	// 				if(data){

	// 					/* Send the data to the worker - src and dst are sent as transferable objects. */
	// 					worker.postMessage(data, [data.src, data.dst]);
	// 					data=null;

	// 				} else{

	// 					worker.terminate();

	// 					/*
	// 						If dst was transfered then its buffer will have been neutered so
	// 						reassign it an empty ImageData object to the scaled dimensions.
	// 					*/
	// 					if(dst.data.buffer.byteLength === 0){
	// 						dst = src.getContext("2d").createImageData(dst.width,dst.height);
	// 					}

	// 					/* Set the array buffer on the scaled ImageData object. */
	// 					dst.data.set(new Uint8Array(e.data));
	// 					/* Set the original canvas dimensions to the scaled dimensions. */
	// 					src.width=dst.width; src.height=dst.height;
	// 					/* Add the data to the original canvas. */
	// 					src.getContext("2d").putImageData(dst, 0, 0);
	// 					dst = null;

	// 					workerCallback.call(self, src);
	// 				}		

	// 			}

	// 			data = this.chunkData(src, dst, indices, dst.data.buffer, scale, chunkSize, chunkSizeScaled);
				
	// 			if(data){
				
	// 				/* Send the data to the worker - src and dst are sent as transferable objects. */
	// 				worker.postMessage(data, [data.src, data.dst]);
	// 				data=null;
				
	// 			}
	

	// 			return "worker";


	// 		} catch(e){

	// 			debug(e);

	// 			if(worker){
	// 				worker.terminate();
	// 			}

	// 			/* Continue and attempt to perform the scale on the main thread. */
	// 		}

	// 	}

	// 	/* Perform the scale on the main thread. */
	// 	while( data=this.chunkData(src, dst, indices, false, scale, chunkSize, chunkSizeScaled) ){

	// 		scaleCallback.apply(null, data);
	// 		data = null;

	// 	}

	// 	/* 
	// 		Set src to the scaled dimensions and add the scaled image data to the 
	// 		original canvas.
	// 	*/
	// 	src.width=dst.width; src.height=dst.height;
	// 	src.getContext("2d").putImageData(dst, 0, 0);
	// 	dst = null;

	// 	return "main";
	// },
	// /* -------------------------------------------------------------------------------- */
	// /*
	// 	Scale a chunk of the source image and place into the same chunk on the 
	// 	destination image.

	// 	If isWorkerThread is true src and dst must be ArrayBuffers and the function will
	// 	return the destination buffer (dst), else they must be Uint8ClampedArrays and 
	// 	the function will not return a value.

	// 	NOTE: DOES NOT PRODUCE THE BEST RESULTS WHEN UP SCALING SMALL IMAGES.

	// 	@param src: chunk of the original image (ArrayBuffer or Uint8ClampedArray).
	// 	@param sw: src width.
	// 	@param sw: src height.
	// 	@param dst: scaled image (ArrayBuffer or Uint8ClampedArray)..
	// 	@param dw: dst width.
	// 	@param x: destination image x offset.
	// 	@param y: destination image y offset.
	// 	@param cw: destination image chunk width.
	// 	@param cw: destination image chunk height.
	// 	@param scale: scale value (float).
	// 	@param isWorkerThread: true if called from a worker and false if not.
	// */
	// bilinearChunk: function(src, sw, sh, dst, dw, x, y, cw, ch, scale, isWorkerThread){

	// 	/* Loop variables */
	// 	var iy_max = sh-1;
	// 	var ix_max = sw-1;
	// 	var i, ix,ix0,ix1, iy,iy0,iy1, p1,p2,p3,p4, w1,w2,w3,w4, dx,dx1,dy,dy1;

	// 	/* Uint8ClampedArray is supported from IE 11, Uint8Array from IE 10. */
	// 	if(isWorkerThread){
	// 		src = new Uint8Array(src);
	// 		dst = new Uint8Array(dst);
	// 	}

	// 	for(var yi=0; yi<ch; ++yi){
			
	// 		iy  = yi / scale;
	// 		iy0 = Math.floor(iy);
	// 		iy1 = Math.ceil(iy);
	// 		if(iy1 > iy_max){ iy1=iy_max; }

	// 		for(var xi=0; xi<cw; ++xi){

	// 			ix  = xi / scale;
	// 			ix0 = Math.floor(ix);
	// 			ix1 = Math.ceil(ix);
	// 			if(ix1 > ix_max){ ix1=ix_max; }
				
	// 			/* Calc the weights for each pixel */
	// 			dx = ix-ix0; 
	// 			dy = iy-iy0;
	// 			dx1 = 1.0-dx;
	// 			dy1 = 1.0-dy;
	// 			w1 = dx1 * dy1;
	// 			w2 = dx * dy1;
	// 			w3 = dx1 * dy;
	// 			w4 = dx * dy;

	// 			/* Index positions of the surrounding pixels */
	// 			p1 = (ix0 + sw * iy0) << 2;
	// 			p2 = (ix1 + sw * iy0) << 2;
	// 			p3 = (ix0 + sw * iy1) << 2;
	// 			p4 = (ix1 + sw * iy1) << 2;

	// 			/* Add the x and y offset to the destination image index */
	// 			i = ((y+yi)*dw + x+xi) << 2;

	// 			dst[i]   = (src[p1] * w1 + src[p2] * w2 + src[p3] * w3 + src[p4] * w4) & 0xff;
	// 			dst[i+1] = (src[p1+1]*w1 + src[p2+1]*w2 + src[p3+1]*w3 + src[p4+1]*w4) & 0xff;
	// 			dst[i+2] = (src[p1+2]*w1 + src[p2+2]*w2 + src[p3+2]*w3 + src[p4+2]*w4) & 0xff;
	// 			dst[i+3] = (src[p1+3]*w1 + src[p2+3]*w2 + src[p3+3]*w3 + src[p4+3]*w4) & 0xff;

	// 		}
	// 	}

	// 	/*
	// 		Return the destination array buffer if called from a web worker.
	// 	*/
	// 	if(isWorkerThread){
	// 		return dst.buffer;
	// 	}
		
	// },
	// /* -------------------------------------------------------------------------------- */ 
	// /*
	// 	Scale a chunk of the source image and place into the same chunk on the 
	// 	destination image.

	// 	If isWorkerThread is true src and dst must be ArrayBuffers and the function will
	// 	return the destination buffer (dst), else they must be Uint8ClampedArrays and 
	// 	the function will not return a value.

	// 	NOTE: DOES NOT PRODUCE THE BEST RESULTS WHEN UP SCALING SMALL IMAGES AND CAN
	// 	PRODUCE SPECS OF RGB PIXELS WHEN DOWN SCALING LARGE IMAGES TO SMALL SIZES.

	// 	@param src: chunk of the original image (ArrayBuffer or Uint8ClampedArray).
	// 	@param sw: src width.
	// 	@param sw: src height.
	// 	@param dst: scaled image (ArrayBuffer or Uint8ClampedArray)..
	// 	@param dw: dst width.
	// 	@param x: destination image x offset.
	// 	@param y: destination image y offset.
	// 	@param cw: destination image chunk width.
	// 	@param cw: destination image chunk height.
	// 	@param scale: scale value (float).
	// 	@param isWorkerThread: true if called from a worker and false if not.
	// */
	// bicubicChunk: function(src, sw, sh, dst, dw, x, y, cw, ch, scale, isWorkerThread){

	// 	/* Loop variables */
	// 	var dx, dy;
	// 	var repeatX, repeatY;
	// 	var r0, r1, r2, r3;
	// 	var c0, c1, c2, c3;
	// 	var v0, v1, v2, v3;
	// 	var yv, y0, xv, x0, i;

	// 	/* Uint8ClampedArray is supported from IE 11, Uint8Array from IE 10. */
	// 	if(isWorkerThread){
	// 		src = new Uint8Array(src);
	// 		dst = new Uint8Array(dst);
	// 	}
		
	// 	for(var yi=0; yi<ch; ++yi){
			
	// 		yv = yi / scale;
	// 		y0 = Math.floor(yv);

	// 		repeatY = 0;
	// 		if(y0 < 1){
	// 			repeatY = -1;
	// 		} else if(y0 > sh - 3){
	// 			repeatY = y0 - (sh - 3); 
	// 		}

	// 		for (var xi=0; xi<cw; ++xi) {
				
	// 			xv = xi / scale;
	// 			x0 = Math.floor(xv);

	// 			repeatX = 0;
	// 			if(x0 < 1){
	// 				repeatX = -1;
	// 			} else if(x0 > sw - 3){
	// 				repeatX = x0 - (sw - 3);
	// 			}

	// 			r1 = (y0 * sw + x0) << 2;
	// 			r0 = repeatY < 0 ? r1 : ((y0-1) * sw + x0) << 2;
	// 			r2 = repeatY > 1 ? r1 : ((y0+1) * sw + x0) << 2;
	// 			r3 = repeatY > 0 ? r2 : ((y0+2) * sw + x0) << 2;

	// 			c0 = repeatX < 0 ? 0 : -4;
	// 			c2 = repeatX > 1 ? 0 : 4;
	// 			c3 = repeatX > 0 ? c2 : 8;

	// 			dx = xv - x0; 
	// 			dy = yv - y0;

				
	// 			i = ((y+yi)*dw + x+xi) << 2;

	// 			/* rgba values: j == 0 is r, j == 1 is g, j == 2 is b, j == 3 is a */ 
	// 			for(var j=0; j<4; ++r0, ++r1, ++r2, ++r3, ++i, ++j){

	// 				v0 = 0.5*(src[r0+c2]-src[r0+c0]+(2*src[r0+c0]-5*src[r0]+4*src[r0+c2]-src[r0+c3]+
	// 					(3*(src[r0]-src[r0+c2])+src[r0+c3]-src[r0+c0])*dx)*dx)*dx+src[r0];

	// 				v1 = 0.5*(src[r1+c2]-src[r1+c0]+(2*src[r1+c0]-5*src[r1]+4*src[r1+c2]-src[r1+c3]+
	// 					(3*(src[r1]-src[r1+c2])+src[r1+c3]-src[r1+c0])*dx)*dx)*dx+src[r1];

	// 				v2 = 0.5*(src[r2+c2]-src[r2+c0]+(2*src[r2+c0]-5*src[r2]+4*src[r2+c2]-src[r2+c3]+
	// 					(3*(src[r2]-src[r2+c2])+src[r2+c3]-src[r2+c0])*dx)*dx)*dx+src[r2];

	// 				v3 = 0.5*(src[r3+c2]-src[r3+c0]+(2*src[r3+c0]-5*src[r3]+4*src[r3+c2]-src[r3+c3]+
	// 					(3*(src[r3]-src[r3+c2])+src[r3+c3]-src[r3+c0])*dx)*dx)*dx+src[r3];

	// 				dst[i] = 0.5*(v2-v0+(2*v0-5*v1+4*v2-v3+(3*(v1-v2)+v3-v0)*dy)*dy)*dy+v1;

	// 			}
	// 		}
	// 	}

	// 	/*
	// 		Return the destination array buffer if called from a web worker.
	// 	*/
	// 	if(isWorkerThread){
	// 		return dst.buffer;
	// 	}

	// },

	// /* -------------------------------------------------------------------------------- */
	// /*
	// 	Scale an area of the canvas (src) into an area of the canvas dst and return the 
	// 	thread used ("main" or "worker"). The canvas is scaled in square blocks defined 
	// 	by chunkSize.

	// 	NOTE: DOES NOT PRODUCE THE BEST RESULTS WHEN UP SCALING SMALL IMAGES.

	// 	@param src: source image canvas.
	// 	@param sx: src x offset.
	// 	@param sy: src y offset.
	// 	@param sw: src width to get data from.
	// 	@param sh: src height to get data from.
		
	// 	@param dst: destination image canvas.
	// 	@param dx: dst x offset.
	// 	@param dy: dst y offset.
	// 	@param dw: dst width to put data in.
	// 	@param dh: dst height to put data in.
		
	// 	@param chunkSize: pixel squared measurement for reading data chunks.
	// 	@param workerCallback: function called once the worker thread has finished. The
	// 		function is passed src.
	// 	@param errorCallback: function called if the worker thread throws an exception.
	// */
	// scaleChunkOffset: function(src, sx, sy, sw, sh, dst, dx, dy, dw, dh, chunkSize, workerCallback, errorCallback){
		
	// 	/* Check that the canvas dimensions are within the browser limits. */
	// 	try{
	// 		/*
	// 			Attempt to read a single piece of data. If the canvas size is too large
	// 			the browser will throw an exception.
	// 		*/
	// 		src.getContext("2d").getImageData(0,0,1,1);
	// 		dst.getContext("2d").getImageData(0,0,1,1);

	// 	} catch(e){
			
	// 		debug(e);

	// 		throw new FileUploadException("The browser you are using does not support \
	// 			editing of large images.");
		
	// 	}

	// 	/* Function global variables */
	// 	var x, y, indices=[], dst_chunk;

	// 	/* Fill indices with the x and y offset multipliers for each chunk */
	// 	this.chunkIndices(Math.ceil(sw/chunkSize), Math.ceil(sh/chunkSize), indices);

	// 	/*
	// 		Set the global variables for each new chunk of data. Return true if there
	// 		is a chunk to process and false if not.
	// 	*/
	// 	var processChunk = function(worker, isScale){

	// 		/* Get the x and y index mulitpliers for the next chunk */
	// 		var index = indices.pop();

	// 		if(index){

	// 			/* Calculate the x and y offset for the original image. */
	// 			x = index.x*chunkSize;
	// 			y = index.y*chunkSize;
	// 			/* Calculate the chunk's width and height for the original image. */
	// 			var w = Math.min(sw-x, chunkSize);
	// 			var h = Math.min(sh-y, chunkSize);

	// 			/* Get the chunk of data from the original image to be scaled. */
	// 			var src_chunk = src.getContext("2d").getImageData(sx+x, sy+y, w, h);

	// 			/* Calculate the x and y offset for the scaled image. */
	// 			x = index.x*chunkSizeScaled;
	// 			y = index.y*chunkSizeScaled;
	// 			/* Calculate the chunk's width and height for the scaled image. */
	// 			w = Math.min(dw-x, chunkSizeScaled);
	// 			h = Math.min(dh-y, chunkSizeScaled);

	// 			/* Get the chunk to put the scaled data into */
	// 			dst_chunk = dst.getContext("2d").getImageData(dx+x, dy+y, w, h);

	// 			/* If given a worker then post the parameters to the worker thread. */
	// 			if(worker){

	// 				worker.postMessage(
	// 					{src:src_chunk.data.buffer, sw:src_chunk.width, sh:src_chunk.height, dst:dst_chunk.data.buffer, dw:w, dh:h, scale:scale}, 
	// 					[src_chunk.data.buffer, dst_chunk.data.buffer])

	// 			} else {

	// 				/* If scale !== 1 then scale the src data into dst */
	// 				if(isScale){

	// 					this.bilinear(src_chunk.data, src_chunk.width, src_chunk.height, dst_chunk.data, dst_chunk.width, dst_chunk.height, scale);
	// 					dst.getContext("2d").putImageData(dst_chunk, dx+x, dy+y);
					
	// 				/* Else just place the src data into dst */
	// 				} else{

	// 					dst.getContext("2d").putImageData(src_chunk, dx+x, dy+y);

	// 				}

	// 				dst_chunk = null;
	// 			}

	// 			src_chunk = null;

	// 			return true;
	// 		}

	// 		return false;

	// 	}

	// 	/* Calculate the scale between src and dst */
	// 	var scale = Math.max(dw/sw, dh/sh);
	// 	/* Throw an exception if scale is not a valid number */
	// 	this.validateScale(scale);
	// 	/* Calculate the chunkSize for the scaled image (dst) */
	// 	var chunkSizeScaled = Math.round(chunkSize*scale);
	// 	/* Set isScale true if the canvas needs to be scaled */
	// 	var isScale = scale!==1;
		

	// 	/*
	// 		Scale the canvas using a web worker.
	// 	*/
	// 	if(isScale && workerCallback && window.Worker){

	// 		try{

	// 			/* Create a web worker to run bilinear(). */
	// 			var args = "e.data.src, e.data.sw, e.data.sh, e.data.dst, e.data.dw, e.data.dh, e.data.scale, true";
	// 			var worker = this.createWorker(this.bilinear, args, errorCallback);

	// 			/*
	// 				Set the data received on the dst canvas and process the next chunk.
	// 				Once complete terminate the worker and run the callback function.
	// 			*/
	// 			var self = this;
	// 			worker.onmessage = function(e){

	// 				/*
	// 					If dst_chunk was transfered then its buffer will have been neutered so
	// 					reassign it an empty ImageData object.
	// 				*/
	// 				if(dst_chunk.data.buffer.byteLength === 0){
	// 					dst_chunk = dst.getContext("2d").createImageData(dst_chunk.width, dst_chunk.height);
	// 				}

	// 				dst_chunk.data.set(new Uint8Array(e.data));
	// 				dst.getContext("2d").putImageData(dst_chunk, dx+x, dy+y);

	// 				if(processChunk(worker) === false){
						
	// 					worker.terminate();
	// 					workerCallback.call(self, dst);

	// 				}

	// 			}

	// 			/* Start the process by sending the first chunk of data */
	// 			processChunk(worker);

	// 			return "worker";

	// 		} catch(e){

	// 			debug(e);

	// 			if(worker){
	// 				worker.terminate();
	// 			}

	// 			/* continue and attempt to perform the scale on the main thread. */

	// 		}

	// 	}

	// 	/* Perform the scale on the main thread. */
	// 	while( processChunk(false, isScale) ){ ; }

	// 	return "main";

	// },

	/* END CANVAS SCALING */
	/* -------------------------------------------------------------------------------- */


	/* CANVAS DATA */
	/* -------------------------------------------------------------------------------- */
	/*
		Return an array containing the elements added to the editor. The elements are
		returned within their container as the container stores the scale values.
		(i.e. <div id="container">  <canvas> OR <video>  </div>)
	*/
	getElements: function(){

		var e = [], divs = this.content.childNodes;

		var length = Math.min(divs.length, this.maxFiles(this.type));

		for(var i=0; i<length; i++){
			
			try{

				/* add the container to the array */
				e.push( divs[i].firstChild );

			} catch(ex){
			
				debug(ex);
			
			}
		}

		return e;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		**********************************************************************************
		OVERRIDDEN BY SUBCLASS TO DISPLAY A PREVIEW AS THE IMAGE IS PROCESSED.
		**********************************************************************************

		@param canvas: final canvas. See getCanvas().
	*/
	preview: function(canvas){
		return false;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		**********************************************************************************
		OVERRIDDEN BY SUBCLASS TO DEFINE THE MAX AND MIN DIMENSIONS OF THE FINAL IMAGE.
		**********************************************************************************

		Returns an object containing the max an min multiples for the final image. The 
		editor's aspect ratio is multiplied by the max an min values to calculate the
		max and min image size.

		e.g. 
			Editor's aspect ratio: 4:3.
			Max and min values: {max: 50, min: 20}.

			The max and min image sizes are therefore:
			Max image size: 200px x 150px or (4x50) x (3x50).
			Min image size: 80px x 60px or (4x20) x (3x30).

			If the image is already within the max and min size it will not be scaled.
			To scale the image to an exact size set max=min.

	*/
	dataSize: function(){
		throw new Error("Subclasses of Editor must define dataSize().");
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Set the current video frame as the canvas image and attach the video file to the
		canvas (canvas.file).
		
		NOTE: The function assumes that only one video can be uploaded at one time.

		@param elements: this.getElements() return value.
		@param canvas: the final canvas returned by getData()/
	*/
	_videoData: function(elements, canvas){

		if(this.type === this.video){

			/* set the video as a property on the canvas */
			var video = elements[0].firstChild;
			/* set the video file as a property on the canvas */
			canvas.file = video.file;

			/* create a new canvas and draw the current frame into it */
			var tmp = document.createElement("canvas");
			tmp.width = video.videoWidth;
			tmp.height = video.videoHeight;
			tmp.getContext("2d").drawImage(video, 0, 0);

			/* replace the video with the canvas */
			video.parentNode.replaceChild(tmp, video);
			video = null;
		}

	},

	/* GET CANVAS DATA VERSION ONE */
	/* -------------------------------------------------------------------------------- */
	/*
	 	Wrapper around setCanvasData to pass the function and its parameters as a
	 	workerCallback function to the scale functions.

	 	params: same as setCanvasData() minus the image canvas which is passed to
	 	the returned function by the scale function.
	*/
	setCanvasDataWorker: function(canvas, container, w, h, i){
		return function(image){
			this.setCanvasData(canvas, container, image, w, h, i);
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Crop the image canvas and place its data in the final canvas.

		@param canvas: canvas to build the final image in.
		@param container: image canvas container which contains the scale information.
		@param image: canvas containing the current image data.
		@param w: width to crop the image to.
		@param h: height to crop the image to.
		@param i: index position of the image canvas.
	*/
	setCanvasData: function(canvas, container, image, w, h, i){

		try{
			/* Get the ratio between the original image and the scaled container */
			var ratio = Math.min(
				image.width/parseFloat(container.style.width),
				image.height/parseFloat(container.style.height)
			)

			/* Calculate the image x offset */
			var x = Math.min(
				/* calculate x */
				Math.round(Math.abs(parseFloat(container.style.left) * ratio)),
				/* maximum value that x can be */
				image.width - w
			);

			/* Calculate the image y offset */	
			var y = Math.min(
				/* calculate y */
				Math.round(Math.abs(parseFloat(container.style.top) * ratio)),
				/* maximum value that y can be */
				image.height - h
			);

			/* NOTE: putImageData DOES NOT WORK CONSISTENTLY */

			/* Get the cropped image data */
			// var data = image.getContext("2d").getImageData(x, y, w, h);

			// /* Remove the pixels from the image canvas */
			// image.width = image.height = 0;

			/* 
				Calculate the x or y offset for the image inside the final canvas 
				depending on the orientation.
			*/
			var xy = this.stacked ? [0, h*i] : [w*i, 0];
			/* Add the data to the final canvas */
			canvas.getContext("2d").drawImage(image, x, y, w, h, xy[0], xy[1], w, h);
			// canvas.getContext("2d").putImageData(data, xy[0], xy[1]);

			/* Remove the pixels from the image canvas */
			image.width = image.height = 0;

			/* Reduce the counter for pending files by 1 */
			canvas.pending -= 1;
		
		}catch(e){

			debug(e);

			/* 
				If the user has disabled css styling the values of 
				container.style.width, container.style.height
				container.style.left and container.style.top will be NaN.

				To work in this scenario just set the values on the container as 
				properties or data attributes in the ScaleElement class instead of
				using inline styles.
			*/
		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return the canvas scaled and cropped to the aspect ratio defined by the editor.
		The output image size is defined by dataSize() which returns an object containing
		the max and min multiples for the aspect ratio to arrive at the max and min
		image size.

		@param async: boolean - true if scale functions should be run on a worker thread
		if supported by the browser.
	*/
	getCanvas: function(async){

		try{

		/* Get the containers of each element in the editor as an array. */
		var elements = this.getElements();

		if(elements.length === 0){
			throw new FileUploadException("We could not find any files to upload.")
		}

		/* Create a new canvas to store the final image into. */
		var canvas = document.createElement("canvas");
		/* Set property pending to the number of files. */
		canvas.pending = elements.length;
		
		/* Replace the video for a canvas and set a video property on the canvas. */
		this._videoData(elements, canvas);

		/* Hook for subclasses to display a preview as the image is processed. */
		this.preview(canvas);

		/* Get the aspect ratio for the file type. */
		var ar = this.aspectRatio[this.type];

		/* 
			Divide the width or height of the aspect ratio value depending on the 
			orientation (stacked is the same as portrait).
		*/
		if(this.stacked){
			ar.height /= elements.length;
		} else{
			ar.width /= elements.length;
		}

		/* Calulate the width and height for each element. */
		var w = 0, h = 0;

		/* Sum the width and height values of all the elements. */
		for(var i=0; i<elements.length; ++i){

			w += elements[i].firstChild.width;
			h += elements[i].firstChild.height;

		}

		/* Calculate the average. */
		w /= elements.length;
		h /= elements.length;


		var size = this.dataSize();
		/* Calulate the largest common multiple within the min and max range. */
		var multiple = Math.max(
			Math.min(Math.round(Math.min(w/ar.width, h/ar.height)), size.max), size.min
		);

		/* Set the width and height to the max values for each individual image. */
		w = ar.width*multiple;
		h = ar.height*multiple;

		/* Set the values of the canvas to hold all the images. */
		if(this.stacked){
	
			canvas.width = w;
			canvas.height = h * elements.length;
	
		} else{
	
			canvas.width = w * elements.length;
			canvas.height = h;
		
		}

		/* Loop variables */
		var container, image, zoom, scale, type, workerCallback;

		/* Crop and scale each elements/ */
		for(var i=0; i<elements.length; i++){

			container = elements[i];
			
			/* Canvas containing the image data/ */
			image = container.firstChild;

			/* Get the zoom value set as a property on the container by the ScaleElement class. */
			zoom = ((parseFloat(container.scale) || 0) * 0.01) + 1;

			/* Calculate the scale value */
			scale = Math.max(w/image.width, h/image.height) * zoom;
			
			/* Throw exception if scale is not a valid number */
			this.validateScale(scale);


			/* Name of the thread used to scale the image. */
			type = "none";

			/* Scale the image */
			if(scale !== 1){

				workerCallback = async && this.setCanvasDataWorker(canvas, container, w, h, i);
				
				/* Returns the thread used to scale the canvas ("main" or "worker"). */
				// type = this.scale(image, scale, w*zoom, h*zoom, this.bilinear, workerCallback);

				type = this.scaleImageData(
					{canvas:image,  x:0, y:0, w:image.width, h:image.height},
					scale, 1024, this.bilinear, workerCallback
				);

			}

			/*
				If the image does not require scaling or the scale has been performed on 
				the main thread then crop the image and place its data inside canvas.
			*/
			if(type !== "worker"){

				this.setCanvasData(canvas, container, image, w, h, i);
			
			}
			
		}

		return canvas;

		}catch(ex){
			console.log(ex);
		}
	},
	/* END GET CANVAS DATA VERSION ONE */
	/* -------------------------------------------------------------------------------- */

	/* GET CANVAS DATA VERSION TWO */
	/* -------------------------------------------------------------------------------- */
	// /*
	// 	Decrement the canvas pending values.
	// 	@param canvas: final image canvas created by getCanvas.
	// */	
	// setCanvas: function(canvas){
	// 	--canvas.pending;
	// },
	// /* -------------------------------------------------------------------------------- */
	// /*
	// 	Return the final canvas scaled and cropped to the aspect ratio defined by the editor.
	// 	The output image size is defined by dataSize() which returns an object containing
	// 	the max and min multiples for the aspect ratio to arrive at the max and min
	// 	image size.
	// */
	// getCanvas: function(async){	
	// 	/* 
	// 		Get the element loaded in the editor wrapped in their containers. The 
	// 		containers store the scale information.
	// 	*/
	// 	var elements = this.getElements();

	// 	if(elements.length === 0){
	// 		throw new FileUploadException("Please upload an image or a video.");
	// 	}

	// 	/* Create a new canvas to store the final image into. */
	// 	var canvas = document.createElement("canvas");
	// 	/*
	// 		Set property pending to the number of files. Callers of getCanvas() can 
	// 		check the value of pending to know when the canvas is ready. 
	// 	*/
	// 	canvas.pending = elements.length;
		
	// 	/*
	// 		Replace the video for a canvas containing the current frame and set a file 
	// 		property on the canvas to retrieve the video file.
	// 	*/
	// 	this._videoData(elements, canvas);

	// 	/* Hook for subclasses to display a preview as the image is processed. */
	// 	this.preview(canvas);


	// 	/* Get the aspect ratio for the file type. */
	// 	var ar = this.aspectRatio[this.type];

	// 	/* 
	// 		Divide the apect ratio width or height depending on the orientation (stacked 
	// 		is the same as portrait).
	// 	*/
	// 	if(this.stacked){
	// 		ar.height /= elements.length;
	// 	} else{
	// 		ar.width /= elements.length;
	// 	}

	// 	/* Calulate the width and height for each element. */
	// 	var w = 0, h = 0;
	// 	/* Sum the width and height values of all the elements. */
	// 	for(var i=0; i<elements.length; ++i){
	// 		w += elements[i].firstChild.width;
	// 		h += elements[i].firstChild.height;
	// 	}
	// 	/* Calculate the average. */
	// 	w /= elements.length;
	// 	h /= elements.length;

	// 	/* Get the min and max mulitples. */
	// 	var size = this.dataSize();
	// 	/* Calulate the largest common multiple within the min and max range. */
	// 	var multiple = Math.max(
	// 		Math.min(Math.round(Math.min(w/ar.width, h/ar.height)), size.max), size.min
	// 	);

	// 	/* Set the width and height to the max values for each individual image. */
	// 	w = ar.width*multiple;
	// 	h = ar.height*multiple;

	// 	/* Set the dimensions of the final canvas to hold all the images. */
	// 	if(this.stacked){
	// 		canvas.width = w;
	// 		canvas.height = h * elements.length;
	// 	} else{
	// 		canvas.width = w * elements.length;
	// 		canvas.height = h;
	// 	}


	// 	var image, zoom, ratio, iw, ih, ix, iy, i, xy, type;
	// 	var container = elements.pop();

	// 	while(container !== undefined){
			
	// 		/* Get the canvas containing the image data. */
	// 		image = container.firstChild;

	// 		/*
	// 			Get the zoom value set as a property on the container by the class 
	// 			ScaleElement.
	// 		*/
	// 		zoom = ((parseFloat(container.scale) || 0) * 0.01) + 1;


	// 		ratio = Math.min(
	// 			image.width/parseFloat(container.style.width),
	// 			image.height/parseFloat(container.style.height)
	// 		)

	// 		/* Calculate the scale value */
	// 		var scale = Math.max(w/image.width, h/image.height) * zoom;

			
	// 		multiple = Math.floor(Math.min(image.width/ar.width, image.height/ar.height) / zoom);
	// 		iw = ar.width*multiple;
	// 		ih = ar.height*multiple;

	// 		/* Calculate the image x offset */
	// 		ix = Math.min(
	// 			/* calculate x */
	// 			Math.round(Math.abs(parseFloat(container.style.left) * ratio)),
	// 			/* maximum value that x can be */
	// 			image.width - iw
	// 		);

	// 		/* Calculate the image y offset */	
	// 		iy = Math.min(
	// 			/* calculate y */
	// 			Math.round(Math.abs(parseFloat(container.style.top) * ratio)),
	// 			/* maximum value that y can be */
	// 			image.height - ih

	// 		);

	// 		/* 
	// 			Calculate the x or y offset for the image inside the final canvas 
	// 			depending on the orientation.
	// 		*/
	// 		i = elements.length;
	// 		xy = this.stacked ? [0, h*i] : [w*i, 0];

	// 		type = this.scaleImageData(
	// 			{canvas:image, x:ix, y:iy, w:iw, h:ih}, 
	// 			{canvas:canvas, x:xy[0], y:xy[1], w:w, h:h}, 
	// 			1024, this.bilinear, async && this.setCanvas
	// 		);

	//		if(type !== "worker"){
	//			this.setCanvas(canvas);
	//		}

	// 		container = elements.pop();
	// 	}
		
	// 	return canvas;

	// },
	/* END GET CANVAS DATA VERSION TWO */

	/* END CANVAS DATA */
	/* -------------------------------------------------------------------------------- */


	/* IMAGE/VIDEO PROCESSORS */
	/* -------------------------------------------------------------------------------- */
	/*
		**********************************************************************************
		OVERRIDDEN BY SUBCLASS TO ADD FILE PROCESSORS TO THE EDITOR
		**********************************************************************************
		Assign a list of file processors for images and/or videos.
	*/
	assignProcessors: function(){
		this.image_processors = [];
		this.video_processors = [];
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Create an instance of each file processor for the given type and store it back in 
		the processors array.
		@param crop: an object containing the editor's width, height and padding.
		@param type: file type ("image" or "video").
	*/
	startProcessors: function(crop, type){

		var p = (type===this.video) ? this.video_processors : this.image_processors;
		
		for(var i=0; i<p.length; i++){
			this.processors[i] = new p[i](crop);
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		End all running processors and empty the processors array.
	*/
	endProcessors: function(){
		while(this.processors.length > 0){
			try{
				this.processors.pop().end(); 
			} catch(e){
				this.processors.length = 0;
			}
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*	
		Call the update function for each processor when an element is added to or removed
		from the editor.
		@param container: canvas container (used to control on screen scaling of the element).
		@param element: canvas (for images) or video element.
		@param crop: an object containing the crop area of an individual element.
	*/
	updateProcessors: function(container, element, crop){
		for(var i=0; i<this.processors.length; i++){
			this.processors[i].update(container, element, crop);
		}
	},

	/* END IMAGE/VIDEO PROCESSORS */
	/* -------------------------------------------------------------------------------- */


	/* DIMENSIONS */
	/* -------------------------------------------------------------------------------- */
	/*
		**********************************************************************************
		OVERRIDDEN BY SUBCLASS TO CHANGE THE MAX WIDTH OF THE EDITOR
		**********************************************************************************
		Return the max width of the editor's crop area.
	*/
	maxWidth: function(){
		return 420;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		**********************************************************************************
		OVERRIDDEN BY SUBCLASS TO SET THE THRESHOLD AT WHICH SCALING IS APPLIED TO AN 
		IMAGE LOADED INTO THE EDITOR - SEE toCanvas().
		**********************************************************************************
		e.g. if an image is 8056 x 3200 then it must be scaled so that it is less than or
		equal to the max resolution of 3072 x 3072.
	*/
	maxResolution: function(){
		return {width: 4096, height: 4096};//{width: 3072, height: 3072};
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return the width of the editor window.
		@param ar: aspect ratio width dimension.
		@param round: nearest value to round the width to.
	*/
	width: function(ar, padding){
		/* get the absolute max width */
		var max = this.maxWidth();
		/* get the width minus any padding of the content area */
		var width = (document.body.clientWidth || max) - padding;
		/* 
			return a width that will render the editor's width and height as even numbers
			(this makes scaling appear smoother as values are rounded to the nearest 2 - 
			see ScaleElement class)
		*/
		return roundDownTo(Math.min(width, max) / ar, 2) * ar;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return the crop dimensions for a single file in the editor.
		@param count: number of elements in the editor.
	*/
	getCrop: function(count){
		/* copy the values of this.crop into a new object */
		var crop = {}
		for(var k in this.crop){
			crop[k] = this.crop[k];
		}
		/*
			divide the width or height (depending on the editor's current orientation) by 
			the number of elements.
		*/
		crop[(this.stacked ? "height" : "width")] /= count;
		return crop;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Set the value of this.crop, which is an object containing the dimensions of the
		editor's crop area (width, height and padding).
		@param type: file type ("image" or "video").
	*/
	setCrop: function(type){
		
		/* get the aspect ratio for the file type */
		var ar = this.aspectRatio[type];

		/* get the padding of the editor's body */
		var p = padding(this.content);

		/* get the width of the editor */
		var w = this.width(ar.width, p.left+p.right);
		
		/* store values of the editor's width, height and padding */
		this.crop = { 
			width: w, 
			height: (w/ar.width)*ar.height, 
			padding: p /* padding surrounding the crop area */
		};

	},

	/* END DIMENSIONS */
	/* -------------------------------------------------------------------------------- */


	/* HELPERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Callback for the scale function called by toCanvas(). The function is passed as 
		the workerCallback and errorCallback to enable editor interactions.
	*/
	enableCallback: function(){
		this.enable();
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Convert an image into a canvas element and scale down the canvas if it is larger 
		than the maxResolution(). If the canvas is scaled using a worker then set
		a property "loading" on the canvas to true.

		@param element: image, video or canvas element (video will be returned as is).
	*/
	toCanvas: function(element){

		/* If the element added to the editor is a video then return it unchanged */
		if(element.nodeName === "VIDEO"){

			return element;
		}

		var cv;
		/* If element is a canvas assign it to cv */
		if(element.nodeName === "CANVAS"){
		
			cv = element;
		
		/* If element is an image convert it to a canvas */
		} else if(element.nodeName === "IMG"){

			cv = document.createElement("canvas");
			cv.setAttribute("id", element.getAttribute("id"));
			/* set the canvas dimensions and draw the image or frame into the canvas */
			cv.width =  Math.round(element.naturalWidth);
			cv.height = Math.round(element.naturalHeight);
			cv.getContext("2d").drawImage(element, 0, 0);
			
		} else{

			throw new FileUploadException("Canvas, image or video element expected.");
		
		}

		/* Check the width and height of the canvas - raise an exception if invalid */
		this.validateCanvas(cv);

		/* Get the max width and height values */
		var res = this.maxResolution();

		/* 
			Scale the image by a power of 2. Use the first value that brings the
			image width and height below res.width and res.height.
		*/
		var r = Math.max(cv.width/(res.width>>1), cv.height/(res.height>>1))
		var exp = Math.floor(Math.log(r)/Math.log(2));
		var scale = 1/Math.pow(2, exp);

		/* 
			Scale the image to be within res.width and res.height.
		*/
		// var scale = Math.min(res.width/cv.width, res.height/cv.height);

		/* Check that scale is a number - raise an exception if invalid */
		this.validateScale(scale);

		/* Downsample the canvas if necessary */
		if(scale < 1){

			// scale = this.scale(cv, scale, 0, 0,
			// 	this.bicubic, this.enableCallback, this.enableCallback);

			scale = this.scaleImageData(
				{canvas:cv, x:0, y:0, w:cv.width, h:cv.height},
				scale, 1024, this.bicubic, this.enableCallback, this.enableCallback
			);
			
			/* 
				Set loading to true if the canvas is being scaled by a worker - add() 
				uses this to know if it should enable editor interaction immediately.
			*/
			cv.loading = scale==="worker";
		
		}

		return cv;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return the max number of files a user can upload for a particular type.
		@param type: file type ("image" or "video").
	*/
	maxFiles: function(type){
		switch(type){
			case this.image: return this.max_img; 
			case this.video: return this.max_vid;
			default: return 0;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Append all the element passed to the function to the editor's body.
	*/
	addContent: function(){
		for(var i=0; i<arguments.length; i++){
			this.content.appendChild( arguments[i] );
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return the file type name ("image" or "video")
	*/
	getType: function(){
		return this.type || "";
	},
	/*
		Return the number of elements currently in the editor.
	*/
	count: function(){
		return this.content.childNodes.length;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Display the editor.
	*/
	show: function(){
		addClass(document.body, this.c_open);
		removeClass(this.modal, this.c_hide);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Hide the editor.
	*/
	hide: function(){
		removeClass(document.body, this.c_open);
		addClass(this.modal, this.c_hide);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Toggle the display of an element by adding or removing the class "hide".
		@param remove: boolean - if true remove class, else add class.
		@param element: element to add or remove the class from.
	*/
	toggleDisplay: function(remove, element){
		(remove ? removeClass : addClass)(element, this.c_hide);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Check that the width and height of the canvas are not zero and throw an 
		exception if either one is.
		@param cv: canvas element.
	*/
	validateCanvas: function(cv){
		if(cv.width === 0 || cv.height === 0){
			throw new Error("Invalid canvas dimensions.");
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Check that scale is a valid number (note: isFinite considers "10" to be a valid 
		number but this should not be an issue). Throw an exception if it is not.
		@param scale: floating point value.
	*/
	validateScale: function(scale){
		if(Number.isFinite(scale) === false){
			throw new Error("Invalid scale value.");
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Set the value of this.active_element and remove the class "active" from the 
		previous element and assign it to the new element. 
		@param element: canvas/video container.
		@param updateProcessors: boolean to force the class to update file processors.
	*/
	activateElement: function(element, updateProcessors){

		var change = element !== this.active_element;
		var multi  = this.count() > 1;

		if(change){

			if(this.active_element){
				/* remove ".active" from the current element */
				removeClass(this.active_element, this.c_active);
			}

			/* if the editor has more than one element... */
			if(multi){
				/* set ".active" on the new element */
				addClass(element, this.c_active);
			}

			/* set the new element as the active element */
			this.active_element = element;
		}

		if( (updateProcessors || change) && multi){
			/* set the active element for each file processor */
			for(var i=0; i<this.processors.length; i++){
				this.processors[i].setElement(element.firstChild);
			}
		}
		
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the browser supports passing data using transferable objects in a
		web worker and false if not.
		https://developers.google.com/web/updates/2011/12/Transferable-Objects-Lightning-Fast
		@param worker: web worker instance.
	*/
	isWorkerTransferSupported: function(worker){
		try{
			worker.postMessage = worker.webkitPostMessage || worker.postMessage;
			var ab = new ArrayBuffer(1);
			worker.postMessage(ab, [ab]);
			return ab.byteLength === 0;
		} catch(e){
			return false;
		}
	}
}
/* ------------------------------------------------------------------------------------ */



/* MEDIA CAPTURE */
/* ------------------------------------------------------------------------------------ */
/*
	Manage photos taken using the user's webcam. Display errors to the user using the 
	Message class in base.js.
*/
function MediaCapture(callback, thisArg){
	/*
		Throw an exception if the user is on a mobile device.
	*/
	if(isMobileDevice){
		throw new Error("Mobile device detected"); 
	}
	/* 
		Set this.supported to true if the browser supports the latest version of
		getUserMedia. Throw an exception if getUserMedia is not supported at all.
	*/
	this.polyfillers();
	/* 
		Function called each time the editor is closed. If the user has saved an image 
		then the canvas will be passed as an argument to the function.
	*/
	this.callback = function(canvas){ callback.call(thisArg, canvas); };

	/* classes to toggle */
	this.hide = "hide";
	this.live = "live";
	this.load = "loading";
	this.open = "modal_open";

	/* text displayed when the browser requests access to the camera */
	this.intro = "Can we use your camera?";
	/* capture button text */
	this.text = "Take Photo";
	/* capture button alternate text */
	this.alt_text = "Retake Photo";

	/* modal */
	this.container = getElement("capture");
	/* content container */
	this.view = getElement("capture_window");

	/* buttons */
	this.capture = getElement("photo_capture");
	this.close = getElement("close_capture");
	this.save = getElement("save_capture");

}

MediaCapture.prototype = {
	/* -------------------------------------------------------------------------------- */
	constructor: MediaCapture,
	/* -------------------------------------------------------------------------------- */
	/*
		Register click events on the capture and save buttons.
	*/
	register: function(){
		$(this.capture).on(Ulo.evts.click, {self: this}, this.captureHandler);
		$(this.save).on(Ulo.evts.click, {self: this}, this.saveHandler);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Unregister click events on the capture and save buttons.
	*/
	unregister: function(){
		$(this.capture).off(Ulo.evts.click, this.captureHandler);
		$(this.save).off(Ulo.evts.click, this.saveHandler);
	},

	/* -------------------------------------------------------------------------------- */
	/*
		Set up the capture window and display it to the user.
	*/
	openCapture: function(){

		try{
			
			/* clear the window's contents and set the button's text */
			this.restore();

			/* set the close capture window event handler */
			$(this.close).off(Ulo.evts.click, this.closeHandler)
				.one(Ulo.evts.click, {self: this}, this.closeHandler);

			/* retrieve the constraints */
			var constraints = this.getConstraints(1280, 720, false, false);
			/* request access to the user's camera */
			var self = this;	
			navigator.mediaDevices.getUserMedia( constraints )
				/* success callback function */
				.then(function(s){ self.streamHandler(s) })
				/* error callback function - do not use .catch as IE8 complains */
				["catch"](function(e){ self.errorHandler(e) });

			/* reset the boolean that indicates if the user has taken a photo */
			this.image_captured = false;

			/* stop the body from scrolling */
			addClass(document.body, this.open);

			/* show the capture window */
			addClass(this.container, this.live);
			addClass(this.container, this.load);
			removeClass(this.container, this.hide);

		} catch(e){

			this.error("Sorry, we could not open your camera.");

		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Close the capture window and release all events.
		@param canvas: optional canvas element passed by the save handler to the 
		callback when the user has taken a photo.
	*/
	closeCapture: function(canvas){

		try{
			/* hide the capture window */
			addClass(this.container, this.hide);
			/* restore scrolling for the document body */
			removeClass(document.body, this.open);
			/* stop all tracks */
			this.stopTracks();


			/* revoke the blob url and unregister all events on the video */
			if(this.video){ 
				URL.revokeObjectURL(this.video.src);
				$(this.video).off();
			}

			/* unregister the click events for the capture and save buttons */
			this.unregister();
			/* clear the interval */
			clearInterval(this.interval);
			/* unregister the close capture button */
			$(this.close).off(Ulo.evts.click, this.closeHandler);

			this.canvas = this.video = this.stream = this.count = null;

			/* process the image */
			this.callback(canvas);

		} catch(e){


		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Close capture event handler.
	*/
	closeHandler: function(e){
		e.data.self.closeCapture();
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Take a photo event handler.
	*/
	captureHandler: function(e){

		try{

			var self = e.data.self;

			/* disable the capture and save buttons */
			self.unregister();

			/* toggle the boolean indicating whether or not the user has taken a photo */
			self.image_captured = !self.image_captured;

			/* if the user has take a photo */
			if(self.image_captured){

				/* set up the coundown */
				var countdown = 3;
				self.count.innerHTML = countdown;
				removeClass(self.count, self.hide);

				self.interval = setInterval(function(){

					try{
						/* update the countdown on screen */
						if(countdown > 1){

							self.count.innerHTML = --countdown;

						/* take the photo and display the image to the user */
						} else{

							
							clearInterval(self.interval);
							/* remove the countdown from the screen */
							addClass(self.count, self.hide);
							
							/* draw the image into the canvas */
							self.canvas.width = self.video.videoWidth;
							self.canvas.height = self.video.videoHeight;
							self.canvas.getContext("2d").drawImage(self.video, 0, 0);

							self.video.pause();
							/* show the image and enable the capture and save buttons */
							self.toggleView();

						}

					} catch(ex){

						this.error();

					}

				}, 1000);				

			} else{

				self.video.play();
				/* show the live feed and enable the capture and save buttons */
				self.toggleView();

			}
			

		} catch(ex){

			this.error();	

		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Save image event handler.
	*/
	saveHandler: function(e){

		var self = e.data.self;

		/*
			if the user has taken an image, close the editor and pass the image to the 
			callback function.
		*/
		if(self.image_captured){

			self.closeCapture(self.canvas);
			
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Success callback function.
		@param s: the stream.
	*/
	streamHandler: function(s){

		try{
			/* store the stream */
			this.stream = s;

			/* create canvas for still images */
			this.canvas = document.createElement("canvas");

			/* create video for live image feed */
			this.video = makeElement("video", {"autoplay": "autoplay"});

			/* create the on screen countdown */
			this.count = makeElement("span", {"class": "countdown "+this.hide});

			/* add all the elements to the capture window */
			appendElements(emptyElement(this.view), [this.canvas, this.video, this.count]);

			var self = this;
			$(this.video).one("loadedmetadata", function(e){

				try{

					clearInterval(self.interval);
					/* unregister previous events on the capture and save buttons */
					self.unregister();
					/* register the event handlers on the capture and save buttons */
					self.register();
					/* remove the loading animation */
					removeClass(self.container, self.load);
				
				} catch(e){
					self.error();
				}

			});

			this.video.src = URL.createObjectURL(s);

		} catch(e){
			this.error();
		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Error callback function.
		@param e: the error.
	*/
	errorHandler: function(e){

		if(e.name === "SecurityError"){
			/* user did not allow access */
		} else if(e.name === "NotFoundError"){
			/* no media types were found for those specified in the constraints */
			this.error("We could not find your camera.");
		} else{
			this.error();
		}

		this.closeCapture();
	},

	/************************************************************************************/
	/* HELPERS */
	/************************************************************************************/
	/*
		Display an error message to the user and close the capture window.
		@param msg: error message.
	*/
	error: function(msg){

		messages( msg || "Something has gone wrong, please try again." );
		this.closeCapture();

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return an object containing video and audio constraints.
	*/
	getConstraints: function(w, h, facing, audio){
		return{
			video: { width:w, height:h, facingMode:(facing ? "user":"environment") },
			audio: audio
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Set the capture button text.
		@param text: string to set the button text to.
	*/
	setCaptureText: function(text){
		emptyElement(this.capture).appendChild( document.createTextNode(text) );
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Toggle the capture window between a live image and a still image when the user
		has taken a photo.
	*/
	toggleView: function(){

		/*
			toggle the class that switches between the video (live feed) and the canvas 
			(still image).
		*/
		var live = toggleClass(this.container, this.live);
		/*
			toggle the text between "Take Photo" (live feed) and "Retake Photot" (still 
			image).
		*/
		this.setCaptureText( (live) ? this.text : this.alt_text );

		/* Enable the capture and save image buttons. */
		this.register();

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Clear the capture window and reset the text.
	*/
	restore: function(){

		/* if browser has full support then display a message when requesting access */
		if(this.supported){

			var span = makeElement("span", {"class": this.load}, this.intro);
			emptyElement(this.view).appendChild(span);

		/* else clear the window as the camera will open without user input */
		} else{

			emptyElement(this.view);

		}
		
		/* restore the capture button text to its original */
		this.setCaptureText(this.text);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Stop all running tracks.
	*/
	stopTracks: function(){

		if(this.stream){

			var tracks = this.stream.getTracks();
			for(var i=0; i<tracks.length; i++){
				try{
					tracks[i].stop();
				} catch(e){

				}
			}
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Add support for browsers using the non standard name webkitURL and support for 
		browsers that still use the deprecated version of getUserMedia.
	*/
	polyfillers: function(){

		window.URL = window.URL || window.webkitURL;

		/* 
		*/
		if(!window.URL) {
			
			new Error('Object URLs are not supported by this browser')
			
		}

		/* https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia */
		/* 
			If the browser does not support mediaDevices at all set an empty object 
			first.
		*/
		if(navigator.mediaDevices === undefined) {
		
			navigator.mediaDevices = {};
		
		}
		
		/* 
			If the browser does not support getUserMedia for mediaDevices then add the
			property.
		*/
		if(navigator.mediaDevices.getUserMedia === undefined) {
			
			navigator.mediaDevices.getUserMedia = function(constraints) {

				/* Get the deprecated version of getUserMedia if available */
				var getUserMedia =	navigator.getUserMedia || navigator.webkitGetUserMedia ||
					navigator.mozGetUserMedia || navigator.msGetUserMedia;
				
				/* 
					If not supported by the browser return a rejected promise with an error
					to keep a consistent interface.
				*/
				if(!getUserMedia) {
					
					return Promise.reject(
					
						new Error('getUserMedia is not implemented in this browser')
					
					);
				
				}

				/*
					Otherwise, wrap the call to the old navigator.getUserMedia with a 
					promise.
				*/
				return new Promise(function(resolve, reject) {
				
					getUserMedia.call(navigator, constraints, resolve, reject);
				
				});

			};
		
		} else{

			/* The browser supports navigator.mediaDevices.getUserMedia */
			this.fullSupport = true;
		
		}
	},
	/* -------------------------------------------------------------------------------- */
}






/* BASE CLASS - FILE UPLOADER */
/* ------------------------------------------------------------------------------------ */
/*
	Manage multiple file uploads form images or videos (but not images and videos at the
	same time). Display errors to the user using the Message class in base.js.
	
	@param FileEditor: Editor class used to display uploaded files to the user.
	@param form_id: id of the form used to submit the uploaded files and extract the 
	input file elements from.
	@param capture_id: id of the button used to launch the media capture window.
	@param max_img: maximum number of image files a user can upload.
	@param max_vid: maximum number of video files a user can upload.
*/
function FileUpload(FileEditor, form_id, capture_id, max_img, max_vid){

	try{

		/* throws an exception if an error occurs */
		this.polyfillers();

		/* set up the class if the browser has full support */
		if(this.isFileAPISupported()){

			/* open uploaded files in this editor */
			this.editor = new FileEditor(max_img, max_vid);

			try{

				/* enable users to take pictures using their webcam */
				this.capture = new MediaCapture(this.processCapture, this);
				this.registerCapture(capture_id);

			} catch(e){
		
				/* remove the button if the browser does not support media capture */
				removeElements( getElement(capture_id) );
			}

			/* form id */
			this.form_id = form_id;
			
			/* array of uploaded files */
			this.files = [];
			/* array of files to verfiy/upload */
			this.pending = [];

			/* file type names - SAME AS EDITOR */
			this.image = "image";
			this.video = "video";
			/* button data attribute - SAME AS EDITOR */
			this.btn_data = "data-id";

			/* the file type being uploaded ("image" or "video") */
			this.verified = null;

			this.max_img = max_img || 0;
			this.max_vid = max_vid || 0;
			this.max_files = Math.max(this.max_img, this.max_vid);

			/* register change events on all file inputs within the form */
			this.registerChange(form_id);

		/* else throw an exception */
		} else{

			throw new FileUploadException();
		
		}

	} catch(e){

		console.log(FileEditor);

		if(!(e instanceof FileUploadException)){
			e = new FileUploadException("We do not support your browser \
					for file uploads. Upgrade to the latest version to continue.")
		}

		this.processException(e, true);

	}

}
FileUpload.prototype = {

	constructor: FileUpload,

	/* EVENTS */
	/* -------------------------------------------------------------------------------- */ 
	/*
		Register change events on all file input elements.
		@param cid: container id - a change event will be registered to all file input 
		elements with this container.
	*/
	registerChange: function(cid){
		try{
			$(document.getElementById(cid).querySelectorAll("input[type=file]"))
				.on("change", {self: this}, this.changeHandler);
		} catch(e){
			throw new FileUploadException("Something has gone wrong, try to refresh the page.");
		}
	},
	/* -------------------------------------------------------------------------------- */ 
	/*
		Register click events on the element that will trigger the captureHandler. Return
		true if capture is supported and false if not.
		@param cid: capture button id.
	*/
	registerCapture: function(cid){
		$(getElement(cid)).on(Ulo.evts.click, {self: this}, this.captureHandler);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Register click events on a button that will trigger the removeFileHandler.
		@param button: button element.
	*/
	registerRemove: function(button){
		$(button).on(Ulo.evts.click, {self: this}, this.removeFileHandler);
	},
	/* END EVENTS */
	/* -------------------------------------------------------------------------------- */ 


	/* EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */
	/*
		**********************************************************************************
		DEFINED BY A SUBCLASS TO HANDLE FILE UPLOADS TO THE SERVER
		**********************************************************************************
	*/
	submitHandler: function(e){
		throw new Error("Subclasses of FileUpload must define submitHandler(e).");
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove all files from the class.
	*/
	removeFilesHandler: function(e){

		e.data.self.empty();

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove an uploaded file from the class.
	*/
	removeFileHandler: function(e){

		var self = e.data.self;
		
		/* get the id of the file set on the button */
		var id = this.getAttribute(self.btn_data);
		/* remove the file from the files array */
		self.removeFileById(self.files, id);
		/* set verified to null if there are no files */
		self.resetVerified();

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Each time a file is selected by the user begin the validation process.
	*/
	changeHandler: function(e){

		try{

			var self = e.data.self;

			/* ignore empty file lists */
			if(!this.files || this.files.length === 0){ 
				throw new FileUploadException(""); 
			}
	
			/* validate the file then open it in the editor */ 
			self.processFiles(this.files);

			/* remove the current value so a user can upload the same file again */
			this.value = null;
		
		} catch(ex){

			e.data.self.processException(ex);
		
		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Open the capture window so the user can upload an image via their webcam.
	*/
	captureHandler: function(e){

		try{

			var self = e.data.self;
			/* throw an exception if the maximum number of images have been uploaded */
			self.maxUploads(self.image);
			/* hide the editor window if it is already open */
			self.hideEditor();
			/* launch the capture window */
			self.capture.openCapture();

		} catch(ex){

			e.data.self.processException(ex);
		
		}
	},
	/* END EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */


	/* EXCEPTION HANDLING */
	/* -------------------------------------------------------------------------------- */ 
	/*
		Handle thrown exceptions and display an error to the user using the global
		helper function messages.
		@param e: exception
		@param persist: boolean, if true the message will remain.
	*/
	processException: function(e, persist){

		/* if a FileUploadException was throw, display its message */
		if(e instanceof FileUploadException){ 

			if(e.msg){
				
				messages(e.msg, persist);

			}

		/* else display generic error message */
		} else{

			debug(e)
			messages("Something has gone wrong, we could not upload your file.", persist);

		}
	},
	/* -------------------------------------------------------------------------------- */ 
	/*
		Helper function that first calls processException() and then dequeue().
		@param e: exception.
		@param file: file object.
	*/
	exceptionDequeue: function(e, file){

		this.processException(e);
		this.dequeue(file);

	},
	/* END EXCEPTION HANDLING */
	/* -------------------------------------------------------------------------------- */


	/* FILE PROCESSING */
	/* -------------------------------------------------------------------------------- */
	/*
		Add a file to the pending files array to queue it for validation. Set the queued 
		flag to true on the file.
		@param file: file object.
	*/
	queue: function(file){

		file.queued = true;
		this.pending.push(file);

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove the file passed as an argument from the pending files array, then dequeue
		the next file for validation by setting queued to false.
		@param file: optional file object.
		@param start: optional boolean to indicate the start of a new batch of files.
	*/
	dequeue: function(file, start){

		if(file){

			/* remove the file from the pending files queue */
			this.removeFile(this.pending, file);
			/* set verified to null if there are no files */
			this.resetVerified();
		}

		/* 
			if it's the start of a new run then dequeue the first file in the array if it 
			has not already been dequeued, else dequeue the next available file.
		*/
		var length = (start===true && this.pending.length) ? 1 : this.pending.length;

		for(var i=0; i<length; i++){
			
			if(this.pending[i].queued){

				/* dequeue the next file (but not literally) */
				this.pending[i].queued = false;
				/* begin file validation */
				this.typeCheck(this.pending[i]);

				break;
			}
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Queue files of a single type for validation. The type is determined by the files 
		already uploaded or the type of the first file in the files list.
		@param files: file list.
	*/
	processFiles: function(files){

		/* get the file type */
		var type = this.verified || files[0].type.split("/", 1)[0];

		/* 
			get the max number of files a user can upload for this type. If the user
			has reached the limit for this type an exception is thrown.
		*/
		var max = this.maxUploads(type);

		for(var i=0; i<max && i<files.length; i++){
			try{

				/* get the type of the current file */
				var t = files[i].type.split("/", 1)[0];
				/*
					if the file has no type or the type is the same as the previous 
					files then queue it for further validation.
				*/
				if(!t || !type || t === type){

					this.queue(files[i]);

				/* else increment max to allow another file to take its place */
				} else{

					++max;

				}
				
			} catch(e){

				/* remove the file if it has been queued */
				this.removeFile(this.pending, files[i]);
				/* display error to the user */
				this.processException(e);
			
			}
		}

		/* begin file validation */
		this.dequeue(null, true);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Display the editor if it was open before the capture was launched then begin 
		validation of the canvas.
		@param canvas: canvas element containing the image taken.
	*/
	processCapture: function(canvas){

		try{

			this.showEditor();

			if(canvas){
				this.validateCapture(canvas);
			}

		} catch(e){

			this.processException(e)
		
		}
	},
	/* END FILE PROCESSING */
	/* -------------------------------------------------------------------------------- */


	/* TYPE VALIDATION */
	/* -------------------------------------------------------------------------------- */
	/*
		Perform basic type check by reading the file type property and checking it 
		against a list of accepted file type. Set the property "verified" on the file 
		object to the name of the file type (e.g. "image" or "video") or null.
		@param file: file object.
	*/
	_basicTypeCheck: function(file){

		var results = file.verified = null;
		/* 
			returns regexps for image and video file types, the accepted types should 
			match those listed in typeCheck.
		*/
		var regexps = [
			/^(image)\/(?:bmp|gif|jpeg|jpg|pjpeg|png|tif|tiff)/,
			/^(video)\//
		]
		
		for(var i in regexps){
			results = file.type.match(regexps[i]);
			if(results !== null){
				file.verified = results[1] || null;
				break;
			}
		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Check the file type by reading the start of the file and evaluating the bytes 
		using magic numbers. Fallback to _basicTypeCheck if all required features are not 
		supported. Set the property "verified" on the file object to the name of the file 
		type (e.g. image) or null.
		@param file: file object.
	*/
	typeCheck: function(file){

		try{

			/* https://developer.mozilla.org/en-US/docs/Web/API/Blob/slice */
			file.slice = file.slice || file.webkitSlice || file.mozSlice;

			/* 
				if all required features are supported, read the file and check its bytes 
				calling validateFile onload, else use basicTypeCheck and call validateFile 
				immediately.
			*/
			if(file.slice){

				var self = this;

				/* get the first four bytes of the file */
				var slice = file.slice(0, 4);
				var reader = new FileReader();

				/* read the slice of the file */
				reader.readAsArrayBuffer(slice);
				
				/* determine file type from the magic numbers */
				reader.addEventListener("load", function(e){
					
					try{

						/* access the bytes */
						var view = new DataView(reader.result);
						/* Read first 4 bytes - Big Endian=false, Little Endian=true */
						var magic = view.getUint32(0, false);
						
						/* magic number - http://www.garykessler.net/library/file_sigs.html */
						switch(magic){
									
							/* 4 byte magic numbers */
							case 0x89504E47: // image/png
							case 0x47494638: // image/gif
							// case 0xFFD8FFE0: // image/jpg
							// case 0x4D4D002A: // image/tif
								file.verified = self.image;
								break;

							case 0x66747970: // video/mp4
							case 0x00000018: // video/mp4
							case 0x00000014: // video/mp4
							case 0x0000001C: // video/mp4
							case 0x00000020: // video/mp4

								file.verified = self.video;
								break;

							default:

								/* Read first 2 bytes */
								magic = view.getUint16(0, false);

								switch(magic){

									/* 2 byte magic numbers */
									case 0xFFD8: // image/jpg	
									case 0x4D4D: // image/tif

										file.verified = self.image;
										break;

									default:

										file.verified = null;
								}
						}
					
					} catch(e){

						/* perform basic type validation instead */
						self._basicTypeCheck(file);
					
					} 

					/* validate the file */
					self.validateFile(file);

				}, false);

			} else{

				/* perform basic type validation instead */
				this._basicTypeCheck(file);

				/* validate the file */
				this.validateFile(file);

			}

		} catch(e){

			/* process exception - remove file from queue and release the next file */
			this.exceptionDequeue(e, file);

		}
	},
	/* END TYPE VALIDATION */
	/* -------------------------------------------------------------------------------- */


	/* FILE VALIDATION */
	/* -------------------------------------------------------------------------------- */
	/*
		Check that the size of the file is lte to the max file size specified by
		maxBytes() or raise a FileUploadException to display an error to the user.
		@param type: file type "image" or "video".
		@param size: file size in bytes.
	*/
	validateSize: function(type, size){
		
		/* get max number of bytes for this type */
		var bytes = this.maxBytes(type);
		
		if(size > bytes){
			
			var name = ((type === this.video) ? "a " : "an ") + type;

			throw new FileUploadException(
				"Please upload "+name+" less than "+this.formatBytes(bytes)+"."
			);
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Check that the verified file type matches the files already uploaded. If this is
		the first file (i.e. this.verified === null) then set the value of this.verified
		to the type.
		@param type: file type "image" or "video".
	*/
	validateType: function(type){

		if(this.verified === null){
			/* if this is the first file to be verified then set the class variable */
			this.verified = type;
		
		} else if(this.verified !== type){
			/* throw exception if the user is trying to upload different file types */
			throw new FileUploadException(
				"Images and videos cannot be uploaded together."
			);
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Check that the dimensions of the uploaded file fall within the minimum/maximum
		dimensions defined by resolution().
		@param element: image, video or canvas element.
		@param type: file type.
	*/
	validateResolution: function(element, type){
		
		/* type article, file resolution, resolution suffix, min/max resolutions */
		var article, res, sx, r = this.resolution(type);
		
		/* calculate the resolution for an image */
		if(type === this.image){
			
			res = (element.naturalWidth) ? element.naturalWidth*element.naturalHeight :
				element.width*element.height;
			
			article = "an ";
			
			sx = "";

		/* calculate the resolution for a video */
		} else{
			
			res = element.videoWidth*element.videoHeight;
			
			article = "a ";
			
			sx = "p";
		}

		/* throw an exception if the values are invalid */
		if(r === null || isNaN(res)){
			throw new FileUploadException("We cannot read the "+type
				+" file dimensions."
			);
		}

		/* start of the error message */
		var error = "Please upload "+article+type;

		/* throw an exception if the files resolution is greater than the maximum  */
		if(res > r.max_w*r.max_h){ 
			
			throw new FileUploadException(
				error+" less than "+r.max_w+sx+" x "+r.max_h+sx+"."
			); 
		
		}

		/* throw an exception if the files resolution is less than the minimum  */
		if(res < r.min_w*r.min_h){ 
		
			throw new FileUploadException(
				error+" larger than "+r.min_w+sx+" x "+r.min_h+sx+"."
			); 
		
		}
		
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Perform file validation once the file type has been checked and assigned to the 
		file. (file.verified === ("image" || "video" || null)).
		@param file: file object.
	*/
	validateFile: function(file){
		
		try{
			/* 
				get the html tag for this file type. If file.verified is invalid then 
				the function will return null.
			*/
			var tag = this.mimeTags(file.verified);

			if(tag === null){
				throw new FileUploadException(
					"Please upload an image (jpg, gif, png) or video file."
				);
			}


			/* check file size */
			this.validateSize(file.verified, file.size);

			/* check the verified type */
			this.validateType(file.verified);
						

			/* create blob url */
			if( this.isBlobURLSupported() ){ 
				this.blobURL(file, tag); 
				return;
			}

			/* create data url */
			if( this.isDataURLSupported() ){
				this.dataURL(file, tag); 
				return;
			}


			/* throw exception if blob and data urls are not supported */
			throw new FileUploadException("We could not upload your "+file.verified+".");
				

		} catch(e){

			/* process exception - remove file from queue and release the next file */
			this.exceptionDequeue(e, file);
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Perform validation on the canvas element used to capture images from the user's
		webcam.
		@param canvas: canvas element.
	*/
	validateCapture: function(canvas){

		try{
			/* check the type */
			var type = this.image;
			this.validateType(type);

			/* check the file size */
			var size = canvas.getContext("2d")
				.getImageData(0,0,canvas.width,canvas.height).data.length;
			this.validateSize(type, size);

			/* open the image in the editor */
			this.openEditor(canvas);

		} catch(e){

			this.processException(e);

		}

	},
	/* /* FILE VALIDATION */
	/* -------------------------------------------------------------------------------- */ 


	/* FILE URL EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */
	/*
		onloadstart event handler.
	*/
	startHandler: function(e){
	},
	/* -------------------------------------------------------------------------------- */
	/*
		onprogress event handler.
	*/
	progressHandler: function(e){
	},
	/* -------------------------------------------------------------------------------- */
	/*
		BLOB URL IMAGE LOAD.
		onload event handler for blob urls.
	*/
	loadBlobURLHandler: function(e){
		var self = e.data.self;
		self.openEditor(e.target, e.data.file);
		URL.revokeObjectURL(e.target.src);
		self.removeEvents(e.target, self.loadBlobURLHandler);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		BLOB URL VIDEO LOAD.
		onloadedmetadata event handler for blob urls of type video.
	*/
	loadedmetadataHandler: function(e){
		var self = e.data.self;
		self.openEditor(e.target, e.data.file);
		e.target.removeEventListener(e.type, self.loadedmetadataHandler);
		//URL.revokeObjectURL(e.target.src);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		DATA URL IMAGE/VIDEO LOAD.
		onload event handler for data urls.
	*/
	loadDataURLHandler: function(e){

		var self = e.data.self;
		var file = e.data.file;
		var el = document.createElement(e.data.tag);

		var handler = function(e){
			self.openEditor(e.target, file); 
			self.removeEvents(e.target, handler);
		}
		self.addEvent(el, "load", handler);
		self.addEvent(el, "error", self.errorHandler, self.getErrorContext(handler, file));
			
		el.src = e.target.result;
		self.removeEvents(e.target, self.loadDataURLHandler);

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Helper function called by an onerror handler to remove the file from the pending
		files queue and display an error to the user.
		@param file: file object.
	*/
	_errorHandler: function(file){
		this.removeFile(this.pending, file);
		this.processException(new Error());
	},
	/*
		onerror event handler for images.
	*/
	errorHandler: function(e){
		e.data.self._errorHandler(e.data.file);
		e.data.self.removeEvents(e.target, e.data.handler);
	},
	/*
		onerror event handler for videos.
	*/
	videoErrorHandler: function(e){
		e.data.self._errorHandler(e.data.file);
		e.target.removeEventListener("loadedmetadata", e.data.handler);
	},
	/* FILE URL EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */


	/*  FILE URLS */
	/* -------------------------------------------------------------------------------- */
	/*
		Create a blob url.
		@param file: file object.
		@param tag: html tag for the file.
	*/
	blobURL: function(file, tag){

		try{

			var element = document.createElement(tag);
			
			if(file.verified === this.image){
		
				this.addEvents(element, this.loadBlobURLHandler, file, tag);
				
			} else{

				var ctx = {self: this, file: file};
				this.addEvent(element, "loadeddata", this.loadedmetadataHandler, ctx);

				ctx = this.getErrorContext(this.loadedmetadataHandler, file);
				this.addEvent(element, "error", this.videoErrorHandler, ctx);

			}
			
			element.src = URL.createObjectURL(file);
			this.dequeue();

		} catch(e){

			throw new FileUploadException(
				"Sorry, we could not upload your "+file.verified+"."
			);

		}
		
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Create a data url.
		@param file: file object.
		@param tag: html tag for the file.
	*/
	dataURL: function(file, tag){

		try{

			var reader = new FileReader();
			this.addEvents(reader, this.loadDataURLHandler, file, tag);
			reader.readAsDataURL(file);

			this.dequeue();

		} catch(e){

			throw new FileUploadException(
				"Sorry, we could not upload your "+file.verified+"."
			);
		
		}
		
	},
	/*  END FILE URLS */
	/* -------------------------------------------------------------------------------- */


	/* EDITOR */
	/* -------------------------------------------------------------------------------- */
	/*
		Open the element in the editor.
		@param element: image, video or canvas element.
		@param file: file object (images created using capture do not have files).
	*/
	openEditor: function(element, file){

		try{

			if(file){
				/* remove the file from the pending files */
				this.removeFile(this.pending, file);
			}

			/* 
				check that the image or video resolution is less than the max. Throw
				an exception if not.
			*/
			this.validateResolution(element, this.verified);
			
			/* generate a unique id for the element */
			var uid = this.uid();
			/* set the id on the element */
			element.setAttribute("id", uid);
			/* add the html element to the files array */
			this.files.push(element);

			if(this.editor.isClosed){
				/* register the removeFilesHandler to the editor's close button */
				this.editor.registerClose(this.removeFilesHandler, {self: this});
				/* register the submitHandler to the editor's submit button */
				this.editor.registerSubmit(this.submitHandler, {self: this});
			}

			this.editor.open(this.verified);

			var self = this;
			setTimeout(function(){
				try{
				
					/* store the file as a property on the element */
					element.file = file
					/* open the element in the editor or raise a FileUploadException */
					var button = self.editor.add(element, self.verified);
					/* register event on the button that removes this file from the editor */
					self.registerRemove(button);

				} catch(e){

					debug(e);
				
					self.removeFile(self.files, element);
					self.processException(e);
					self.resetVerified();
				
				}
			}, 1);
			
			

		} catch(e){

			debug(e);

			this.removeFile(this.files, element);
			this.processException(e);
			this.resetVerified();

		}

	},

	/* -------------------------------------------------------------------------------- */
	/*
		Show the editor window if the editor is already open.
	*/
	showEditor: function(){
		if(this.editor.isClosed === false){ this.editor.show(); }
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Hide the editor window if the editor is already open.
	*/
	hideEditor: function(){
		if(this.editor.isClosed === false){ this.editor.hide(); }
	},
	/* END EDITOR */
	/* -------------------------------------------------------------------------------- */


	/************************************************************************************/
	/* HELPERS */
	/************************************************************************************/

	/* FILE PROCESSING HELPERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Return the max number of files a user can upload for a particular type. If type
		is invalid return the max number of all types.
		@param type: file type ("image" or "video").
	*/
	maxFiles: function(type){
		switch(type){
			case this.image: return this.max_img; 
			case this.video: return this.max_vid;
			default: return this.max_files;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return the number of files a user can upload for a given type taking into account
		the uploaded and pending files.
		@param type: file type ("image" or "video").
	*/
	maxUploads: function(type){

		var max = this.maxFiles(type);
		var available = max - (this.files.length+this.pending.length);

		if(available <= 0){

			var name = (!type) ? "file" : type;
			if(max !== 1){ name += "s" }

			throw new FileUploadException(
				"A maximum of "+max+" "+name+" can be uploaded at one time."
			);

		}

		return  available;
	},

	/* END FILE PROCESSING HELPERS */
	/* -------------------------------------------------------------------------------- */


	/* UPLOADED FILE TYPE HELPERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the uploaded file is an image.
	*/
	isImageUploaded: function(){
		return this.verified === this.image;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the uploaded file is a video.
	*/
	isVideoUploaded: function(){
		return this.verified === this.video;
	},
	/* END UPLOADED FILE TYPE HELPERS */
	/* -------------------------------------------------------------------------------- */


	/* FILE VALIDATION HELPERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Return the html tag for a given type or null.
		@param type: file type ("image" or "video").
	*/
	mimeTags: function(type){
		switch(type){ 
			case this.image: return "img"; 
			case this.video: return "video";
			default: return null;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		**********************************************************************************
		OVERRIDDEN BY SUBCLASS TO CHANGE THE ACCEPTED FILE SIZES.
		**********************************************************************************
		Return the max number of bytes a file of a certain type can be.
		@param type: file type ("image" or "video").
	*/
	maxBytes: function(type){
		switch(type){ 
			case this.image: return 10485760; 
			case this.video: return 73400320;
			default: return 0;
		}
	},
	/*
		**********************************************************************************
		OVERRIDDEN BY SUBCLASS TO CHANGE THE MIN/MAX FILE RESOLUTION.
		**********************************************************************************
		Return an object containing the minimum/maximum width and height dimensions of a 
		file.
		@param type: file type ("image" or "video").
	*/
	resolution: function(type){
		switch(type){ 
			case this.image: return {min_w:30, min_h:30, max_w:104028, max_h:104028}; 
			case this.video: return {min_w:30, min_h:30, max_w:3840, max_h:2160};
			default: return null;
		}
	},
	/* END FILE VALIDATION HELPERS */
	/* -------------------------------------------------------------------------------- */

	
	/* FILE URL EVENT HANDLERS HELPERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Return an object containing all image event handlers.
		@param handler: load handler (loadBlobURLHandler or loadDataURLHandler).
	*/
	getHandlers: function(handler){
		return {
			"loadstart": this.startHandler,
			"progress": this.progressHandler, 
			"load": handler, 
			"error": this.errorHandler
		};
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return an object containing the context information for an onerror event handler.
		@param handler: the event handler to remove when an error occurs.
		@param file: uploaded file.
	*/
	getErrorContext: function(handler, file){
		return {self:this, file:file, handler:handler}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return an object containing the context information for all image event handlers.
		@param handler: load handler (loadBlobURLHandler or loadDataURLHandler).
		@param file: file object.
		@param tag: html tag for the file.
	*/
	getContexts: function(handler, file, tag){
		return {
			"loadstart": {self:this},
			"progress": {},
			"load": {self:this, file:file, tag:tag}, 
			"error": {self:this, file:file, handler:handler}
		};
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Add an event to the element with optional context information.
		@param element: javascript element.
		@param evt: name of the event.
		@param handler: event handler.
		@param ctx: context data to pass to the handler.
	*/
	addEvent: function(element, evt, handler, ctx){
		element.addEventListener(evt, function(e){ e.data=ctx; handler(e); }, false);
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Add all image upload events to an element.
		@param element: javascript element.
		@param handler: load handler.
		@param file: file object.
		@param tag: html tag for the file.
	*/
	addEvents: function(element, handler, file, tag){
		var h = this.getHandlers(handler);
		var c = this.getContexts(handler, file, tag)
		for(var e in h){
			this.addEvent(element, e, h[e], c[e]);
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove all image upload events from an element.
		@param element: javascript element.
		@param handler: load handler (loadBlobURLHandler or loadDataURLHandler).
	*/
	removeEvents: function(element, handler){
		var h = this.getHandlers(handler);
		for(var e in h){
			element.removeEventListener(e, h[e]);
		}
	},
	/* END FILE URL EVENT HANDLERS HELPERS */
	/* -------------------------------------------------------------------------------- */

	
	/* GENERIC HELPERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Remove all files from the FileUpload class and close the editor.
	*/
	end: function(){
		this.empty();
		this.editor.close();
		this.editor = this.capture = null;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove all files from the class.
	*/
	empty: function(){
		this.files.length = 0;
		this.pending.length = 0;
		this.verified = null;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		If there are no files (uploaded or pending) then set this.verified to null.
	*/
	resetVerified: function(){
		if(this.files.length === 0 && this.pending.length === 0){ 
			this.verified = null; 
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove an element from the array.
		@param array: array of elements.
		@param element: element to remove from the array.
	*/
	removeFile: function(array, element){
		for(var i in array){
			if(array[i] === element){
				array.splice(i, 1)[0];
				return;
			}
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Remove an element from the array.
		@param array: array of elements.
		@param id: id of the element to remove from the array.
	*/
	removeFileById: function(array, id){
		for(var i in array){
			if(array[i].id === id){
				array.splice(i, 1);
				return;
			}
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Generate a unique id for elements added to the DOM.
	*/
	uid: (function(){

		/* randomly chosen prefix */
		var prefix = "uid2-uiu4-sen8-";
		/* counter suffix */
		var counter = 0;

		function uid(){
			/* 
				precaution - if the value wraps then create a new prefix and reset the 
				counter.
			*/
			if(counter === Number.MAX_VALUE){ 
				var p = ""; while(!(p=Math.random().toString(36).substr(2))){}
				prefix = p+"-";
				counter = 0;
			}
			
			return prefix+counter++;
		}
		return uid;

	}()),
	/* -------------------------------------------------------------------------------- */
	/*
		Convert the number of bytes into human readable format.
		http://codeaid.net/javascript/convert-size-in-bytes-to-human-readable-format-
			(javascript)
		@param bytes: number of bytes to convert.
		@param precision: number of digits after the decimal point.
	 */
	formatBytes: function(bytes, precision){

		precision = precision || 0;
		var kb = 1024;
		var mb = kb * 1024;
		var gb = mb * 1024;

		if ((bytes >= 0) && (bytes < kb)) {

			return bytes + " B";
		
		} else if ((bytes >= kb) && (bytes < mb)) {
		
			return (bytes / kb).toFixed(precision) + " KB";

		} else if ((bytes >= mb) && (bytes < gb)) {
		
			return (bytes / mb).toFixed(precision) + " MB";

		} else{
		
			return (bytes / gb).toFixed(precision) + " GB";
		}

	},
	/* -------------------------------------------------------------------------------- */
	/*
		Add support for browsers using the non standard name webkitURL and support for 
		browsers that do no support canvas.toBlob().
	*/
	polyfillers: function(){

		/* https://developer.mozilla.org/en-US/docs/Web/API/Window/URL */
		window.URL = window.URL || window.webkitURL;

		/* https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toBlob */
		if (!HTMLCanvasElement.prototype.toBlob) {
			Object.defineProperty(HTMLCanvasElement.prototype, "toBlob", {
				value: function (callback, type, quality) {

					var binStr = atob( this.toDataURL(type, quality).split(",")[1] ),
					len = binStr.length,
					arr = new Uint8Array(len);

					for (var i=0; i<len; i++ ) {
						arr[i] = binStr.charCodeAt(i);
					}

					return callback( new Blob( [arr], {type: type || "image/png"} ) );
				}
			});
		}
	},
	/* END GENERIC HELPERS */
	/* -------------------------------------------------------------------------------- */


	/* SUPPORT */
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the browser fully supports the file API and false if not.
	*/
	isFileAPISupported: function(){
		try{
			return Boolean(window.File && window.FileList && window.URL && window.Blob &&
				window.atob && window.Int8Array && window.DataView) && 
				this.isFileUploadSupported();
		} catch(e){
			return false;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if input type=file is supported and false if not.
		https://github.com/Modernizr/Modernizr/blob/master/feature-detects/forms/fileinput.js
	*/
	isFileInputSupported: function() {
		if (navigator.userAgent.match(/(Android (1.0|1.1|1.5|1.6|2.0|2.1))|(Windows Phone (OS 7|8.0))|(XBLWP)|(ZuneWP)|(w(eb)?OSBrowser)|(webOS)|(Kindle\/(1.0|2.0|2.5|3.0))/)) {
			return false;
		}
		var el = document.createElement("input");
		el.type = "file";
		return !el.disabled;
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the canvas element is supported and false if not.
	*/
	isCanvasSupported: function(){
		var elem = document.createElement("canvas");
		try{
			return Boolean( elem.getContext && elem.getContext("2d") );
		} catch(e){
			return false;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the browser supports file uplaods and false if not.
	*/
	isFileUploadSupported: function(){

		try{
			return Boolean(window.FormData) && this.isFileInputSupported() && 
				this.isCanvasSupported();
		} catch(e){
			return false;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if all properties required to read the bytes of a file are supported
		and false if not.
	*/
	isByteReadingSupported: function(){
		try{
			return Boolean( window.FileReader && window.Int8Array && window.DataView );
		} catch(e){
			return false;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the browser supports blob urls and false if not.
	*/
	isBlobURLSupported: function(){
		try{
			return Boolean(window.URL);
		} catch(e){
			return false;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the browser supports data urls and false if not.
	*/
	isDataURLSupported: function(){
		try{
			return Boolean( window.FileReader );
		} catch(e){
			return false;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the architecture is little endian and false if big endian.
		Return null if this cannot be determined.
	*/
	isLittleEndian: function(){
		try{
			return new Int8Array( new Int32Array([1]).buffer)[0] === 1;
		} catch(e){
			return null;
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		Return true if the onprogress event is supported and false if not.
	*/
	isXhrProgressSupported: function(){
		try{
			var xhr = new XMLHttpRequest();
			return new Boolean( xhr && xhr.upload && xhr.upload.onprogress );
		} catch(e){
			return false;
		}
	}
	/* END SUPPORT */
	/* -------------------------------------------------------------------------------- */
}

/* -------------------------------------------------------------------------------- */

Ulo.refClass(FileUpload, "FileUpload", true);
Ulo.refClass(Editor, "Editor", true);

/* -------------------------------------------------------------------------------- */





