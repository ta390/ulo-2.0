/* Post File Javascript File */
/* Dependencies: jQuery, Base.js, Upload.js */

/* ------------------------------------------------------------------------------------ */
(function () {

"use strict";

/* ------------------------------------------------------------------------------------ */
	
	if(!Array.isArray){

		Array.isArray = function(arg) {
		
			return Object.prototype.toString.call(arg) === '[object Array]';
		
		};
		
	}

/* ------------------------------------------------------------------------------------ */

	function PostForm(){

		this.jqxhr = null;

		this.TEXT_TID = 4;

		this.setSubmitDisabled(true);


		this.FileUpload = new FileUpload(

			getElement("post"), /* Context */

			this.submitEditor.bind(this), /* Submit handler */

			{ "image": { count: 2 }, "video": { count: 1 } }, /* Constraints */

			{ width: 4, height: 3}, /* Aspect ratio */

			false /* Media capture */

		);

	}

	PostForm.prototype = {

		constructor: PostForm,

		/* ---------------------------------------------------------------------------- */

		setSubmitDisabled: function(disable){

			getElement("post_submit").disabled = disable;

		},

		/* ---------------------------------------------------------------------------- */

		submitEditor: function(){

			/* 
				Min and max multipliers (aspect ratio 4:3).

				Min (image and video):
					
					32 will output a min width or height of 128px (32 X 4) which is the 
					same min value used by the Editor's isValid function for both images 
					and videos.

				Max (image):

					512 will ensure that all images are lte 2048px (512 X 4) in any
					dimension.

				Max (video):

					150 will ensure that all video posters are lte 600px (150 X 4) in any
					dimension.
			*/

			var max = this.FileUpload.file_type === "video" ? 150 : 512;

			var canvas = this.FileUpload.Editor.getCanvas(true, 32, max);

			if(canvas !== null){

				this.FileUpload.terminate();

				var file_container = getElement("file_selection");

				addClass(file_container, "disabled");

				emptyElement(file_container).appendChild(canvas);

				this.canvasReady(canvas);


				/* Setup the form */

				getElement("option1").disabled = true;


				$(getElement("post_form")).on("submit", {self: this}, this.submit);

				$(getElement("title")).on("input", {self: this}, this._textValidation);
				
				$(getElement("toggle_settings")).on(Ulo.evts.click, this.toggleSettings);
				
				$(getElement("add_option")).on(Ulo.evts.click, {self: this}, this.addOption);


				var SelectOptionBind = this.SelectOption.bind(this);
				
				menus("toggle_option0", "height", "open", SelectOptionBind);
				
				menus("toggle_option1", "height", "open", SelectOptionBind);
				
				menus("toggle_category", "height", "open", SelectOptionBind);
				
				menus("toggle_comment_settings", "height", "open", SelectOptionBind);


				removeClass(getElement("form_fields"), Ulo.cls.hide);

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Periodically check to see if the canvas has finished processing and define 
			the class variable "files" with the files to upload to the server.

			@param canvas: canvas returned from the editor.
		*/
		canvasReady: function(canvas){

			var self = this;

			var interval = setInterval(function(e){


				if(canvas.failed){

					clearInterval(interval);

					messages("We were unable to create your post. Please try again.", true);

				}


				/* If the canvas has no more files pending... */
				else if(canvas.pending === 0){

					clearInterval(interval);

					/* Convert the canvas image to a blob */
					canvas.toBlob(function(blob){
						
						/* 
							If the file is a video the canvas image is its thumbnail and
							the video file is set on the canvas as the property video.
							See Editor.videoToCanvas().
						*/
						if(canvas.videos){

							var video = canvas.videos[0];

							self.files = {

								"file0": video.file,

								"thumbnail": blob,

								"duration": video.duration,

								"thumbnail_time": video.currentTime

							};

							self.path = "video/";

							URL.revokeObjectURL(video.src);
							
							canvas.videos = null;
						}

						/*
							Else blob is the image file and the thumbnail for images are
							created server side.
						*/
						else{
						
							self.files = {

								"file0": blob

							};

							self.path = "image/";
						
						}

						self.setSubmitDisabled(false);

						removeClass(canvas.parentNode, "disabled");


					}, 'image/jpeg', 95);

				}

			}, 800);

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Toggle settings.
		*/
		toggleSettings: function(e){

			var hidden = toggleClass(getElement("settings"), Ulo.cls.hide);

			(hidden ? removeClass : addClass)(e.currentTarget, "menu_open");

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Toggle the extra/optional option.
		*/
		addOption: function(e){

			var self = e.data.self,
			menu = getElement(this.getAttribute("data-id")),
			container = self.getField(menu.id),
			html;

			/*
				If the option is being hidden disable the element. This is used by the 
				serialise() function to know if the option should be included.
			*/
			menu.disabled = toggleClass(container, Ulo.cls.hide);

			if(menu.disabled){

				html = "+ Add option";

				removeClass(this, "remove");

				/* Reset the selection to the first element in the menu */
				self.SelectOption(true, self.getToggle(menu.id), menu, $("li", menu)[1]);

			} else{

				html = "<span class='cross'>x</span> Remove option";
				
				addClass(this, "remove");

				self.removeText(container);

			}

			emptyElement(this).innerHTML = html;

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Change event handler for input[type='color']. Update the background colour of
			icon menu button and update data-colour attribute of the li element.
		*/
		colourPicker: function(e){

			/* Icon menu button. */
			e.data.button.style.backgroundColor = e.target.value;

			/* Colour picker li element in the icons menu */
			e.data.li.setAttribute("data-colour", e.target.value);

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Apply the colour value to the option in focus.
		*/
		focusColourPicker: function(e){

			var self = e.data.self;

			/* All colour picker input elements */
			var colours = $("input[type='color']");

			/*
				NOTE: The colour input element that was last instantiated is the active 
				one. If you track which colour input element is active you could apply 
				these settings to that input element only or just update all.
			 */
			for(var i=0; i<colours.length; ++i){

				/* Set the current value of each colour picker to the current element */
				colours[i].value = e.data.li.getAttribute("data-colour") || "#FFFFFF";

				/*
					Reset the change event to give each input element the data of the 
					element in focus.
				*/
				$(colours[i]).off("change", self.colourPicker)
					.on("change", e.data, self.colourPicker);

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Remove the colourPicker and focusColourPicker events.

			@param content: content of the button that toggles the drop down menu.
		*/
		removeColourPicker: function(content){

			if(content.data){

				/*
					NOTE: This event may be turned on by focusColourPicker for the input 
					element after it has been turned off if there is another colour input
					element in use. This isn't an issue just something to know.
				*/
				$(content.data.input).off("change", this.colourPicker);

				$(content).off(Ulo.evts.click, this.focusColourPicker);

				content.data = null;

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Prevent the space bar form toggling the drop down menu. See SelectOption().
		*/
		interceptSpaceBar: function(e){
		
			if(e.which === 32){

				e.preventDefault(); 

			}
		
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Option menu callback function triggered each time the menu is opened or 
			closed.

			@params - See menus(...) - base.js
		*/
		SelectOption: function(isClosed, button, menu, target){

			if(isClosed){

				/* If the selected element is a descendance of the menu.. */
				if( isDescendant(menu, target, 3) ){

					/* Normalise target to the li element */
					while(target.nodeName !== "LI"){

						target = target.parentNode;

					}

					/* Get the current selection */
					var selected = $("li.selected", menu)[0];

					/* If a new element has been selected update the current selection */
					if(target !== selected){

						removeClass(selected, "selected");

						addClass(target, "selected");


						var content = $("div.content", button)[0];

						appendElements(emptyElement(content), target.childNodes, true);

						this.removeColourPicker(content);


						/*
							If the element selected is the text input field add events to
							validate the input and toggle the icons menu.
						*/
						if(hasClass(target, "text")){

							var text_input = content.getElementsByTagName("input")[0];

							/*
								Used by validateForm() to determine if this option should 
								be included in the validation.
							*/
							text_input.setAttribute("required", "required");

							$(text_input).on("keyup", this.interceptSpaceBar);

							$(text_input).on("input", {self: this}, this._textValidation);


							var icon_menu_btn = content.getElementsByTagName("button")[0];

							menus(icon_menu_btn, "height", false, this.SelectIcon.bind(this));

							var icon_menu = getElement(icon_menu_btn.getAttribute("data-toggle"));

							this.SelectIcon(true, icon_menu_btn, icon_menu, $("li.default", icon_menu)[0]);

							$(icon_menu_btn).trigger(Ulo.evts.click);

						}

					}

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Icon menu callback function triggered each time the menu is opened or closed.

			@params - See menus(...) - base.js
		*/
		SelectIcon: function(isClosed, button, menu, target){

			if(isClosed){

				/* The icon menu is wrapped in a div so get the ul from the container */
				// menu = menu.getElementsByTagName("ul")[0];

				/* If the selected element is a descendance of the menu.. */
				if( isDescendant(menu, target, 3) ){

					/* Normalise target to the li element */
					while(target.nodeName !== "LI"){

						target = target.parentNode;

					}

					/* Get the current selection */
					var selected = $("li.selected", menu)[0];

					/* If a new element has been selected update the current selection */
					if(target !== selected){

						removeClass(selected, "selected");

						addClass(target, "selected");


						var content = button.parentNode;

						this.removeColourPicker(content);

						/*
							If the selected element has an icon attribute set it on the
							button.
						*/
						var icon = target.getAttribute("data-icon");

						if(icon){

							button.setAttribute("data-icon", icon);
							button.className = "toggle_icons font_icon " + icon;

						} else{

							button.removeAttribute("data-icon");
							button.className = "toggle_icons";

						}

						/*
							If the selected element has a colour attribute set it on the
							button.
						*/
						var colour = target.getAttribute("data-colour");

						if(colour){

							button.setAttribute("data-colour", colour);
							button.style.backgroundColor = colour;

							var colour_input = target.querySelector("input");

							/*
								If the icon selected is the colour picker add events to
								manipulate the colour picker.
							*/
							if(colour_input !== null){

								colour_input.value = colour;

								content.data = {

									input: colour_input, 
									button: button, 
									li: target,
									self: this

								};

								$(colour_input).off("change", this.colourPicker)
									.on("change", content.data, this.colourPicker);

								$(content).on(Ulo.evts.click, content.data, this.focusColourPicker);

							}


						} else{

							button.removeAttribute("data-colour");
							button.style.backgroundColor = null;

						}

					}

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return the menu element's toggle button.

			@param id: menu element id.
		*/
		getToggle: function(id){

			return getElement("toggle_" + id);

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return the selected icon.

			@param id: menu element id.
		*/
		getIcon: function(id){

			return $("li.selected", getElement(id + "_icons"))[0];

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return the element's container.

			@param id: element id.
		*/
		getField: function(id){

			return getElement(id + "_container");

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return the form's error container for no field specific error messages.

			@param form: form.
		*/
		getNonField: function(form){

			var id = form.id + "_errors";

			var container = getElement(id);

			if(container === null){

				container = makeElement("div", {id:id, "class": "form_errors"});

				form.insertBefore(container, form.firstChild);

			}

			return  container;

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Remove all messages.

			@param context: option context to narrow the scope.
		*/
		removeText: function(context){

			$("p.validation", context).remove();

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Add all messages.

			@param container: container to add messages to.
			@param messages: string or array of strings.
			@param cls: optional class set on the <p> element - defaults to "error".
		*/
		addText: function(container, messages, cls){

			if(container !== null){

				if( Array.isArray(messages)===false ){

					messages = [messages];

				}

				for(var i=0; i<messages.length; ++i){

					if(messages[i]){

						container.insertBefore( 

							makeElement("p", {

								"class": "validation " + (cls===undefined ? "error" : cls)

							}, messages[i]),

							container.firstChild

						);

					}

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Add all server error messages.

			@param form: form.
			@param xhr: XMLHttpRequest.
		*/
		addServerErrors: function(form, xhr){
			
			this.removeText(form);

			if( xhr.responseJSON!==undefined && xhr.responseJSON.errors!==undefined ){

				for(var n in xhr.responseJSON.errors){

					this.addText(

						n==="__all__" ? this.getNonField(form) : this.getField(n),
						xhr.responseJSON.errors[n]

					);

				}

			} else{

				xhrErrorMessage(xhr, "Oh no, either your connection has gone or we have \
					rejected your file.");

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return true if the form is valid and false if not.
		*/
		validateForm: function(){

			var is_valid = true;

			var text_inputs = $("input[type='text']", getElement("post_form"));

			for(var i=0; i<text_inputs; ++i){

				if( this.textValidation(text_inputs[i]) === false ){

					is_valid = false;

				}

			}

			return is_valid ? Boolean(this.files) : false;

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return true if the text input field has a value and false if not.

			@param input: input[type='text'].
		*/
		textValidation: function(input){

			var is_valid = false;

			var container = this.getField(input.name);

			this.removeText(container);

			var required = input.getAttribute("required");

			if(required && regexp_unicode_ws.test(input.value)){

				this.addText(container, "This field is required.");

			} else{

				is_valid = true;

			}


			return is_valid;

		},

		/* ---------------------------------------------------------------------------- */
		/*
			textValidation() event handler.
		*/
		_textValidation: function(e){

			e.data.self.textValidation(e.currentTarget);

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Add all option data attributes to the FormData instance.

			@param element: Element to read the data attributed from.
			@param prefix: name prefix.
			@paeam data: FormData instance. 
		*/
		serialiseAttrs: function(element, prefix, data){

			var attrs = /^data-(text|icon|colour|option_type)/;

			for(var i=0, name; i<element.attributes.length; ++i){

				name = element.attributes[i].name;

				if(attrs.test(name)){

					name = name.replace(/^data/, prefix)

					data.append(name, element.attributes[i].value);

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Serialse the form and return a FormData instance.
		*/
		serialise: function(form){

			var data = new FormData();


			var fields = [

				getElement("title"), 
				getElement("description"), 
				form.querySelector("input[name='" + Ulo.Session.CSRF_TOKEN_NAME + "']")

			];

			for(var i=0; i<fields.length; ++i){

				data.append(fields[i].name, fields[i].value); 

			}


			fields = [getElement("category"), getElement("comment_settings")];

			for(var i=0, li; i<fields.length; ++i){

				li = $("li.selected", fields[i])[0]

				data.append(fields[i].getAttribute("name"), li.getAttribute("value"));

			}


			fields = [getElement("option0"), getElement("option1")];

			for(var i=0, li; i<fields.length; ++i){

				if( fields[i].disabled!==true ){

					var name = fields[i].getAttribute("name");

					li = $("li.selected", fields[i])[0];

					this.serialiseAttrs(li, name, data);

					var type = li.getAttribute("data-option_type");

					if(type == this.TEXT_TID){

						li = this.getIcon(name);

						console.log(li);

						this.serialiseAttrs(li, name, data);

						var input = this.getToggle(name).getElementsByTagName("input")[0];

						data.append(name+"-text", input.value);

					}

				}

			}


			for(var key in this.files){

				data.append(key, this.files[key]);

			}

			return data;

		},

		/* ---------------------------------------------------------------------------- */
		/*

		*/
		submit: function(e){

			e.preventDefault();

			var self = e.data.self, form = this;

			// var formData = self.serialise();
			// var iter = formData.entries();
			// var en;
			// while( (en = iter.next().value) ){
			// 	console.log(en);
			// }

			if(self.jqxhr===null && self.validateForm() && self.path !== undefined){

				/* Send the form data for server side validation */
				self.jqxhr = $.ajax({

						type: "POST",
						data: self.serialise(form),
						statusCode: requestCodes,
						cache: false,
						processData: false,
						contentType: false,
						url: form.getAttribute("action") + self.path

					})

					.done(function(data, sc, xhr){

						Ulo.replacePage(data.url || "/");

					})
				
					.fail(function(xhr){

						console.log(xhr);

						self.addServerErrors(form, xhr);			
				
					})

					.always(function(xhr){
					
						self.jqxhr = null;	
				
				});

			}

			e.preventDefault();

		}

	}


	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */

	$(function(){

		try{

			new PostForm();

		} catch(e){

			messages("The editor did not loaded successfully. Please refresh the page and try again.");

		}

	});

/* ------------------------------------------------------------------------------------ */

}());

