/* Post Report Javascript File */
/* Dependencies: jQuery, Base.js */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";


/* ------------------------------------------------------------------------------------ */
	
	function PostReport(page_id, ul_id){

		Ulo.checkDependencies("Session", "Pip");

		this.page_id = page_id;

		this.ul_id = ul_id;

		this.jqxhr = null;

		this.setSubmitDisabled(true);

		this.register();	

	}
	PostReport.prototype = {

		constructor: PostReport,

		/* ---------------------------------------------------------------------------- */
		/*
			Register all event handlers.
		*/
		register: function(){
			
			$(this.getForm()).on("submit", {self: this}, this.submit);

			/* Detect when a radio button has been selected */
			$(getElement(this.ul_id)).on("change", {self: this}, this.enableSubmit);
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Unregister all event handlers.
		*/
		unregister: function(){
			
			$(this.getForm()).off("submit", this.submit);

			$(getElement(this.ul_id)).off("change", this.enableSubmit);
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/* 
			Return the report form.
		*/
		getForm: function(){

			return getElement(this.page_id+"_form");
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Disable the form's submission button.
		*/
		setSubmitDisabled: function(disable){
		
			var submit = getElement(this.page_id+"_submit")

			submit.disabled = disable;

			(disable ? addClass : removeClass)(submit, "disabled");
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Enable the form's submission button.
		*/
		enableSubmit: function(e){

			e.data.self.setSubmitDisabled(false);
			
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return the field's error container.

			@param name: field name.
		*/
		getField: function(name){

			return getElement( name + "_container" );

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Return the form's error container.

			@param form: form.
		*/
		getNonField: function(form){

			var id = form.id + "_errors", container = getElement(id);

			if(container===null){

				container = makeElement("div", {"id": id, "class": "form_errors"});
				form.parentNode.insertBefore(container, form);

			}

			return container;
		
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Remove all messages.

			@param context: option context to narrow the scope.
		*/
		removeText: function(context){

			$("p.validation", context).remove();

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Add all messages.

			@param container: container to add messages to.
			@param messages: string or array of strings.
			@param cls: optional class set on the <p> element - defaults to "error".
		*/
		addText: function(container, messages, cls){

			if(container !== null){

				if( Array.isArray(messages)===false ){

					messages = [messages];

				}

				for(var i=0; i<messages.length; ++i){

					if(messages[i]){

						container.insertBefore( 

							makeElement("p", {

								"class": "validation " + (cls===undefined ? "error" : cls)

							}, messages[i]),

							container.firstChild

						);

					}

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Add all server error messages.

			@param form: form.
			@param xhr: XMLHttpRequest.
		*/
		addServerErrors: function(form, xhr){
			
			this.removeText(getElement(this.page_id));

			if( xhr.responseJSON!==undefined && xhr.responseJSON.errors!==undefined ){

				for(var n in xhr.responseJSON.errors){

					this.addText(

						n==="__all__" ? this.getNonField(form) : this.getField(n),
						xhr.responseJSON.errors[n]

					);

				}

			} else{

				xhrErrorMessage(xhr);

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Handle form submissions.
		*/
		submit: function(e){

			e.preventDefault();

			var self=e.data.self, form=this;

			if(self.jqxhr===null && hasClass(getElement(self.page_id), Ulo.cls.hide)===false){

				self.jqxhr = $.ajax({

						type: "POST",
						url: form.getAttribute("action"),
						data: $(form).serialize(),
						statusCode: requestCodes,
					
					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/* 
							Render the report complete page or the login form.
						*/

						if(data.html && Ulo.Pip.isPip(form)){

							Ulo.Pip.displayPage(xhr, "post_report_complete");

						}

						/* 
							Else redirect to the url.
						*/

						else{

							Ulo.replacePage(data.url);

						}
					
					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr){

						if(Ulo.Session.csrfExpired(xhr)){

							self.setSubmitDisabled(true);
							
							self.unregister();
							
						}

						self.addServerErrors(form, xhr);

					})

					.always(function(){

						self.jqxhr=null;

				});

			}

		},

	}

	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */

	$(function(){

		new PostReport("post_report", "issue");

	});

/* ------------------------------------------------------------------------------------ */
}());