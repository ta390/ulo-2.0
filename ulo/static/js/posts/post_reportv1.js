/* Post Report Javascript File */
/* Dependencies: jQuery, Base.js */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";


/* ------------------------------------------------------------------------------------ */
	
	function PostReport(page_id, ul_id){

		Ulo.checkDependencies(true, "Session", "PIP");

		this.page_id = page_id;
		this.ul_id = ul_id;

		this.auto_submit = true;
		this.jqxhr = null;

		this.disableSubmit();
		this.register();

		/* ID of the textarea field */
		this.info_id = "information";
		this.info_error = this.radio_selected = false;
		this.info_evt = isEventSupported("input") ? "input" : "change";
			

	}
	PostReport.prototype = {

		constructor: PostReport,

		/* ---------------------------------------------------------------------------- */
		/*
			Register all event handlers.
		*/
		register: function(){
			
			$(this.getForm()).on("submit", {self:this}, this.submitReport);
			/* Detect when a radio button is selected */
			$(getElement(this.ul_id)).on("change", {self:this}, this.enableSubmit);
			/* Validate the input length of the textarea field */
			$(getElement(this.info_id)).on(this.info_evt, {self:this}, this.validateInfoLength);
		
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Unregister all event handlers.
		*/
		unregister: function(){
			
			$(this.getForm()).off("submit", this.submitReport);
			$(getElement(this.ul_id)).off("change", this.enableSubmit);
			$(getElement(this.info_id)).off(this.info_evt, this.validateInfoLength);
		
		},
		/* ---------------------------------------------------------------------------- */
		/* 
			Return the post report form.
		*/
		getForm: function(){
			return getElement(this.page_id+"_form");
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the post report form submit button.
		*/
		getSubmit: function(){
			return getElement(this.page_id+"_submit");
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Display the form completed page.
		*/
		showCompletePage: function(){
			
			if(Ulo.PIP.loginRequired()===false){

				removeClass(getElement(this.page_id+"_complete"), Ulo.cls.hide);
			
			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Disable the form's submission button.
		*/
		disableSubmit: function(){
			this.getSubmit().disabled=true;
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Enable the form's submission button if there are no errors.
		*/
		enableSubmit: function(e){

			/*
				If e is undefined then the function was called from the validateInfoLength
				function, else it was triggered by the change event attached to the radio
				input elements.
			*/
			var self = (e === undefined) ? this : e.data.self;

			/* Set radio_selected to true when a radio button is selected. */
			self.radio_selected = self.radio_selected || self !== this;
			
			/*
				Enable the submit button if a radio button is selected and the textarea
				field length is less than or equal to the maxlength attribute.
			*/
			if(self.radio_selected===true && self.info_error===false){
				self.getSubmit().disabled=false;
			}
			
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Validate the length of the textarea input and display/remove an error message
			depending on its status.
		*/
		validateInfoLength: function(e){

			var self = e.data.self;
			var max = this.getAttribute("maxlength");

			if(max && max < this.value.length){
			
				if(self.info_error===false){
					
					var error = {}
					error[this.id]=["Please explain the issue in "+max+" characters or less."]
					/* This function sets info_error to true. */
					self.addErrors(error);
					self.disableSubmit();

				}	

			} else if(self.info_error){
				
				self.removeErrors(self.getErrorContainer(this.id));
				self.info_error=false;
				self.enableSubmit();
			}
			
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the container used to append error messages to. Each field is 
			assumed to have a container with the id "<element.id>_container".

			@param id: element id.
		*/
		getErrorContainer: function(id){

			var container = getElement(id+"_container");

			/*
				If the field does not have its own container then create a generic errors
				container and place it above the form.
			*/
			if(container === null){

				container = getElement(this.page_id+"_form_errors");

				if(container === null){

					container = makeElement("div", {"id": this.page_id+"_form_errors"});
					var form = this.getForm();
					form.parentNode.insertBefore(container, form);
				
				}
				
			}

			return container;
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Append each error to its field container.

			@param errors: object containing an array of errors for each field. The key 
				is the id of the field. E.g. {"field_id": ["error one", "error two"], ...}
		*/
		addErrors: function(errors){

			/* Append each error to its field container */
			for(var id in errors){

				var error, container=this.getErrorContainer(id);

				for(var i in errors[id]){

					error = makeElement("p", {"class": "error"}, errors[id][i]);
					container.appendChild( error );
				
				}

				/*
					Set info_error in this function so that it is updated when either
					validateInfoLength or the server returns an error for this field.
				*/
				if(id===this.info_id){
					this.info_error = true;
				}

			}
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Remove all errors within a container. Errors are identified as p tags with 
			the class .error

			@param container: DOM element.
		*/
		removeErrors: function(container){
			$("p.error", container).remove();
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Handle form submissions.
		*/
		submitReport: function(e){

			e.preventDefault();

			var self=e.data.self, form=this;

			if(Ulo.PIP.loginRequired()===false && self.jqxhr===null){

				self.disableSubmit();

				self.jqxhr = $.ajax({

						type: "POST",
						url: form.getAttribute("action"),
						data: $(form).serialize(),
						statusCode: requestCodes,
					
					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/*
							If no html was returned then redirect to the report complete page.
						*/
						if(data.html===undefined || Ulo.PIP.isPIP(form)===false){

							Ulo.replacePage(data.url);

						/*
							Else render the report complete page within the current page.
						*/
						} else{

							/* Hide the report form's container */
							var report = addClass(getElement(self.page_id), Ulo.cls.hide);

							Ulo.PIP.displayPage(
								xhr, data, undefined, undefined, self.showCompletePage.bind(self)
							);
						
						}
					
					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr){


						if(Ulo.Session.hasErrors(xhr)){

							/* Remove all form errors */
							self.removeErrors(getElement(self.page_id));

							/* Add each new error to the form */
							self.addErrors(xhr.responseJSON.errors);

						}

						else {

							if(Ulo.Session.csrfExpired(xhr)){
							
								self.unregister();
							
							}

							requestMessages(xhr);							

						}

					})

					.always(function(){

						self.jqxhr=null;

				});

			}

		},

	}

	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */

	$(function(){

		new PostReport("post_report", "issue");

	});

/* ------------------------------------------------------------------------------------ */
}());