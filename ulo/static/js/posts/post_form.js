/* Profile Page Javascript File */
/* Dependencies: jQuery */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";


	/*
		Manage image and video uploads for posts.
	*/
	function PostForm(form_id){

		/* boolean indicating whether the browser supports the input event */
		this.isInputEvent = !isEventSupported("input");

		/* use the best supported event for field validation */
		this.evt = this.isInputEvent ? "input" : "change";

		/* store reference to the submit button */
		this.submitButton = getElement("id_submit");

		/* 
			class name given to an active li element - initially set on the active 
			element server side - posts .forms PostOptionForm
		*/
		this.active = "active";

		/* current ajax request */
		this.jqxhr = null;

		/* the number of errors */
		this.errors = 0;

		/* data attributes set on the elements */
		this.data = {
			/* content editable data attributes */
			placeholder: "data-placeholder",
			autofocus: "data-autofocus",
			maxlength: "data-maxlength",
			/* id of the menu to toggle */
			toggle: "data-toggle",
			/* hexadecimal colour value set on an li element */
			colour: "data-colour",
			/* option names prefix set on the ul element */
			prefix: "data-prefix",
			/* drop down menu li element numeric values */
			value: "data-value",
			/* drop down menu li element text values */
			text: "data-text",
			/* input element text to display for an empty field error */
			help: "data-help",
			/* class/icon name of the option set on an li element */
			icon: "data-icon"
		}

		/* TEXT FIELDS */
		/* ---------------------------------------------------------------------------- */
		/* text input field */
		this.fields = [

			getElement("id_title"), 
			getElement("id_description"),	

		];

		/* add text validation events to all text input fields */
		for(var i=0; i<this.fields.length; ++i){
			
			if(this.fields[i].getAttribute("required") && /\s*/.test(this.fields[i].value)){
				
				++this.errors;
				
			} else{

				this.fields[i].is_valid = true;

			}

			this.register(this.evt, this.fields[i], this.textValidationHandler);

		}

		/* enable the button if there are no errors or the change event is being used */
		if(this.isInputEvent === false || this.errors === 0){

			this.submitButton.removeAttribute("disabled");

		}

		/* DROP DOWN MENU FIELDS */
		/* ---------------------------------------------------------------------------- */
		/* 
			The toggle buttons used to show/hide the options menu and display the 
			selected option.
		*/
		this.options = [

			getElement("toggle_option0"), 
			getElement("toggle_option1"),
		];

		this.menuSetUp(this.options, true);
		

		/* 
			The toggle buttons used to show/hide the category and comment settings and 
			display the selected item.
		*/
		this.settings = [
			getElement("toggle_category"), 
			getElement("toggle_comment_settings")
		]
	
		this.menuSetUp(this.settings, false);

		
		/* ---------------------------------------------------------------------------- */
		/* toggle the display of the extra vote option */
		$(getElement("toggle_options")).on(Ulo.evts.click, {self: this}, this.addVoteOne);
		/* toggle the display of the settings panel */ 
		$(getElement("toggle_settings")).on(Ulo.evts.click, this.toggleSettings);
		/* submit the form */
		$(this.submitButton).on(Ulo.evts.click, {self: this}, this.submitHandler);
		/* ---------------------------------------------------------------------------- */
	}
	PostForm.prototype = {

		constructor: PostForm,

		/* HELPER FUNCTIONS */
		/* ---------------------------------------------------------------------------- */
		/*
			Helper function to add an event handler to an element.
			@param evt: event name.
			@param element: element to attach the event to.
			@param handler: function to call when the event is triggered.
		*/
		register: function(evt, element, handler, data){

			data = data || {};
			data.self = this;

			$(element).on(evt, data, handler);

		},
		/*
			Helper function to remove an event handler from an element.
			@param evt: event name.
			@param element: element to attach the event to.
			@param handler: function to call when the event is triggered.
		*/
		unregister: function(evt, element, handler){

			$(element).off(evt, handler);

		},
		/* END HELPER FUNCTIONS */
		/* ---------------------------------------------------------------------------- */


		/* ERROR TRACKING */
		/* ---------------------------------------------------------------------------- */
		/* 
			enable the submit button 
		*/
		removeError: function(element){

			if(element.is_valid === false){
				--this.errors;
			}

			if(this.errors === 0){
				this.submitButton.removeAttribute("disabled");
			}

			element.is_valid = true;

		},
		/* 
			disable the submit button 
		*/
		addError: function(element, force_error){

			if(element.is_valid || force_error === true){
				++this.errors;
			}
				
			if(this.isInputEvent && this.errors > 0){
				this.submitButton.setAttribute("disabled", "disabled");
			}

			element.is_valid = false;
		},
		/* END ERROR TRACKING */
		/* ---------------------------------------------------------------------------- */


		/* FORM FIELD MESSAGES */
		/* ---------------------------------------------------------------------------- */
		/*
		*/
		addMessage: function(element, msg){

			this.addError(element);

			var name = element.getAttribute("name");

			if(name){

				var id = name+"_error";
				var div = getElement(id);
				var container = getElement(name+"_container");

				if(div){

					div.replaceChild(document.createTextNode(msg), div.lastChild);

				} else if(container){

					div = makeElement("div", {"id":id, "class":"error"});

					var icon_div = makeElement("div", {"class": "icon_container"});
					var icon = makeElement("span", {"class": "icon_error"});

					div.appendChild(icon_div).appendChild(icon);
					div.appendChild(document.createTextNode(msg));

					container.insertBefore(div, container.firstChild);
				}

			}

		},
		/*
		*/
		removeMessage: function(element){

			this.removeError(element);
			removeElements(getElement(element.getAttribute("name")+"_error"));

		},
		/* END FORM FIELD MESSAGES */
		/* ---------------------------------------------------------------------------- */


		/* DROP DOWN MENU SETUP */
		/* ---------------------------------------------------------------------------- */
		menuSetUp: function(elements, isOption){

			/* set the default menu information on the drop down button */
			for(var i=0; i<elements.length; ++i){

				/* create an element to store the selected items data within the button */
				var span = makeElement("span", {"class": "content"});

				/* get the ul element that the button will toggle */
				var ul = getElement(elements[i].getAttribute(this.data.toggle));

				/* 
					add a button to the input text field which is the last li in the ul.
					NOTE: li.firstChild is assumed to be the input and li.lastChild is
					assumed to be the button by callback(), iconCallback() and changeContent().
				*/
				if(isOption){

					/* 
						remove TextNodes until the the ul.lastChild is the input text field. The
						ul list renders the input field as the last element in the ul 
					*/
					while(ul.lastChild && ul.lastChild.nodeType !== ul.lastChild.ELEMENT_NODE){
						ul.removeChild(ul.lastChild);
					}

					/* create a button to toggle the icon/colour menu button */
					var menu_btn = makeElement(
						"button", {
							/* id of the menu to toggle */
							"data-toggle": "icons"+i,
							/*
								ignore click events on this field when it is the child element of a 
								button that triggers a menu click event. See Menu Class base.js.
							*/
							"data-ignore": "true",
							"class": "icon",
							"type": "button"
						}
					);

					/* add the menu button to the li element */
					ul.lastChild.appendChild(menu_btn);
					/* mark the li element to easily identify it as the text field */
					ul.lastChild.isText = true;

					/* get the default icon option and set it as a property on the ul */
					var menu = getElement(menu_btn.getAttribute(this.data.toggle));
					menu.active = menu.original = menu.querySelectorAll("."+this.active)[0];
					/* add the icon class to the button */
					addClass(menu_btn, menu.active.getAttribute(this.data.icon));
					/* store reference to the ul on the button */
					menu_btn.menu = menu;

				}

				/*
					store reference to the span element on the button for easy access 
					in callback() and iconCallback(). DO THIS BEFORE setContent().
				*/
				elements[i].content = span;
				/* 
					store boolean to indicate if the button is an options button.
					DO THIS BEFORE setContent(). 
				*/
				elements[i].isOption = isOption;


				/* add the contents of the default li element to the span */
				var active_element = ul.querySelectorAll("."+this.active)[0];
				this.setContent(elements[i], active_element);
				
				
				/* append the span element to the button */
				elements[i].appendChild(span);
				/* store the field name as an attribute on the button */
				elements[i].setAttribute("name", ul.getAttribute("name"));
				/* add menu click event to toggle the ul element */
				menus(elements[i], "height", 200, false, this.callback, this);

			}

		},

		/* DROP DOWN MENU SETUP */
		/* ---------------------------------------------------------------------------- */


		/* TEXT FIELD VALIDATION */
		/* ---------------------------------------------------------------------------- */
		textValidation: function(element){

			var required = element.getAttribute("required");
			var value = element.value;
			var max = 10; // parseInt(element.getAttribute("maxlength")) || false

			if(required && regexp_unicode_space.test(value)){

				var help_text = element.getAttribute(this.data.help) || "Please fill out this field.";
				this.addMessage(element, help_text);

			} else if(max && $.trim(value).length > max){

				this.addMessage(element, "Maximum of "+max+" characters.");

			} else{

				this.removeMessage(element);
				return true;

			}

		},

		textValidationHandler: function(e){

			var self = e.data.self, input = e.target;
			var timeout = (self.isInputEvent) ? 500 : 0;

			setTimeout(function(e){
				self.textValidation(input);
			}, timeout);
			

		},
		/* END TEXT FIELD VALIDATION */
		/* ---------------------------------------------------------------------------- */

		getOptionText: function(option){

			var button = false;

			if(option.nodeName === "BUTTON"){

				button = option;
				option = option.element;

			}

			if(option && option.nodeName === "LI"){

				if(button && option.isText){

					option = button.content.firstChild.value;

				} else{
					
					option = option.getAttribute(this.data.text) || "";
				
				}

			}

			/* option can be null at this point but $.trim can handle it */
			return $.trim(option).toLowerCase();

		},

		optionValidation: function(button, element){

			if(button.isOption){

				var text = this.getOptionText(element || button);

				console.log(text);

				if(text){

					for(var i=0; i<this.options.length; ++i){
						
						if(button !== this.options[i]){

							if(text === this.getOptionText(this.options[i])){

								messages("The button text should be different");
								return false;

							}

						}
						
					}

				}

			}

			return true;

		},

		optionValidationHandler: function(e){

			e.data.self.optionValidation(e.data.button, e.target.value);
		},

		/* HASH TAGS */
		/* ---------------------------------------------------------------------------- */
		/*
		*/
		interceptBackspace: function(e){

			if(hasClass(this, e.data.placeholder) && (e.which === 8 || e.which === 46)){
				e.preventDefault();
			}
		},
		/*
			Add events to handle the display of placeholder text and hash tags
			for content editable divs.
			@param divs: array of content editable divs.
		*/
		contentEditable: function(divs){
			
			// var class_name = "placeholder";

			// var hashTags = (this.evt === "input") ? this.typedHashTags : this.hashTags;

			// for(var i=0; i<divs.length; ++i){

				

			// 	this.register(this.evt, divs[i], hashTags);

			// 	//$(divs[i]).on("keydown", {placeholder: class_name}, this.trapBackspace);

			// 	addClass(divs[i], class_name);

			// 	emptyElement(divs[i]);

			// 	divs[i].appendChild(document.createElement("div"))
			// 		.appendChild(document.createElement("br"));
				

			// }

		},
		/*
			Convert a string of text nodes to a string of text nodes and <a> nodes for
			nodes that match the hash tag regular expression.
		*/
		hashTags: function(e){

			try{

				/* normalise the text nodes to a single text node */
				e.target.normalize();

				/* get the text node */
				var node = e.target.firstChild;

				if(node){

					/* loop variables */
					var re=/#\w+/g, tag, a;

					for(var i=0; i<node.data.length; ++i){

						/* if the character is a hash... */
						if( /#/.test(node.data[i]) ){

							/* set the start index of the regex expression re */
							re.lastIndex = i;

							/* if the hash if followed by a string of \w+... */
							if( re.test(node.data) ){

								/* split at the hash tag */
								tag = node.splitText(i);
								/* split at the end of the hash tag */
								node = tag.splitText(re.lastIndex-i);

								/* replace the hash tag string with an a tag containing the string */
								a = document.createElement("a");
								a.appendChild(tag.parentNode.replaceChild(a, tag))

								/* set i back to the start */
								i = -1;

							}

						}

					}

				}

				/* normalise adjacent text nodes to single text nodes */
				e.target.normalize();

			} catch(ex){

				e.target.normalize();

			}

		},
		/*
			Convert a string of text nodes to a string of text nodes and <a> nodes for
			nodes that match the hash tag regular expression as the user types.

			WARNING: NEEDS WORK - DOES NOT PERFORM WELL WITH LOTS OF TAGS.
		*/
		typedHashTags: function(e){

			try{

				/* VERSION TWO - HANDLES MANY HASH TAGS MORE EFFICIENTLY */

				/* get the current selection */
				var selection = window.getSelection();

				/* loop variables */
				var node = selection.anchorNode, re=/#\w+/g, text, tag, a;
				var offset = selection.anchorOffset, range = document.createRange();;

				/* strip the anchor tag of the current node */
				if(node.parentNode.nodeName === "A"){

					node.parentNode.parentNode.replaceChild(node, node.parentNode);

				}

				/* if a change occurred at a boundary merge the neighbouring node */
				if(offset === node.length && node.nextSibling){

					/* get the neighbouring node text */
					if(node.nextSibling.nodeName === "A"){
						text = node.nextSibling.firstChild.nodeValue || "";
					} else{
						text = node.nextSibling.nodeValue || "";
					}

					/* create a new node combining the current and neighbouring node text */
					text = document.createTextNode(node.nodeValue+text);
					
					/* replace the current and neighbouring node with the new node */
					removeElements(node.nextSibling);
					node.parentNode.replaceChild(text, node);

					/* set node to be the new node */
					node = text;

				}

				console.log(node, node.length)


				for(var i=0; i<node.length; ++i){

					/* if the character is a hash... */
					if( /#/.test(node.nodeValue[i]) ){

						/* start the full regex search from the current position */
						re.lastIndex = i;

						/* if the hash if followed by a string of \w+... */
						if( re.test(node.nodeValue) ){

							/* set tag to the start of the hash tag text */
							tag = (i === 0) ? node : node.splitText(i);

							/* remove any trailing text that is not part of the hash tag */
							node = tag.splitText(re.lastIndex-i);

								
							a = document.createElement("a");
							a.appendChild(tag.parentNode.replaceChild(a, tag));

							if(offset <= re.lastIndex){

								range.setStart(tag, offset-i);
								selection.removeAllRanges();
								selection.addRange(range);

								/* set offset to be out of range */
								offset = Number.MAX_VALUE;

							} else{

								offset -= re.lastIndex;

							}
							
							/* set i back to the start */
							i = -1;

						}
					}
				}

				if(offset <= node.length){

					range.setStart(node, offset);
					selection.removeAllRanges();
					selection.addRange(range);

				}

				/* normalise adjacent text nodes to single text nodes */
				e.target.normalize();



				// /* VERSION ONE - PERFORMANCE ISSUES WITH MANY HASH TAGS */
				// /* get the current selection */
				// var selection = window.getSelection();
				// /* create a new range object */
				// var range = document.createRange();

				// /* loop variables */
				// var count = selection.anchorOffset, stopCount = false, isAnchor = false;
				// var node = e.target.childNodes, re=/#\w+/g, tag, a;
				

				// /* 
				// 	normalise the input to a string and count the selection offset for the 
				// 	whole string 
				// */
				// for(var i=0; i<node.length; ++i){

				// 	/* if the node is an a link replace it with its text */
				// 	if(node[i].nodeType === node[i].ELEMENT_NODE){

				// 		/* set isAnchor, checking to see if the text is the anchor */
				// 		isAnchor = selection.anchorNode === node[i].firstChild
				// 		e.target.replaceChild(node[i].firstChild, node[i]);

				// 	}

				// 	/* if this node is the anchor node then stop counting */
				// 	if(stopCount === false){

				// 		if(isAnchor || node[i]===selection.anchorNode){

				// 			stopCount = true;

				// 		} else{

				// 			count += node[i].length;

				// 		}

				// 	}

				// }

				// /* normalize the text nodes to a single text node */
				// e.target.normalize();

				// /* get the text node */
				// node = e.target.firstChild;

				// if(node){

				// 	for(var i=0; i<node.data.length; ++i){

				// 		console.log(node.data);

				// 		/* if the character is a hash... */
				// 		if( /#/.test(node.data[i]) ){

				// 			/* start the regex search from the current position */
				// 			re.lastIndex = i;

				// 			/* if the hash if followed by a string of \w+... */
				// 			if( re.test(node.data) ){

				// 				/* split at the hash tag */
				// 				tag = node.splitText(i);
				// 				/* split at the end of the hash tag */
				// 				node = tag.splitText(re.lastIndex-i);

				// 				/* replace the hash tag string with an a tag containing the string */
				// 				a = document.createElement("a");
				// 				a.appendChild(tag.parentNode.replaceChild(a, tag))

				// 				/* if the selection falls within this tag set the selection */
				// 				if(count >= i && count <= re.lastIndex){
			
				// 					range.setStart(tag, count-i);
				// 					// range.setEnd(tag, count-i);
				// 					selection.removeAllRanges();
				// 					selection.addRange(range);

				// 				/* if the selection falls in the next node set the selection */
				// 				} else if(count > re.lastIndex) {

				// 					range.setStart(node, count-re.lastIndex);
				// 					//range.setEnd(node, count-re.lastIndex);
				// 					selection.removeAllRanges();
				// 					selection.addRange(range);

				// 				}

				// 				/* reduce count by the length of this tag */
				// 				count -= re.lastIndex;

				// 				/* set i back to the start */
				// 				i = -1;

				// 			/* else set selection to the end of the node */
				// 			} else{

				// 				range.setStart(node, count);
				// 				//range.setEnd(node, count);
				// 				selection.removeAllRanges();
				// 				selection.addRange(range);

				// 			}

				// 		}

				// 	}

				// }

				// /* normalise adjacent text nodes to single text nodes */
				// e.target.normalize();

			} catch(ex){

				console.log(ex);
				e.target.normalize();

			}

		},
		/* END HASH TAGS */
		/* ---------------------------------------------------------------------------- */


		/* HIDE/SHOW FIELDS */
		/* ---------------------------------------------------------------------------- */
		/*
			Event handler to toggle the display of the extra voting option.
		*/
		addVoteOne: function(e){

			var self = e.data.self;
			
			/* get the button used to toggle the additional option */
			var button = self.options[1], option = null;
			
			/* 
				if the menu button is being display set the content of the button to
				the first available element.
			*/
			if(toggleClass(this, "open")){

				var li = getElement(button.getAttribute(self.data.toggle)).childNodes;
				var text =  self.getOptionText(self.options[0]);

				for(var i=0; i<li.length; ++i){
					
					if(li[i].nodeType === li[i].ELEMENT_NODE){

						if(text !==  self.getOptionText(li[i]) ){

							option = li[i];
							break;

						}
					}
				}

			}

			/* set the option on the button */
			self.changeContent(button, option);
			toggleClass(getElement("option1_container"), "hide")

		},
		/*
			Event handler to toggle the display of the settings panel.
		*/
		toggleSettings: function(e){

			$(getElement("settings")).slideToggle(200);

		},
		/* END HIDE/SHOW FIELDS */
		/* ---------------------------------------------------------------------------- */


		/* DROP DOWN MENU FIELDS */
		/* ---------------------------------------------------------------------------- */
		/*
			Intercept click event triggered by the space bar and prevent it.
		*/
		interceptSpaceBar: function(e){
			if(e.which === 32){ e.preventDefault(); }
		},
		/*
			Event handler to update the value of input[type="color"].
		*/
		colourPicker: function(e){

			e.data.btn.style.backgroundColor = e.target.value;
			e.data.li.setAttribute(e.data.attr, e.target.value);
		
		},

		setContent: function(button, element){

			if(element){

				appendElements(button.content, element.childNodes, true);
			
			}
			
			/* store reference to the current element on the button */
			button.element = element

		},
		/*
			Update the content (span element) within the button.
			@param button: drop down menu button.
			@param element: element to copy data from.
		*/
		changeContent: function(button, element){
			
			if(button.element && button.element.isText){

				/* turn off events that toggle the icon menu */
				menus(button.content.lastChild);
				/* remove any errors added for the input element */
				this.removeMessage(button.content.firstChild);
				/* turn off events added to the inout element */
				this.unregister(this.evt, button.content.firstChild, this.textValidationHandler);
				this.unregister("keyup", button.content.firstChild, this.interceptSpaceBar);
				this.unregister(this.evt, button.content.firstChild, this.optionValidationHandler);
			
			}
			
			/* update the button to contain the data of the new element */
			emptyElement(button.content)
			this.setContent(button, element);
		},
		/*
			Function called each time the icon menu is toggled. See Menu Class base.js.
			@param close: true if the menu has been closed.
			@param button: the button that toggles the menu.
			@param menu: the menu toggled by the button.
			@param target: event target element.
		*/
		iconCallback: function(close, button, menu, target){

			if(close){

				/* get the ul which is a child of the element */
				var ul = menu.getElementsByTagName("ul")[0];

				/* if the target is a child of the ul... */
				if(menu.active !== target && isDescendant(ul, target, 3)){

					/* if the previously selected item was the colour input... */
					if(button.isColour){

						$(button.isColour).off("change", this.colourPicker);
						button.isColour = null;
					
					}
					
					/* if the selected element is the input[type="color"]... */
					if(target.nodeName === "INPUT"){ 

						var data = {btn: button, li: target.parentNode, attr: this.data.colour}
						/* add a change event handler to update the selected colour */
						this.register("change", target, this.colourPicker, data);
						/* set the input element as a property on the button */
						button.isColour = target;
						/* set the data-colour attribute on the li element */
						target.parentNode.setAttribute(this.data.colour, target.value);
						/* normalise target to be the li element */
						target = target.parentNode;

					}			

					/* remove the active class from the previously selected element */
					removeClass(menu.active, this.active);
					/* add the active class to the selected element */
					addClass(target, this.active);

					/* remove the previously selected elements icon class from the button */
					removeClass(button, menu.active.getAttribute(this.data.icon) || "");
					/* add the selected elements icon class to the button */
					addClass(button, target.getAttribute(this.data.icon) || "");
					
					/* set the background colour of the button */
					button.style.backgroundColor = target.getAttribute(this.data.colour);
					/* set the active element on the menu which is accessed by callback() and submitHandler() */
					button.active = menu.active = target;

				}

				/* draw focus to the input element */
				button.previousSibling.focus();
			}
		},
		/*
			Function called each time a drop down menu is toggled. See Menu Class base.js.
			@param close: true if the menu has been closed.
			@param button: the button that toggles the menu.
			@param menu: the menu toggled by the button.
			@param target: event target element.
		*/
		callback: function(close, button, menu, target){
				
			/* if the target is a child of the menu... */
			if(close && isDescendant(menu, target, 2)){

				/* normalise target to the li element */
				while(target.nodeName !== "LI"){ 

					target = target.parentNode;

				}

				if(this.optionValidation(button, target)){

					/* update the buttons content with the values of the target element */
					this.changeContent(button, target);

					if(target.isText){

						/* force the input field to be invalid as it is empty */
						this.addError(button.content.firstChild, true);

						/* draw focus to the input element */
						button.content.firstChild.focus();
						
						/* register event to perform text validation */
						this.register(this.evt, button.content.firstChild, this.textValidationHandler);
						/* prevent the space bar from opening the drop down menu */
						this.register("keyup", button.content.firstChild, this.interceptSpaceBar);
						/* prevent the space bar from opening the drop down menu */
						this.register(this.evt, button.content.firstChild, this.optionValidationHandler, {button: button});
						
						/* get the newly added icon menu button */
						var icon_button = button.content.lastChild;
						/* get the icon menu - reference to the menu is stored on the target's menu button */
						var icon_menu = target.lastChild.menu;

						/* if the active element is not the default (original) selection... */
						if(icon_menu.original !== icon_menu.active){

							/* remove the active class from the current active element */
							removeClass(icon_menu.active, this.active);
							/* set the active class on the default element */
							addClass(icon_menu.original, this.active);

						}

						/* set the active element as a property on the button - used by optionData() */
						icon_button.active = icon_menu.active = icon_menu.original;

						/* add click event to toggle the text field's icon menu */
						menus(icon_button, "height", false, this.iconCallback, this);

						/* open the icon menu */
						$(icon_button).trigger(Ulo.evts.click);

						/* scroll to the top to place the menu in view on mobiles */
						window.scrollTo(0, 0);

					}

				}

			}
		},
		/* END DROP DOWN MENU FIELDS */
		/* ---------------------------------------------------------------------------- */

		textData: function(data){

			for(var i=0; i<this.fields.length; ++i){

				if(this.fields[i].is_valid){

					data.append(
						this.fields[i].name, 
						fixedEncodeURIComponent(this.fields[i].value) 
					);
				
				} else{

					this.textValidation(this.fields[i]);
					throw new PostFormException();

				}
			
			}

		},

		optionData: function(data){

			var data_regexp = /^data-/, data_length=5;

			if(this.optionValidation(this.options[0]) === false){


				throw new PostFormException("Button name should be different.");

			}

			for(var i=0; i<this.options.length; ++i){

				/* toggle ul menu button */
				var button = this.options[i];
				/* active li element */
				var element = button.element;

				if(element){

					/* get the prefix name set on the ul used to differentiate each option */
					var prefix = element.parentNode.getAttribute(this.data.prefix) || "";

					if(element.isText){

						var input = button.content.firstChild;

						if(!input.is_valid){

							this.textValidation(input);
							throw new PostFormException();

						}

						/* get the active li element of the icon menu */			
						var icon_element = button.content.lastChild.active;

						for(var j=0; j<icon_element.attributes.length; ++j){

							if(data_regexp.test(icon_element.attributes[j].name)){

								data.append(
									prefix+icon_element.attributes[j].name.slice(data_length),
									fixedEncodeURIComponent(icon_element.attributes[j].value) 
								);

							}

						}

						/* store the value of the input element on the options li element */
						element.setAttribute(this.data.text, $.trim(input.value) );

					}

					for(var j=0; j<element.attributes.length; ++j){

						if(data_regexp.test(element.attributes[j].name)){

							data.append(
								prefix+element.attributes[j].name.slice(data_length),
								fixedEncodeURIComponent(element.attributes[j].value) 
							);

						}

					}

				}

			}


			data.set('1-option_type', '');
			data.set('1-text', 'p');

		},

		settingsData: function(data){

			for(var i=0; i<this.settings.length; ++i){

				var button  = this.settings[i];
				var element = button.element;

				if(element){

					data.append(button.name, element.getAttribute(this.data.value));

				}
			}
		},

		submitHandler: function(e){

			var formData = new FormData();

			var self = e.data.self;

			try{

				console.log( $(getElement("post_form")).serialize() )

				var canvas = getElement("preview").firstChild;

				console.log(canvas, canvas.file0);

				if(canvas.file0){

					formData.append("file0", canvas.file0);

				} else{

					messages("File uploading");
				}


				self.textData(formData);

				self.optionData(formData);

				self.settingsData(formData);

				var iter = formData.entries();
				var en;
				while( (en = iter.next().value) ){
					console.log(en);
				}


				//self.enableSubmit();


				if(self.jqxhr === null){

					var request = {
						type: "POST",
						url: getElement("post_form").getAttribute("action")+"image/",
						data: formData,
						cache:false,
						processData: false,
						contentType: false,
						statusCode: requestCodes,
					}

					self.jqxhr = $.ajax(request)
						/* params: server data, status code, xhr */
						.done(function(data){
							
							console.log("SUCCESS: ", data);

						})
						/* params: xhr, status code, error type */
						.fail(function(xhr){

							console.log("FAILED: ", xhr)

							try{
								
								var errors = xhr.responseJSON;

								for(var key in errors){

									//messages(errors[key]);
									console.log(key, errors[key])
								}
				
							} catch(e){
								console.log(e)
								messages("The form contains errors.");
							}
							
						})
						.always(function(){

							self.jqxhr = null;
							//self.enableSubmit();
					
					});
				}

			} catch(e){

				var error;

				console.log(e instanceof PostFormException);

				if(e instanceof PostFormException){
					error = e.msg;
				} else{
					error = "The form contains errors.";
				}

				messages(error);

			}

		},

	}







/* ------------------------------------------------------------------------------------ */
	
	function submit(e){
		e.preventDefault()
		var request = {
			type: "POST",
			url: $("#post_form").attr("action"),//+"video/",
			data: new FormData($("#post_form")[0]),
			processData: false, // Don't process the files
			contentType: false
		}

		console.log(request.url)

		/* WHEN THE FILE IS LARGE YOU WILL GET A NETWORK ERROR SO DISPLAY ERROR MESSAGE FOR IT */
		/* SEE SERVER SIDE FOR WHAT CAUSES IT. USUALLY FILE TOO BIG OR RESOLUTION TOO BIG OR TYPE 
			NOT SUPPORTED */

		$.ajax(request)
			.done(function(url, sc, xhr){
					console.log('success: ', url)
				})
				/* params: xhr, status code, error type */
				.fail(function(xhr){
					/* display error message for all fields */
					var json = $.parseJSON(xhr.responseText);
					console.log(json)					
				});
	}

	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */

	$(function(){

		$("#id_title").on("keydown", function(e){
			//console.log(this.value)
		})

		/* If user hits the enter key to submit the form */
		$("#post_form").on("submit", submit)
		/* If user submits the form by pressing the button */
		$("#id_submit").on("click", submit)


	});

/* ------------------------------------------------------------------------------------ */
}());