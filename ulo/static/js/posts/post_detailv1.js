/* Post Detail Page Javascript File */
/* Dependencies: JQuery, Base.js, Users.js */


/* ------------------------------------------------------------------------------------ */

function toggleDescription(){

	menus(getElement("toggle_description"), "height", 100);

}

/* ------------------------------------------------------------------------------------ */

(function () {
"use strict";

	/* -------------------------------------------------------------------------------- */

	var Actions = {

		PK: "000",

		url_attr: "data-url",

		item_attr: "data-item-id",

		getID: function(elem){

			return elem ? elem.getAttribute(this.item_attr) || this.PK : this.PK;

		},

		getURL: function(elem){

			return elem ? elem.getAttribute(this.url_attr) || null : null;

		},

		setURL: function(elem, val){

			var url = this.getURL(elem);
			url!==null && elem.setAttribute(this.url_attr, url.replace(/\d+/, val))

		}

	}

	/* -------------------------------------------------------------------------------- */
	/*
		Manage PIP requests for report forms. 
	*/
	function BaseReportForm(btn_id, page_id, submit_id){

		this.btn_id = btn_id;
		this.page_id = page_id;
		this.submit_id = submit_id || page_id+"_submit";

		this.register();

	}
	BaseReportForm.prototype = {

		constructor: BaseReportForm,

		/* ---------------------------------------------------------------------------- */

		done: undefined,

		/* ---------------------------------------------------------------------------- */

		fail: undefined,

		/* ---------------------------------------------------------------------------- */

		always: undefined,

		/* ---------------------------------------------------------------------------- */

		register: function(context){
			$(getElement(this.btn_id), context).on(Ulo.evts.click, {self:this}, this.getForm);
		},
		
		/* ---------------------------------------------------------------------------- */

		getPage: function(){
			return getElement(this.page_id);
		},

		/* ---------------------------------------------------------------------------- */

		getSubmit: function(){
			return getElement(this.submit_id);
		},

		/* ---------------------------------------------------------------------------- */
		
		values: function(url){

			return {
		
				"form": { attr:"action", value:url },

				"input": { attr:"checked", value:false },
		
				"textarea": { attr:"value", value:"" }
		
			}
		
		},
		
		/* ---------------------------------------------------------------------------- */
		/*

		*/
		resetForm: function(page, url){

			var v = this.values(url);

			for(var tag in v){

				var elems = page.getElementsByTagName(tag);

				for(var i=0; i<elems.length; ++i){
					elems[i][v[tag].attr]=v[tag].value;
				}

			}

			var submit = this.getSubmit();
			
			if(submit!==null){

				submit.disabled=true;

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Display the form to the user.
		*/
		getForm: function(e){

			e.preventDefault();
			var self = e.data.self;

			var url = Actions.getURL(e.currentTarget);

			console.log(url);
		
			if(url!==null){

				var page = self.getPage();

				if(page===null){

					Ulo.PIP.getPage(url, self.done, self.fail, self.always);

				} else{

					if(Ulo.PIP.loginRequired()===false){

						removeClass(page, Ulo.cls.hide);
						self.resetForm(page, url);
						Ulo.PIP.modal.open(e);						
					
					}
					
					if(self.done!==undefined){

						self.done();

					}

					if(self.always!==undefined){

						self.always();

					}
				
				}			

			}

		}

	}

	/* -------------------------------------------------------------------------------- */




	/* -------------------------------------------------------------------------------- */
	/*
		Manage PIP requests for the post report form. 
	*/
	function PostReport(){
	
		BaseReportForm.call(this, "report_post", "post_report");
	
	}
	
	PostReport.prototype = inherit(BaseReportForm.prototype);

	PostReport.prototype.constructor = PostReport;

	/* -------------------------------------------------------------------------------- */
	/*
		Manage PIP requests for the comment report form. 
	*/
	function CommentReport(){
	
		BaseReportForm.call(this, "report_comment", "comment_report");
	
	}
	
	CommentReport.prototype = inherit(BaseReportForm.prototype);

	CommentReport.prototype.constructor = CommentReport;

	/* -------------------------------------------------------------------------------- */




	/* -------------------------------------------------------------------------------- */
	/*
	
	*/
	function PostMenu(selector, evt, id){

		this.selector = selector;
		this.evt = evt;
		this.id = id;

		this.register();
	
	}

	PostMenu.prototype = {

		constructor: PostMenu,

		/* ---------------------------------------------------------------------------- */
		/*
		*/
		register: function(context){

			$(this.selector, context).on(this.evt, {self:this}, this.showMenu);

		},

		/* ---------------------------------------------------------------------------- */
		/*
		*/
		showMenu: function(e){

			e.preventDefault();

			var menu = getElement(e.data.self.id);

			if(menu!==null && hasClass(menu, Ulo.cls.hide)){

				if(e.data.self.beforeShow(e, menu)!==false){

					Ulo.Menu.queueData({
					
						callback: e.data.self.beforeClose,
						menu_class: Ulo.cls.hide, 
						self: Ulo.Menu,
						menu: menu,

					}, true);

				}
				
				removeClass(menu, Ulo.cls.hide);

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
		*/
		beforeShow: function(e, menu){

			var btns = menu.getElementsByTagName("button"),
			id = Actions.getID(e.currentTarget);

			console.log("BUTTON: ", e.currentTarget);

			for(var i=0, url; i<btns.length; ++i){

				
			
				Actions.setURL(btns[i], id);
			
			}

			e.currentTarget.parentNode.insertBefore(menu, e.currentTarget);

		},

		/* ---------------------------------------------------------------------------- */
		/*
		*/
		beforeClose: function(isClosed, button, menu, target){
			
			if(isClosed){

				document.body.appendChild(menu);

			}

		}

	}
	/* -------------------------------------------------------------------------------- */

	function PostAction(){

		PostMenu.call(this, "a.post_menu_btn", Ulo.evts.click, "post_menu");

	}

	PostAction.prototype = inherit(PostMenu.prototype);
	
	PostAction.prototype.constructor = PostAction;

	/************************************************************************************/

	Ulo.newClass(PostAction, "PostAction", true);

	/************************************************************************************/

	/* -------------------------------------------------------------------------------- */

	function CommentAction(){

		PostMenu.call(this, "a.comment_menu_btn", Ulo.evts.click, "comment_menu");
		
		this.modal = new Ulo.Modal("comment_modal");

	}

	CommentAction.prototype = inherit(PostMenu.prototype);
	
	CommentAction.prototype.constructor = CommentAction;

	CommentAction.prototype.getID = function(id){
		return "comment_"+id;
	}

	CommentAction.prototype.getComment = function(id){
		return getElement(this.getID(id));
	}

	CommentAction.prototype.beforeShow = function(e, menu){

		if(this.modal.isClosed()){

			/* Attributes set by PostComment.addComments */
			var comment = this.getComment(Actions.getID(e.currentTarget));
			var uid = e.currentTarget.getAttribute("data-user-id") || null;

			if(comment!==null && uid!==null){

				PostMenu.prototype.beforeShow.call(this, e, menu);

				/* Display the menu with the relevant options */
				var cls = Ulo.Session.isOwner(uid) ? "owner" : "user";
				addClass(menu, cls);

				addClass(comment, "protrude");
				
				this.modal.beforeClose = this.beforeClose.bind(null, comment, menu, cls);
				this.modal.open(e);

			}

		}

		return false;

	}

	CommentAction.prototype.beforeClose = function(comment, menu, cls){

		removeClass(menu, cls);
		addClass(menu, Ulo.cls.hide);
		document.body.appendChild(menu);
		
		removeClass(comment, "protrude");		

	}

	/************************************************************************************/

	Ulo.newClass(CommentAction, "CommentAction", true);

	/************************************************************************************/

	/* -------------------------------------------------------------------------------- */


	function PostMobile(){

		PostMenu.call(this, "div.vote_data", "touchend", "mobile_data");

	}

	PostMobile.prototype = inherit(PostMenu.prototype);

	PostMobile.prototype.constructor = PostMobile;

	PostMobile.prototype.beforeShow = function(e, menu){
		
		var data=menu.getElementsByTagName("div")[0], 
		nodes=e.currentTarget.childNodes;
		
		emptyElement(data);

		for(var i=0; i<nodes.length; ++i){

			if(hasClass(nodes[i], "abbr_count")===false){

				data.appendChild(nodes[i].cloneNode(true));

			}

		}

		e.currentTarget.parentNode.parentNode.parentNode.appendChild(menu);

	};

	/************************************************************************************/

	Ulo.newClass(PostMobile, "PostMobile", true);

	/************************************************************************************/

	/* -------------------------------------------------------------------------------- */




	/* -------------------------------------------------------------------------------- */
	/*
		Manage the retrieval and submission of post comments.
	*/
	function PostComment(Action){

		Ulo.checkTempDependencies("CommentAction");
		this.Action = Ulo.TEMP.CommentAction;
		
		this.jqxhr = null;
		
		this.setup();

 		$(getElement("delete_comment_form")).on("submit", {self:this}, this.deleteComment);

	}

	PostComment.prototype = {

		constructor: PostComment,

		/* ---------------------------------------------------------------------------- */
		/*
		*/
		setup: function(){

			$(getElement("comment_form")).on("submit", {self:this}, this.createComment);
			
			/* Handle to the comments container */
			this.container = getElement("comments_container");

			/* Profile url with a placeholder value of "000" */
			this.profile_url = (this.container===null) ?
				"" : this.container.getAttribute("data-profile-url");
			
			/* Handle to the button that triggers a get request for more comments */
			this.load_comments = getElement("load_comments");

			if(this.load_comments!==null){
				
				this.register();
			
			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the comments input=text field.
		*/
		getInput: function(){
			return getElement("comment");
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Display the "show more" comments button and register a click event on the
			button to get comments from the server.
		*/
		register: function(){
			/* The class makes a distinction between "undefined" and "null" for max_id */
			this.max_id = undefined;
			removeClass(this.load_comments, Ulo.cls.hide).disabled=false;
			$(this.load_comments).on(Ulo.evts.click, {self:this}, this.getComments);
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Hide the "show more" comments button and unregister the click event on the
			button that fetches comments from the server.
		*/
		unregister: function(){
			/* The class makes a distinction between "undefined" and "null" for max_id */
			this.max_id = null;
			addClass(this.load_comments, Ulo.cls.hide).disabled=true;
			$(this.load_comments).off(Ulo.evts.click, this.getComments);
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Remove all errors relating to the comments input field and return the errors
			container or null if there is no container.
		*/
		removeErrors: function(){
			var container = getElement("comment_errors");
			if(container !== null){ emptyElement(container); }
			return container;
		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Create the html for a comment and append or prepend the new comment(s) to the
			comments container.

			@param comments: array of comments returned by the request.
			@param get_request: boolean - true if the addComments function is being 
				called following a GET request to load more comments and false if the
				function is being called after the auth user has posted a comment.
		*/
		addComments: function(comments, get_request){

			if(comments!==undefined && comments.length!==0){

				var comment, elem;

				if(get_request || this.max_id===undefined){
				
					this.max_id=comments[get_request ? comments.length-1 : 0].id;
					
				}
			
				for(var i in comments){


					/* Comment container */
					comment = makeElement("div", {
						
						"id": this.Action.getID(comments[i].id),
						"class": "comment_panel"
					
					});

					/* Comment actions */
					elem = comment.appendChild(makeElement("div", {"class": "user_actions"}));
					elem = elem.appendChild(makeElement("a", {
						"class":"icon comment_menu_btn",
						"href": "/comment/"+comments[i].id+"/actions/",
						"data-close-modal": "true",
						"data-item-id": comments[i].id,
						"data-user-id": comments[i].user_id,
						"title": "Comment Actions"
					}));
					this.Action.register(comment);
					elem.appendChild(makeElement("span", {"class": Ulo.cls.hide}, "Comment Actions"));
					
					/* User profile */
					elem = comment.appendChild(makeElement("div", {"class": "user_profile"}));
					elem = elem.appendChild(makeElement("a", {
						"data-apl":"true",
						"href":this.profile_url.replace(Actions.PK, comments[i].username),
						"title": "Profile"
					}));
					registerPageLink(elem);
					elem.appendChild(makeElement("img", {
						"class": "user_thumbnail",
						"src": Ulo.MEDIA_URL+comments[i].thumbnail
					}));
					elem = elem.appendChild(makeElement("div", {"class": "user_names"}));
					elem.appendChild(makeElement("h2", {"class":"row name ellipsis"}, comments[i].name));
					elem.appendChild(makeElement("span", {"class":"row username ellipsis"}, "@"+comments[i].username))

					/* Comment text */
					comment.appendChild(makeElement("div", {"class": "comment_text"}, comments[i].comment));

					if(get_request){

						this.container.appendChild(comment);

					} else{

						this.container.insertBefore(comment, this.container.firstChild);

					}

				}

			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Make a request to get the next set of comments.
		*/
		getComments: function(e){

			e.preventDefault();
			
			var self=e.data.self;

			if(self.jqxhr===null && self.max_id!==null){

				self.jqxhr = $.ajax({

						type:"GET",
						data:{"max_id": self.max_id},
						url:this.getAttribute("data-action"),
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data){

						/* Append all comments to the comments container */
						self.addComments(data.comments, true);
						/* Unregister this event if there a no more comments */
						data.has_next || self.unregister();

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){

						requestMessages(xhr);
					
					})

					.always(function(){

						self.jqxhr = null;
				
				});

			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Create a comment for this post and prepend it to the comments container.
		*/
		createComment: function(e){
			
			e.preventDefault();
			var self=e.data.self, form=this;

			if(Ulo.PIP.loginRequired()===false && self.jqxhr===null && 
				$.trim(self.getInput().value)!==""){

				self.jqxhr = $.ajax({

						type:"POST",
						data: $(this).serialize(),
						url:this.getAttribute("action"),
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						if(data.html!==undefined){

							Ulo.PIP.displayPage(xhr, data);

						}

						else{

							self.removeErrors();
							self.getInput().value="";
							self.addComments(data.comments, false);

						}

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){

						if(Ulo.Session.hasErrors(xhr)){

							/* Get the errors returned by the server */
							var error, errors = xhr.responseJSON.errors;
							var container = self.removeErrors();

							/* Create the container if it does not already exist */
							if(container===null){
								
								container=makeElement("div", {"id": "comment_errors"});
								var comment = self.getInput()
								comment.parentNode.insertBefore(container, comment);

							}

							for(var id in errors){

								/* Append each error to its field container */
								for(var i in errors[id]){

									container.appendChild( 
										makeElement("p", {"class": "error"}, errors[id][i])
									);
								
								}

							}

						}

						else{

							requestMessages(xhr);

						}

					
					})

					.always(function(){

						self.jqxhr = null;
				
				});
			}
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Delete the comment.
		*/
		deleteComment: function(e){

			e.preventDefault();

			var self = e.data.self;

			/*
				If a login form has been rendered then force the user to complete the 
				form.
			*/
			if(Ulo.PIP.loginRequired()){
				
				self.Action.modal.close();

			}

			/*
				Else perform the request to delete the comment.
			*/
			else if(self.jqxhr===null){

				var form=this,
				url = Actions.getURL(this.getElementsByTagName("button")[0]);

				if(url!==null){

					self.jqxhr = $.ajax({

							type:"POST", 
							url: url,
							data: $(this).serialize(),
							statusCode:requestCodes
						
						})

						/* Params: server data, status code, xhr */
						.done(function(data, sc, xhr){
					
							/*
								If the user is not logged in the request will return the
								html for the login page.
							*/
							if(data.html!==undefined){

								Ulo.PIP.displayPage(xhr, data);

							}

							/*
								Else remove the comment from the DOM and inform the user
								that the comment has been removed.
							*/
							else{

								removeElements(self.Action.getComment(data.cid));
								messages("Comment deleted.");

							}

						})

						/* Params: xhr, status code, error type */
						.fail(function(xhr){

							if(Ulo.Session.csrfExpired(xhr)){

								var submit = getElement("delete_comment_submit");

								if(submit!==null){
								
									submit.disabled=true;
								
								}

							}

							requestMessages(xhr);
						
						})

						.always(function(){
						
							self.jqxhr = null;
					
					});

				}

			}

		}
	}

	/************************************************************************************/

	Ulo.newClass(PostComment, "PostComment", true);

	/************************************************************************************/

	/* -------------------------------------------------------------------------------- */




	/* -------------------------------------------------------------------------------- */
	/*
		Manage the submission of a post vote, updating the styling and counter values
		for each successful request.
	*/
	function PostVote(){

		this.jqxhr = null;
		/* Class set on the button to show that it has been selected */
		this.select_class = "selected";
		this.register();

	}
	
	PostVote.prototype = {

		constructor: PostVote,

		/* ---------------------------------------------------------------------------- */
		/*
			Register click events on the option form's buttons.
			@param form: voting options form.
		*/
		register: function(context){

			var forms = $("form.post_votes_form", context);

			for(var i=0; i<forms.length; ++i){
			
				$(forms[i].getElementsByTagName("button"))
					.on(Ulo.evts.click, {self:this, form:forms[i]}, this.submitHandler);
			
			}
			
		},
		
		/* ---------------------------------------------------------------------------- */
		/*
			Update all counter values for the vote button.
			@param button: voting button.
			@param inc: increment value to apply to the current counter value. 
		*/
		setCounters: function(button, inc){

			/* Returns the new counter value or NaN */
			var val = updateCounters(button.parentNode, inc);

			if(val===val){

				/* Update the counter value set on the button */
				button.setAttribute("data-count", val);

			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the selected button or null
			@param form: voting options form.
		*/
		getSelected: function(form){

			var buttons = form.getElementsByTagName("button");

			for(var i=0; i<buttons.length; ++i){

				if(hasClass(buttons[i], this.select_class)){

					return buttons[i];
					
				}

			}

			return null
			
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the hidden form element input[name=voted]
			@param form: voting options form. 
		*/
		getVoted: function(form){

			var inputs = form.getElementsByTagName("input");

			for(var i=0; i<inputs.length; ++i){

				if(inputs[i].name==="voted"){

					return inputs[i];
					
				}

			}

			return null
			
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Set the value of the form's input[name=voted] element after a successful
			request.
			@param form: voting options form.
			@param value: value of the current vote.
		*/
		setVoted: function(form, value){

			/* Get the input[name=voted] element */
			var voted = this.getVoted(form);

			/* If the for does not have an input[name=voted] element then create one */
			if(voted===null){

				voted = form.appendChild(

					makeElement("input", {

						"id":"voted",
						"name":"voted", 
						"type":"hidden"
					
					})

				);

			}

			/* If the user has toggled their existing vote set value=null */
			voted.value = voted.value===value ? null : value
			
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the the form data. Add the value of the voted field only if it has
			a valid value. The server expects no value if the user has not voted.

			@param form: voting options form.
			@param button: voting button.
		*/
		serialise: function(form, button){

			var data = button.name+"="+button.value;
			var voted = form.getElementsByTagName("input");

			for(var i=0; i<voted.length; ++i){

				if(voted[i].value){

					data += "&"+voted[i].name+"="+voted[i].value;
				
				}

			}

			return data;
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Set the value of the "voted" field.
		*/
		submitHandler: function(e){

			e.preventDefault();

			var self=e.data.self, form=e.data.form;

			if(Ulo.PIP.loginRequired()===false && self.jqxhr===null){

				console.log(self.serialise(form, e.currentTarget))

				self.jqxhr = $.ajax({

						type:"GET",
						data: self.serialise(form, e.currentTarget),
						url: form.getAttribute("action"),
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/* 
							If the session changed then refresh the current page.
						*/
						if(Ulo.Session.hasChanged(xhr)){

							Ulo.PIP.updatePage(xhr, data);
						
						/*
							Else if the user is not logged in display the login form.
						*/
						} else if(data.html!==undefined){

							Ulo.PIP.displayPage(xhr, data);

						/*
							Else update the vote.
						*/
						} else{

							/* Update the form's input[name=voted] element */
							self.setVoted(form, e.currentTarget.value);

							/* Get the current vote button */
							var selected = self.getSelected(form),
							
							/* Get the container of the decorative votes bar */
							sparkbar = getElement("sparkbar"),
							
							/* Get the total votes counter */
							total = sparkbar===null ? 
								NaN : parseInt(sparkbar.getAttribute("data-count-total"));


							/*
								Update the styling and counter values to reflect the new 
								vote.
							*/
							if(selected!==e.currentTarget){
							
								addClass(e.currentTarget, self.select_class);
								self.setCounters(e.currentTarget, 1);

								if(selected!==null){
									
									removeClass(selected, self.select_class);
									self.setCounters(selected, -1);

								} else{

									total += 1;
								
								}
							
							} else{

								var inc = toggleClass(e.currentTarget, self.select_class) ? 1 : -1;
								self.setCounters(e.currentTarget, inc);
								total += inc;

							}
							
							/*
								Update the styling of the decorative counters bar.
							*/
							if(total===total){

								/* Store the new total on the container */
								sparkbar.setAttribute("data-count-total", total);
								
								/* Get the span elements used to create the lines */
								var lines = sparkbar.getElementsByTagName("span");

								/* Get the buttons to read their count values */
								var buttons = form.getElementsByTagName("button");


								console.log(lines, lines.length, buttons.length)

								if(lines.length===buttons.length){

									/* 
										Update the width of each line according to its 
										count value. 
									*/
									for(var i=0, v=NaN; i<lines.length; ++i){
										
										v = parseInt(buttons[i].getAttribute("data-count"));

										console.log("NO. :", v);
										
										if(v===v){

											lines[i].style.width = (total===0 ? 0 : 100*v/total)+"%";

										}

									}

								}

								

							}

						}

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){

						requestMessages(xhr);
						
					})

					.always(function(){
					
						self.jqxhr = null;
				
				});

			}

		}

	}

	/************************************************************************************/

	Ulo.newClass(PostVote, "PostVote", true);

	/************************************************************************************/




	/* -------------------------------------------------------------------------------- */
	/*
		Connect class callback function to update the follower/following counter value
		for each successful request.

		@param type: string "follow" or "unfollow" to denote the available action.
	*/
	function ConnectCallback(anchor, type){

		var counter = anchor.previousSibling;

		while(counter!==null && counter.nodeType!==counter.ELEMENT_NODE){
			
			counter = counter.previousSibling;
		
		}

		updateCounters(counter, type==="follow" ? -1 : 1);
	
	}

	/* -------------------------------------------------------------------------------- */




	/* -------------------------------------------------------------------------------- */
	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */

	$(function(){

		try{

			Ulo.checkDependencies("PIP", "Session");

			new PostReport();
			new CommentReport();

			toggleDescription();
			Ulo.newClass(Connect, "Connect", true, ConnectCallback);
		
		} catch(e){

			debug(e);

		}

	});

	/* -------------------------------------------------------------------------------- */

}());



