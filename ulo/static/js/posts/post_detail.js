/* Post Detail Page Javascript File */
/* Dependencies: JQuery, Base.js, Users.js */


/* ------------------------------------------------------------------------------------ */

(function () {

"use strict";

/* ------------------------------------------------------------------------------------ */
	

function Draggable(id, settings){
	
	try{
		
		/* Events */
		this.start_events = "mousedown touchstart pointerdown";
		this.move_events = "mousemove touchmove pointermove";
		this.end_events = "mouseup touchend pointerup";

		/* Client X and Y positions set when a start event is triggered. */
		this.start = { X:0, Y:0 };

		/*
			Optional target element - If set to null the class will not trigger the start
			events.
		*/
		this.target = true;

		/* Relative or absolute positioned html element */
		this.element = document.getElementById(id);

		/* Fefault settings */
		this.settings = {
			
			/* Css box boundaries */
			top: 0, right: 0, bottom: 0, left: 0,
			
			/* Directions of movement */
			horizontal: true, vertical: true
		
		}
		
		for(var key in settings){
		
			/* Override or add variables to the settings object */
			this.settings[key] = settings[key];
		
		}

	} catch(e){

		debug(e);
	
	}

}

Draggable.prototype = {
	
	constructor: Draggable,

	/* -------------------------------------------------------------------------------- */
	/*
		Register start events on the element.
	*/
	register: function(element){
	
		$(element || this.element).off(this.start_events, this.startHandler)
			.on(this.start_events, {self: this}, this.startHandler);

	},

	/* -------------------------------------------------------------------------------- */
	/*
		Unregister start events on the element.
	*/
	unregister: function(element){
		
		$(element || this.element).off(this.start_events, this.startHandler);

	},

	/* -------------------------------------------------------------------------------- */


	/* EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Set the starting postions (X and Y) and register the move and end events.
	*/
	startHandler: function(e){
		
		try{

			var self = e.data.self;
			
			if(self.element !== null && self.target !== null){

				self.start = self.getPosition(e);
				self.start.X -= e.target.offsetLeft;
				self.start.Y -= e.target.offsetTop;

				/* Add move and end events to the document */
				$(document).on(self.move_events, {self: self}, self.moveHandler);
				$(document).on(self.end_events, {self: self}, self.endHandler);

				/* Event handled */
				e.stopPropagation();
				e.preventDefault();

			}

		}catch(e){
	
			debug(e);
	
		}
	
	},

	/* -------------------------------------------------------------------------------- */
	/*
		Set the new left and top position of the element. See calcPosition.
	*/
	moveHandler: function(e){
		
		try{
		
			var self = e.data.self,
			pos = self.getPosition(e);

			/* If horizontal movements are enabled set the left postition */
			if(self.settings.horizontal){
			
				self.calcPosition(pos.X-self.start.X, "left", "right");
			
			}

			/* If vertical movements are enabled set the top postition */
			if(self.settings.vertical){
				
				self.calcPosition(pos.Y-self.start.Y, "top", "bottom");
			
			}

			/* Event handled */
			e.stopPropagation();
			e.preventDefault();

		}catch(e){
		
			debug(e);
		
		}
	
	},

	/* -------------------------------------------------------------------------------- */
	/*
		Remove the move and end events from the document.
	*/
	endHandler: function(e){
	
		try{
			
			var self = e.data.self;
			
			/* Remove move and end events */
			$(document).off(self.move_events, self.moveHandler);
			$(document).off(self.end_events, self.endHandler);
	
			/* Event handled */
			e.stopPropagation();
			e.preventDefault();	
			
		}catch(e){
	
			debug(e);
	
		}
	
	},

	/* END EVENT HANDLERS */
	/* -------------------------------------------------------------------------------- */


	/* ELEMENT POSITIONING */
	/* -------------------------------------------------------------------------------- */
	/*
		Set the new position of the element constraining it to its bounding box positions 
		defined by the settings object.
	
		@param e: Event object.
		@param pos: X or Y position of the event. 
		@param p1: First bounding position ("left" or "top").
		@param p2: Second bounding position ("right" or "bottom").
	*/
	calcPosition: function(pos, p1, p2){

		if(pos < this.settings[p1]){ 
		
			pos = this.settings[p1]; 
		
		} else if(pos > this.settings[p2]){ 
		
			pos = this.settings[p2]; 
		
		}

		this.element.style[p1] = pos + "px";

	},

	/* END ELEMENT POSITIONING */
	/* -------------------------------------------------------------------------------- */


	/* HELPER FUNCTIONS */
	/* -------------------------------------------------------------------------------- */
	/*
		Return the pixel value (pix) as a percentage.
		
		@param pixels: Pixel length.
		@param length: Full length of the element to which pix is a section of.
	*/
	pixelToPercent: function(pixels, length){
	
		return 100*pixels / length;
	
	},

	/* -------------------------------------------------------------------------------- */
	/*
		Return the on screen position of the current event or throw an exception.
		
		@params: e: the event object. 
		@param coord: the client coordinate (must be a capitalised string "X" or "Y") 
	*/
	getPosition: function(e){
		
		var pos;
		
		/* JQuery's normalised pageX/Y */
		if(e.pageX !== undefined){
		
			return { X: e.pageX, Y: e.pageY };
	
		} 

		/* Raw event data with scroll offsets applied */
		else if(e.originalEvent.clientX !== undefined){
		
			var scroll = getScrollOffsets();

			return { 

				X: e.originalEvent.clientX + scroll, 
				Y: e.originalEvent.clientY + scroll

			}
		
		}

		/* Touch events */
		else if(e.originalEvent.touches !== undefined){
		
			return { 

				X: e.originalEvent.touches[0].pageX, 
				Y: e.originalEvent.touches[0].pageY

			} 
		
		}

		// /* Pointer events: UNTESTED!!! */
		// else if(e.originalEvent.currentPoint){
			
		// 	return e.originalEvent.currentPoint[coord.toLowerCase()];
		
		// }
		
		throw new Error("Start position not found.");
	
	},

	/* -------------------------------------------------------------------------------- */
	/* 
		Return the scroll X and Y offset positions.
		Javascript The Definitive Guide (6th Edition) pg: 391
	*/
	getScrollOffsets: function(){
	
		/* All browsers except IE8 and before */
		if(window.pageXOffset !== null){
	
			return {X: window.pageXOffset, Y: window.pageYOffset};
	
		}
	
		/* For IE or any browser in standard mode */
		var d = window.document;
	
		if(d.compatMode === "CSS1Compat"){
	
			return { X: d.documentElement.scrollLeft, Y: d.documentElement.scrollTop };
	
		}
	
		/* For browsers in Quirks mode */
		return { X: d.body.scrollLeft, Y: d.body.scrollTop };
	
	}

	/* END HELPER FUNCTIONS */
	/* -------------------------------------------------------------------------------- */

}

/* ------------------------------------------------------------------------------------ */




/* ------------------------------------------------------------------------------------ */

function VideoPlayer(settings, selector){

	/* Slider button selector */

	this.selector = selector;

	/* Run base class constructor */

	Draggable.call(this, null, settings);

	/* Css positions */

	this.css = this.settings.vertical ? ["top", "bottom"] : ["left", "right"];

}

/* ------------------------------------------------------------------------------------ */

VideoPlayer.prototype = inherit(Draggable.prototype);

/* ------------------------------------------------------------------------------------ */

VideoPlayer.prototype.constructor = VideoPlayer;

/* ------------------------------------------------------------------------------------ */

VideoPlayer.prototype.register = function(video, container, container_selector){

	/* Slider container */

	container = container.querySelector(container_selector);

	$(container).on(this.start_events, {self: this}, this.positionSlider);


	/* Slider button */

	container = container.querySelector(this.selector);

	Draggable.prototype.register.call(this, container);


	/* Store reference to the video on the slider button */

	container.video = video;

};

/* ------------------------------------------------------------------------------------ */

VideoPlayer.prototype.startHandler = function(e){

	e.data.self.element = e.target;

	Draggable.prototype.startHandler.call(this, e);

};

/* ------------------------------------------------------------------------------------ */

VideoPlayer.prototype.getOffset = function(e){

	throw Error("Subclasses of VideoPlayer must define getOffset().");

};

/* ------------------------------------------------------------------------------------ */

VideoPlayer.prototype.positionSlider = function(e){

	var self = e.data.self;

	self.element = e.target = e.currentTarget.querySelector( self.selector );

	self.calcPosition( self.getOffset(e), self.css[0], self.css[1]  );

	self.startHandler.call(this, e);

};

/* ------------------------------------------------------------------------------------ */

VideoPlayer.prototype.calcPosition = function(pos, p1, p2){

	if(pos < this.settings[p1]){ 
	
		pos = this.settings[p1]; 
	
	} else if(pos > this.settings[p2]){ 
	
		pos = this.settings[p2]; 
	
	}

	return this.pixelToPercent(pos, this.settings[p2]);

};

/* ------------------------------------------------------------------------------------ */



/* ------------------------------------------------------------------------------------ */

function TrackControl(){

	VideoPlayer.call(this, { vertical: false }, "button.track_slider");

}

/* ------------------------------------------------------------------------------------ */

TrackControl.prototype = inherit(VideoPlayer.prototype);

/* ------------------------------------------------------------------------------------ */

TrackControl.prototype.constructor = TrackControl;

/* ------------------------------------------------------------------------------------ */

TrackControl.prototype.getOffset = function(e){

	return (

		this.getPosition(e).X - 
		e.currentTarget.offsetLeft -
		e.currentTarget.getBoundingClientRect().left

	);

};

/* ------------------------------------------------------------------------------------ */

TrackControl.prototype.calcPosition = function(pos, p1, p2){

	this.settings[p2] = this.element.parentNode.clientWidth;

	pos = VideoPlayer.prototype.calcPosition.call(this, pos, p1, p2);

	this.element.style[p1] = pos + "%";

	this.element.video.currentTime = pos * 0.01 * this.element.video.duration;

};

/* ------------------------------------------------------------------------------------ */




/* ------------------------------------------------------------------------------------ */

function VolumeControl(){

	VideoPlayer.call(this, { horizontal: false, bottom: 100 }, "button.volume_slider");

}

/* ------------------------------------------------------------------------------------ */

VolumeControl.prototype = inherit(VideoPlayer.prototype);

/* ------------------------------------------------------------------------------------ */

VolumeControl.prototype.constructor = VolumeControl;

/* ------------------------------------------------------------------------------------ */

VolumeControl.prototype.getOffset = function(e){

	return (

		this.getPosition(e).Y -
		e.currentTarget.offsetTop - 32 -
		e.currentTarget.getBoundingClientRect().top

	);

};

/* ------------------------------------------------------------------------------------ */

VolumeControl.prototype.calcPosition = function(pos, p1, p2){

	pos += this.element.clientHeight;

	pos = 100 - VideoPlayer.prototype.calcPosition.call(this, pos, p1, p2);

	// this.element.style[p2] = pos + "%";

	this.element.video.volume = pos * 0.01;

	this.element.video.muted = this.element.video.volume === 0;

};

/* ------------------------------------------------------------------------------------ */




/* ------------------------------------------------------------------------------------ */


	

	/* -------------------------------------------------------------------------------- */

	function PostVideo(){

		var video = document.createElement("video");

		var videoSupported = !!video.canPlayType;


		if (videoSupported){

			var fullScreenSupported = !!(

				document.fullscreenEnabled || 
				document.mozFullScreenEnabled || 
				document.msFullscreenEnabled || 
				document.webkitSupportsFullscreen || 
				document.webkitFullscreenEnabled || 
				video.webkitRequestFullScreen

			);


			if(fullScreenSupported){

				Element.prototype.requestFullscreen =

					Element.prototype.requestFullscreen || 
					Element.prototype.webkitRequestFullscreen ||
					Element.prototype.mozRequestFullScreen ||
					Element.prototype.msRequestFullscreen;


				document.onfullscreenchange =
				document.onwebkitfullscreenchange =
				document.onmozfullscreenchange =
				document.onmsfullscreenchange =
					
					this.fullscreenChange.bind(this);


				document.exitFullscreen =

					document.exitFullscreen || 
					document.webkitExitFullscreen || 
					document.mozCancelFullScreen || 
					document.msExitFullscreen;

			}


			this.timeout_id = this.stopPropagation = null;

			this.TrackControl = new TrackControl();

			this.VolumeControl = new VolumeControl();

			this.register();


		} else{

			this.register = function(){}

		}

	}

	PostVideo.prototype = {

		constructor: PostVideo,

		/* ---------------------------------------------------------------------------- */

		register: function(context){

			var containers = $("div.video", context);

			for(var i=0, video; i<containers.length; ++i){


				video = containers[i].querySelector("video");
				
				var data = { container: containers[i], video: video, self: this };

				var preload = video.getAttribute("preload");


				$(video).on("error", data, this.error)


				if(preload === "" || preload === null || preload === "none"){

					$(containers[i]).on(Ulo.evts.click, data, this.load);

				} else{

					$(video).on("canplay", data, this.setup);

				}

			}

		},

		/* ---------------------------------------------------------------------------- */

		unregister: function(data){

			$(data.container.getElementsByTagName("button")).off();

			$(data.container).off();

			$(data.video).off();

		},

		/* ---------------------------------------------------------------------------- */
		
		load: function(e){

			$(e.currentTarget).off(e.type, e.data.self.load);

			var video = e.data.video;

			$(video).on("canplay", e.data, e.data.self.setup);

			video.setAttribute("autoplay", "autoplay");

			video.setAttribute("preload", "metadata");

			video.load();

		},

		/* ---------------------------------------------------------------------------- */

		setup: function(e){

			$(e.currentTarget).off(e.type, e.data.self.setup);


			var container = e.data.container, 
			
			video = e.data.video, 
			
			self = e.data.self, 
			
			elem;


			/* Remove the image poster */

			removeClass(container, "poster");

			/* Remove the div that lets the user know this is a video */

			$("div.video_overlay", container).remove();


			/* Play / pause button */

			elem = container.querySelector("button.play");

			$(elem).on(Ulo.evts.click, e.data, self.togglePlay);


			/* Loop button */

			elem = container.querySelector("button.loop");

			$(elem).on(Ulo.evts.click, e.data, self.loop);


			/* Volume button (mute / unmute) */

			elem = container.querySelector("button.volume");

			$(elem).on(Ulo.evts.click, e.data, self.mute);


			/* Fullscreen button */

			elem = container.querySelector("button.fullscreen");

			if( video.requestFullscreen ){

				$(elem).on(Ulo.evts.click, e.data, self.fullscreen);

			} else{

				removeElements( elem );

			}


			/* Video duration text */

			if(video.duration){

				self.durationchange( { data: e.data } );

			} else{

				$(video).one("durationchange", e.data, self.durationchange);

			}
			
			
			/* Video buffer indicator */

			if(video.buffered.length > 0){

				self.progress( { data: e.data } );

			}


			/* Video events */

			$(video)

				.on(Ulo.evts.click, e.data, self.togglePlay)

				.on("volumechange", e.data, self.volumechange)

				.on("timeupdate", e.data, self.timeupdate)

				.on("progress", e.data, self.progress)

				.on("ended", e.data, self.ended);


			/* Detect hovering */

			$(container)

				.on("mousemove", e.data, self.showControls)
				
				.on("mouseleave", e.data, self.hideControls)


			/* Display thumbnail frame */

			elem = container.querySelector("div.thumbnail_position");

			$(elem)

				.on("mouseenter", e.data, self.showThumbnail)
			
				.on("mouseleave", e.data, self.hideThumbnail);


			/* Media track controls */

			self.TrackControl.register(video, container, "div.track");


			/* Volume slider controls */

			self.VolumeControl.register(video, container, "div.volume_controls");

		},

		/* ---------------------------------------------------------------------------- */

		showControls: function(e){

			var self = e.data.self;

			if(self.stopPropagation === true){

				self.stopPropagation = false;

			} else{

				clearTimeout( self.timeout_id );

				self.timeout_id = setTimeout(self.hideControls.bind(this, e), 3000);


				var controls = e.data.container.querySelector("div.video_controls");
				
				if( hasClass(controls, Ulo.cls.hide) ){

					removeClass(controls, Ulo.cls.hide);

				}

			}

		},

		/* ---------------------------------------------------------------------------- */

		hideControls: function(e){

			if(e.data.video.paused === false){

				var self = e.data.self;

				clearTimeout( self.timeout_id );

				var controls = e.data.container.querySelector("div.video_controls");

				self.stopPropagation = isDescendant(controls.parentNode, e.target, 12);

				addClass(controls, Ulo.cls.hide);

			}

		},

		/* ---------------------------------------------------------------------------- */

		showThumbnail: function(e){

			var frame = e.data.container.querySelector("div.thumbnail_frame");

			removeClass(frame, Ulo.cls.hide);


			var max_width = 

				e.currentTarget.parentNode.clientWidth + 

				e.currentTarget.clientWidth;			


			var left = 

				e.currentTarget.offsetLeft + 

				e.currentTarget.clientWidth * 0.5 - 

				frame.clientWidth * 0.5;


			frame.style.left = 

				Math.min(

					Math.max( 0, left ), 

					max_width - frame.clientWidth

				) + "px";

		},

		/* ---------------------------------------------------------------------------- */

		hideThumbnail: function(e){

			var frame = e.data.container.querySelector("div.thumbnail_frame");

			addClass(frame, Ulo.cls.hide);

		},

		/* ---------------------------------------------------------------------------- */

		changeIcon: function(icon, name){

			icon.className = icon.className.replace(/(icon_)[\w]+/, "$1" + name);

		},

		/* ---------------------------------------------------------------------------- */

		togglePlay: function(e){

			var self = e.data.self;

			var video = e.data.video;

			var icon = e.data.container.querySelector("span.play");

			
			if(video.paused || video.ended){

				self.changeIcon(icon, "pause");

				var promise = video.play();

				if( promise ){

					promise["catch"](function(){ 

						console.log('PLAY ERROR'); 

						self.error(e);

					});

				}

			}else{

				clearTimeout( self.timeout_id );

				var controls = e.data.container.querySelector("div.video_controls");

				removeClass(controls, Ulo.cls.hide);

				self.changeIcon(icon, "play");

				video.pause();

			}

		},

		/* ---------------------------------------------------------------------------- */

		loop: function(e){

			var video = e.data.video;

			video.loop = !video.loop;

			(video.loop ? addClass : removeClass)(e.currentTarget, "selected");

		},


		/* ---------------------------------------------------------------------------- */

		mute: function(e){

			e.data.video.muted = !e.data.video.muted;

		},

		/* ---------------------------------------------------------------------------- */

		volumechange: function(e){

			var self = e.data.self;

			var video = e.data.video;

			var container = e.data.container;


			var icon = container.querySelector("span.volume");

			var level = container.querySelector("span.level");

			var slider = container.querySelector("button.volume_slider");


			if(video.muted){

				slider.style.bottom = level.style.height = "0%";

				self.changeIcon(icon, "mute");

			} else{

				slider.style.bottom = level.style.height =  (video.volume * 100) + "%";

				self.changeIcon(icon, "volume");

			}

		},

		/* ---------------------------------------------------------------------------- */

		secondsToTime: function(secs){

			secs = Math.round( secs );

			/* Minutes divisor */
			var md = secs % 3600;

			var h = Math.floor(secs / 3600);

			var m = Math.floor(md / 60);
			
			var s = Math.ceil(md % 60);


			return (h === 0 ? "" : h + ":") + m + ":" + (s < 10 ? "0" + s : s);

		},

		/* ---------------------------------------------------------------------------- */

		durationchange: function(e){

			var duration = e.data.container.querySelector("span.duration");

			emptyElement( duration )

				.appendChild(

					document.createTextNode(

						e.data.self.secondsToTime( e.data.video.duration || 0 )

					)

			);

		},

		/* ---------------------------------------------------------------------------- */

		timeupdate: function(e){

			var container = e.data.container;
 

			container.querySelector("span.elapsed").style.width = 

			container.querySelector("button.track_slider").style.left = (

				((e.currentTarget.currentTime / e.currentTarget.duration) * 100) + "%"

			);


			emptyElement( container.querySelector("span.current_time") )

				.appendChild(

					document.createTextNode(

						e.data.self.secondsToTime( e.currentTarget.currentTime )

					)

			);

		},

		/* ---------------------------------------------------------------------------- */

		progress: function(e){

			var video = e.data.video;

			if(video.duration > 0){

				var buffered = video.buffered;

				var container = e.data.container.querySelector("div.buffered");


				var range = container.querySelector("span.range");

				range.style.width = 

					Math.round(

						(buffered.end(buffered.length - 1) / video.duration) * 100

					) + "%";

			}

		},

		/* ---------------------------------------------------------------------------- */

		getFullscreenElement: function(){

			return (

				document.fullscreenElement || 
				document.webkitFullscreenElement|| 
				document.mozFullScreenElement || 
				document.msFullscreenElement

			);

		},

		/* ---------------------------------------------------------------------------- */

		fullscreen: function(e){

			if(!e.data.self.getFullscreenElement()){
				
				e.data.container.requestFullscreen();
			
			} else{

				if(document.exitFullscreen){
					
					document.exitFullscreen(); 
				
				}

			}

		},

		/* ---------------------------------------------------------------------------- */

		fullscreenChange: function(e){

			(!this.getFullscreenElement() ? removeClass : addClass)(document.body, "fullscreen");

		},

		/* ---------------------------------------------------------------------------- */

		ended: function(e){

			if( e.currentTarget.loop === false ){

				var icon = e.data.container.querySelector("span.play");

				e.data.self.changeIcon(icon, "repeat");

			}

		},

		/* ---------------------------------------------------------------------------- */

		error: function(e){

			console.log('VIDEO ERROR: ', e);

			e.data.self.unregister( e.data );


			var error = makeElement("div", {"class": "video_overlay"});

			var reload = makeElement("button",

				{

					"class": "reload",
					"type": "button"

				},

				"Reload"

			);

			$(reload).on(Ulo.evts.click, e.data, e.data.self.load);


			error.appendChild( 

					makeElement("div", {

						"class": "video_error"

					}, "The video has stopped working.")

				)

				.appendChild( 

					reload

				)

				.appendChild( 

					makeElement("span", {"class": "icon icon_repeat"})

			);

			e.data.container.appendChild( error );

		},

	}

	/* -------------------------------------------------------------------------------- */
	/*
		Manage all interactions with a post whether on the timeline or the post detail 
		page.
	*/
	function PostDetail(){


		Ulo.checkDependencies("Session", "Pip");


		/*
			Register post menu actions - MUST REGISTER BEFORE moveActionsMenu.
		*/
		var post_menu = getElement("post_menu");

		$(post_menu.querySelector("button.edit_post"))
			.on(Ulo.evts.click, this.editPost);

		$(post_menu.querySelector("button.delete_post"))
			.on(Ulo.evts.click, {self: this}, this.deletePost);

		$(post_menu.querySelector("button.report_post"))
			.on(Ulo.evts.click, {self: this, name: "post"}, this.reportForm);


		/*
			Register comment menu actions - MUST REGISTER BEFORE moveActionsMenu.
		*/
		var comment_menu = getElement("comment_menu");

		if(comment_menu !== null){

			$(comment_menu.querySelector("button.report_comment"))
				.on(Ulo.evts.click, {self: this, name: "comment"}, this.reportForm);

			$(comment_menu.querySelector("button.delete_comment"))
				.on(Ulo.evts.click, {self: this}, this.deleteComment);

		}

		
		/*
			Register all post actions.
		*/
		this.registerAll();


		this.jqxhr = null;

	}

	PostDetail.prototype = {

		constructor: PostDetail,

		


		/* ---------------------------------------------------------------------------- */
		/*
			Register post actions common to the detail page and the timeline within the 
			context.

			@param context: Optional context to narrow the scope.
		*/
		register: function(context){

			/*
				Check that utils.js has loaded.
			*/
			if(window.Connect){

				Connect.register(context);

			}

			this.registerVotePost(context);

			/*
				Register after all post_menu and comment_menu actions.
			*/
			this.registerMoveActionsMenu(context);

		},	

		/* ---------------------------------------------------------------------------- */
		/*
			Register the button that toggles the posts description.
		*/
		registerToggleDescription: function(){

			var button = getElement("toggle_post_description");

			if(button !== null){

				menus(button, "height");

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Register all post actions.

			@param context: Optional context to narrow the scope.
		*/
		registerAll: function(context){

			this.register(context);

			this.registerComment(context);

			this.registerLoadComments(context);

			this.registerToggleDescription(context);

		},

		/* ---------------------------------------------------------------------------- */




		/* ---------------------------------------------------------------------------- */
		/* ACTION MENUS
		/* ---------------------------------------------------------------------------- */
		/*
			Register all links that toggle the post actions menus (post_menu or 
			comment_menu).

			@param context: Optional context to narrow the scope.
		*/
		registerMoveActionsMenu: function(context){

			var links = $("a.toggle_actions_menu", context);

			for(var i=0; i<links.length; ++i){

				menus(links[i], "height", false, this.moveActionsMenu, true);

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Callback function executed each time an action menu is opened or closed. The
			function moves the menu to the required position.
			
			@param *: See base.js Menu Class.
		*/
		moveActionsMenu: function(isClosed, button, menu, target){

			/*
				Attribute that stores the post or comment id.
			*/
			var attr = "data-id";

			/*
				The action menu (post_menu or comment_menu) buttons.
			*/
			var buttons = menu.getElementsByTagName("button");

			/*
				Keep the menu open if the menu is being closed but the target element is
				another link that also toggles this particular menu.
			*/
			var keep_open = target.getAttribute("data-toggle")===menu.id && target!==button;


			/*
				If closing the menu strip each button of the "data-id" attribute "data-id" 
				and place the menu at the foot of the container.
			*/
			if(isClosed && keep_open === false){

				getMain().appendChild(menu);

				for(var i=0; i<buttons.length; ++i){

					buttons[i].removeAttribute(attr);

				}

			}

			/*
				If opening the menu set the post or comment id on each of the buttons
				and place the menu above the link.
			*/
			else{

				if(isClosed && keep_open){

					button = target;

					/* Force the menu open */
					$(button).trigger(Ulo.evts.click);

				}

				button.parentNode.insertBefore(menu, button);

				for(var i=0; i<buttons.length; ++i){

					buttons[i].setAttribute(attr, button.getAttribute(attr));

				}

				/* Toggle the class "owner" to display the relevant menu options */

				var isOwner = button.getAttribute("data-is-owner") === "true";

				(isOwner ? addClass : removeClass)(menu, "owner");

				menu.style.height = "auto";

			}

		},

		/* ---------------------------------------------------------------------------- */
		/* END ACTION MENUS */
		/* ---------------------------------------------------------------------------- */




		/* ---------------------------------------------------------------------------- */
		/* REPORTING */
		/* ---------------------------------------------------------------------------- */
		/*
			Display the post or comment report form.
		*/
		reportForm: function(e){

			var self = e.data.self;

			var name = e.data.name;

			var page_id = name + "_report";

			var id = this.getAttribute("data-id");


			if(id){

				var page = getElement( page_id );

				var url = "/" + name + "/" + id + "/report/";

				/*
					Get the page if it has not been rendered.
				*/
				if(page === null){

					Ulo.Pip.getPage( url, page_id );
				
				}

				/*
					Else reset the form before displaying it.
				*/
				else{

					var radios = page.getElementsByTagName("input");

					for(var i=0; i<radios.length; ++i){

						radios[i].checked = false;

					}


					var text = page.getElementsByTagName("textarea");

					for(var i=0; i<text.length; ++i){

						text[i].value = "";

					}


					var form = page.getElementsByTagName("form")[0];

					/* Update the url for this particular post */
					form.setAttribute("action", url);


					var submit = getElement( name + "_report_submit" );
					
					addClass(submit, "disabled");
					
					submit.disabled = true;


					Ulo.Pip.open(page);

				}
				
			}

		},

		/* ---------------------------------------------------------------------------- */
		/* END REPORTING */
		/* ---------------------------------------------------------------------------- */




		/* ---------------------------------------------------------------------------- */
		/* VOTES */
		/* ---------------------------------------------------------------------------- */
		/*
			Register the post vote buttons.

			@param context: optional context to narrow the scrope.
		*/
		registerVotePost: function(context){

			if(window.updateCounters){

				var forms = $("form.post_vote_form", context);

				for(var i=0, buttons, voted; i<forms.length; ++i){

					voted = forms[i].querySelector("input[name='voted']");

					/*
						If the user has not voted on a post it will not have the input
						element input[name='voted'].

						Add the element to normalise all forms.
					*/
					if(voted === null){

						forms[i].appendChild(

							makeElement("input", {"type": "hidden", "name": "voted"})

						)

					}

					buttons = forms[i].getElementsByTagName("button");

					$(buttons).on(Ulo.evts.click, {self: this}, this.votePost);

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Vote on a post.
		*/
		votePost: function(e){

			e.preventDefault();

			var self = e.data.self;

			if(Ulo.Pip.login() !== null){

				Ulo.Pip.open();

			}

			else if(self.jqxhr === null){

				/* Get the form relative to a vote button */
				var form = e.currentTarget.parentNode.parentNode;

				var vote = e.currentTarget.value;

				var voted = form.querySelector("input[name='voted']");

				var data = "vote=" + vote + "&voted=" + voted.value;

				self.jqxhr = $.ajax({

						type:"GET",
						data: data,
						url: form.getAttribute("action"),
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/* If the session has changed update the entire page.  */
						
						if(Ulo.Session.hasChanged(xhr)){

							Ulo.Pip.updatePage(xhr);

							return true;

						}


						/* If the login form is returned display it. */

						if(data.html){

							Ulo.Pip.displayPage(xhr, "login");

							return true;

						}


						/* Update the counter values for the votes */

						var current_vote = form.querySelector("button[value='"+ voted.value +"']");

						var cls = "selected";

						if(current_vote !== e.currentTarget){

							addClass(e.currentTarget, cls);

							if(current_vote !== null){

								updateCounters(current_vote.parentNode, false);

							}

						}

						removeClass(current_vote, cls);


						voted.value = vote!==voted.value ? vote : "";

						updateCounters(e.currentTarget.parentNode, !!voted.value);


						/* Update the sparklines */

						var sparkbar = getElement("sparkbar");

						if(sparkbar !== null){

							var total = 0;

							var counters = $("span.vote_count", form);

							for(var i=0; i<counters.length; ++i){

								total += parseInt( counters[i].innerHTML );

							}

							if(total === total){

								var lines = sparkbar.getElementsByTagName("span");

								for(var i=0, value; i<counters.length && i<lines.length; ++i){

									value = parseInt( counters[i].innerHTML );

									if(value===value){

										lines[i].style.width = (total===0 ? 0 : 100*value/total)+"%";

									}

								}

							}

						}

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){

						if(Ulo.Session.hasChanged(xhr)){

							/* Suppress the default error message. */
							xhr.statusCode = function(){}

							messages("Your session has expired. Refresh the page and try again.");

						}
						
					})

					.always(function(){
					
						self.jqxhr = null;
				
				});


			}

		},

		/* ---------------------------------------------------------------------------- */
		/* END VOTES */
		/* ---------------------------------------------------------------------------- */




		/* ---------------------------------------------------------------------------- */
		/* POST ACTIONS
		/* ---------------------------------------------------------------------------- */
		/*
			Navigate to the post updat view.
		*/
		editPost: function(e){

			var post_id = this.getAttribute("data-id");

			if(post_id){

				var url = "/post/" + post_id + "/update/";

				if(Ulo.Page !== undefined){

					Ulo.Page.getPage( url, true );

				} else{

					window.location.assign( url );

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Display a dialog box that gives the user a button to delete the post.
		*/
		deletePost: function(e){

			var self = e.data.self;

			var post_id = this.getAttribute("data-id");

			/*
				Determine if the post belongs to the user.

				Timeline View: Posts that belong to the user are rendered in their 
				timeline with an id of "post_<id>".

				Post Detail View: The button attribute "data-page" is given the value 
				"detail" if the post belongs to the user.
			*/

			if(getElement("post_" + post_id) || this.getAttribute("data-page") === "detail"){


				var elem = makeElement("div", {"class": "delete_dialog"});


				var content = elem.appendChild(

						makeElement("div", {

							"class": "centre", "data-close-pip": "true"

						}) 

					)

					.appendChild( makeElement("div", {"class": "content"}) );

				content.appendChild(

					makeElement("h3", {}, "Are you sure you want to delete this post?") 

				);

				content.appendChild(

					makeElement("button", {

						"type": "button", "data-close-pip": "true"

					}, "Cancel")

				);


				var button = e.currentTarget.cloneNode();

				button.setAttribute("data-close-pip", "true");

				button.appendChild(document.createTextNode("Delete"));


				$(button).on(Ulo.evts.click, {self: self}, self._deletePost);


				content.appendChild(button);


				Ulo.Pip.open(elem, true);

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Delete the post.
		*/
		_deletePost: function(e){

			var self = e.data.self;


			if(Ulo.Pip.login() !== null){

				Ulo.Pip.open();

			}

			else if(self.jqxhr === null){

				var post_id = this.getAttribute("data-id");

				var csrf_token = Ulo.Session.get()[Ulo.Session.TOKEN_NAME];

				self.jqxhr = $.ajax({

						type: "POST",
						data: Ulo.Session.CSRF_TOKEN_NAME + "=" + csrf_token,
						url: "/post/" + post_id + "/delete/",
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/* If the login form is returned display it. */

						if(data.html){

							Ulo.Pip.displayPage(xhr, "login");

							return true;

						}

						var page = e.currentTarget.getAttribute("data-page");

						/*
							Redirect to the homepage if the post was deleted from the
							post detail page.
						*/
						if(page === "detail"){

							Ulo.replacePage("/");

						}

						/*
							Delete the post from the timeline
						*/
						else if(page === "timeline"){

							var post = getElement("post_" + post_id);

							/* Delete the post from the timeline and the Pip */

							removeElements(post, getElement("post_detail"));

							messages("Your post has been deleted.");

						}

						else{

							/* Error */

						}

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){


					
					})

					.always(function(){

						self.jqxhr = null;
				
				});

			}

		},
		
		/* ---------------------------------------------------------------------------- */
		/* POST ACTIONS
		/* ---------------------------------------------------------------------------- */




		/* ---------------------------------------------------------------------------- */
		/* COMMENTS ACTIONS */
		/* ---------------------------------------------------------------------------- */
		/*
			Register the comment form.
		*/
		registerComment: function(){

			$(getElement("comment_form")).on("submit", {self: this}, this.comment);

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Register the button that fetches the next set of comments.
		*/
		registerLoadComments: function(){

			$(getElement("load_comments")).on(Ulo.evts.click, {self: this}, this.loadComments);

		},

		/* ---------------------------------------------------------------------------- */
		/*
			unregister the button that fetches the next set of comments.
		*/
		unregisterLoadComments: function(){

			var button = getElement("load_comments");

			$(button).off(Ulo.evts.click, this.loadComments);

			removeElements(button);

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Create the html for the comments and add them to the container.

			@param comments: Array of comments:
			@param append: Boolean - if true append each comment to the container else prepend.
		*/
		renderComments: function(comments, append){

			var container = getElement("comments_container");

			var comment, element, attrs;

			for(var i in comments){

				comment = makeElement("div", {"class": "comment_container"});


				/* Comment actions link */

				element = comment.appendChild(

					makeElement("div", {"class": "comment_actions"})

				);

				attrs = {

					"href": "/comment/" + comments[i].id + "/actions/",
					"data-id": comments[i].id,
					"data-toggle": "comment_menu",
					"class": "toggle_actions_menu icon icon_menu",
					"title": "Comment actions"

				}

				/*
					Give the comment an id if it belongs to the logged in user and give
					the link the attribute "data-is-owner".
				*/
				if(Ulo.Session.isOwner(comments[i].user_id)){

					comment.setAttribute("id", "comment_" + comments[i].id);

					attrs["data-is-owner"] = "true";

				}

				element.appendChild( makeElement("a", attrs) )

					.appendChild( 

						makeElement("span", {"class": "hide"}, "Comment actions") 

					);

				this.registerMoveActionsMenu(comment);

				/* END Comment actions link */


				/* Profile of the user that made the comment */

				element = comment.appendChild(

						makeElement("div", {"class": "user_profile"})

					)

					.appendChild(

						makeElement("a", {

							"href": "/user/" + comments[i].username + "/",
							"data-apl": "true",
							"class": "profile",
							"title": "Profile"

						})

					);

				registerLinks(comment);

				element.appendChild( makeElement("img", {

					"class": "user_thumbnail", 
					"src": Ulo.MEDIA_URL + comments[i].thumbnail

				}));

				element = element.appendChild( 

					makeElement("div", {"class": "user_names"})

				);

				element.appendChild(

					makeElement("h3", {"class": "name thin ellipsis"}, comments[i].name) 

				);

				element.appendChild( 

					makeElement("span", {

						"class": "username ellipsis"

					}, "@" + comments[i].username) 

				);

				/* END Profile of the user that made the comment */


				/* Comment text */

				element = makeElement("div", {"class": "comment_text"}, comments[i].comment);

				/* END comment text */


				comment.appendChild( element );

				if(append){

					container.appendChild(comment);

				} else{

					container.insertBefore(comment, container.firstChild);

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Update max_id to the id of the last comment rendered. Each request uses this
			values as cursor in the database.

			@param comments: Array of comments.
		*/
		setMaxID: function(comments){

			if(comments !== undefined && comments.length > 0){

				this.max_id = comments[comments.length-1].id;
					
			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Get the next set of comments and render them.
		*/
		loadComments: function(e){

			e.preventDefault();
			
			var self = e.data.self;

			if(self.jqxhr === null){

				self.jqxhr = $.ajax({

						type: "GET",
						data: {"max_id": self.max_id},
						url: this.getAttribute("data-action"),
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data){

						self.setMaxID(data.comments);

						self.renderComments(data.comments, true);

						/* Either there are more comments to get or unregister the button */
						data.has_next || self.unregisterLoadComments();

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){


					
					})

					.always(function(){

						self.jqxhr = null;
				
				});

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Delete a comment.
		*/
		deleteComment: function(e){

			e.preventDefault();

			
			var self = e.data.self;

			var comment_id = this.getAttribute("data-id");

			/*
				Ids are only given to comments that belongs to the logged in user. See 
				renderComments().
			*/
			var comment = getElement( "comment_" + comment_id );


			if(Ulo.Pip.login() !== null){

				Ulo.Pip.open();

			}

			else if(self.jqxhr === null && comment !== null){

				var csrf_token = Ulo.Session.get()[Ulo.Session.TOKEN_NAME];

				self.jqxhr = $.ajax({

						type:"POST",
						data: Ulo.Session.CSRF_TOKEN_NAME + "=" + csrf_token,
						url: "/comment/" + comment_id + "/delete/",
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/* If the login form is returned display it. */

						if(data.html){

							Ulo.Pip.displayPage(xhr, "login");

						} else{

							removeElements( comment );

							messages("Comment deleted");

						}

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){

						if(Ulo.Session.hasChanged(xhr)){

							addClass(e.currentTarget, "disabled").disabled = true;

						}
					
					})

					.always(function(){

						self.jqxhr = null;
				
				});

			}

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Remove all validation errors within the context.

			@param context: Optional context to narrow the scope.
		*/
		removeText: function(context){

			$("p.validation", context).remove();

		},

		/* ---------------------------------------------------------------------------- */
		/*
			Create a comment for this post.
		*/
		comment: function(e){

			e.preventDefault();

			var self = e.data.self, form = this;

			if(Ulo.Pip.login() !== null){

				Ulo.Pip.open();

			}

			else if(self.jqxhr === null && form.disabled !== true){

				/* Comment input field */
				var comment = getElement("comment");

				if(regexp_unicode_ws.test(comment.value) === false){

					self.jqxhr = $.ajax({

							type: "POST",
							data: $(this).serialize(),
							url: this.getAttribute("action"),
							statusCode: requestCodes

						})

						/* Params: server data, status code, xhr */
						.done(function(data, sc, xhr){


							/* If the login form is returned display it. */

							if(data.html){

								Ulo.Pip.displayPage(xhr, "login");

								return true;

							}


							comment.value = "";

							self.removeText(form);

							self.renderComments(data.comments, false);

							if(self.max_id === undefined){

								self.setMaxID(data.comments);

							}

						})

						/* Params: xhr, status code, error type */
						.fail(function(xhr, sc, type){

							self.removeText(form);

							if(xhr.responseJSON!==undefined && 
								xhr.responseJSON.errors!==undefined){

								var errors = xhr.responseJSON.errors

								for(var n in errors){

									for(var i=0; i<errors[n].length; ++i){

										form.insertBefore( 

											makeElement("p", {

												"class": "validation error"

											}, errors[n][i]),

											form.firstChild

										);

									}

								}

							} else if(Ulo.Session.hasChanged(xhr)){

								addClass(form, "disabled").disabled = true;

							}
							
						})

						.always(function(){
						
							self.jqxhr = null;
					
					});

				}

			}

		},

		/* ---------------------------------------------------------------------------- */
		/* END COMMENTS ACTIONS */
		/* ---------------------------------------------------------------------------- */

	}

	/* -------------------------------------------------------------------------------- */




	/* -------------------------------------------------------------------------------- */
	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */

	$(function(){

		Ulo.newClass(PostDetail, "PostDetail", true);

		Ulo.newClass(PostVideo, "PostVideo", true);
		
	});

	/* -------------------------------------------------------------------------------- */

}());



