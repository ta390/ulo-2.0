/* Post File Javascript File */
/* Dependencies: jQuery, base.js, upload.js */

/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";
/* ------------------------------------------------------------------------------------ */

	/* File input element used by all classes */
	var file_input = getElement("file0");

	/* True if upload.js was loaded successfully */
	var dependencies = true;
	try{ Editor && FileUpload; } catch(e){ dependencies = false; }


	/* -------------------------------------------------------------------------------- */
	/* POST IMAGE/VIDEO EDITOR */
	/* -------------------------------------------------------------------------------- */
	/*
		Manage image and video editing for posts.
	*/
	function PostEditor(max_img, max_vid){

		/* run base class constructor */
		Editor.call(this, max_img, max_vid);
	
	}
	/* -------------------------------------------------------------------------------- */
	
	/* Base class Editor */
	PostEditor.prototype = inherit(dependencies && Editor.prototype);
	
	/* -------------------------------------------------------------------------------- */
	/* Override inhertied constructor */
	PostEditor.prototype.constructor = PostEditor;
	/* -------------------------------------------------------------------------------- */
	/*
		OVERRIDE
		Define image and video file processors to load when the editor is opened.
	*/
	PostEditor.prototype.assignProcessors = function(){
		this.image_processors = [ScaleElement, MoveElement];
		this.video_processors = [ScaleElement, MoveElement, VideoThumbnail];
	};
	/* -------------------------------------------------------------------------------- */
	/*
		OVERRIDE
		Define image and video file processors to load when the editor is opened.
	*/
	PostEditor.prototype.dataSize = function(){
		switch(this.type){
			case this.image: return {max: 320, min: 120}; 
			case this.video: return {max: 12, min: 12};
			default: throw new FileUploadException("The file type is not supported.");
		}
	},
	/* -------------------------------------------------------------------------------- */
	/*
		OVERRIDE
		Set the title of the editor according to the file type and move the image input
		element into the edtior to enable the adding of images while in the editor.
	*/
	PostEditor.prototype.open = function(type){

		/* Set the title and move the input element the first time the editor is opened */
		if(this.isClosed){
			try{

				var title;

				if(type === this.video){
					title = "Choose a thumbnail";
				}else{
					title = "Photo editor";
					this.moveInputTo("add_file");
				}

				emptyElement( getElement("editor_title") )
					.appendChild( document.createTextNode(title) );

			
			} catch(e){}
		}

		/* hide the file selection container when opening the editor */
		addClass(getElement("select_container"), this.c_hide);
		/* call base class function */
		Editor.prototype.open.call(this, type);
	};
	/* -------------------------------------------------------------------------------- */
	/*
		OVERRIDE
		Move the input element place in the editor back to its original position and
		show the file selection container.
	*/
	PostEditor.prototype.close = function(){

		/* move the input element back to its container */
		this.moveInputTo("select_files");
		/* show the file selection container */
		removeClass(getElement("select_container"), this.c_hide);
		/* call base class function */
		Editor.prototype.close.call(this);

	};
	/* -------------------------------------------------------------------------------- */
	/*
		OVERRIDE
		Define preview to display the canvas at the top of the form while it is being
		processed.
	*/
	PostEditor.prototype.preview = function(canvas){

		/* get the container used to display the image */
		var preview = getElement("preview");
		/* add the image to the container */
		preview.appendChild(canvas);
		/*
			move the file input element into the container before removing the file 
			selection container.
		*/
		preview.appendChild(file_input).setAttribute("type", "hidden");

		/* set the type and orientation as class names on the container*/
		var class_names = this.type + (this.stacked ? " portrait" : " landscape");
		addClass(preview, class_names);
		

		/* remove all element from the DOM leaving only the upload form */
		removeElements(getElement("select_container"), getElement("capture"), getElement("editor"));
		/* show the form */
		removeClass(getElement("form_container"), this.c_hide);	
		
	};
	/* -------------------------------------------------------------------------------- */
	/*
		HELPER
		Move the image file input element to the container 'id'. See open() and close().
		@param id: id of the container to move the element to.
	*/
	PostEditor.prototype.moveInputTo = function(id){
		try{ 
			getElement(id).appendChild(getElement("select_image")); 
		}catch(e){}
	};


	/* -------------------------------------------------------------------------------- */
	/* POST IMAGE/VIDEO UPLOADER */
	/* -------------------------------------------------------------------------------- */
	/*
		Manage image and video uploads for a post.
	*/
	function PostUpload(FileEditor, form_id, capture_id, max_img, max_vid){
		
		/* run base class constructor */
		FileUpload.call(this, FileEditor, form_id, capture_id, max_img, max_vid);

		/* div styled as a button to either toggle the menu or trigger the file input */
		var image_upload = getElement("image_upload")

		/* if the browser supports media capture FileUpload will have set this.capture */
		if(this.capture){

			/* add click event to toggle the menu for uploading images */
			menus(image_upload, "height", "menu_open");

		/* else if it is not supported remove the extra buttons */
		} else{

			/* move the input element into the image upload container */
			image_upload.appendChild( file_input );
			/* remove the menu */
			removeElements(getElement("image_menu"));

		}
	}
	/* -------------------------------------------------------------------------------- */
	/* Base class FileUpload */
	PostUpload.prototype = inherit(dependencies && FileUpload.prototype);
	/* -------------------------------------------------------------------------------- */
	/* Override inhertied constructor */
	PostUpload.prototype.constructor = PostUpload;
	/* -------------------------------------------------------------------------------- */
	/*
		Subclasses of FileUpload must define submitHandler.
		Call the editor's getData function to retrieve the final canvas and create an 
		instance of the post form.
	*/
	PostUpload.prototype.submitHandler = function(e){

		try{
			var self = e.data.self;
			
			self.capture = null;

			new PostForm(self.editor.getCanvas(true), self.editor.getType());

		} catch(e){

			debug(e)

			messages(
				(e instanceof FileUploadException) ? e.msg : "The file could not be processed."
			);

			emptyElement(getElement("form_container"));

		}

		/* close editor and clear files */
		e.data.self.end();

	};


	/* -------------------------------------------------------------------------------- */
	/* POST IMAGE/VIDEO UPLOADER */
	/* -------------------------------------------------------------------------------- */
	/*
		Handle form validation and interaction for posts.
		@param canvas: canvas returned by the editor containing the image or video data.
		@param file_type: file type name ("image" or "video").

		NOTE: An assumption has been made that the last li element in the ul of each
		options field (option0 and option1) is the text input option.
	*/
	function PostForm(canvas, file_type){

		/* file and thumbnail to upload to the server - See processFile */
		this.file = this.thumbnail = null;
		/* file type being uploaded ("image" or "video") */
		this.file_type = file_type;
		/* set this.file and this.thumbnail once the file has finished being processed */
		this.processFile(canvas);
		/* ajax request */
		this.jqxhr = null;

		/* ---------------------------------------------------------------------------- */

		/* use the best supported event for field validation */
		this.evt = isEventSupported("input") ? "input" : "change";

		/* ---------------------------------------------------------------------------- */

		/* data attributes and class names used throughout the class */
		this.data = {
			/* data attribute which store the id of the element that a button toggles */
			toggle: "data-toggle",
			/* prefix set on the ul for option fields - used to differentiate field names */
			prefix: "data-prefix",
			/* stores the text to display when an input field is empty */
			help: "data-help",
			/* class name given to the default selection */
			selected: "default",
			/* class name given to the active li element in the icons menu */
			active: "active",
			/* class name give to li elements that are no longer valid selections */
			disable: "disabled",
			/* class name given to the span that contains the info of the current selection */
			content: "content",
			/* class name set on an element with an error */
			error: "error"
		}

		/* option field (option0 and option1) data attributes */
		this.opt = {
			option: "data-option_type",
			text: "data-text",
			icon: "data-icon",
			colour: "data-colour"
		}

		/* ---------------------------------------------------------------------------- */

		/* text input fields */
		this.inputs = [
			getElement("title"), 
			getElement("description"),
		]

		/* register a validation handler on each element */
		for(var i=0; i<this.inputs.length; ++i){
			this.register(this.evt, this.inputs[i], this.textHandler);
		}

		/* ---------------------------------------------------------------------------- */

		/* option field menu toggle buttons */
		this.options = [
			getElement("option0"),
			getElement("option1"),
		]

		this.setUp(this.options, true);

		/* other menu toggle buttons */
		this.toggles = [
			getElement("category"),
			getElement("comment_settings")
		]

		this.setUp(this.toggles, false);

		/* ---------------------------------------------------------------------------- */

		/* store reference to the submit button used throughout the class */
		this.submit_button = getElement("id_submit");

		/* show/hide the additional option field */
		this.register(Ulo.evts.click, getElement("toggle_option1_container"), this.toggleOption1);
		/* show/hide the additional settings */
		this.register(Ulo.evts.click, getElement("toggle_settings"), this.toggleSettings);
		/* register click event on the button to submit the form */
		this.register(Ulo.evts.click, this.submit_button, this.submit);
		/* prevent the backspace button when not inside a text field */
		this.register("keydown", document, this.interceptBackspace);
	}

	PostForm.prototype = {

		constructor: PostForm,

		/* HELPER FUNCTIONS */
		/* ---------------------------------------------------------------------------- */
		/*
			Helper function to add an event handler to an element.
			@param evt: event name.
			@param element: element to attach the event to.
			@param handler: function to call when the event is triggered.
			@param data: optional context data to pass to the handler.
		*/
		register: function(evt, element, handler, data){
			data = data || {};
			data.self = this;
			$(element).on(evt, data, handler);
		},
		/*
			Helper function to remove an event handler from an element.
			@param evt: event name.
			@param element: element to attach the event to.
			@param handler: function to call when the event is triggered.
		*/
		unregister: function(evt, element, handler){
			$(element).off(evt, handler);
		},
		/*
			Return the input element placed inside the content span of a button which 
			toggles the menu for options.
			@param button: button that toggle the options field. See this.options.
		*/
		getInput: function(button){
			return button.content.querySelector("input");
		},
		/*
			Return the other option button to the one passed as an argument.
			@param button: button that toggle the options field. See this.options.
		*/
		getOtherOption: function(button){
			return (button === this.options[0]) ? this.options[1] : this.options[0];
		},
		/*
			Return the icon ul that the button toggles.
			@param button: button that toggle the icons menu for an options text input
			field. See textEvents.
		*/
		getIconUl: function(button){
			return getElement(
				button.getAttribute(this.data.toggle)).querySelector("ul");
		},
		/*
			Enable the currently disabled option by removing the class "disabled". Each 
			option button defines the property "disable" which is a reference to the 
			disabled option set by disableOption().
			@param button: button that toggle the options field. See this.options / 
				this.setUp().
			@param opt: other option to button, see getOtherOption(). This optional 
				parameter is passed by validateOption() only.
		*/
		enableOption: function(button, opt){
			/* enable the currently disabled option for button */
			removeClass(button.disable, this.data.disable);
			button.disable = null;
			/*
				validateOption() will pass the opposite option (e.g. if "button" is 
				this.options[0] "opt" will be option[1]. See getOtherOption()) to 
				enableOption() when called. If the opposite option is invalid by the end of 
				the function call it will not be (due to when enableOption() is called 
				within validateOption()). Therefore its selected option which is soon to be 
				valid must be disabled on button.
			*/
			if(opt && button.is_valid===false){
				this.disableOption(
					new RegExp("^\\s*"+button.li.getAttribute(this.opt.text)+"\\s*$", "i"), 
					opt
				);
			}
		},
		/*
			Disable the option that matches the regexp.
			@param regex: regular expression to match the options data-text attributes against
			@param button: button that toggle the options field. See this.options / this.setUp().
		*/
		disableOption: function(regexp, button){
			var length = button.ul.childNodes.length-1;
			for(var i=0; i<length; ++i){
				if(regexp.test(button.ul.childNodes[i].getAttribute(this.opt.text)) ){
					var content = button.ul.childNodes[i].querySelector("span");
					addClass(content, this.data.disable);
					button.disable = content;
				}
			}
		},
		/* 
			Remove the attribute disabled from the submit button. 
		*/
		enableSubmit: function(){
			this.submit_button.removeAttribute("disabled");
		},
		/* 
			Add the attribute disabled from the submit button. 
		*/
		disableSubmit: function(){
			this.submit_button.setAttribute("disabled", "true");
		},
		/*
			Remove all text nodes between the li element of a ul.
			@param ul: ul element.
		*/
		removeTextNodes: function(ul){
			var nodes = ul.childNodes;

			for(var i=nodes.length-1; i>=0; --i){
				if(nodes[i].nodeType === nodes[i].TEXT_NODE){
					ul.removeChild(nodes[i]);
				}
			}
		},
		/*
			Set the values of this.file and this.thumbnail once the editor has 
			finished processing the canvas.
			@param canvas: canvas returned by the editor's getData function.
		*/
		processFile: function(canvas){
			if(canvas){
				var self = this;

				/* periodically check to see if the canvas has been processed */
				var interval = setInterval(function(e){

					/* if the canvas has no more files pending... */
					if(canvas.pending === 0){

						clearInterval(interval);

						/* convert the canvas image to a blob (jpeg) */
						canvas.toBlob(function(blob){
							/* 
								if the file is a video the canvas image is its thumbnail and
								the video file is set on the canvas as the property file.
								See Editor._videoData() and FileUpload.openEditor() to see how
								and when the file is set as a property on the element/canvas
							*/
							if(self.file_type === "video"){

								self.file = canvas.file;
								self.thumbnail = blob
							
							/*
								else blob is the image file and the thumbnail for images are
								created server side.
							*/
							} else{
							
								self.file = blob;
							
							}

							/* 
								show the user that the file has finished processing and
								enable the submit button.
							*/
							addClass(getElement("status"), "complete");
							self.enableSubmit();


						}, 'image/jpeg', 95);

					}

				}, 800);
			}

		},
		/*
			Set up the drop down menus toggles by this.options and this.toggles.

			Each button is given the following properties:

				button.li: selected li element.
				button.ul: element toggled by the button.
				button.content: a span element inside the button where the content
					of the selected li is placed.
				button.disable: the disabled li element (this.options only).
				button.isOption: boolean - true if the button toggles an option (this.options).

			@param elements: this.options or this.toggles.
			@param isOption: boolean - true if elements is this.options and false if not.
		*/
		setUp: function(elements, isOption){

			/* 
				Remove all child nodes of the ul toggled by the button (this helps
				simplify code later in the class) and set is_valid for each option
				to true by default.
			*/
			for(var i=0; i<elements.length; ++i){
				elements[i].ul = getElement(elements[i].getAttribute(this.data.toggle));
				if(isOption){ 
					elements[i].is_valid = true;
					this.removeTextNodes(elements[i].ul) 
				};
			}

			for(var i=0; i<elements.length; ++i){

				/* set property to easily identify option buttons */
				elements[i].isOption = isOption

				/*
					store reference to the button's own span.content element as a 
					property of the button.
				*/
				elements[i].content = elements[i].querySelector("."+this.data.content);

				/* get the default li element from the ul that this button toggles */
				var li_active = elements[i].ul.querySelector("."+this.data.selected);

				/* 
					option1 which is not a required field does not have a default 
					element. This assumption must hold true.
				*/
				if(li_active){
					/* 
						update the buttons span.content element with the content of 
						li_active.content so it appears selected to the user.
					*/
					this.changeContent(elements[i], li_active, true);
				}

				/*
					register event to toggle the ul and process selections eith
					this.menuCallback
				*/
				menus(elements[i], "height", false, this.menuCallback, this);

			}

		},
		/* END HELPER FUNCTIONS */
		/* ---------------------------------------------------------------------------- */


		/* EVENT HANDLERS */
		/* ---------------------------------------------------------------------------- */
		/*
			Stop the space bar from triggering click events on the toggle buttons. This 
			is necessary when the selected option is the text input field.
		*/
		interceptSpaceBar: function(e){
			if(e.which === 32){ e.preventDefault(); }
		},
		/*
			Stop the back space button from moving the browser back through its history
			when not inside a potential text field (div[contenteditable]).
		*/
		interceptBackspace: function(e){
			try{
				if(e.which===8 && /INPUT|TEXTAREA|DIV/.test(e.target.nodeName)===false){
					e.preventDefault();
				}
			} catch(e){}
		},
		/*
			Toggle the display of the settings panel.
		*/
		toggleSettings: function(e){
			$(getElement("settings")).slideToggle(200);
			toggleClass(this, "menu_open");
		},
		/*
			Set the colour value of input[type=color] as the background colour on the
			button (for the user) and the data-colour attribute on the selected li.
		*/
		colourPicker: function(e){
			var button = e.data.button;
			button.style.backgroundColor = e.target.value;
			button.li.setAttribute(e.data.attr, e.target.value);
		},
		/*
			Event handler wrapper for validatText.
		*/
		textHandler: function(e){
			e.data.self.validateText(e.target);
		},
		/*
			Run text validateText and validateOption when the selected option is
			the text input field.
		*/
		optionHandler: function(e){

			var self = e.data.self;

			/* text input element */
			var target = e.target;
			/* options button */
			var button = e.data.button;

			/* Run validateText, if successful... */
			if(self.validateText(target)){

				/* set the value on the li's data attribute data-text */
				button.li.setAttribute(self.opt.text, $.trim(target.value));

				/*
					Run validateOption, if successful remove all errors previously 
					set by this function. (Not errors set by validateText)
				*/
				if(self.validateOption(button, button.li, true)){

					/* remove the class "error" from the button */
					removeClass(button, self.data.error);

				/*
					else add an error to the form, setting the error on the button to
					distinguish it from errors set by validateText on the text input
					element.
				*/
				} else{

					self.addError(button, "Please choose different names for each button.");

				}
				
			/*
				else give the button the "error" class and remove any errors set on the 
				other option button.
				WHY?: validateOption checks that the two options selected are unique, so if
				one option has a text validation error then it cannot be a valid option
				so a conflict cannot occur.
			*/
			} else {

				addClass(button, self.data.error);
				self.removeOptionError( self.getOtherOption(button) );

			}
			
		},
		/*
			Toggle the display of option1.
		*/
		toggleOption1: function(e){

			var self = e.data.self;
			var opt1 = self.options[1];

			/*
				If the class remove has been set then the additional option will be
				made visible to the user.
			*/
			if(toggleClass(this, "remove")){

				/* set option1 to the first option that is not set for option0 */
				var nodes = opt1.ul.childNodes;
				for(var i=0; i<nodes.length; ++i){
					if(self.changeContent(opt1, nodes[i])){
						break;
					}
				}

			/* else the option has been removed so reset all its values */
			} else{

				/* enable all options for option0 */
				self.enableOption(self.options[0]);
				/* remove events set by textEvents */
				self.textEvents(opt1, false);
				/* set the selected li property to null */
				opt1.li = null;

			}

			/* show/hide the option using jQuery's slideToggle */
			$(getElement("option1_container")).slideToggle(100);

		},

		/* END EVENT HANDLERS */
		/* ---------------------------------------------------------------------------- */


		/* ERROR MESSAGES */
		/* ---------------------------------------------------------------------------- */
		/*
			Add an error message to the form.
			NOTE: each form field is assumed to be inside a container where the id is
			'field_name'_container. Error messages are added above the firstChild in the
			container.
			@param element: any element which defines the property 'name'
			@param msg: error message to add to the form.
		*/
		addError: function(element, msg){

			/* get the name of the element */
			var name = element.getAttribute("name");
			/* create an id for the error div by adding '_error' as a suffix */
			var id = name+"_error";
			/* see if an error div is already in place */
			var div = getElement(id);
			/*
				set the property is_valid on the element. This is currently only used by
				removeOptionError to identify errors set by optionHandler.
			*/
			element.is_valid = false;


			/* if the div exists then replace the text */
			if(div){
				/* 
					lastChild is span.text and lastChild.firstChild is the TextNode.
					See below.
				*/
				div.lastChild.replaceChild(
					document.createTextNode(msg), 
					div.lastChild.firstChild
				);


			/* else create the div and place it inside the container */
			} else{

				var container = getElement(name+"_container");

				if(container){

					/* error container - give it an id so removeError can identify it */
					div = makeElement("div", {"id":id, "class":"error"});

					/* error icon */
					var icon = div.appendChild(makeElement("span", {"class":"icon"}));

					/* error message */
					var text = div.appendChild(makeElement("span", {"class":"text"}, msg));

					/* prepend the error div to the container */
					container.insertBefore(div, container.firstChild);
					/* give the element the class "error" */
					addClass(element, this.data.error);
				
				}

			}

		},
		/*
			Remove an error message for a given element.
			@param element: any element which defines the property 'name'
		*/
		removeError: function(element){

			var div = getElement(element.name+"_error");
			
			if(div){
				div.parentNode.removeChild(div);
			}

			removeClass(element, this.data.error);
			element.is_valid = true;
		},
		/*
			Remove error messages set on the button element only.
			@param button: this.options.
		*/
		removeOptionError: function(button){
			if(button.is_valid === false){
				this.removeError(button);
			}
		},

		/* END ERROR MESSAGES */
		/* ---------------------------------------------------------------------------- */


		/* VALIDATION */
		/* ---------------------------------------------------------------------------- */
		/*
			Validate the length and value of an input[type=text] field. Return true if
			valid and false if not.
			@para input: text input element.
		*/
		validateText: function(input){

			var value = $.trim(input.value);
			var required = input.getAttribute("required");
			var max = parseInt(input.getAttribute("maxlength"));

			if(required && value===""){
				
				this.addError(
					input, 
					input.getAttribute(this.data.help) || "Please fill out this field."
				);

			} else if(max && value.length > max){

				this.addError(input, "Maximum length of "+max+" characters.");

			} else{

				this.removeError(input);
				return true;

			}

			return false;
		},
		/*
			Validate the selected option ensuring that the two options (option0 and 
			option1) are unique. Return true if the selected option is valid and false
			if not.
			@param button: button that toggle the options field. See this.options / 
				this.setUp().
			@param li: the selected option.
			@param isEvent: boolean - true if the function is called from the event 
				handler optionHandler.
		*/
		validateOption: function(button, li, isEvent){

			/* get the text value of the selected li */
			var text = li.getAttribute(this.opt.text);

			/* get the other option button */
			var opt = this.getOtherOption(button);

			/* if the selected element has a text value... */
			if(text){

				/* create a regexp ignoring case and leading/trailing whitespace */
				var regexp = new RegExp("^\\s*"+text+"\\s*$", "i");	

				/* if the other option's selected element match the current selection... */
				if(opt.li && regexp.test(opt.li.getAttribute(this.opt.text))){
					
					/*
						if the option is the text input field check and disable any options
						on itself that match the text of the other options selected item.
					*/
					if(isEvent){
						this.disableOption(regexp, button);
					}
					
					/* return false only if the other option is error free i.e. is_valid is true */
					return opt.is_valid===false;
					
				/* 
					else there is no conflict so disable the selected option on the other option
					and return true.
				*/
				} else{
					/* remove the currently disabled option from "opt"*/
					this.enableOption(opt, button);
					/* disable the option that matched the selected elements text */
					this.disableOption(regexp, opt);
					
				}

			/* 
				else enable the disabled option on the other option "opt" as there
				can be no conflicts if the selected option is blank.
			*/
			} else{

				this.enableOption(opt, button);

			}

			this.removeOptionError(button);
			this.removeOptionError(opt);

			return true;

		},
		/*
			Validate all form fields and throw an exception if any field is invalid.
		*/
		validateForm: function(){

			var errors = false;

			/* validate text input fields. this.inputs */
			for(var i=0; i<this.inputs.length; ++i){
				if(this.validateText(this.inputs[i]) === false){
					errors = true;
				}
			}

			/* validate the option fields. this.options */
			for(var i=0; i<this.options.length; ++i){
				var opt = this.options[i];
				var input = this.getInput(opt);
				
				if(opt.li && this.validateOption(opt, opt.li, false) === false ||
					input && this.validateText(input) === false)
				{
					errors = true;
				}
			}

			/* validate the uploaded file */
			if(!this.file){
		
				this.addError(file_input, "We are processing your file.");
				errors = true;
		
			} else if(!this.file_type){
				
				this.addError(
					file_input, 
					"The file type is not supported. Please upload another file."
				);		
				errors = true;
			}

			if(errors){ 
				throw new Error(); 
			}

		},

		/* ICON MENU (OPTION TEXT FIELD) */
		/* ---------------------------------------------------------------------------- */
		/*
			Toggle events on an input[type=color] field.
			@param button: button that the toggle the icons menu.
			@param set: boolean - true if events need to be added and false if removed.
		*/
		colourEvents: function(button, set){

			/* get the input element from the selected li */
			var input = button.li.querySelector("input");

			/* if the selected li has an input element... */
			if(input){

				/* 
					if set: update the button's background colour and set the colourPicker
					event on the input.
				*/
				if(set){

					button.style.backgroundColor = input.value;	
					this.register("change", input, this.colourPicker, 
						{button: button, attr: this.opt.colour});
				
				/* else remove the event */
				} else{
				
					this.unregister("change", input, this.colourPicker);
				
				}

			}
		},
		/*
			Change the content placed in the icons menu button with the content of the
			selected icon (li).
			@param button: button that the toggle the icons menu.
			@param li: selected icon.
		*/
		changeIconContent: function(button, li){

			/* read the colour and icon values of the selected element */
			var colour = li.getAttribute(this.opt.colour) || ""
			var icon = li.getAttribute(this.opt.icon) || "";

			/*
				update the class values if the current icon is different from the 
				selected icon 
			*/
			if(button.li !== li){

				removeClass(button, button.li.getAttribute(this.opt.icon));
				addClass(button, icon);
				removeClass(button.li, this.data.active);
				addClass(li, this.data.active);

			}

			/* set the icon and colour values on the button */
			button.setAttribute(this.opt.icon, icon);
			button.setAttribute(this.opt.colour, colour);
			button.style.backgroundColor = colour;
			/* update the values of the selected li */
			button.li = li;

		},
		/*
			Callback function triggered each time the icons menu is opened or closed.
			See Menu class in base.js
		*/
		iconCallback: function(close, button, ul, target){

			/* if an icon has been selected... */
			if(close && isDescendant(button.ul, target, 3)){
			
				/* 
					normalise target to the li (the li stores all data attributes), 
					see this.opt
				*/
				while(target.nodeName !== "LI"){
					target = target.parentNode;
				}

				/* remove events that may have been added to the current selection */
				this.colourEvents(button, false);
				/* update the icon button's content */
				this.changeIconContent(button, target);
				/* add events to the newly selected element */
				this.colourEvents(button, true);

			}

		},
		/* END ICON MENU (OPTION TEXT FIELD) */
		/* ---------------------------------------------------------------------------- */


		/* TOGGLE BUTTONS (this.options and this.toggles) */
		/* ---------------------------------------------------------------------------- */
		/*
			Toggle events on an input[type=text] field.
			@param button: button that toggle the options field. See this.options.
			@param set: boolean - true if events need to be added and false if removed.
		*/
		textEvents: function(button, set){

			/* get the input element stored in the button */
			var input = this.getInput(button);			

			if(input){

				/* get the icons menu button also stored in the button */
				var icon_btn = button.content.querySelector("button");
				
				if(set){

					/* 
						add events to validate option selections and ignore the space bar 
						key which would toggle the menu.
					*/
					this.register(this.evt, input, this.optionHandler, {button: button});
					this.register("keyup", input, this.interceptSpaceBar);
					input.focus();
				
					/* get the default icon */
					icon_btn.ul = icon_btn.ul || this.getIconUl(icon_btn);
					icon_btn.li = icon_btn.li || icon_btn.ul.querySelector("."+this.data.active);
					var selected = icon_btn.ul.querySelector("."+this.data.selected);
					/* set the default icon as the icon button's content */
					this.changeIconContent(icon_btn, selected);

					/* regitser event to toggle the icons menu */
					menus(icon_btn, "height", false, this.iconCallback, this);
					/* show the menu so the user has an idea that it exists */
					$(icon_btn).trigger(Ulo.evts.click);
				
				} else{


					/* unregister all text input events */
					this.unregister("keyup", input, this.interceptSpaceBar);
					this.unregister(this.evt, input, this.optionHandler);
					menus(icon_btn);
					
					/* if the button has a previously selected text option... */
					if(button.li){
						/* unregister events added to the icons menu button */
						this.colourEvents(icon_btn, false);
						/* remove the data-text value from the text option's li element */
						button.li.removeAttribute(this.opt.text);
					}

				}

			}
			
		},
		/*
			Helper function to select to the right changeContent function for the
			button type.
			@param button: button that toggle the menu (this.options or this.toggles).
			@param li: selected option.
		*/
		changeContent: function(button, li){

			if(button.isOption){
				return this._changeOptionContent(button, li);
			} else{
				return this._changeToggleContent(button, li);
			}

		},
		/*
			Change the content placed in the button with the content of the selected 
			element.
			@param button: button that toggle the menu (this.options or this.toggles).
			@param li: selected option.
		*/
		_changeToggleContent: function(button, li){

			/* remove any previous errors */
			this.removeError(button);
			/* get the content of the selected element */
			var content = li.querySelector("."+this.data.content);
			/* add the content to the button - clone the nodes */
			appendElements(emptyElement(button.content), content.childNodes, true); 
			/* update the value of the selected li element */
			button.li = li;
			
		},
		/*
			Change the content placed in the button with the content of the selected 
			option. 
			@param button: button that toggle the menu (this.options or this.toggles).
			@param li: selected option.
		*/
		_changeOptionContent: function(button, li){

			/* update the content only if the selection is valid */
			if(this.validateOption(button, li, false)){

				/* remove events that may have been added to the current selection */
				this.textEvents(button, false);
				/* update the button's content with the selected option */
				this._changeToggleContent(button, li);
				/* add events to the newly selected element */
				this.textEvents(button, true);

				return true;
			}

			return false;
		},
		/*
			Callback function triggered each time the icons menu is opened or closed.
			See Menu class in base.js
			@param close: true if the menu has been closed.
			@param button: the button that toggles the ul.
			@param ul: the ul toggled by the button.
			@param target: event target element.
		*/
		menuCallback: function(close, button, ul, target){

			/* if an element within the ul has been selected... */
			if(close && isDescendant(ul, target, 4)){

				/* 
					normalise target to the li (the li stores all data attributes), 
					see this.opt
				*/
				while(target.nodeName !== "LI"){
					target = target.parentNode;
				}

				/* if a new selection was made change the button's content */
				if(button.li !== target){
					this.changeContent(button, target);
				}
				
				
			}

		},

		/* END TOGGLE BUTTONS (this.options and this.toggles) */
		/* ---------------------------------------------------------------------------- */


		/* FORM DATA */
		/* ---------------------------------------------------------------------------- */
		/*
			Helper to convert data attributues into name, value pairs and add the value to
			the FormData instance. If name is not given then the name for the value
			is the name of the data attributes minus the prefix "data-".
			@param data: FormData instance.
			@param attrs: attributes object.
			@param prefix: string to prefix each name with.
			@param name: options name value to give to all attributes.
		*/
		_addDataAttrs: function(data, attrs, prefix, name){
			for(var i=0; i<attrs.length; ++i){
				if(/^data-/.test(attrs[i].name)){
					data.append(
						prefix+(name || attrs[i].name.slice(5)),
						attrs[i].value
					);
				}	
			}
		},
		/*
			Helper to add the values stored on the icons menu button to the FormData 
			instance.
			@param data: FormData instance.
			@param button: this.options button.
			@param prefix: string to prefix each name with.
		*/
		_addIconData: function(data, button, prefix){
			/*
				if the current option has an input element the add the data attribute
				values from the icons menu button to the form.
			*/
			if(this.getInput(button)){
				/* get the icons menu button */
				var button = button.content.querySelector("button");
				/* get that data-icon value */
				data.append(
					prefix+this.opt.icon.slice(5), 
					button.getAttribute(this.opt.icon) || ""
				)
				/* get that data-colour value */
				data.append(
					prefix+this.opt.colour.slice(5),
					button.getAttribute(this.opt.colour) || ""
				)

			}

		},
		/*
			Convert all data attributes of the selected element into name value pairs
			and add them to the FormData instance.
			@param data: FormData instance.
			@param buttons: this.options or this.toggles
		*/
		addData: function(data, buttons){

			for(var i=0; i<buttons.length; ++i){

				/* get the selected element */
				var li = buttons[i].li;
				
				if(li){

					/* get the name prefix value (optional) */
					var prefix = buttons[i].ul.getAttribute(this.data.prefix) || "";
					/* get the name to give each option (optional) */
					var name = buttons[i].ul.getAttribute("name") || "";
					/* add the data attributes stored on the li */
					this._addDataAttrs(data, li.attributes, prefix, name);
					/*
						add the data attributes stored on the icons menu button. Only
						relevant for option fields if text input is the selection option.
					*/
					this._addIconData(data, buttons[i], prefix);

				}
				
			}

		},
		/*
			Convert input[type=text] fields into name, value pairs and add then to the
			FormData instance.
			@param data: FormData instance.
		*/
		addTextData: function(data){
			for(var i=0; i<this.inputs.length; ++i){
				data.append(this.inputs[i].name, this.inputs[i].value)
			}
		},
		/* END FORM DATA */
		/* ---------------------------------------------------------------------------- */


		/* FORM SUBMISSION */
		/* ---------------------------------------------------------------------------- */
		/*
			Event handler called when the form's submit button is clicked.
		*/
		submit: function(e){

			try{

				var self = e.data.self;
				self.disableSubmit();
				self.validateForm();
				
				var data = new FormData();
				var form = getElement("post_form");
				var csrf = form.querySelector("input[name='"+Ulo.Session.CSRF_TOKEN_NAME+"']");
				
				/* add all form data to the FormData instance */
				data.append(csrf.name, csrf.value);
				self.addTextData(data);
				self.addData(data, self.options);
				self.addData(data, self.toggles);
				/* make the files the last form field. See posts.views */
				data.append(file_input.name, self.file);
				data.append("thumbnail", self.thumbnail);
				

				// var iter = data.entries();
				// var en;
				// while( (en = iter.next().value) ){
				// 	console.log(en);
				// }

				/* if no requests are in progress... */
				if(self.jqxhr === null){

					/* create the request object */
					var request = {
						type: "POST",
						url: form.getAttribute("action")+self.file_type+"/",
						data: data,
						cache:false,
						processData: false,
						contentType: false,
						statusCode: requestCodes,
					}

					/* make the ajax request */
					self.jqxhr = $.ajax(request)

						/* params: server data, status code, xhr */
						.done(function(data){

							window.location.replace(data.url);

						})

						/* params: xhr, status code, error type */
						.fail(function(xhr){

							try{
 								
 								/* true if a flash message should be shown */
								var show_message = true;
								/* if responseJSON is undefined an error will be thrown */
								var errors = xhr.responseJSON.errors;

								/* add each error to its form field */
								for(var key in errors){
									var element = getElement(key);

									if(element){
										self.addError(element, errors[key][0]);
										show_message = false;
									}

								}
				
							} catch(e){

								/*
									if the reponse is empty or 403 then the server may have killed
									the connection due to the file being larger than the file limit 
									or an invalid file type.
								*/

							}

							self.jqxhr = null;
							self.enableSubmit();

							if(show_message){

								requestMessages(
									xhr, "Sorry, we are unable to make your post. Please try again later."
								);
								
							}

					});
				}

			} catch(ex){


				console.log(ex);

				/* Form validation errors will throw an exception */

				e.data.self.enableSubmit();
				messages("The form contains errors.");

			}

		},

	}
	/* END FORM SUBMISSION */
	/* ---------------------------------------------------------------------------- */


	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */

	$(function(){

		try{

			new PostUpload(PostEditor, "select_container", "open_capture", 2, 1);


		} catch(e){

			debug(e);
			messages("The editor did not loaded successfully. Please refresh the page and try again.");

		}

		

	});




/* ------------------------------------------------------------------------------------ */
}());

