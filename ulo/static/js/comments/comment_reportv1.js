/* Post Report Javascript File */
/* Dependencies: jQuery, Base.js */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";


/* ------------------------------------------------------------------------------------ */
	/*
		Manage submission of the comment report form.
	*/
	function CommentReport(page_id, ul_id){

		Ulo.checkDependencies(true, "Session", "PIP");

		this.page_id = page_id;
		this.ul_id = ul_id;

		this.auto_submit = true;
		this.jqxhr = null;

		this.disableSubmit();
		this.register();
		

	}
	CommentReport.prototype = {

		constructor: CommentReport,

		/* ---------------------------------------------------------------------------- */
		/*
			Register all event handlers.
		*/
		register: function(){

			$(this.getForm()).on("submit", {self:this}, this.submitReport);
			/* Detect when a radio button is selected */
			$(getElement(this.ul_id)).on("change", {self:this}, this.enableSubmit);
		
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Unregister all event handlers.
		*/
		unregister: function(){

			$(this.getForm()).off("submit", this.submitReport);
			$(getElement(this.ul_id)).off("change", this.enableSubmit);

		},
		/* ---------------------------------------------------------------------------- */
		/* 
			Return the form.
		*/
		getForm: function(){
			return getElement(this.page_id+"_form");
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the form's submit button.
		*/
		getSubmit: function(){
			return getElement(this.page_id+"_submit");
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Display the form completed page.
		*/
		showCompletePage: function(){
			
			if(Ulo.PIP.loginRequired()===false){

				removeClass(getElement(this.page_id+"_complete"), Ulo.cls.hide);
			
			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Disable the form's submission button.
		*/
		disableSubmit: function(){
			this.getSubmit().disabled=true;
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Enable the form's submission button if there are no errors.
		*/
		enableSubmit: function(e){
			e.data.self.getSubmit().disabled=false;
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Return the container used to append error messages to. Each field is 
			assumed to have a container with the id "<element.id>_container".

			@param id: element id.
		*/
		getErrorContainer: function(id){

			var container = getElement(id+"_container");

			/*
				If the field does not have its own container then create a generic errors
				container and place it above the form.
			*/
			if(container === null){

				container = getElement(this.page_id+"_form_errors");

				if(container === null){

					container = makeElement("div", {"id": this.page_id+"_form_errors"});
					var form = this.getForm();
					form.parentNode.insertBefore(container, form);
				
				}
				
			}

			return container;
		},
		/* ---------------------------------------------------------------------------- */
		/*
			Append each error to its field container.

			@param errors: object containing an array of errors for each field. The key 
				is the id of the field. E.g. {"field_id": ["error one", "error two"], ...}
		*/
		addErrors: function(errors){

			/* Append each error to its field container */
			for(var id in errors){

				var error, container=this.getErrorContainer(id);

				for(var i in errors[id]){

					error = makeElement("p", {"class": "error"}, errors[id][i]);
					container.appendChild( error );
				
				}

			}

		},
		/* ---------------------------------------------------------------------------- */
		/*
			Remove all errors within a container. Errors are identified as p tags with 
			the class .error

			@param container: element node.
		*/
		removeErrors: function(container){
			$("p.error", container).remove();
		},

		/* ---------------------------------------------------------------------------- */
		/*
			Handle form submissions.
		*/
		submitReport: function(e){

			e.preventDefault();

			var self=e.data.self, form=this;

			if(Ulo.PIP.loginRequired()===false && self.jqxhr===null){

				self.disableSubmit();

				self.jqxhr = $.ajax({

						type: "POST",
						url: form.getAttribute("action"),
						data: $(form).serialize(),
						statusCode: requestCodes,
					
					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						/*
							If no html was returned then redirect to the report complete page.
						*/
						if(data.html===undefined || Ulo.PIP.isPIP(form)===false){

							Ulo.replacePage(data.url);

						/*
							Else render the report complete page within the current page.
						*/
						} else{

							/* Hide the report form's container */
							var report = addClass(getElement(self.page_id), Ulo.cls.hide);

							Ulo.PIP.displayPage(
								xhr, undefined, undefined, self.showCompletePage.bind(self)
							);
						
						}
					
					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr){


						if(Ulo.Session.hasErrors(xhr)){

							/* Remove all form errors */
							self.removeErrors(getElement(self.page_id));

							/* Add each new error to the form */
							self.addErrors(xhr.responseJSON.errors);

						}

						else {

							if(Ulo.Session.csrfExpired(xhr)){
							
								self.unregister();
							
							}

							requestMessages(xhr);							

						}

					})

					.always(function(){

						self.jqxhr=null;

				});

			}

		},

	}

	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */

	$(function(){

		new CommentReport("comment_report", "comment_issue");

	});

/* ------------------------------------------------------------------------------------ */
}());