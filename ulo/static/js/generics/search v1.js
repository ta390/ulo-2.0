/* Search page javascript file */
/* Dependencies: jQuery, base,js */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";
/* ------------------------------------------------------------------------------------ */


	/* VARIABLES */
	/* -------------------------------------------------------------------------------- */
	var hasHistoryAPI = Boolean(window.history && window.history.pushState);


	/* HELPERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Add a class to an element. 
		WARNING: The function does not check if the class name already exists.
		@param element: javascript element
		@param class_name: name of the class to append.
	*/
	function addClass(element, class_name){
		if(class_name){
			element.className += ((element.className === "") ? "" : " ") + class_name;
		}
	}
	/*
		Remove a class from an element.
		@param element: javascript element
		@param class_name: name of the class.
	*/
	function removeClass(element, class_name){
		var regexp = new RegExp('\\s*'+class_name+'[^\\s]*');
		element.className = element.className.replace(regexp, "");
	}


	/* SIDE SCROLL */
	/* -------------------------------------------------------------------------------- */
	/*

	*/
	function Scroll(){
		
		this.container = document.getElementById("user_results_wrapper");
		this.left = document.getElementById("left_scroll_btn");
		this.right = document.getElementById("right_scroll_btn");

		if(this.container && this.left && this.right){
			
			this.class_name = "hide";
			this.max_left_scroll = this.container.scrollWidth - this.container.clientWidth;
				
			if(this.max_left_scroll < this.container.clientWidth){
				addClass(this.right, this.class_name);
			} else{
				$(this.container).off().on("scroll", {self: this}, this.displayScrollButtons);
				$(this.left).off().on(Ulo.evts.click, {self: this}, this.LeftScroll);
				$(this.right).off().on(Ulo.evts.click, {self: this}, this.RightScroll);
			}
		}		
	}

	Scroll.prototype = {
		constructor: Scroll,

		register: function(){

		},
		animateScroll: function(scroll, operator){
			$(this.container).animate({ scrollLeft: operator+scroll }, 200);
		},
		LeftScroll: function(e){
			e.data.self.animateScroll(e.data.self.container.clientWidth, "-=");
		},
		RightScroll: function(e){
			e.data.self.animateScroll(e.data.self.container.clientWidth, "+=");
		},
		displayScrollButtons: function(e){
			e.data.self.toggleScrollLeft(e.target.scrollLeft);
			e.data.self.toggleScrollRight(e.target.scrollLeft);
		},
		toggleScrollLeft: function(scroll){
			if(scroll > 0){
				removeClass(this.left, this.class_name);
			} else{
				addClass(this.left, this.class_name);
			}
		},
		toggleScrollRight: function(scroll){
			if(scroll < this.max_left_scroll){
				removeClass(this.right, this.class_name);
			} else{
				addClass(this.right, this.class_name);
			}
		}
	}



	/* SEARCH */
	/* -------------------------------------------------------------------------------- */
	/*

	*/
	function Search(){

		/* provide context to avoid name conflicts with the nav bar search form */
		var context = $("#search_container");

		/* REQUEST */
		this.page = null;
		this.jqxhr = null;
		this.filter_query = "";
		/* 
			NOTE:
			origin is appended to the url to show that the request came from the search 
			page - used server side to return the results only and not the whole page.
		*/
		this.origin = "&sp"; 
		this.results = $("#results_container", context);
		/* END REQUEST */

		/* SEARCH */
		this.search_box = $("#search_input", context);
		this.suggestions = $("#search_suggestions", context);
		this.filter = new Filter(context);
		this.autocomplete = new Ulo.Autocomplete(this.search_box[0], this.suggestions[0]);
		this.side_scroll = new Scroll();
		/* END SEARCH */

		/* EVENTS */
		this.registerAccountsEvent();

		$("#search_form", context).on("submit", {self: this}, this.submit_handler);
		$("#more_results_form", context).on("submit", {self: this}, this.results_handler);
		var unload_context = {
			search_box: this.search_box,
			nav_search_id: "#nav_search #search_input"
		}
		$(window).one("beforeunload", unload_context, this.unload_handler);
		/* END EVENTS */

	}
	Search.prototype = {

		registerAccountsEvent: function(){
			var context = {
				"remove_class": "min_view",
				"container_id": "user_results"
			}
			$("#accounts_form").off().on("submit", context, this.accounts_handler);
		},

		/* HTML */
		/* -------------------------------------------------------------------------------- */
		userWrapper: function(html){
			return "<div id='user_results' > \
				<div id='user_results_wrapper'>"+html+"</div> \
				<span id='scroll_btns'> \
					<button id='left_scroll_btn' class='hide'> \
						<span class='hide'>Left Side Scroll</span> \
					</button> \
					<button id='right_scroll_btn'> \
						<span class='hide'>Right Side Scroll</span> \
					</button> \
				</span> \
			</div>"
		},

		userHTML: function(user, following){

			var f = ($.inArray(user.pk, following) >= 0) ? 'unfollow' : 'follow';
			
			return "<div class='result user_result box_border'> \
				<div class='thumbnail'> \
					<a href='/home/'> \
						<img src="+user.picture+"> \
					</a> \
				</div> \
				<div class='profile box_border'> \
					<a href='/home/'> \
						<h3 class='name'>"+user.name+"</h3> \
						<div class='username'>"+user.username+"</div> \
					</a> \
					<div class='connect'> \
						<button type='button'> \
							<span class='hide'>Connect</span> \
						</button> \
					</div> \
				</div> \
			</div>"
		},

		postWrapper: function(html){
			return "<div id='post_results'>"+html+"</div>";
		},
		postHTML: function(post){
			
			return "<div class='result post_result box_border'> \
				<div class='thumbnail'> \
					<a href='/home/'> \
						<img src="+post.thumbnail+"> \
					</a> \
				</div> \
				<div class='profile box_border'> \
					<a href='/home/'> \
						<div class='title'> \
							<h3>"+post.title+"</h3> \
							<div class='username'>"+post.username+"</div> \
						</div> \
						<div class='stats box_border'> \
							<span class='hide'><span class='icon time'></span>6 days ago</span> \
							<span class='hide'><span class='icon votes'></span>1200 votes</span> \
							<span><span class='icon views'></span>17,000&nbsp;views</span> \
						</div> \
					</a> \
				</div> \
			</div>"
		},
		searchResults: function(results, type, args){
		
			var result = '', get_html, wrap_html, i;

			if( results ){
		
				if(type === "user"){
					get_html = this.userHTML;
					wrap_html = this.userWrapper;
				} else{
					get_html = this.postHTML;
					wrap_html = this.postWrapper;
				}
			
				for(i in results){
					result += get_html(results[i], args);
				}

				result = wrap_html(result);
			}
			
		
			return result
		},
		/* END HTML */
		/* -------------------------------------------------------------------------------- */


		/* REQUEST HELPERS */
		/* -------------------------------------------------------------------------------- */
		/*
			Abort the current request.
		*/
		abort: function(e){
			this.jqxhr && jqxhr.abort();
			this.autocomplete.cancel();
		},
		/*
			Set jqhrx to null and update the url.
			@param url: url to change the current url to.
		*/
		complete: function(url){
			this.jqxhr = this.page = null;
			if(location.pathname+location.search !== url){
				history.pushState({}, document.title, url);
			}
		},
		next_page: function(form){
			if(this.page === null){
				return parseInt($("input[name=page]", form).val());
			}

			return this.page + 1;
		},
		/*
			Return an object used as the ajax request object.
			@param url: url of the request.
		*/
		request: function(url){
			var sc = {
				500: function(){
					// display flash message for server error
				}
			}
			/* NOTE: append origin to indicate that the request came from the search page */
			return { type:"GET", url:url+this.origin, timeeout:30, cache:true, statusCode:sc };
		},
		/*
			Register page links to update content using an ajax request and register click
			events for the scroll buttons. Return the container.
			@param container: element containing page links.
		*/
		register: function(container){
			/* add submit handler to the view all accounts form */
			this.registerAccountsEvent();
			/* run constructor to register click events for the scroll buttons */
			this.side_scroll.constructor();
			/* registerPageLinks - global helper function found in base.js */
			return registerPageLinks(container);
		},
		/* END REQUEST HELPERS */
		/* -------------------------------------------------------------------------------- */


		/* EVENTS */
		/* -------------------------------------------------------------------------------- */
		/*
			Make a search request and display the results to the user.
		*/
		submit_handler: function(e){

			var self = e.data.self;
			self.abort();
			self.filter.hide_filters();

			var value = self.search_box.val().trim();
			if(value !== ""){
		
				var url = $(this).attr("action")+"?q="+fixedEncodeURIComponent(value);
				self.filter_query = self.filter.queryString();
				url += self.filter_query;

				/* 
					if the url cannot be updated using the history api then perform a normal 
					request. The forms action value will be reset by the page reload.
				*/
				if(hasHistoryAPI === false){
					$(this).attr("action", url);
					return; 
				}

				self.jqxhr = $.ajax(self.request(url))
				
					.done(function(data, sc, xhr){

						var div = $("<div>").append(data.html);
						var results_id = "#"+self.results.attr("id");
						var results = div.find(results_id).addClass("hidden");

						self.results.replaceWith(results);
						/* registerPageLinks - global helper function found in base.js */
						self.results = self.register($(results_id)).removeClass("hidden");

					})
					.fail(function(){
					
					})
					.always(function(){
						self.complete(url);
				});

			}


			e.preventDefault();
		},
		results_handler: function(e){

			var self = e.data.self;
			self.abort();
			self.filter.hide_filters();

			var url = $(this).attr("action")
			var q = "?q="+fixedEncodeURIComponent( $("input[name=q]", this).val() );
			var results = "&results=" + $("input[name=results]", this).val();
			self.page = self.next_page(this);
			url += q + "&page="+self.page + results + self.filter_query;

			/* 
				if the url cannot be updated using the history api then perform a normal 
				request. The forms action value will be reset by the page reload.
			*/
			if(hasHistoryAPI === false){
				$(this).attr("action", url);
				return; 
			}

			self.jqxhr = $.ajax(self.request(url))
				
				.done(function(data, sc, xhr){

					console.log(self.page, data);

				})
				.fail(function(){
				
				})
				.always(function(){
					self.jqxhr = null;
			});

			e.preventDefault();
		},
		/*

		*/
		accounts_handler: function(e){

			e.preventDefault();

			var container = document.getElementById(e.data.container_id);
			removeClass(container, e.data.remove_class);
			this.parentNode.removeChild(this);
		},
		/*
			Before navigating away from the page add the current search value to the nav bar
			search box.
		*/
		unload_handler: function(e){
			$(e.data.nav_search_id).val( e.data.search_box.val() );
		}
		/* END EVENTS */
		/* -------------------------------------------------------------------------------- */
	}

	function Filter(){

		var context = $("#search_filters");
		
		this.disabled = null;
		this.headings = $("#filter_headings", context);
		this.container = $("#options", context);
		this.active_class = "active";
		this.user_class = 'user'
		this.post_class = 'post'
		this.filter = "data-filter";
		this.value = "data-value";
		this.type_filter = "id_type_filter";

		/*
			key, value pairs of the ul id and the query string name used for each filter.
		*/
		this.query_names = {
			"id_date_filter": "date", 
			"id_sort_filter": "sort"
		}
		this.query_names[this.type_filter] = "type";

		/* EVENTS */
		$("#filter_btn", context).on(Ulo.evts.click, {self: this}, this.display_handler);
		$("li", this.container).on(Ulo.evts.click, {self: this}, this.filter_handler);
		/* END EVENTS */
	}

	Filter.prototype = {
		hide_filters: function(){
			this.container.addClass("hide");
			this.headings.removeClass("open");
		},
		/*

		*/
		display_handler: function(e){
			e.data.self.container.toggleClass("hide");
			e.data.self.headings.toggleClass("open");
		},
		/*

		*/
		disabled_class_name: function(name){
			return "disable_"+name;
		},
		/*
			Add the class disable_user or disable_post to the container replacing the
			existing class.
			@param class_name: suffix name ("user" or "post").
		*/
		replaceClass: function(class_name){

			removeClass(this.container[0], this.disabled_class_name(''));
			addClass(this.container[0], class_name);
		
		},
		/*

		*/
		addButton: function(element){ 

			if(element.siblings(".remove_filter").length === 0){
				element.siblings(".heading").after(
					$("<button class='remove_filter'><span class='hide'>Remove filter</span></button>")
						.on(Ulo.evts.click, {self: this}, this.remove_filter_handler)
				);
			}
		},
		/*

		*/
		filter_handler: function(e){

			var self = e.data.self;
			var regexp_post = new RegExp('\\b'+self.post_class+'\\b');
			var disable = (regexp_post.test(this.className) ? self.user_class : self.post_class)

			if(self.disabled === null || self.disabled === disable){
				var jq = $(this);
				jq.siblings("."+self.active_class).removeClass(self.active_class);
				jq.addClass(self.active_class).parent().attr(self.filter, jq.attr(self.value));
				self.addButton(jq);
			}

			if(self.disabled === null){
				self.replaceClass( self.disabled_class_name(disable) );
				self.disabled = disable
			}
			
		},
		/*

		*/
		remove_filter_handler: function(e){

			var self = e.data.self;

			$(this).siblings("."+self.active_class).removeClass(self.active_class)
				.parent().removeAttr(self.filter);
			$(this).remove();

			if(self.hasFilter() === false){
				self.replaceClass("");
				self.disabled = null;
			}
		},
		/*
			Return true if at least one filter is beig applied and false otherwise.
		*/
		hasFilter: function(){

			for(var id in this.query_names){
				var value = document.getElementById(id).getAttribute(this.filter);
				if(value !== null && value !== ""){
					return true;
				}
			}
			return false;
		},
		/*

		*/
		addUserTypeFilter: function(){
			/* remove all filters */
			this.container.remove("."+self.active_class, "button.remove_filter");
			/* get the user type filter */
			var user_filter = $("#"+this.type_filter+"> ."+this.user_class);
			
			/* activate that filter */
			user_filter.addClass(this.active_class);
			/* set its values on its parent */
			user_filter.parent().attr(this.filter, user_filter.attr(this.value));
			/* add button to remove the filter */
			this.addButton(user_filter);
			this.replaceClass( this.disabled_class_name(this.post_class) );
			this.disabled = this.post_class;
		},
		/*

		*/
		queryString: function(){

			var q = ""
			for(var id in this.query_names){
				var value = document.getElementById(id).getAttribute(this.filter);
				if(value !== null && value !== ""){
					q += "&"+this.query_names[id]+"="+value;
				}
			}
			return q;
		
		}
	}

	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */
	$(function(){
		try{

			new Search();
			

		} catch(e){
			debug(e);
		}
	});

/* ------------------------------------------------------------------------------------ */
}());
