/* Post Detail Page Javascript File */
/* Dependencies: JQuery, Base.js, Users.js */


/* ------------------------------------------------------------------------------------ */

(function () {
"use strict";


	/* -------------------------------------------------------------------------------- */
	
	function View(){

		Ulo.checkTempDependencies("toggleDescription", "PostComment", "PostVote",
			"PostAction", "PostMobile", "Connect");

		if(Ulo.checkDependencies(true, "Page")){

			Ulo.Page.beforePageUnload = this.beforePageUnload.bind(this);

		}

		this.modal = new Ulo.Modal("post_view", "view_modal_open", "view");

		this.jqxhr = null;
		
		this.registerNext();
		this.register();

		this.VIDEO = 2;

	}

	View.prototype = {

		constructor: View,

		/* ---------------------------------------------------------------------------- */

		register: function(context){

			$("button.view", context).on(Ulo.evts.click, {self: this}, this.loadPost);

		},

		/* ---------------------------------------------------------------------------- */

		registerNext: function(){
		
			$(getElement("next_page")).on(Ulo.evts.click, {self: this}, this.loadPage);
		
		},

		/* ---------------------------------------------------------------------------- */

		registerPost: function(context){

			toggleDescription();

			Ulo.TEMP.Connect.register(context);

			Ulo.TEMP.PostComment.setup();
			
			Ulo.TEMP.PostVote.register(context);
			Ulo.TEMP.PostAction.register(context);
			Ulo.TEMP.PostMobile.register(context);

		
		},

		/* ---------------------------------------------------------------------------- */

		unregisterNext: function(button){
		
			$(button).off(Ulo.evts.click, this.loadPost);
		
		},

		/* ---------------------------------------------------------------------------- */

		beforePageUnload: function(){

			if(this.modal.isClosed()===false){

				this.modal.close();
				removeClass(document.body, Ulo.cls.modal_open);

			}

		},

		/* ---------------------------------------------------------------------------- */

		beforeClose: function(button){

			var detail = getElement("post_detail"),
			post = button.parentNode.parentNode.parentNode;

			var elem = $("div.post_votes", detail)[0];
			elem!==undefined && replaceWith(elem, $("div.post_votes", post)[0]);

			updateURL("/", false);

		},

		/* ---------------------------------------------------------------------------- */

		setMaxID: function(anchor, max_id){

			var href = anchor.getAttribute("href");
			
			anchor.setAttribute(
				"href", href.replace(/(max_id=)(\d*)(\s*|&|$)/, "$1"+max_id+"$3")
			);

		},

		/* ---------------------------------------------------------------------------- */

		loadPage: function(e){

			e.preventDefault();

			var self = e.data.self

			if(Ulo.PIP.loginRequired()===false && self.jqxhr===null){

				self.jqxhr = $.ajax({

						type: "GET",
						url: this.getAttribute("href")+"&view=view",
						cache: false,
						timeout: 20000,
						statusCode: requestCodes
					
					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						if(data.has_next===true){

							self.setMaxID(e.currentTarget, data.max_id);

						} else{
							
							self.unregisterNext(e.currentTarget);
							removeElements(e.currentTarget.parentNode);

						}

						self.html(data.posts);

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr){

						console.log(xhr);

						requestMessages(xhr);
					
					})

					.always(function(){
						
						self.jqxhr = null;
				
				});

			}

		},

		/* ---------------------------------------------------------------------------- */

		loadPost: function(e){

			console.log("LOADED");

			var self = e.data.self, 
			url = this.getAttribute("data-url");

			emptyElement(self.modal.get());

			if(Ulo.PIP.loginRequired()===false && self.jqxhr===null){

				self.jqxhr = $.ajax({

						type: "GET",
						url: url+"?view=true",
						statusCode: requestCodes
					
					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){
				
						Ulo.PageContent.change(
							
								self.modal.get(), 
								Ulo.ids.main,
								data.html, 
								xhr, 
								false
							
							)

							.done(function(){

								addClass(getElement("post_detail"), "box_border");

								self.registerPost( self.modal.get() );

								updateURL(url, true);

								self.modal.beforeClose = self.beforeClose.bind(null, e.currentTarget);
								self.modal.open();

							})

							.fail(function(){

								requestMessages(xhr);

						});

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr){

						if(Ulo.Session.csrfExpired(xhr)){

							var submit = getElement("delete_comment_submit");

							if(submit!==null){
							
								submit.disabled=true;
							
							}

						}

						requestMessages(xhr);
					
					})

					.always(function(){
					
						self.jqxhr = null;
				
				});

			}

		},

		/* ---------------------------------------------------------------------------- */

		html: function(posts){

			var timeline = getElement("timeline"), 
			now = new Date().getTime(), 
			is_owner, 
			post,
			options,
			parent, 
			child,
			form,
			data;

			for(var i in posts){

				options = posts[i].options;

				post = makeElement("div", {"class": "post"});

				is_owner = Ulo.Session.isOwner(posts[i].user_id);

				/* POST HEADER */
				/* -------------------------------------------------------------------- */


				/* Post Header Container */
				parent = post.appendChild(makeElement("div", {"class": "post_header"}));


				/* POST ACTIONS */

				child = parent.appendChild(makeElement("div", {"class": "user_actions"}));

				child.appendChild(makeElement("button", {
				
						"data-url": "/post/"+posts[i].id+"/",
						"class": "view",
						"type": "button"
				
					}))
				
					.appendChild(makeElement("span", {"class": Ulo.cls.hide})

				);

				this.register(child);

				/* POST ACTIONS */


				/* POST CREATOR / META */

				child = parent.appendChild(makeElement("div", {"class": "user_profile"}));
				
				child = child.appendChild(makeElement("a", {
				
					"data-apl": "true",
					"href": "/user/"+posts[i].username+"/",
					"title": "Profile"
					
				}));

				registerLinks(parent);
				
				child.appendChild(makeElement("img", {
				
					"class": "user_thumbnail",
					"src": Ulo.MEDIA_URL+posts[i].user_thumbnail
				
				}));
				
				child = child.appendChild(makeElement("div", {"class": "user_names"}));
				
				child.appendChild(makeElement("h2", {"class": "row name ellipsis"}, posts[i].name));
				
				// child = child.appendChild(makeElement("div", {"class": "post_meta row"}));
				
				// child.appendChild(makeElement("span", {"class": "full_count count"}, posts[i].views));
				
				// child.appendChild(document.createTextNode(" view"+(posts[i].views===1 ? "" : "s")));
				
				/* END POST CREATOR / META */


				/* END POST HEADER */
				/* -------------------------------------------------------------------- */


				/* POST BODY */
				/* -------------------------------------------------------------------- */

				parent = post.appendChild(makeElement("div", {"class": "post_body"}));

				data = Ulo.MEDIA_URL+posts[i].file0+"/";

				if(posts[i].media_type===this.VIDEO){

					parent = parent.appendChild(makeElement("div", {"class": "video"}))
						
						.appendChild(makeElement("video", {"controls": "true"})

					);
						
					parent.appendChild(makeElement("source", {
						
						"src": data,
						"type": "video/mp4"
					
					}));
						
					parent.appendChild(document.createTextNode(
					
						"Your browser does not support video playback."

					));

				} else{

					parent.appendChild(makeElement("div", {"class":"image"}))
						
						.appendChild(makeElement("img", {"src": data})

					);

				}

				/* END POST BODY */
				/* -------------------------------------------------------------------- */


				/* POST FOOTER */
				/* -------------------------------------------------------------------- */

				parent = post.appendChild(makeElement("div", {"class": "post_footer"}));
				
				/* PANEL 1 - Title */
				child = parent.appendChild(makeElement("div", {"class": "panel"}))
				
				child.appendChild(makeElement("div", {"class": "post_published"}))
					
					.appendChild(makeElement("span", {}, elapsedTime(now, posts[i].published))

				);

				child.appendChild(makeElement("h1", {"class": "post_title"}, posts[i].title));
				/* END PANEL 1 - Title and Get Post Button */


				/* PANEL 3 - Vote Buttons and Post Actions Menu Button */
				parent = parent.appendChild(makeElement("div", {"class": "post_actions panel"}));

				/* Post Actions Menu Button */
				if( is_owner===false ){

					parent.appendChild(makeElement("div", {"class": "post_actions_menu"}))
						
						.appendChild(makeElement("a", {
						
							"class": "icon post_menu_btn",
							"href": "/post/"+posts[i].id+"/actions/",
							"data-item-id": posts[i].id,
							"title": "Post Actions"
						
						}))
						
						.appendChild(makeElement("span", {"class": Ulo.cls.hide}, "Post Actions")

					);

					Ulo.TEMP.PostAction.register(parent);
				
				}
				/* END Post Actions Menu Button */


				/* Post Vote Options */
				parent = parent.appendChild(makeElement("div", {"class": "post_votes"}));
				
				form = parent.appendChild(makeElement("form", {
				
					"class": "post_votes_form",
					"action": "/post/"+posts[i].id+"/vote/",
					"method": "get"
				
				}));

				for(var j in options){

					child = form.appendChild(makeElement("div", {"class": "vote"}));

					if(options[j].vote_id===null){

						data = (options[j].icon) ? 
							["vote icon "+options[j].icon, ""] :
							["vote", "background-color:"+options[j].colour];

					} else{

						child.appendChild(makeElement("input", {
						
							"class": "voted",
							"name": "voted",
							"type": "hidden",
							"value": options[j].vote_id
						
						}));

						data = (options[j].icon) ? 
							["vote selected icon "+options[j].icon, ""] :
							["vote selected", "background-color:"+options[j].colour];

					}

					child.appendChild(makeElement("button", {
							
							"class": data[0],
							"style": data[1],
							"name": "vote",
							"value": options[j].option_id,
							"type":"submit"
						
						}))
							
						.appendChild(makeElement("span", {"class": "selected"})

					);

					child = child.appendChild(makeElement("div", {

						"class": "vote_data ellipsis"

					}));

					child.appendChild(makeElement("span", {

						"class": "abbr_count sm med"

					}, abbreviate(options[j].count)));

					child.appendChild(makeElement("span", {

						"class": "full_count lrg"

					}, options[j].count));

					child.appendChild(document.createTextNode("\u00A0")); /* unicode nbsp */

					child.appendChild(makeElement("span", {

						"class": "vote_text"

					}, options[j].text))

				}

				Ulo.TEMP.PostVote.register(parent);
				Ulo.TEMP.PostMobile.register(parent);
				/* END Post Vote Options */
				
				/* END PANEL 3 - Vote Buttons and Post Actions Menu Button */

				/* END FOOTER */
				/* -------------------------------------------------------------------- */

				
				timeline.appendChild(post);
			
			
			}

		}

	};

	/* -------------------------------------------------------------------------------- */




	/* -------------------------------------------------------------------------------- */
	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */

	$(function(){

		try{

			new View();
		
		} catch(e){

			console.log(e);
			debug(e);
		}

	});

	/* -------------------------------------------------------------------------------- */

}());



