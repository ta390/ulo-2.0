/* Search page javascript file */
/* Dependencies: jQuery, base,js */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";
/* ------------------------------------------------------------------------------------ */

	/* FILTER */
	/* -------------------------------------------------------------------------------- */
	/*
		Manage the selection and styling of an individual filter. A filter must have the 
		following structure.
		<div>
			<ul>
				<div>some heading</div>
				<li>filter one</li>
				<li>filter two</li>
				...
			</ul>
		</div>
		A group of filters must have the same parent.
		<div>
			<ul>...</ul>
			<ul>...</ul>
			...
		</div>

		@params id: css id of the ul containing all filters.
		@param query_string: name of the filter when passed as a url parameter.
	*/
	function Filter(id, query_name){

		/* ul list containing the filter options */
		this.container = getElement(id);
		/* the overall container */
		this.parent = this.container.parentNode;
		/* query string name given to this filter */
		this.query_name = query_name;
		/* filter heading - its the only div element */
		this.header = this.container.getElementsByTagName("div")[0];
		/* filter options */
		this.filters = this.container.getElementsByTagName("li");

		/* class given to an active filter */
		this.active = "active";
		/* prefix given to a class when disabling a filter type e.g. disable_user */
		this.prefix = "disable_";
		/* name of the class given to user filters */
		this.user = "user";
		/* name of the class given to post filters */
		this.post = "post";
		/* name of the data attribute used to access/store the filters value with */
		this.data = "data-value";

		/* name of the filter type that is currently disabled ("user" or "post") */
		this.disabled = null;
		/* flag to show whether this filter has been applied */
		this.has_filter = false;

		/* register click events on each filter */
		this.register();
	}
	/*
		Static variable to count the number of filters that have been applied.
	*/
	Filter.count = 0;
	/*
		Filter functions.
	*/
	Filter.prototype = {
		constructor: Filter,

		/*
			Register click events on each filter.
		*/
		register: function(){
			$(this.filters).on(Ulo.evts.click, {self: this}, this.applyHandler);
		},
		/*
			Set the value of has_filter and the static variable count.
			@param set: boolean - if set is true and has_counter is false increment the 
			counter and set has_filter to true. If set is false decrement the counter
			and set has_filter to false;
		*/
		setHasFilter: function(set){
			if(set === true){
				if(this.has_filter === false){
					++Filter.count
				}
			} else{
				--Filter.count;
			}
			this.has_filter = set;
			return Filter.count;
		},
		/*
			Return the name of the class to be disabled when this element is selected.
			e.g. if filter is of type "post" then "user" is returned.
			@param element: javascript element.
		*/
		disableClassName: function (element){
			return hasClass(element, this.post) ? this.user : this.post;
		},
		/*
			Activate a filter, disabling all filters that are no longer selectable in the
			process.
			@param element: javascript element (a filter).
			@param disable: name of the filter type to be disabled ("user" or "post")
		*/
		addFilter: function(element, disable){
			/* add class to the filter showing it is active */
			addClass(element, this.active);
			/* set its value on the container */
			this.container.setAttribute(this.data, element.getAttribute(this.data));
			/* get the name of the type to be disabled ("user" or "post") */
			this.disabled = disable;
			/* add class to the containers parent to disable filters of a certain type */
			addClass(this.parent, this.prefix+this.disabled);
		},
		/*
			Remove the active class from all filters.
		*/
		removeFilter: function(){
			var length = this.filters.length;
			for(var i=0; i<length; i++){
				removeClass(this.filters[i], this.active);
			}
		},
		/*
			Append a button the header that will remove the filter when clicked.
		*/
		addButton: function(){
			if(this.header.getElementsByTagName("button").length === 0){
				var btn = $("<button><span class='hide'>Remove Filter</span></button>");
				$(this.header).append(btn);
				btn.on(Ulo.evts.click, {self: this}, this.removeHandler);
			}
		},
		/*
			Remove a button from the header.
		*/
		removeButton: function(btn){
			try{
				this.header.removeChild(btn || this.header.getElementsByTagName("button")[0]);
			} catch(e){}
		},
		/*
			Apply a filter.
		*/
		applyHandler: function(e){

			var self = e.data.self;
			/* 
				get name of the type that this filter will disable ("user" or "post"). 
				i.e. if class="post" then return "user" as it must disable all user 
				filters when applied.
			*/
			var type = self.disableClassName(this);

			/* 
				if no filters have been disabled or the type that this filter will disable
				is already disabled, then apply filter.
			*/
			if(self.parent.className === "" || hasClass(self.parent, self.prefix+type)){
				/* remove all filters first */
				self.removeFilter();
				/* add this filter */
				self.addFilter(this, type);
				/* indicate that this filter is now active */
				self.setHasFilter(true);
				/* add button to remove filter */
				self.addButton();
			}
		},
		/*
			Remove a filter.
		*/
		clearFilter: function(btn){
			/* remove the filters value from the container */
			this.container.removeAttribute(this.data);
			/* remove the active filter class from the filter */
			this.removeFilter();
			/* remove the button itself */
			this.removeButton(btn);
			
			/* if no filters are being applied remove the disable filter class */
			if(this.setHasFilter(false)===0){
				removeClass(this.parent, this.prefix+this.disabled);
			}

			/* set disbaled to null after calling removeClass */
			this.disabled = null
		},
		/*
			Remove a filter event handler.
		*/
		removeHandler: function(e){
			e.data.self.clearFilter(this);
		},
		/*
			Return the name and value as a url parameter or an empty string if
			no filter has been applied.
		*/
		serialise: function(){
			var value = this.container.getAttribute(this.data);
			if(value === null || value === ""){
				return "";
			}
			return "&"+this.query_name+"="+value;
		}
	}

	/* FILTERS */
	/* -------------------------------------------------------------------------------- */
	/*
		Manage the display of filters and group multiple filters together.
	*/
	function Filters(){

		/* container for a group of filters */
		this.container = getElement("options");
		/* button to toggle the display of filters */
		this.btn = getElement("filter_btn");
		/* button to remove all filters */
		this.remove = getElement("remove_filters");
		/* class names */
		this.hide = "hide";
		this.closed = "closed";
		
		/* the filters */
		this.filters = [
			new Filter("id_type_filter", "type"),
			new Filter("id_date_filter", "date"),
			new Filter("id_sort_filter", "sort"),
		]

		/* register click event to show/hide the filters */
		this.register();

	}
	Filters.prototype = {
		
		constructor: Filters,
		/*
			Register click event to toggle the filters and remove the filters.
		*/
		register: function(){
			$(this.btn).on(
				Ulo.evts.click, 
				{self:this}, 
				this.toggleFilters
			);
			$(this.remove).on(
				Ulo.evts.click, 
				{filters:this.filters, class_name:this.hide}, 
				this.removeFilters
			);
		},
		/*
			Event handler to show/hide filters.
		*/
		toggleFilters: function(e){
			
			var self = e.data.self;

			/* toggle the display of the filter options */
			toggleClass(self.container, self.hide);
			/* 
				if the filters were closed and a filter was applied, diplay remove 
				filters button, else hide the button.
			*/
			if(toggleClass(this, self.closed) && self.hasFilter()){
				removeClass(self.remove, self.hide);	
			} else{
				addClass(self.remove, self.hide);
			}
		},
		/*
			Event handler to remove all filters.
		*/
		removeFilters: function(e){
			var filters = e.data.filters;
			for(var i in filters){
				if(filters[i].has_filter){
					filters[i].clearFilter();
				}
			}
			addClass(this, e.data.class_name);
		},
		/*
			Close the filters and show or hide the remove all filters button.
		*/
		closeFilters: function(){
			addClass(this.container, this.hide);
			addClass(this.btn, this.closed);
			if(this.hasFilter()){
				removeClass(this.remove, this.hide);	
			}
		},
		/*
			Return true if at least one filter is being applied, else return false.
		*/
		hasFilter: function(){
			for(var i in this.filters){
				if(this.filters[i].has_filter){
					return true;
				}
			}
			return false;
		},
		/*
			Return a string of name value pairs for all the filters that are being applied.
			e.g. "&type=0&date=1"
		*/
		serialise: function(){
			var q = ""
			for(var i in this.filters){
				q += this.filters[i].serialise();
			}
			return q;
		}
	}


	/* BASE CLASS FOR SEARCH REQUESTS */
	/* -------------------------------------------------------------------------------- */
	
	function SearchRequest(){

		this.jqxhr = null;
	}
	SearchRequest.prototype = {

		serialise: function(form, required){
			
			var data = $(form).serializeArray();
			var q = "", value = null;

			for(var i in data){
				value = $.trim(data[i].value);
				if(required === true && value === ""){ return null; }
				q += data[i].name+"="+fixedEncodeURIComponent(value)+"&";
			}

			if(q === ""){ return null; }
			return "?"+q.slice(0, -1);
		},
		getURL: function(form, required){
			
			var url = form.getAttribute("action");
			var data = this.serialise(form, required);
			
			if(url === null || url === "" || data === null){
				return null;
			}
			return url+data;
		},
		/*

		*/
		abort: function(){
			this.jqxhr && this.jqxhr.abort();
		},
		/*

		*/
		complete: function(url){
		
			this.jqxhr = null;

			if(location.pathname+location.search !== url){
				history.pushState({}, document.title, url);
			}
		},
		/*

		*/
		request: function(url){
			var sc = {
				500: function(){
					// display flash message for server error
				}
			}

			return { type:"GET", url:url, timeeout:30, cache:true, statusCode:sc };
		},
	}

	/* PAGINATE */
	/* -------------------------------------------------------------------------------- */

	function Paginate(){

		this.url = "/search/paginate/";
		this.form_id = "paginate_form";
		this.user_class = "min_view";
		this.setup();
	}
	Paginate.prototype = inherit(SearchRequest.prototype);

	Paginate.prototype.register = function(){
		$(this.form).off().on("submit", {self: this}, this.submitHandler);
	};
	Paginate.prototype.setup = function(){

		this.container = getElement("paginate");
		this.form = getElement(this.form_id);
		this.user_form = getElement("paginate_user_form");
		this.user_results = getElement("user_results");
		this.post_results = getElement("post_results");
		$(this.user_form).off().one("submit", {self: this}, this.userSubmitHandler);

		this.setURL(this.form);
		this.register();
	};
	Paginate.prototype.setURL = function(form){
		if(form){
			form.setAttribute("action", this.url);
		}
	};
	Paginate.prototype.incrementPage = function(form){
		var input = $("input[name=page]", form);
		var page = parseInt(input.val());
		input.val(++page);
	};
	Paginate.prototype.createForm = function(form){
		
		return $("<form>").attr({"id":this.form_id, "action":this.url, "method":"get"})
			.append(
				form.getElementsByTagName("input"),
				"<button type='submit' class='icon'> \
					<span class='hide'>Load results</span> \
				</button>"
		)[0];
	}
	Paginate.prototype.userSubmitHandler = function(e){

		var self = e.data.self;

		self.incrementPage(this);
		/* create a new form using the current forms input fields */
		self.form = self.createForm(this);

		/* remove the current pagination form and append the new form */
		/* CALLING emptyElement does not work in IE8! */
		//emptyElement(self.container).appendChild(self.form);
		while(self.container.firstChild){
			self.container.removeChild(self.container.firstChild);
		}
		self.container.appendChild(self.form);
		
		/* remove the user form container and post results */
		/* CALLING removeElements does not work in IE8! */
		//removeElements(this.parentNode, self.post_results);
		this.parentNode.parentNode.removeChild(this.parentNode);
		self.post_results.parentNode.removeChild(self.post_results);

		/* display user results in the main search space */
		removeClass(self.user_results.parentNode, self.user_class);

		/* register submit handler on the new form */
		self.register();
		e.preventDefault()
	};
	Paginate.prototype.submitHandler = function(e){

		var form = this;
		var self = e.data.self;
		self.abort();
		var url = self.getURL(this, true)
		
		if(url !== null){

			self.jqxhr = $.ajax(self.request(url))
				
				.done(function(data, sc, xhr){

					if(data.posts){
						self.updateHTML(
							self.postHTML, 
							self.post_results, 
							data.posts
						);
					} else{
						self.updateHTML(
							self.userHTML,
							self.user_results,
							data.users,
							data.following
						);
					}

					if(data.display_form === true){
						self.incrementPage(form);
					} else{
						emptyElement(self.container);
					}
					
				})
				.fail(function(){
				
				})
				.always(function(){

					self.jqxhr = null;
			});

		}

		e.preventDefault()
	};
	Paginate.prototype.updateHTML = function(getHTML, container, data, args){
		for(var i in data){
			$(container).append( getHTML(data[i], args) );
		}
	};
	Paginate.prototype.userHTML = function(result, following){
		
		var follow = ($.inArray(result.pk, following) < 0) ? "follow" : "unfollow";
		var action = "/"+follow+"/"+result.username+"/";

		return "<div class='result user_result box_border'> \
			<div class='thumbnail'> \
				<a href='/user/"+result.username+"'> \
					<img src='"+result.picture+"'> \
				</a> \
			</div> \
			<div class='profile box_border'> \
				<a href='/user/"+result.username+"'> \
					<h3 class='name'>"+result.name+"</h3> \
					<div class='username'>"+result.username+"</div> \
				</a> \
				<div class='connect'> \
					<button type='button' class='"+follow+"' data-action='"+action+"'> \
						<span class='hide'>Connect</span> \
					</button> \
				</div> \
			</div> \
		</div>"
	};
	Paginate.prototype.postHTML = function(result){
		return "<div class='result post_result box_border'> \
			<div class='thumbnail'> \
				<a href='/home/'> \
					<img src='/media/default/profile.jpg'> \
				</a> \
			</div> \
			<div class='profile box_border'> \
				<a href='/home/'> \
					<div class='title'> \
						<h3>"+result.title+"</h3> \
						<div class='username'>"+result.name+"</div> \
					</div> \
					<div class='stats box_border'> \
						<span class='hide'> \
							<span class='icon time'></span>6 days ago \
						</span> \
						<span class='hide'> \
							<span class='icon votes'></span>1200 votes \
						</span> \
						<span> \
							<span class='icon views'></span>17,000&nbsp;views \
						</span> \
					</div> \
				</a> \
			</div> \
		</div>"
	};


	/* SEARCH */
	/* -------------------------------------------------------------------------------- */
	
	function Search(){

		this.si_id = "search_input";
		this.form = getElement("search_form");
		this.results_container = getElement("results_container");
		this.history_api = Boolean(window.history && window.history.pushState);

		this.filters = new Filters();
		this.paginate = new Paginate();
		this.autocomplete = new Ulo.Autocomplete(
			getElement(this.si_id), 
			getElement("search_suggestions")
		);

		this.register();
	}
	Search.prototype = inherit(SearchRequest.prototype);
	Search.prototype.constructor = Search;
	/*

	*/
	Search.prototype.register = function(){
		$(this.form).on("submit", {self: this}, this.submitHandler);
		$(window).one("beforeunload.search", {si_id: this.si_id}, this.unloadHandler);
	},
	Search.prototype.abort = function(){
		SearchRequest.prototype.abort.call(this);
		this.autocomplete.cancel();
	},
	/*
	
	*/
	Search.prototype.submitHandler = function(e){

		var self = e.data.self;

		self.abort();
		self.filters.closeFilters();
		var url = self.getURL(this, true)
		
		if(url !== null){
	
			url += self.filters.serialise();

			if(self.history_api){

				self.jqxhr = $.ajax(self.request(url))
				
					.done(function(data, sc, xhr){

						var div = $("<div>").append(data.html);
						var jq_id = "#"+self.results_container.getAttribute("id");
						var results = addClass(div.find(jq_id)[0], "hidden");

						self.results_container = replaceWith(results, self.results_container);
						
						self.paginate.setup();
						/* registerPageLinks - global helper function found in base.js */
						registerPageLinks(this.results_container);
						removeClass(self.results_container, "hidden");
						

					})
					.fail(function(){
					
					})
					.always(function(){

						self.complete(url);
				});

			}else{

				/* navigate to the page normally if you can't update the url using the history api. */
				location.assign(url);
			}

		}


		e.preventDefault();
	};
	/*
		Before navigating away from the page add the current search value to the nav bar
		search input.
	*/
	Search.prototype.unloadHandler = function(e){
		/* get the nav search input element */
		var nav = getElement("nav_search_input");
		var si  = getElement(e.data.si_id);

		if(nav && si){
			/* update the value of the nav search input to match the current input value */
			nav.value = si.value;
			
		}
		
	}


	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */
	$(function(){
		try{

			new Search();

		} catch(e){
			debug(e);
		}
	});

/* ------------------------------------------------------------------------------------ */
}());
