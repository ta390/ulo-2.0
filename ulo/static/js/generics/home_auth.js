/* Post Detail Page Javascript File */
/* Dependencies: JQuery, Base.js, Users.js */


/* ------------------------------------------------------------------------------------ */

(function (){


"use strict";


	/* -------------------------------------------------------------------------------- */

	function Timeline(){

		Ulo.checkDependencies("Session", "Pip");

		Ulo.checkTempDependencies("PostDetail", "PostVideo");

		this.jqxhr = null;

		this.registerLoad();

		this.registerView( this.getPage() );

	}

	Timeline.prototype = {

		constructor: Timeline,

		/* ---------------------------------------------------------------------------- */

		registerView: function(context){

			$("a.view", context).on(Ulo.evts.click, {self: this}, this.loadPage);

		},

		/* ---------------------------------------------------------------------------- */

		registerLoad: function(){

			$(getElement("load_more")).on(Ulo.evts.click, {self: this}, this.load);

		},

		/* ---------------------------------------------------------------------------- */

		unregisterLoad: function(anchor){

			$(anchor).off(Ulo.evts.click, this.load);

			removeElements(anchor);

		},

		/* ---------------------------------------------------------------------------- */

		getPage: function(){

			return getElement("home_auth");

		},

		/* ---------------------------------------------------------------------------- */

		getTimeline: function(){

			return this.getPage().querySelector("div.content");

		},

		/* ---------------------------------------------------------------------------- */

		setMaxID: function(anchor, max_id){

			anchor.href = anchor.href.replace(

				/(max_id=)(\d*)(\s*|&|$)/, "$1" + (max_id || "") + "$3"

			);

		},

		/* ---------------------------------------------------------------------------- */

		load: function(e){

			e.preventDefault();

			var self = e.data.self;

			if(Ulo.Pip.login() !== null){

				Ulo.Pip.open();

			}

			else if(self.jqxhr === null){

				self.jqxhr = $.ajax({

						type: "GET",
						url: e.currentTarget.href + "&timeline=true",
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){


						/* If the session has changed update the entire page.  */
					
						if(Ulo.Session.hasChanged(xhr)){

							Ulo.Pip.updatePage(xhr);

							return true;

						}

						/* If the login form is returned display it. */

						if(data.html){

							Ulo.Pip.displayPage(xhr, "login");

							return true;

						}


						/* Update href */
						self.setMaxID(e.currentTarget, data.max_id);

						/* Either there are more posts to get or unregister the anchor */
						data.has_next || self.unregisterLoad(e.currentTarget);

						/* Render the posts */
						self.posts( data.posts );

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){


					
					})

					.always(function(){

						self.jqxhr = null;
				
				});

			}

		},

		/* ---------------------------------------------------------------------------- */

		detail: function(opened){

			var page = getElement("post_detail");

			if(page !== null){

				if(opened){

					if( hasClass(page, Ulo.cls.hide) === false ){

						page.setAttribute("data-close-pip", "true");

						var button = makeElement("button", { 

							"id": "back_to_timeline", 
							"data-close-pip": "true"

						});

						button.appendChild( 

							makeElement("span", {

								"data-close-pip": "true",
								"class": "icon icon_left_arrow_white"

							}) 

						);

						page.appendChild( button );

						Ulo.TEMP.PostDetail.registerAll( page );

						Ulo.TEMP.PostVideo.register( page );

						/* Force repaint */
						page.offsetLeft;

						addClass(page, "slide");


						return;

					}

				}

				
				var post = this.parentNode;

				while(post !== null && hasClass(post, 'post') === false){

					post = post.parentNode;

				}

				if(post !== null){

					var page_form = page.querySelector("form.post_vote_form");

					var timeline_form = post.querySelector("form.post_vote_form");

					timeline_form.parentNode.replaceChild(page_form, timeline_form);

					Ulo.PageContent.removeFiles( page );

				}

			}

		},

		/* ---------------------------------------------------------------------------- */

		loadPage: function(e){

			e.preventDefault();

			var self = e.data.self;

			if(self.jqxhr === null){

				self.jqxhr = $.ajax({

						type: "GET",
						url: e.currentTarget.href + "?timeline=true",
						statusCode: requestCodes

					})

					/* Params: server data, status code, xhr */
					.done(function(data, sc, xhr){

						
						/* If the session has changed update the entire page.  */
					
						if(Ulo.Session.hasChanged(xhr)){

							Ulo.Pip.updatePage(xhr);

							return true;

						}


						Ulo.Pip.displayPage(

							xhr, 
							"post_detail", 
							true, 
							self.detail.bind(e.currentTarget)

						);

					})

					/* Params: xhr, status code, error type */
					.fail(function(xhr, sc, type){

						console.log('FAIL: ', xhr);
					
					})

					.always(function(){

						self.jqxhr = null;
				
				});

			}

		},

		/* ---------------------------------------------------------------------------- */

		posts: function(posts){

			var timeline = this.getTimeline();

			if(timeline !== null && posts !== undefined){

				var post, parent, child, form, icon, vote, data, is_owner, is_video, now = new Date().getTime();

				for(var i in posts){

					is_owner = Ulo.Session.isOwner( posts[i].user_id );



					/* */

					post = makeElement("div", { "class": "post" });

					if( is_owner ){

						post.id = "post_" + posts[i].id;

					}
					
					/* */


					/* */

					parent = post.appendChild( makeElement("div", {"class": "post_media"}) )

						.appendChild(

							makeElement("div", {"class": "media_container"})

					);


					is_video = posts[i].media_type === Ulo.media.VIDEO;

					if( is_video ){

						addClass(parent, "video");


						parent.appendChild(

								makeElement("div", {"class": "video_overlay"})
							
							)

							.appendChild(

								makeElement("div", {"class": "video_icon"})
							
							)

							.appendChild(

								makeElement("span", {"class": "icon icon_play"})
							
						)


						child = parent.appendChild(

							makeElement("video", { 

								"preload": "none",
								"poster": Ulo.MEDIA_URL + posts[i].thumbnail

							})

						);

						child.appendChild(

							makeElement("source", {

								"src": Ulo.MEDIA_URL + posts[i].file0,
								"type": "video/mp4"

							})

						)

						child.appendChild(

							document.createTextNode("Your browser does not support video playback.")


						);


						parent = parent.appendChild(

							makeElement("div", {"class": "video_controls hide"})

						);

						child = parent.appendChild(

								makeElement("div", {"class": "hide thumbnail_frame"})

							)

							.appendChild(

								makeElement("div", {"class": "wrapper"})

						);

						child.appendChild(

							makeElement("img", {

								"src": Ulo.MEDIA_URL + posts[i].thumbnail,

							})

						);

						child.appendChild(

							makeElement("p", {}, "Thumbnail")

						);


						var track = parent.appendChild(

							makeElement("div", {"class": "track cf"})

						)

						track.appendChild(

								makeElement("div", {"class": "track_slider slider"})

							)

							.appendChild(

								makeElement("button", {"class": "track_slider slider", "type": "button"})

						);

						child = track.appendChild(

							makeElement("div", {"class": "progress track_bar"})
						
						);

						child.appendChild(

							makeElement("span", {"class": "elapsed slider"})
						
						);

						child.appendChild(

								makeElement("div", {"class": "buffered"})
							
							)

							.appendChild(

								makeElement("span", {"class": "range"})
						
						);



						child = track.appendChild(

							makeElement("div", {"class": "thumbnail_track"})
						
						);

						child.appendChild(

								makeElement("div", {

									"class": "thumbnail_position",
									"style": "left: " + (

										(posts[i].thumbnail_time / posts[i].duration) * 100

									) + "%"

								})
							
							)

							.appendChild(

								makeElement("span", {"class": "track_bar"})

						);


						parent = parent.appendChild(

							makeElement("div", {"class": "controls cf"})
						
						);

						child = parent.appendChild(

							makeElement("div", {"class": "left"})

						);

						child.appendChild(

								makeElement("button", {

									"class": "play action inline", 
									"type": "button"

								})

							)

							.appendChild(

								makeElement("span", {"class": "play icon icon_play"})

						);

						child.appendChild(

								makeElement("button", {

									"class": "loop action inline", 
									"type": "button"

								})

							)

							.appendChild(

								makeElement("span", {"class": "icon icon_loop"})

						);

						child = child.appendChild(

							makeElement("span", {"class": "time inline"})

						)

						child.appendChild(

							makeElement("span", {"class": "current_time"}, "0:00")

						);

						child.appendChild(

							document.createTextNode(" / ")

						);

						child.appendChild(

							makeElement("span", {"class": "duration"}, "0:00")

						);


						parent = parent.appendChild(

							makeElement("div", {"class": "right"})
						
						);

						var volume = parent.appendChild(

							makeElement("div", {"class": "volume_container inline"})

						);

						child = volume.appendChild(

								makeElement("div", {"class": "target"})

							)

							.appendChild(

								makeElement("div", {"class": "volume_controls"})

						);

						child.appendChild(

								makeElement("div", {"class": "volume_slider slider"})

							)

							.appendChild(

								makeElement("button", {"class": "volume_slider slider", "type": "button"})

						);

						child.appendChild(

								makeElement("span", {"class": "volume_range"})

							)

							.appendChild(

								makeElement("span", {"class": "level slider"})

						);

						volume.appendChild(

								makeElement("button", {"class": "volume action", "type": "button"})

							)

							.appendChild(

								makeElement("span", {"class": "icon icon_volume volume"})

						);

						parent.appendChild(

								makeElement("button", {"class": "fullscreen action inline", "type": "button"})

							)

							.appendChild(

								makeElement("span", {"class": "icon icon_expand"})

						);


					} else{

						addClass(parent, "image");

						parent.appendChild(

							makeElement("img", {"src": Ulo.MEDIA_URL + posts[i].file0 }) 

						);

					}

					/* */



					/* */

					parent = post.appendChild( makeElement("div", {"class": "post_data"}) );


					child = parent.appendChild( makeElement("div", {"class": "post_panel"}) );

					child = child.appendChild( 

						makeElement("div", {"class": "post_header"}) 

					);

					child.appendChild( 

							makeElement("div", {"class": "title"}) 

						)

						.appendChild( 

							makeElement("a", {

								"href": "/post/" + posts[i].id + "/",
								"title": "Post Detail",
								"data-apl": "true"

							}) 

						)

						.appendChild( 

							makeElement("h1", {"class": "thin"}, posts[i].title) 

					);

					child.appendChild( 

							makeElement("div", {"class": "expand_view"}) 

						)

						.appendChild( 

							makeElement("a", {

								"class": "view",
								"title": "Post Detail",
								"href": "/post/" + posts[i].id + "/"
							
							})

					);

					this.registerView( child );


					child = parent.appendChild(

						makeElement("div", {"class": "post_actions post_panel"})

					);


					form = child.appendChild(

						makeElement("form", {

							"class": "post_vote_form",
							"action": "/post/" + posts[i].id + "/vote/",
							"method": "get"

						})

					);

					for(var j in posts[i].options){

						data = form.appendChild( makeElement("div", {"class": "vote"}) );

						vote = data.appendChild(

								makeElement("button", {

								"class": "vote",
								"data-count": posts[i].options[j].count,
								"name": "vote",
								"value": posts[i].options[j].option_id,
								"type": "submit"

							})

						);


						if(posts[i].options[j].icon){

							addClass(vote, "font_icon " + posts[i].options[j].icon);

						}

						if(posts[i].options[j].colour){

							vote.style.backgroundColor = posts[i].options[j].colour

						}

						if(posts[i].options[j].vote_id !== null){

							addClass(vote, "selected ");

							vote.appendChild(

								makeElement("input", {

									"name": "voted", 
									"type": "hidden",
									"value": posts[i].options[j].vote_id

								}) 

							);

						}

						
						vote = data.appendChild(

							makeElement("div", {"class": "vote_data"})

						);

						vote.appendChild(

							makeElement("span", {

								"class": "vote_text ellipsis"

							}, posts[i].options[j].text)

						);

						vote.appendChild(

							makeElement("span", {

								"class": "vote_count full_count ellipsis"

							}, posts[i].options[j].count)

						);

					}

					child = child.appendChild(

						makeElement("a", {

							"class": "toggle_actions_menu icon icon_menu",
							"href": "/post/" + posts[i].id + "/actions/",
							"title": "Post Actions",
							"data-toggle": "post_menu",
							"data-id": posts[i].id

						})

					);

					child.appendChild( makeElement("span",  {"class": "hide"}, "Post actions") );

					if( is_owner ){

						child.setAttribute("data-is-owner", "true");

					}



					/* */

					child = parent.appendChild( makeElement("div", {"class": "post_panel"}) );

					child.appendChild(

						makeElement("span", {

							"class": "published"

						}, elapsedTime(now, posts[i].published)) 

					);

					child = child.appendChild( makeElement("div", {"class": "user_profile"}) );

					child = child.appendChild( 

						makeElement("a", {

							"href": "/user/" + posts[i].username,
							"class": "profile",
							"data-apl": "true",
							"title": "Profile"

						}) 

					);

					child.appendChild(

						makeElement("img", {

							"class": "user_thumbnail", 
							"src": Ulo.MEDIA_URL + (posts[i].user_thumbnail || "default/profile.jpg")

						})

					);

					child = child.appendChild(

						makeElement("div", { "class": "user_names" })

					);

					child.appendChild(

						makeElement("h2", { "class": "name thin ellipsis" }, posts[i].name)

					);

					child.appendChild(

						makeElement("span", { "class": "username ellipsis" }, "@" + posts[i].username)

					);


				
					registerLinks( post );
				
					Ulo.TEMP.PostDetail.register( post );

					Ulo.TEMP.PostVideo.register( post );

					timeline.appendChild( post );

				}

				post, parent, child, form, icon, vote, data, is_owner, now = null

			}

		},


	}

	/* -------------------------------------------------------------------------------- */




	/* -------------------------------------------------------------------------------- */
	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */

	$(function(){

		try{

			new Timeline();
		
		} catch(e){

			console.log(e);

		}

	});

	/* -------------------------------------------------------------------------------- */

}());



